package com.builds.uimaps.fs;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LeadSummaryUI {

	@FindBy(xpath = ".//*[@id='fsLeadTable']/tbody/tr/td[2]/table/tbody/tr[1]/td/table/tbody/tr/td[2]/table/tbody/tr[1]/td[1]/a")
	public WebElement addLeadLnk ;

	@FindBy(xpath = ".//a[@href='addLeadPrimaryInfoForm?addAnotherLead=true']")
	public WebElement addLeadLnkAlternate ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement cboxCloseBtn;

	@FindBy(xpath = ".//button[contains(text(),'Close')]")
	public WebElement cboxCloseBtn1 ;

	@FindBy(xpath = ".//input[@value='Change Status']")
	public WebElement changeStatus ;

	@FindBy(xpath = ".//input[@name='checkBox']")
	public WebElement checkBoxAll;
	@FindBy(xpath = ".//*[@id='contentdivTopSearch']/table/tbody/tr[1]/td/a/span/u")
	public WebElement contentdivTopSearch;
	
	/*
	 * Add To Group
	 */

	@FindBy(xpath = ".//*[@id='table1']/tbody/tr[2]/td/table/tbody/tr[2]/td/table[2]/tbody/tr[2]/td/table/tbody/tr[2]/td[1]/input")
	public WebElement firstGroupCboxChk ;

	@FindBy(xpath = ".//input[@value='Add To Group']")
	public WebElement addToGroupCboxBtn ;

	@FindBy(xpath = ".//input[@value='Ok']")
	public WebElement okCboxBtn ;

	@FindBy(xpath = ".//input[@value=\"D'accord\"]")
	public WebElement okCboxBtn2fr ;

	/*
	 * Change Status
	 */

	@FindBy(id = "leadStatusID")
	public WebElement leadStatusIDDrp;

	@FindBy(id = "remarks")
	public WebElement remarks;

	@FindBy(name = "change_status")
	public WebElement changeStatusCboxBtn;

	/*
	 * change owner
	 */

	@FindBy(id = "leadOwnerID")
	public WebElement leadOwnerDrp;

	@FindBy(name = "change_status")
	public WebElement changeOwnerBtn;

	/*
	 * Send Email
	 */

	@FindBy(id = "mailTemplateID")
	public WebElement existingEmailTemplatesDrp;

	@FindBy(name = "subject")
	public WebElement subject;

	@FindBy(xpath = ".//iframe[@id='ta_ifr']")
	public WebElement htmlFrame ;

	@FindBy(xpath = ".//iframe[@id='templateText_ifr']")
	public WebElement htmlFrame1 ;

	@FindBy(id = "mailContent_ifr")
	public WebElement htmlFrame2;

	@FindBy(xpath = "html/body")
	public WebElement mailText ;

	@FindBy(id = "sendButton2")
	public WebElement sendBtn2;

	@FindBy(xpath = ".//input[@value='Send']")
	public WebElement sendBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//a[contains(text(),'Send Email')]")
	public WebElement sendEmailTopLnk ;

	// Anukaran
	@FindBy(xpath = ".//textarea[@id='mailContent']")
	public WebElement mailContent ;

	/*
	 * Log a task
	 */
	@FindBy(xpath = ".//input[@id='radioOwner' and @value='0']")
	public WebElement assignTaskToLeadOwner ;

	@FindBy(id = "subject")
	public WebElement taskSubject;

	@FindBy(name = "schduleTime")
	public WebElement noReminderTask;

	@FindBy(id = "status")
	public WebElement taskStatusDrp;

	@FindBy(id = "taskType")
	public WebElement taskType;

	@FindBy(name = "add")
	public WebElement addTaskBtn;

	@FindBy(linkText = "Log a Task")
	public WebElement logATask ;

	@FindBy(id = "comments")
	public WebElement taskComments;

	@FindBy(xpath = ".//input[@name='add' and @value='Create']")
	public WebElement createTaskBtn ;

	@FindBy(id = "radioUser")
	public WebElement otherUserRadio;

	@FindBy(id = "ms-parentassignTo")
	public WebElement assignToDrp;

	@FindBy(id = "schduleTime3")
	public WebElement setTimeYourselfRadio;

	/*
	 * Log a Call
	 */

	@FindBy(id = "subject")
	public WebElement callSubject;

	@FindBy(name = "submitButton")
	public WebElement addCallBtn;

	@FindBy(xpath = ".//input[@value='No']")
	public WebElement callConfirmScheduleTaskNoBtn ;

	@FindBy(linkText = "Log a Call")
	public WebElement logACall ;

	@FindBy(id = "callStatus")
	public WebElement callStatus;

	@FindBy(id = "callType")
	public WebElement communicationType;

	@FindBy(xpath = ".//*[@id='sTime']")
	public WebElement callTimeHr;

	@FindBy(xpath = ".//*[@id='sMinute']")
	public WebElement callTimeMin;

	@FindBy(xpath = ".//*[@id='APM']")
	public WebElement callTimeAPM;

	/*
	 * Change Owner
	 */

	@FindBy(id = "notes")
	public WebElement notesArchive;

	@FindBy(id = "Submit")
	public WebElement notesSubmitBtn;

	/*
	 * Delete Lead
	 */

	@FindBy(xpath = ".//input[@value='Ok']")
	public WebElement okBtn ;

	@FindBy(name = "changeStatusButton1")
	public WebElement changeStatusBottomBtn;

	@FindBy(name = "assignLead2")
	public WebElement changeOwnerBottomBtn;

	@FindBy(name = "sendMail2")
	public WebElement sendEmailBottomBtn;

	@FindBy(xpath = ".//input[@name='AddToGroup' or @name='addToGroup']")
	public WebElement addToGroupBottomBtn;

	@FindBy(name = "associtaCampaign1")
	public WebElement associateWithCampaignBottomBtn;

	@FindBy(name = "mailMerge")
	public WebElement mailmergeBottomBtn;

	@FindBy(name = "mergeLeads")
	public WebElement mergeLeadBottomBtn;

	@FindBy(name = "archive")
	public WebElement archiveBottomBtn;

	@FindBy(name = "deleteButton1")
	public WebElement deleteBottomBtn;

	@FindBy(xpath = ".//*[@id='fsLeadTable']/tbody/tr/td[2]/form/table[2]/tbody/tr[5]/td/table/tbody/tr[2]/td/table/tbody//td[2]/table/tbody/tr/td/span/a")
	public List<WebElement> listing;

	/*
	 * Add Remarks
	 */

	@FindBy(id = "remarks")
	public WebElement remarksTextFields ;

	@FindBy(xpath = ".//input[@value='Submit']")
	public WebElement submitRemarks ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeRemarksBtn ;

	@FindBy(linkText = "Add Remarks")
	public WebElement addRemarks ;

	@FindBy(id = "coApplicantSearch")
	public WebElement coApplicantSearch;

	@FindBy(id = "coApplicantRelationshipID")
	public WebElement coApplicantRelationshipIDDrp;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addCboxBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='showNotification']")
	public WebElement clkNotification ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='showBar']/img")
	public WebElement notificationBarShow ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='notification']//span[contains(text(),'View All')]")
	public WebElement viewAllNotification ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='cboxClose']")
	public WebElement closeCBox ;

	@FindBy(xpath = ".//*[@name='Close']")
	public WebElement closeCBoxBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='vTabs']//a[contains(text(),'Documents')]")
	public WebElement documentsTab ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='fimUl']//a[contains(text(),'Owners')]")
	public WebElement ownersTab ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='fimUl']//a[contains(text(),'Documents')]")
	public WebElement infoMgrDocumentsTab ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='vTabs']//a[contains(text(),'Personal Profile')]")
	public WebElement personalProfileTab ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='fimUl']//a[contains(text(),'Real Estate')]")
	public WebElement realEstateTab ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='vTabs']//a[contains(text(),'Real Estate')]")
	public WebElement realEstate ;

	@FindBy(xpath = ".//a[contains(text(),'Contact History') and @qat_tabname='Contact History']")
	public WebElement contactHistory ;
	
	// Anukaran
	@FindBy(xpath = ".//*[@id='fimUl']//a[contains(text(),'Contract Signing')]")
	public WebElement contractSigning ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='documentTitle']")
	public WebElement documentTitle ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='fsDocumentAttachment']")
	public WebElement documentAttachment ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@type='reset']")
	public WebElement documentReset ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@name='Submit']")
	public WebElement saveDoc ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@name='Add More']")
	public WebElement addMoreBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='vTabs']//a[contains(text(),'Co-Applicants')]")
	public WebElement coApplicantsTab ;

	// Anukaran
	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@name='mailSubject']")
	public WebElement fddEmailSub ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@name='timeInterval']")
	public WebElement timeInterval ;

	// Anukaran
	@FindBy(xpath = ".//*[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//*[@id='sendButton2']")
	public WebElement sendVirtualBrosChureBtn ;

	@FindBy(xpath = ".//a[contains(text(),'Log a Call')]")
	public WebElement LogACallLnk ;

	@FindBy(xpath = ".//a[contains(text(),'Send Email')]")
	public WebElement sendEmailLnk ;

	@FindBy(id = "showQuickLinks")
	public WebElement showQucikLinkBtn;
	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@value='More-Actions']")
	public WebElement moreActionsBtn ;

	@FindBy(xpath = ".//a[contains(text(),'Add Broker')]")
	public WebElement addBrokerQuickLnk ;

	// Anukaran
	@FindBy(xpath = ".//input[@value='Send FDD']")
	public WebElement sendFddMailBtn ;

	// Anukaran
	@FindBy(xpath = ".//input[@value='Send FDD Email']")
	public WebElement sendFddBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='sellerNamesAdded']")
	public WebElement defaultSellerBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='mailTemplateID']")
	public WebElement fddEmailTemplate;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@value='Send']")
	public WebElement sendbtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='asave']")
	public WebElement cnfrmbtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='vTabs']//a[contains(text(),'Candidate Portal')]")
	public WebElement candidatePortalBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='vTabs']//a[contains(text(),'Virtual Brochure')]")
	public WebElement virtualBrochureTab ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='showLD']/a[contains(text(),'Show Lead Details')]")
	public WebElement showleadDetailsBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='leadUfocAction_leadSignature']")
	public WebElement candidatePortalSignature ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='leadUfocAction_signatoryName']")
	public WebElement candidatePortalSignatureName ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainDiv']//input[@name='leadConfirmation' and @value='Yes']")
	public WebElement agreeChkBox ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='submit12']")
	public WebElement downloadFddBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='pageContainer1']/xhtml:div[3]/xhtml:section/xhtml:a")
	public WebElement clickHereDocumentlink ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='signature']")
	public WebElement acknowkledgeSignature ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='signatureName']")
	public WebElement acknowkledgeName ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='subbutton']")
	public WebElement acknowledgeSubmit ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='fddAdditionalContact']")
	public WebElement fddAdditionalContact ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='vTabs']//a[contains(text(),'Primary Info')]")
	public WebElement leadPrimaryInfo ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='for_bar']//input[@name='MoveToFo']")
	public WebElement moveToOpener ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='for_bar']//input[@name='MoveToFIM']")
	public WebElement moveToInfoMgr ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='franchiseeName']")
	public WebElement StroeNo ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='centerName']")
	public WebElement centreName ;

	@FindBy(xpath = ".//input[@type='button' and @value='More-Actions']")
	public WebElement moreActionsLink ;

	@FindBy(xpath = ".//div[@id='actionListButtons1']/table/tbody/tr[2]/td[2]/table/tbody//td")
	public List<WebElement> menu;

	// Anukaran
	@FindBy(xpath = ".//*[@id='vTabs']//a[contains(text(),'Personal Profile')]")
	public WebElement personalProfile ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='gender' and @value='Male']")
	public WebElement maleGender ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='vTabs']//a[contains(text(),'Compliance')]")
	public WebElement complianceTab ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='francCommiteeApproval' and @value='1']")
	public WebElement francCommiteeApprovalYes ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='fddDate']")
	public WebElement fddDate ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='recByFrancDate1']")
	public WebElement fddRecievedDate ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='francSignAgrDate']")
	public WebElement agreementSignedDate ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='faRequestedDate']")
	public WebElement fddRequestedDate ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='franFeeAmt']")
	public WebElement franFeeAmt ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='areaFeeAmt']")
	public WebElement areaFeeAmt ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='franchiseeName']")
	public WebElement franchiseeName ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='centerName']")
	public WebElement centerName ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='areaID']")
	public WebElement areaRegion;

	// Anukaran
	@FindBy(xpath = ".//*[@id='storeTypeId']")
	public WebElement storeTypeId;
	
	@FindBy(xpath = ".//*[@id='countryID']")
	public WebElement countryID;
	// Anukaran
	@FindBy(xpath = ".//*[@id='openingDate']")
	public WebElement openingDate ;
	
	@FindBy(xpath = ".//input[@id='grandStoreOpeningDate']")
	public WebElement OpeningDate ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='city']")
	public WebElement InfoMgrcity ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='storePhone']")
	public WebElement storePhone ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='Submit']")
	public WebElement addBtn ;

	// Anukaran
	@FindBy(xpath = ".//input[@name='shiftdoc']")
	public WebElement moveDocuments ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='regionNo']")
	public WebElement state ;

	// Anukaran
	@FindBy(xpath = ".//input[@name='fimSearchString']")
	public WebElement searchBar ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='fimReportTable']//a/img[@class='topSearch']")
	public WebElement searchBtn ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='vTabs']//a[contains(text(),'Visit')]")
	public WebElement visitTab ;

	@FindBy(xpath = ".//*[@id='vTabs']//a[contains(text(),'Primary Info')]")
	public WebElement primaryInfo ;

	@FindBy(xpath = ".//*[@name='changeStatusButton']")
	public WebElement changeStatusButton ;

	@FindBy(xpath = ".//*[@id='hm']/a")
	public WebElement heatMeter ;

	@FindBy(xpath = ".//a[contains(text(),'Modify')]")
	public WebElement leadModify ;

	@FindBy(xpath = ".//*[@id='leadSource2ID']")
	public WebElement leadSourceCatagory;

	@FindBy(xpath = ".//*[@id='leadSource3ID']")
	public WebElement leadSourcesDetails;

	// Configure Heat Index Element
	@FindBy(xpath = ".//*[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@name='isPhoneEnable']")
	public WebElement isPhoneEnable ;

	@FindBy(xpath = ".//*[@id='phone0']")
	public WebElement phoneMaxScore ;

	@FindBy(xpath = ".//*[@id='phone1']")
	public WebElement phoneOutboundCall ;

	@FindBy(xpath = ".//*[@id='phone2']")
	public WebElement phoneInboundCall ;

	@FindBy(xpath = ".//input[@name='isEmailEnable']")
	public WebElement isEmailEnable ;

	@FindBy(xpath = ".//*[@id='marketing0']")
	public WebElement marketingMaxScore ;

	@FindBy(xpath = ".//*[@id='marketing1']")
	public WebElement marketingOpenRead ;

	@FindBy(xpath = ".//*[@id='marketing2']")
	public WebElement marketingLinkClicked ;

	@FindBy(xpath = ".//input[@name='isBrochureEnable']")
	public WebElement isBrochureEnable ;

	@FindBy(xpath = ".//*[@id='brochure0']")
	public WebElement brochureMaxscore ;

	@FindBy(xpath = ".//*[@id='brochure1']")
	public WebElement brochureSectionVisited ;

	@FindBy(xpath = ".//input[@name='isDetailedQualificationEnable']")
	public WebElement isDetailedQualificationEnable ;

	@FindBy(xpath = ".//*[@id='detail0']")
	public WebElement formCompletionScore ;

	@FindBy(xpath = ".//*[@id='detail3']")
	public WebElement under199999 ;

	@FindBy(xpath = ".//*[@id='detail4']")
	public WebElement under399999 ;

	@FindBy(xpath = ".//*[@id='detail5']")
	public WebElement under599999 ;

	@FindBy(xpath = ".//*[@id='detail6']")
	public WebElement under600000 ;

	@FindBy(xpath = ".//*[@id='detail9']")
	public WebElement networth499999 ;

	@FindBy(xpath = ".//*[@id='detail10']")
	public WebElement networth2499999 ;

	@FindBy(xpath = ".//*[@id='detail11']")
	public WebElement netWorth1500000 ;

	@FindBy(xpath = ".//*[@id='detail12']")
	public WebElement networth4999999 ;

	@FindBy(xpath = ".//*[@id='detail13']")
	public WebElement networth5000000 ;

	@FindBy(xpath = ".//*[@id='detail16']")
	public WebElement oneMonth ;

	@FindBy(xpath = ".//*[@id='detail17']")
	public WebElement oneTOoThreemonth ;

	@FindBy(xpath = ".//*[@id='detail19']")
	public WebElement overSixMonth ;

	@FindBy(xpath = ".//*[@id='detail1']")
	public WebElement BckChckApprvlScore ;

	@FindBy(xpath = ".//*[@id='detail16']")
	public WebElement Under1Month ;

	@FindBy(xpath = ".//*[@id='detail17']")
	public WebElement oneto3Months ;

	@FindBy(xpath = ".//*[@id='detail18']")
	public WebElement threeTo6Months ;

	@FindBy(xpath = ".//*[@id='detail19']")
	public WebElement Over6Months ;

	@FindBy(xpath = ".//*[@id='fdd0']")
	public WebElement fddRecievedScore ;

	@FindBy(xpath = ".//input[@name='isFddReceipt']")
	public WebElement isFddReceipt ;

	@FindBy(xpath = ".//input[@name='isFinancingApproved']")
	public WebElement isFinancingApproved ;

	@FindBy(xpath = ".//*[@id='financing0']")
	public WebElement qualificationMetScore ;

	@FindBy(xpath = ".//input[@name='isAgreementSigned']")
	public WebElement isAgreementSigned ;

	@FindBy(xpath = ".//*[@id='agreement0']")
	public WebElement agreementSignedScore ;

	@FindBy(xpath = ".//input[@name='isDiscoveryDayVisit']")
	public WebElement isDiscoveryDayVisit ;

	@FindBy(xpath = ".//*[@id='discovery0']")
	public WebElement discoveryDayVisitScore ;
	// end

	@FindBy(xpath = ".//*[@id='liquidCapitalMin']")
	public WebElement currentNetWorth ;

	@FindBy(xpath = ".//*[@id='liquidCapitalMax']")
	public WebElement CashAvailableforInvestment ;

	@FindBy(xpath = ".//*[@id='visitor1Name']")
	public WebElement visitorName ;

	@FindBy(xpath = ".//*[@id='visitdate']")
	public WebElement visitorDate ;

	@FindBy(xpath = ".//*[@id='duedate']")
	public WebElement scheduledDate ;

	@FindBy(xpath = ".//input[@value='Cancel']")
	public WebElement fddcancelBtn ;

	@FindBy(xpath = ".//*[@id='ufocSearchString']")
	public WebElement fddSearch ;

	@FindBy(xpath = ".//a[@href='advanceSearch']")
	public WebElement advancedSearch ;

	@FindBy(xpath = ".//td[contains(text(),'Activity History-Remarks')]/input[@name='selTables']")
	public WebElement advSearchActivityHistoryRemarks ;

	@FindBy(xpath = ".//td[contains(text(),'Activity History-Calls')]/input")
	public WebElement advSearchActivityHistoryCalls ;

	@FindBy(xpath = ".//input[@value='Search Data']")
	public WebElement searchData ;

	@FindBy(xpath = ".//input[@name='FS_LEAD_REMARKS20_remarks']")
	public WebElement searchReamrk ;

	@FindBy(xpath = ".//input[@name='FS_LEAD_CALL20_subject']")
	public WebElement searchCallSubject;

	@FindBy(xpath = ".//*[@id='resultsPerPage']")
	public WebElement viewPerPage;

	@FindBy(xpath = ".//input[@name='checkBox']")
	public WebElement selectAllCheckBox ;

	@FindBy(xpath = ".//input[@value='Actions']")
	public WebElement actionsBtn ;

	@FindBy(xpath = ".//input[@value='View Data']")
	public WebElement veiwDataBtn ;

	@FindBy(xpath = ".//input[@value='Add Lead Marketing Code']")
	public WebElement addLeadMarketingCode ;

	@FindBy(xpath = ".//input[@name='leadCodeName']")
	public WebElement leadMarketingCode ;

	@FindBy(xpath = ".//input[@name='Addbutton8888']")
	public WebElement addBtnMarketingCode ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='vTabs']//a[contains(text(),'Qualification Details')]")
	public WebElement qualificationDetailsTab ;

	

	
	@FindBy(xpath = ".//textarea[@name='message']")
	public WebElement candidatePortalMessage ;

	@FindBy(xpath = ".//input[@value='Post Message']")
	public WebElement postMessage ;

	@FindBy(xpath = ".//*[@id='firstName']")
	public WebElement firstName ;

	@FindBy(xpath = ".//*[@id='lastName']")
	public WebElement lastName ;

	@FindBy(xpath = ".//*[@id='mailFrom']")
	public WebElement emailFrom ;

	@FindBy(xpath = ".//*[@id='subject']")
	public WebElement emailSubject ;

	@FindBy(xpath = ".//*[@id='ms-parentmailFields']/button")
	public WebElement emailFieldsDropDown ;

	@FindBy(xpath = ".//*[@id='selectAll']")
	public WebElement selectAllDrpDwn ;

	@FindBy(xpath = ".//*[@id='isAttachPdf' and @value='Y']")
	public WebElement pdfYesRadio ;

	@FindBy(xpath = ".//*[@id='subject']")
	public WebElement scheduleAppSubject ;

	@FindBy(xpath = ".//*[@id='description']")
	public WebElement scheduleAppDescription ;

	@FindBy(xpath = ".//a[contains(text(),'Schedule Appointment')]")
	public WebElement scheduleAppointment ;

	@FindBy(xpath = ".//a[@title='Add Remarks']/ancestor::tr/td[1][not(contains(text(),'Locked'))]/following-sibling::td/a[@title='Add Remarks']")
	public WebElement addUnlockedRemarks ;

	@FindBy(xpath = ".//a[@title='Add Remarks']/ancestor::tr/td[1][not(contains(text(),'Locked'))]/following-sibling::td/a[@title='View Remarks']")
	public WebElement veiwUnlockedRemarks ;

	@FindBy(xpath = ".//*[@id='activityComments']")
	public WebElement addRemarksCandidatePortal ;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtnCandidatePortal ;

	@FindBy(xpath = ".//a[contains(text(),'Initial Call With')]/ancestor::tr/following-sibling::tr/td/table/tbody/tr/td/a[@title='View Remarks']")
	public WebElement veiwRemarksInitialCallWith ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr[1]/td/table[3]/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/table/tbody/tr[2]/td/table/tbody/tr/td[3]/a/img[@alt='View Remarks']")
	public WebElement veiwRemarksInitialCall ;

	@FindBy(xpath = ".//td[contains(text(),'QUICK LINKS')]/ancestor::tr/following-sibling::tr/td/a/span[contains(text(),'Send Email')]")
	public WebElement sendEmailQuickLinks ;

	@FindBy(xpath = ".//*[@id='isRFCEMailSend' and @value='Y']")
	public WebElement sendRFCMail ;

	@FindBy(xpath = ".//*[@id='showFilter']/a")
	public WebElement showFilter ;

	@FindBy(xpath = ".//*[@id='go']")
	public WebElement searchFilterBtn ;

	@FindBy(xpath = ".//input[@name='divisionName']")
	public WebElement divisionName ;

	@FindBy(xpath = ".//input[@value='Submit']")
	public WebElement submitBtn ;

	@FindBy(xpath = ".//input[@name='Submit']")
	public WebElement submitBtn1 ;

	@FindBy(xpath = ".//input[@name='isDivisionConfigureDisplay' and @value='Y\']")
	public WebElement configureNewHierarchyLevelYes ;

	@FindBy(xpath = ".//input[@id='divisionLogo']")
	public WebElement divisionLogo ;

	@FindBy(xpath = ".//button[contains(text(),'Save')]")
	public WebElement saveCboxButton ;

	@FindBy(xpath = ".//a[contains(text(),'Add New Activity')]")
	public WebElement addNewActivityBtn ;

	@FindBy(xpath = ".//*[@id='activityName']")
	public WebElement activityName ;

	@FindBy(xpath = ".//*[@id='description']")
	public WebElement activityDescription ;

	@FindBy(xpath = ".//*[@id='activityLink']")
	public WebElement activityLink ;

	@FindBy(xpath = ".//*[@id='ms-parentbrandDivisionID']")
	public WebElement activityDivision ;

	@FindBy(xpath = ".//*[@id='documentName']")
	public WebElement documentName ;

	@FindBy(xpath = ".//input[@value='Add Division']")
	public WebElement addDivisionBtn ;

	@FindBy(xpath = ".//*[@id='pageid']//u[contains(text(),'Show All')]")
	public WebElement showAllButton ;

	@FindBy(xpath = ".//*[@id='searchString']")
	public WebElement leadSearch ;

	@FindBy(xpath = ".//img[@alt='Search Lead by Name']")
	public WebElement leadSearchBtn ;

	@FindBy(xpath = ".//select[@name='area1']")
	public WebElement defaultOwnerDrpDown ;

	@FindBy(xpath = ".//input[@name='Button22']")
	public WebElement continueButton ;

	@FindBy(xpath = ".//select[@id='allOwners']")
	public WebElement availableLeadOwners ;

	@FindBy(xpath = ".//input[@value='Update']")
	public WebElement updateLeadOwnerBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentmyLeads']/button")
	public WebElement viewLeadsBelongingTo ;

	@FindBy(xpath = ".//*[@id='ms-parentmyLeads']/div/div/input")
	public WebElement viewLeadsBelongingToInput ;

	@FindBy(xpath = ".//*[@id='ms-parentmineall']")
	public WebElement taskFilterVeiw ;

	@FindBy(xpath = ".//a[contains(text(),'Associate Campaign')]")
	public WebElement associateCampaign ;

	@FindBy(xpath = ".//*[@data-role='ico_Associate']")
	public WebElement associateCampaignIcon ;

	@FindBy(xpath = ".//button[contains(text(),'Confirm')]")
	public WebElement confirmBtn ;

	@FindBy(xpath = ".//input[@value='Add Lead Source Category']")
	public WebElement addLeadSourceCarogoryBtn ;

	@FindBy(xpath = ".//*[@id='add']")
	public WebElement addLeadSourceCarogoryCboxBtn ;

	@FindBy(xpath = ".//input[@name='leadSource2Name' and @type='text']")
	public WebElement leadSourceName ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement ModifyBtn ;

	@FindBy(xpath = ".//input[@class='cm_new_button_link']")
	public WebElement addLeadSourceDetailsBtn ;

	@FindBy(xpath = ".//input[@name='leadSource2Name' and @class='fTextBox']")
	public WebElement leadSourceDetails ;

	@FindBy(xpath = ".//input[@name='ownerSetting' and @value='YES']")
	public WebElement hiddenlinkslinkLeadYes ;

	@FindBy(xpath = ".//*[@id='leadNames']/a[3]")
	public WebElement ownerChangHistoryTab;

	@FindBy(xpath = ".//td[contains(text(),'Archived Leads Summary')]/ancestor::tbody/tr[4]/td/table/tbody/tr/td/input[@name='active']")
	public WebElement moveToActiveLeads;

	@FindBy(xpath = ".//a[contains(text(),'Add Lead')]")
	public WebElement addLeadQuickLinkBtn ;

	@FindBy(xpath = ".//a[contains(text(),'Add Group')]")
	public WebElement addGroupQuickLinkBtn ;
	
	// Akshat 
	
	@FindBy(xpath=".//input[@name='duplicate' and @value='Y']")
	public WebElement showOnlyDuplicates_Yes_RadioBtn;
	
	@FindBy(xpath=".//input[@name='duplicate' and @value='N']")
	public WebElement showOnlyDuplicates_No_RadioBtn;
	
	@FindBy(id="search")
	public WebElement filterSearchBtn;
	
	@FindBy(id="bussDayExp10Date")
	public WebElement dateHoldingPeriodRequirementsExpireForFDD; 
	

	public LeadSummaryUI(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}
}
