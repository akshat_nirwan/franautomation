package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LeadUI {

	/*
	 * Lead Type
	 */

	@FindBy(id = "ownerType")
	public WebElement SelectLeadType;

	@FindBy(id = "autoComplete")
	public WebElement ExistingOwner;

	@FindBy(id = "autoComplete")
	public WebElement ExistingLead;

	/*
	 * Contact Information
	 */

	@FindBy(id = "salutation")
	public WebElement Salutation;

	@FindBy(id = "firstName")
	public WebElement firstName;

	@FindBy(id = "lastName")
	public WebElement lastName;

	@FindBy(id = "address")
	public WebElement address1;

	@FindBy(id = "address2")
	public WebElement address2;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "country")
	public WebElement country;

	@FindBy(id = "stateID")
	public WebElement stateID;

	@FindBy(id = "zip")
	public WebElement zip;

	@FindBy(id = "countyID")
	public WebElement countyID;

	@FindBy(id = "primaryPhoneToCall")
	public WebElement PreferredModeofContact;

	@FindBy(id = "bestTimeToContact")
	public WebElement bestTimeToContact;

	@FindBy(id = "phone")
	public WebElement workPhone;

	@FindBy(id = "phoneExt")
	public WebElement workPhoneExt;

	@FindBy(id = "homePhone")
	public WebElement homePhone;

	@FindBy(id = "homePhoneExt")
	public WebElement homePhoneExt;

	@FindBy(id = "fax")
	public WebElement fax;

	@FindBy(id = "mobile")
	public WebElement mobile;

	@FindBy(id = "emailID")
	public WebElement emailID;

	@FindBy(id = "companyName")
	public WebElement companyName;

	@FindBy(id = "comments")
	public WebElement comments;

	/*
	 * Lead Details
	 */

	@FindBy(id = "assignUser")
	public WebElement assignUser;

	@FindBy(id = "leadOwnerID")
	public WebElement leadOwnerID;

	@FindBy(id = "automatic")
	public WebElement basedonAssignmentRules;

	@FindBy(id = "leadRatingID")
	public WebElement leadRatingID;

	@FindBy(id = "marketingCodeId")
	public WebElement marketingCodeId;

	@FindBy(id = "leadSource2ID")
	public WebElement leadSource2ID_Category;

	@FindBy(id = "leadSource3ID")
	public WebElement leadSource3ID_Details;

	@FindBy(name = "otherLeadSourceDetail")
	public WebElement otherLeadSourceDetail;

	@FindBy(id = "liquidCapitalMin")
	public WebElement CurrentNetWorth;

	@FindBy(id = "liquidCapitalMax")
	public WebElement CashAvailableforInvestment;

	@FindBy(id = "investTimeframe")
	public WebElement investTimeframe;

	@FindBy(id = "background")
	public WebElement background;

	@FindBy(id = "sourceOfFunding")
	public WebElement sourceOfFunding;

	@FindBy(id = "nextCallDate")
	public WebElement nextCallDate;

	@FindBy(id = "noOfUnitReq")
	public WebElement noOfUnitReq;

	@FindBy(id = "brandMapping_0brandID")
	public WebElement brandMapping_0brandID;

	/*
	 * Preferred Locations
	 */

	@FindBy(id = "temppreferredCity1")
	public WebElement temppreferredCity1;

	@FindBy(id = "temppreferredCountry1")
	public WebElement temppreferredCountry1;

	@FindBy(id = "temppreferredStateId1")
	public WebElement temppreferredStateId1;

	@FindBy(id = "temppreferredCity2")
	public WebElement temppreferredCity2;

	@FindBy(id = "temppreferredCountry2")
	public WebElement temppreferredCountry2;

	@FindBy(id = "temppreferredStateId2")
	public WebElement temppreferredStateId2;

	@FindBy(id = "templocationId1")
	public WebElement AvailableSites;

	@FindBy(id = "templocationId1b")
	public WebElement ExistingSites;

	@FindBy(id = "templocationId2")
	public WebElement ResaleSites;

	/*
	 * Forecast Details
	 */

	@FindBy(id = "forecastClosureDate")
	public WebElement forecastClosureDate;

	@FindBy(id = "probability")
	public WebElement probability;

	@FindBy(id = "forecastRating")
	public WebElement forecastRating;

	@FindBy(id = "forecastRevenue")
	public WebElement forecastRevenue;

	/*
	 * Email Campaigns
	 */

	@FindBy(xpath = ".//input[@name='assignAutomaticCampaign' and @value='no']")
	public WebElement campaignName;

	@FindBy(xpath = ".//input[@name='assignAutomaticCampaign' and @value='yes']")
	public WebElement BasedonWorkflowAssignmentRules;

	@FindBy(id = "campaignID")
	public WebElement campaignID;

	/*
	 * Buttons
	 */

	@FindBy(id = "Submit")
	public WebElement Save;

	@FindBy(xpath = ".//input[@type='RESET' and @value='Reset']")
	public WebElement Reset;

	@FindBy(xpath = ".//input[@id='Button1'")
	public WebElement Cancel;

	// CoApplicant Radio/Buttons

	@FindBy(id = "chooseExisting")
	public WebElement ContactWithAdditionOfLead;

	@FindBy(id = "addAdditionalContact")
	public WebElement ContactWithoutAdditionOfLead;

	@FindBy(id = "addNewLead1")
	public WebElement ChooseNewLead;

	@FindBy(id = "chooseExisting1")
	public WebElement ChooseExistingLead;

	@FindBy(xpath = ".//input[@class='cm_new_button_link']")
	public WebElement AddCoApplicant;

	/*
	 * Lead Modify
	 */

	@FindBy(xpath = ".//a[contains(text(),'Modify')]")
	public WebElement Modify;

	/*
	 * Co_Applicant
	 */

	@FindBy(id = "coApplicantRelationshipID")
	public WebElement CoApplicantRelationship;

	@FindBy(xpath = ".//a[@qat_tabname='Co-Applicants']")
	public WebElement CoApplicantsTab;

	public LeadUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
