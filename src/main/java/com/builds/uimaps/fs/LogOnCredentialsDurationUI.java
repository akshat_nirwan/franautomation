package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LogOnCredentialsDurationUI {

	@FindBy(xpath = ".//select[@class='multiList' and @name='dataValue']")
	public WebElement selectLogOnCredentialsDuration;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn;

	public LogOnCredentialsDurationUI(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}

}
