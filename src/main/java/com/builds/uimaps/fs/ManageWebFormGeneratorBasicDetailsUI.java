package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManageWebFormGeneratorBasicDetailsUI {
	
	public ManageWebFormGeneratorBasicDetailsUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="formName")
	public WebElement formName;
	
	@FindBy(id="formDisplayTitle")
	public WebElement formTitle;
	
	@FindBy(id="displayFormTitleCheckBox")
	public WebElement displayFormTitle_checkbox;
	
	@FindBy(id="formDescription")
	public WebElement formDescription;
	
	@FindBy(id="formFormat")
	public WebElement formFormat;
	
	@FindBy(id="columnCount")
	public WebElement noOfColumns;
	
	@FindBy(id="filedLabelAlignment")
	public WebElement defaulFieldLabelAlignment;
	
	@FindBy(id="formLanguage")
	public WebElement formLanguage;
	
	@FindBy(id="iframeWidth")
	public WebElement iframeWidth;
	
	@FindBy(id="iframeHeight")
	public WebElement iframeHeight;
	
	@FindBy(id="formUrl")
	public WebElement formURL;
	
	
	// Buttons
	@FindBy(id="detailsNextBtn")
	public WebElement saveAndNext_Btn;
	
	@FindBy(id="detailsCancelBtn")
	public WebElement cancel_Btn;

}
