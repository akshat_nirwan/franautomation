package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManageWebFormGeneratorConfirmationUI {
	
	public ManageWebFormGeneratorConfirmationUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
//  Manage Web Form Generator  > Confirmation
	
	@FindBy(id="iframeCode")
	public WebElement iframeEmbedCode_radioBtn;
	
	@FindBy(id="hostURL")
	public WebElement hostURL_radioBtn;
	
	@FindBy(id="iframeCodeBox")
	public WebElement iframeEmbedCode_textBox;
	
	@FindBy(id="confirmationBackBtn")
	public WebElement previous_Btn;
	
	@FindBy(id="confirmationNextBtn")
	public WebElement ok_btn;
	

}
