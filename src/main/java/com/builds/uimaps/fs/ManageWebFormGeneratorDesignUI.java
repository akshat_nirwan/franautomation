package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManageWebFormGeneratorDesignUI {
	
	public ManageWebFormGeneratorDesignUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "sectionBtn")
	public WebElement addSection_btn;

	@FindBy(id = "sectionBtnPrw")
	public WebElement saveAndPreview_btn;

	@FindBy(id = "designBackBtn")
	public WebElement previous_btn;

	@FindBy(id = "designNextBtn")
	public WebElement saveAndNext_btn;

	@FindBy(id = "designCancelBtn")
	public WebElement cancel_btn;
	
	@FindBy(id = "ui-accordion-subModules-header-1")
	public WebElement primaryInfo_availableField;
	
	@FindBy(xpath=".//input[@class='clearable' and @data-searchid='fsLeadDetails']")
	public WebElement searchFields_primaryInfo;

}
