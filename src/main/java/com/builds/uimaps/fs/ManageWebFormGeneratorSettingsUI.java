package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManageWebFormGeneratorSettingsUI {

	public ManageWebFormGeneratorSettingsUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	

	// Default values for Fields
	@FindBy(xpath = ".//select[@id='leadStatusID']")
	public WebElement leadStatus;

	@FindBy(id = "assignUser")
	public WebElement LeadOwner_selectRadioBtn;

	@FindBy(id = "leadOwnerID")
	public WebElement leadOwner_dropDown;

	@FindBy(id = "automatic")
	public WebElement LeadOwner_basedOnAssignmentRules_radioBtn;

	@FindBy(id = "campaignID")
	public WebElement campaignName_dropdown;

	@FindBy(xpath = ".//select[@id='leadSource2ID']")
	public WebElement leadSourceCategory_dropdown;

	@FindBy(xpath = ".//select[@id='leadSource3ID']")
	public WebElement leadSourceDetails_dropdown;

	@FindBy(id = "brandID")
	public WebElement division_dropdown;

	// After Submission
	@FindBy(xpath = ".//input[@type='radio' and @value='msg']")
	public WebElement afterSubmission_ConfirmationMessage_radioBtn;

	@FindBy(xpath = ".//input[@type='radio' and @value='url']")
	public WebElement afterSubmission_RedirectURL_radioBtn;

	@FindBy(id = "settingsBackBtn")
	public WebElement previousBtn;

	@FindBy(id = "settingsNextBtn")
	public WebElement finishBtn;

	@FindBy(id = "settingsCancelBtn")
	public WebElement cancelBtn;

	@FindBy(id = "preBtn1")
	public WebElement previewFormBtn;

	@FindBy(id = "tinymce")
	public WebElement bodyTextArea;

}
