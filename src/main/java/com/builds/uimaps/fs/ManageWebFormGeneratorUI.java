package com.builds.uimaps.fs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManageWebFormGeneratorUI {
	
	public ManageWebFormGeneratorUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className="cm_new_button_link")
	public WebElement createNewForm_btn;
	
	@FindBy(id="searchMyForm")
	public WebElement search_TextBox;
	
	

	

}
