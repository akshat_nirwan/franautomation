package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PersonalProfileUI {

	@FindBy(id = "firstName")
	public WebElement firstName;

	@FindBy(id = "lastName")
	public WebElement lastName;

	@FindBy(xpath = ".//*[@id='gender' and @value='Male']")
	public WebElement genderMale;
	@FindBy(xpath = ".//*[@id='gender' and @value='Female']")
	public WebElement genderFemale;

	@FindBy(id = "homeAddress")
	public WebElement homeAddress;

	@FindBy(id = "howLongAtAddress")
	public WebElement howLongAtAddress;

	@FindBy(id = "homeCity")
	public WebElement homeCity;

	@FindBy(id = "homeCountry")
	public WebElement homeCountry;

	@FindBy(id = "homeState")
	public WebElement homeState;

	@FindBy(id = "homeZip")
	public WebElement homeZip;

	@FindBy(id = "birthMonth")
	public WebElement birthMonth;

	@FindBy(id = "birthDate")
	public WebElement birthDate;

	@FindBy(id = "homePhone")
	public WebElement homePhone;

	@FindBy(id = "homePhoneExt")
	public WebElement homePhoneExt;

	@FindBy(xpath = ".//*[@id='homeOwnership' and @value='Own']")
	public WebElement homeOwnershipOwn;
	@FindBy(xpath = ".//*[@id='homeOwnership' and @value='Renting']")
	public WebElement homeOwnershipRenting;

	@FindBy(id = "timeToCall")
	public WebElement timeToCall;

	@FindBy(id = "email")
	public WebElement email;

	@FindBy(xpath = ".//*[@id='maritalStatus' and @value='Single']")
	public WebElement maritalStatusSingle;
	@FindBy(xpath = ".//*[@id='maritalStatus' and @value='Married']")
	public WebElement maritalStatusMarried;

	@FindBy(id = "spouseName")
	public WebElement spouseName;

	@FindBy(id = "heardProformaFrom")
	public WebElement heardProformaFrom;

	@FindBy(id = "seekingOwnBusiness")
	public WebElement seekingOwnBusiness;

	@FindBy(id = "fullTimeBusiness")
	public WebElement fullTimeBusiness;

	@FindBy(id = "otherInvestigation")
	public WebElement otherInvestigation;

	@FindBy(id = "presentEmployer")
	public WebElement presentEmployer;

	@FindBy(id = "percentOwn")
	public WebElement percentOwn;

	@FindBy(id = "title")
	public WebElement title;

	@FindBy(id = "dateStarted")
	public WebElement dateStarted;

	@FindBy(id = "employerAddress")
	public WebElement employerAddress;

	@FindBy(id = "employerCity")
	public WebElement employerCity;

	@FindBy(id = "employerCountry")
	public WebElement employerCountry;

	@FindBy(id = "employerState")
	public WebElement employerState;

	@FindBy(id = "employerZip")
	public WebElement employerZip;

	@FindBy(id = "businessPhone")
	public WebElement businessPhone;

	@FindBy(id = "callAtWork")
	public WebElement callAtWork;

	@FindBy(id = "hourPerWeek")
	public WebElement hourPerWeek;

	@FindBy(id = "salary")
	public WebElement salary;

	@FindBy(id = "responsibility")
	public WebElement responsibility;

	@FindBy(xpath = ".//input[@id='selfEmployed' and @value='Y']")
	public WebElement selfEmployedYes;
	@FindBy(xpath = ".//input[@id='selfEmployed' and @value='N']")
	public WebElement selfEmployedNo;

	@FindBy(xpath = ".//input[@id='limitProforma' and @value='Y']")
	public WebElement limitProformaYes;
	@FindBy(xpath = ".//input[@id='limitProforma' and @value='N']")
	public WebElement limitProformaNo;

	@FindBy(xpath = ".//input[@id='similarWork' and @value='Y']")
	public WebElement similarWorkYes;
	@FindBy(xpath = ".//input[@id='similarWork' and @value='N']")
	public WebElement similarWorkNo;

	@FindBy(id = "financeProforma")
	public WebElement financeProforma;

	@FindBy(xpath = ".//input[@id='partner' and @value='Y']")
	public WebElement partnerYes;
	@FindBy(xpath = ".//input[@id='partner' and @value='N']")
	public WebElement partnerNo;

	@FindBy(id = "supportHowLong")
	public WebElement supportHowLong;

	@FindBy(id = "income")
	public WebElement income;

	@FindBy(id = "otherSalary")
	public WebElement otherSalary;

	@FindBy(id = "otherIncomeExplaination")
	public WebElement otherIncomeExplaination;

	@FindBy(xpath = ".//input[@id='soleSource' and @value='Y']")
	public WebElement soleSourceYes;
	@FindBy(xpath = ".//input[@id='soleSource' and @value='N']")
	public WebElement soleSourceNo;

	@FindBy(id = "howSoon")
	public WebElement howSoon;

	@FindBy(xpath = ".//input[@id='runYourself' and @value='Y']")
	public WebElement runYourselfYes;
	@FindBy(xpath = ".//input[@id='runYourself' and @value='N']")
	public WebElement runYourselfNo;

	@FindBy(id = "responsibleForOperation")
	public WebElement responsibleForOperation;

	@FindBy(xpath = ".//input[@id='convictedForFelony' and @value='Y']")
	public WebElement convictedForFelonyYes;
	@FindBy(xpath = ".//input[@id='convictedForFelony' and @value='N']")
	public WebElement convictedForFelonyNo;

	@FindBy(xpath = ".//input[@id='liabilites' and @value='Y']")
	public WebElement liabilitesYes;
	@FindBy(xpath = ".//input[@id='liabilites' and @value='N']")
	public WebElement liabilitesNo;

	@FindBy(xpath = ".//input[@id='bankruptcy' and @value='Y']")
	public WebElement bankruptcyYes;
	@FindBy(xpath = ".//input[@id='bankruptcy' and @value='N']")
	public WebElement bankruptcyNo;

	@FindBy(xpath = ".//input[@id='lawsuit' and @value='Y']")
	public WebElement lawsuitYes;
	@FindBy(xpath = ".//input[@id='lawsuit' and @value='N']")
	public WebElement lawsuitNo;

	@FindBy(xpath = ".//input[@id='convicted' and @value='Y']")
	public WebElement convictedYes;
	@FindBy(xpath = ".//input[@id='lawsuit' and @value='N']")
	public WebElement convictedNo;

	@FindBy(id = "familyFeelings")
	public WebElement familyFeelings;

	@FindBy(id = "otherFacts")
	public WebElement otherFacts;

	@FindBy(id = "personalPersonalityStyle")
	public WebElement personalPersonalityStyle;

	@FindBy(id = "backgroundOverview")
	public WebElement backgroundOverview;

	@FindBy(id = "goalDream")
	public WebElement goalDream;

	@FindBy(id = "concerns")
	public WebElement concerns;

	@FindBy(id = "timing")
	public WebElement timing;

	@FindBy(id = "hotButtons")
	public WebElement hotButtons;

	@FindBy(id = "otherOptions")
	public WebElement otherOptions;

	// Buttons

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn;

	@FindBy(xpath = ".//*[@id='title']")
	public WebElement employmentTitle ;

	@FindBy(xpath = ".//*[@id='otherOptions']")
	public WebElement otherComments ;

	/*
	 * @FindBy(xpath=".//*[@id='percentOwn']") public WebElement
	 * percentOwn=DDDDDDDDDDDDDDDDDDDDD
	 */

	public PersonalProfileUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
