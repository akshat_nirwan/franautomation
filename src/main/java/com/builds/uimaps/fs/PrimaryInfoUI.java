package com.builds.uimaps.fs;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PrimaryInfoUI {

	
	@FindBy(xpath = ".//td[@class='btm-menu-rgt-link']//a[contains(text(),'Modify')]")
	public WebElement modify_Link;

	
	@FindBy(xpath = ".//td[@class='btm-menu-rgt-link']//a[contains(text(),'Send Email')]")
	public WebElement SendEmail_Link;

	
	@FindBy(xpath = ".//td[@class='btm-menu-rgt-link']//a[contains(text(),'Log a Task')]")
	public WebElement LogaTask_Link;

	
	@FindBy(xpath = ".//td[@class='btm-menu-rgt-link']//a[contains(text(),'Log a Call')]")
	public WebElement LogaCall_Link;

	
	@FindBy(xpath = ".//td[@class='btm-menu-rgt-link']//a[contains(text(),'Add Remarks')]")
	public WebElement AddRemarks_Link;
	
	
	
	@FindBy(xpath = ".//div[@id='heatMeter']//a[contains(text(),'Activity Timeline')]")
	public WebElement ActivityTimeline;

	
	@FindBy(xpath = ".//div[@id='heatMeter']//a[contains(text(),'Heat Meter')]")
	public WebElement HeatMeter;

	
	@FindBy(xpath = ".//a[contains(text(),'Back to Search Results')]")
	public WebElement BacktoSearchResults_Link;

	
	@FindBy(id = "leadStatusID")
	public WebElement LeadStatus_Select;

	
	@FindBy(xpath = ".//img[contains(@src,'pickList.gif')]")
	public WebElement leadSourceDetails_View_Img;

	
	@FindBy(xpath = ".//a[contains(text(),'Associate Campaign')]")
	public WebElement AssociateCampaign_Link;

	
	@FindBy(xpath = ".//a[contains(text(),'Stop Campaign')]")
	public WebElement StopCampaign_Link;

	
	@FindBy(xpath = ".//td[@class='pvs_hdr2_new']//a[contains(text(),'Add Remarks')]")
	public WebElement AddRemarks_Link_ActivityHistory;

	
	@FindBy(xpath = ".//td[@class='pvs_hdr2_new']//a[contains(text(),'Log a Call')]")
	public WebElement AddACall_Link_ActivityHistory;

	
	@FindBy(xpath = ".//td[@class='pvs_hdr2_new']//a[contains(text(),'Detailed History')]")
	public WebElement DetailedHistory_Link_ActivityHistory;

	
	@FindBy(name = "changeStatusButton")
	public WebElement changeStatus_Button;

	
	@FindBy(name = "changeOwner")
	public WebElement changeOwner_button;

	
	@FindBy(name = "sendUfoc")
	public WebElement sendFDDMail_button;

	
	@FindBy(name = "AddToGroup")
	public WebElement addToGroup_button;

	
	@FindBy(name = "associtaCampaign1")
	public WebElement associateCampaign_button;

	
	@FindBy(name = "MailMerge")
	public WebElement mailMerge_button;

	
	@FindBy(name = "MoveToFIM")
	public WebElement MoveToFIM_button;

	
	@FindBy(name = "MoveToFo")
	public WebElement MoveToFo_button;

	
	@FindBy(name = "addLead")
	public WebElement addLead_button;

	
	@FindBy(name = "printButton")
	public WebElement print_button;

	
	@FindBy(xpath = ".//span[@id='pmlink']/a")
	public WebElement provenMatchLink;

	/*
	 * Others
	 */

	public String leadLogCallSummary_IFrame_ById = "leadLogCallSummary";

	public String leadOpenActivitesSummary_IFrame_ById = "leadOpenActivitesSummary";

	public String getXPathOfActivityHistoryRemarksActions(String actionName) {
		return ".//*[.='"+actionName+"']//ancestor::tr[1]";
	}
	
	public String actionButtonAgainst_ActivityLink(String activityName) {
		return ".//*[.='"+activityName+"']//ancestor::tr[1]/td[6]//a/img";
	}

	
	@FindBy(xpath = ".//table[@id='outerTable']//a[.='Next']")
	public WebElement next_Xpath;

	
	@FindBy(xpath = ".//table[@id='outerTable']//a[.='Prev']")
	public WebElement prev_Xpath;

	public String checkValueUnderRightMeter(String comments) {
		return ".//td[@id='rightMeter']//*[contains(text(),'" + comments + "')]";
	}

	
	@FindBy(xpath = ".//input[@value='More-Actions' and @type='button']")
	public WebElement moreAction_RigthMenu_Xpath;

	
	@FindBy(xpath = ".//a[@qat_tabname='Co-Applicants']")
	public WebElement CoApplicants_tab_link;

	
	@FindBy(xpath = ".//a[@qat_tabname='Compliance']")
	public WebElement Compliance_tab_link;

	
	@FindBy(xpath = ".//a[@qat_tabname='Documents']")
	public WebElement Documents_tab_link;

	
	@FindBy(xpath = ".//a[@qat_tabname='Personal Profile']")
	public WebElement PersonalProfile_tab_link;

	
	@FindBy(xpath = ".//a[@qat_tabname='Primary Info']")
	public WebElement PrimaryInfo_tab_link;

	
	@FindBy(xpath = ".//a[@qat_tabname='Qualification Details']")
	public WebElement QualificationDetails_tab_link;

	
	@FindBy(xpath = ".//a[@qat_tabname='Real Estate']")
	public WebElement RealEstate_tab_link;

	
	@FindBy(xpath = ".//a[@qat_tabname='Virtual Brochure']")
	public WebElement VirtualBrochure_tab_link;

	
	@FindBy(xpath = ".//a[@qat_tabname='Visit']")
	public WebElement Visit_tab_link;

	
	@FindBy(xpath = ".//a[@qat_tabname='bQual']")
	public WebElement bQual_tab_link;

	
	@FindBy(xpath = ".//a[@qat_tabname='Proven Match Assessment']")
	public WebElement ProvenMatchAssessment_tab_link;

	
	@FindBy(linkText = "Remarks and Call History")
	public WebElement RemarksandCallHistory_link;

	
	@FindBy(linkText = "Status Change History")
	public WebElement StatusChangeHistory_link;

	
	@FindBy(linkText = "Owner Change History")
	public WebElement OwnerChangeHistory_link;

	
	@FindBy(linkText = "Tasks")
	public WebElement Tasks_link;

	
	@FindBy(linkText = "Activity History")
	public WebElement ActivityHistory_link;

	
	@FindBy(linkText = "Message History")
	public WebElement MessageHistory_link;

	
	@FindBy(linkText = "SMS History")
	public WebElement SMSHistory_link;

	
	@FindBy(xpath = ".//div[@id='actionListButtons1']//table[@class='text']//tr/td")
	public List<WebElement> list_RightActionsMenu;

	public PrimaryInfoUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
