package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class QualificationDetailsUI {

	/*
	 * Personal Information
	 */

	@FindBy(id = "date")
	public WebElement Date;

	@FindBy(id = "firstName")
	public WebElement Name;

	@FindBy(xpath = ".//input[@id='gender' and @value='Male']")
	public WebElement GenderMale;

	@FindBy(xpath = ".//input[@name='gender' and @value='Female']")
	public WebElement GenderFemale;

	@FindBy(id = "presentAddress")
	public WebElement PresentAddress;

	@FindBy(id = "howLong")
	public WebElement HowManyYearsAtThisAddress;

	@FindBy(id = "city")
	public WebElement City;

	@FindBy(id = "country")
	public WebElement Country;

	@FindBy(id = "stateID")
	public WebElement StateProvince;

	@FindBy(id = "zipCode")
	public WebElement ZipPostalCode;

	@FindBy(id = "workPhone")
	public WebElement WorkPhone;

	@FindBy(id = "phoneExt")
	public WebElement WorkPhoneExt;

	@FindBy(id = "homePhone")
	public WebElement HomePhone;

	@FindBy(id = "homePhoneExt")
	public WebElement HomePhoneExt;

	@FindBy(id = "emailID")
	public WebElement Email;

	@FindBy(xpath = ".//input[@id='usCitizen' and @value='Yes']")
	public WebElement USCitizenYes;

	@FindBy(xpath = ".//input[@id='usCitizen' and @value='No']")
	public WebElement USCitizenNo;

	@FindBy(id = "ssn")
	public WebElement SocialSecurity;

	@FindBy(id = "previousAddress")
	public WebElement PreviousAddress;

	@FindBy(id = "previousCity")
	public WebElement PreviousCity;

	@FindBy(id = "previousCountry")
	public WebElement PreviousCountry;

	@FindBy(id = "previousStateID")
	public WebElement PreviousStateProvince;

	@FindBy(id = "previousZipCode")
	public WebElement PreviousZipPostalCode;

	@FindBy(id = "birthMonth")
	public WebElement BirthMonth;

	@FindBy(id = "birthDate")
	public WebElement BirthDate;

	@FindBy(id = "timeToCall")
	public WebElement BestTimeToCall;

	@FindBy(xpath = ".//input[@id='homeOwnership' and @value='Own']")
	public WebElement HomeOwnershipOwn;

	@FindBy(xpath = ".//input[@id='homeOwnership' and @value='Renting']")
	public WebElement HomeOwnershipRenting;

	@FindBy(xpath = ".//input[@id='maritalStatus' and @value='Single']")
	public WebElement MaritalStatusSingle;

	@FindBy(xpath = ".//input[@id='maritalStatus' and @value='Married']")
	public WebElement MaritalStatusMarried;

	@FindBy(id = "spouseName")
	public WebElement SpouseName;

	@FindBy(id = "spouseSsn")
	public WebElement SpouseSocialSecurity;

	@FindBy(xpath = ".//input[@id='spouseUsCitizen' and @value='Yes']")
	public WebElement SpouseUSCitizenYes;

	@FindBy(xpath = ".//input[@id='spouseUsCitizen' and @value='No']")
	public WebElement SpouseUSCitizenNo;

	@FindBy(xpath = ".//select[@id='spouseBirthMonth']")
	public WebElement SpouseBirthMonth;

	@FindBy(xpath = ".//select[@id='spouseBirthDate']")
	public WebElement SpouseBirthDate;

	/*
	 * Assets & Liabilities
	 */

	@FindBy(id = "cashOnHand")
	public WebElement CashOnHandAndBanks;

	@FindBy(id = "mortgages")
	public WebElement Mortgages;

	@FindBy(id = "marketableSecurities")
	public WebElement MarketableSecurities;

	@FindBy(id = "accountsPayable")
	public WebElement AccountsPayable;

	@FindBy(id = "accountsReceivable")
	public WebElement AccountsNotesReceivable;

	@FindBy(id = "notesPayable")
	public WebElement NotesPayable;

	@FindBy(id = "retirementPlans")
	public WebElement RetirementPlans;

	@FindBy(id = "loansOnLifeInsurance")
	public WebElement LoansOnLifeInsurance;

	@FindBy(id = "realEstate")
	public WebElement RealEstate;

	@FindBy(id = "creditCardBalance")
	public WebElement CreditCardsTotalBalance;

	@FindBy(id = "personalProperty")
	public WebElement PersonalProperty;

	@FindBy(id = "unpaidTaxes")
	public WebElement UnpaidTaxes;

	@FindBy(id = "businessHoldings")
	public WebElement BusinessHoldings;

	@FindBy(id = "lifeInsurance")
	public WebElement LifeInsurance;

	@FindBy(id = "otherAssets")
	public WebElement OtherAssets;

	@FindBy(id = "otherLiabilities")
	public WebElement OtherLiabilities;

	@FindBy(id = "liablitiesDescription")
	public WebElement LiabilitiesDescription;

	@FindBy(id = "assestsDescription")
	public WebElement AssetsDescription;

	@FindBy(id = "totalAssets")
	public WebElement TotalAssets;

	@FindBy(id = "totalLiabilities")
	public WebElement TotalLiabilities;

	@FindBy(id = "totalNetworth")
	public WebElement TotalNetWorth;

	/*
	 * Real Estate Owned
	 */

	@FindBy(id = "reAddress1")
	public WebElement RealEstateAddress1;

	@FindBy(id = "reDatePurchased1")
	public WebElement RealEstateDatePurchased1;

	@FindBy(id = "reOrigCost1")
	public WebElement RealEstateOriginalCost1;

	@FindBy(id = "rePresentValue1")
	public WebElement RealEstatePresentValue1;

	@FindBy(id = "reMortgageBalance1")
	public WebElement RealEstateMortgageBalance1;

	@FindBy(id = "reAddress2")
	public WebElement RealEstateAddress2;

	@FindBy(id = "reDatePurchased2")
	public WebElement RealEstateDatePurchased2;

	@FindBy(id = "reOrigCost2")
	public WebElement RealEstateOriginalCost2;

	@FindBy(id = "rePresentValue2")
	public WebElement RealEstatePresentValue2;

	@FindBy(id = "reMortgageBalance2")
	public WebElement RealEstateMortgageBalance2;

	@FindBy(id = "reAddress3")
	public WebElement RealEstateAddress3;

	@FindBy(id = "reDatePurchased3")
	public WebElement RealEstateDatePurchased3;

	@FindBy(id = "reOrigCost3")
	public WebElement RealEstateOriginalCost3;

	@FindBy(id = "rePresentValue3")
	public WebElement RealEstatePresentValue3;

	@FindBy(id = "reMortgageBalance3")
	public WebElement RealEstateMortgageBalance3;

	/*
	 * Annual Sources Of Income
	 */

	@FindBy(id = "annualSalary")
	public WebElement Salary;

	@FindBy(id = "annualInvestment")
	public WebElement Investment;

	@FindBy(id = "annualReIncome")
	public WebElement RealEstateIncome;

	@FindBy(id = "otherAnnualSource")
	public WebElement Other;

	@FindBy(id = "otherAnnualSourceDescription")
	public WebElement Description;

	@FindBy(id = "annualSourceTotal")
	public WebElement TotalAnnualSource;

	/*
	 * Total Contingent Liabilities
	 */

	@FindBy(id = "loanCoSign")
	public WebElement LoanCoSignature;

	@FindBy(id = "legalJudgement")
	public WebElement LegalJudgement;

	@FindBy(id = "incomeTaxes")
	public WebElement IncomeTaxes;

	@FindBy(id = "otherSpecialDebt")
	public WebElement OtherSpecialDebt;

	@FindBy(id = "totalContingent")
	public WebElement TotalContingent;

	/*
	 * Specific Data
	 */

	@FindBy(id = "whenReadyIfApproved")
	public WebElement WhenReadyIfApproved;

	@FindBy(id = "skillsExperience")
	public WebElement SkillsExperience;

	@FindBy(id = "enableReachGoals")
	public WebElement EnableReachGoals;

	@FindBy(id = "responsibleForDailyOperations")
	public WebElement ResponsibleForDailyOperations;

	@FindBy(id = "cashAvailable")
	public WebElement CashAvailableForInvestment;

	@FindBy(xpath = ".//input[@id='approvedForFinancing' and @value='Yes']")
	public WebElement ApprovedForFinancingYes;

	@FindBy(xpath = ".//input[@id='approvedForFinancing' and @value='No']")
	public WebElement ApprovedForFinancingNo;

	@FindBy(id = "amountApprovedForFinance")
	public WebElement AmountApprovedForFinance;

	@FindBy(xpath = ".//input[@id='soleIncomeSource' and @value='Yes']")
	public WebElement SoleIncomeSourceYes;

	@FindBy(xpath = ".//input[@id='soleIncomeSource' and @value='No']")
	public WebElement SoleIncomeSourceNo;

	@FindBy(xpath = ".//input[@id='liabilites' and @value='Yes']")
	public WebElement ContingentLiabilitesYes;

	@FindBy(xpath = ".//input[@id='liabilites' and @value='No']")
	public WebElement ContingentLiabilitesNo;

	@FindBy(xpath = ".//input[@id='lawsuit' and @value='Yes']")
	public WebElement LawsuitYes;

	@FindBy(xpath = ".//input[@id='lawsuit' and @value='No']")
	public WebElement LawsuitNo;

	@FindBy(xpath = ".//input[@id='convicted' and @value='Yes']")
	public WebElement ConvictedYes;

	@FindBy(xpath = ".//input[@id='convicted' and @value='No']")
	public WebElement ConvictedNo;

	@FindBy(xpath = ".//input[@id='convictedOfFelony' and @value='Yes']")
	public WebElement ConvictedOfFelonyYes;

	@FindBy(xpath = ".//input[@id='convictedOfFelony' and @value='No']")
	public WebElement ConvictedOfFelonyNo;

	@FindBy(id = "explainConviction")
	public WebElement ExplainConviction;

	@FindBy(xpath = ".//input[@id='filedBankruptcy' and @value='Yes']")
	public WebElement FiledBankruptcyYes;

	@FindBy(xpath = ".//input[@id='filedBankruptcy' and @value='No']")
	public WebElement FiledBankruptcyNo;

	@FindBy(id = "dateFiled")
	public WebElement DateFieldBankruptcy;

	@FindBy(id = "dateDischarged")
	public WebElement DateDischargedField;

	@FindBy(id = "locationPreference1")
	public WebElement AreaLocationPreferences1;

	@FindBy(id = "locationPreference2")
	public WebElement AreaLocationPreferences2;

	@FindBy(id = "locationPreference3")
	public WebElement AreaLocationPreferences3;

	@FindBy(id = "businessQuestion1")
	public WebElement BusinessQuestion1;

	@FindBy(id = "businessQuestion2")
	public WebElement BusinessQuestion2;

	@FindBy(id = "businessQuestion3")
	public WebElement BusinessQuestion3;

	/*
	 * Internal Analysis of Applicant
	 */

	// * Applicant

	@FindBy(id = "finFirstName")
	public WebElement FinFirstName;

	@FindBy(id = "lastName")
	public WebElement FinLastName;

	@FindBy(id = "address")
	public WebElement FinAddress;

	@FindBy(id = "finCity")
	public WebElement FinCity;

	@FindBy(id = "phone")
	public WebElement FinWorkPhone;

	@FindBy(id = "finCountry")
	public WebElement FinCountry;

	@FindBy(id = "finState")
	public WebElement FinStateProvince;

	@FindBy(id = "phoneExt2")
	public WebElement FinWorkPhoneExt;

	@FindBy(xpath = ".//input[@id='calledOffice' and @value='Yes']")
	public WebElement CalledOfficeYes;

	@FindBy(xpath = ".//input[@id='calledOffice' and @value='No']")
	public WebElement CalledOfficeNo;

	// * Heat Index Components

	@FindBy(id = "liquidCapitalMin")
	public WebElement HeatIndexCurrentNetWorth;

	@FindBy(id = "liquidCapitalMax")
	public WebElement HeatIndexCashAvailableForInvestment;

	@FindBy(id = "investTimeframe")
	public WebElement InvestTimeframe;

	@FindBy(id = "background")
	public WebElement EmploymentBackground;

	@FindBy(xpath = ".//input[@id='backgroundCheck' and @value='Yes']")
	public WebElement BackgroundCheckApprovalYes;

	@FindBy(xpath = ".//input[@id='backgroundCheck' and @value='No']")
	public WebElement BackgroundCheckApprovalNo;

	/*
	 * Qualification Checklists
	 */

	// #1 Lendable Net Worth

	@FindBy(name = "qfnChkAckValue_0")
	public WebElement LendableNetWorth_Value;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_0' and @value='Y']")
	public WebElement LendableNetWorth_Completed_yes ;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_0' and @value='N']")
	public WebElement LendableNetWorth_Completed_No ;

	@FindBy(id = "qfnChkAckCompletionDate_0")
	public WebElement LendableNetWorth_CompletionDate;

	@FindBy(id = "qfnChkAssociateDoc_0")
	public WebElement LendableNetWorth_associatedDocument;

	@FindBy(name = "qfnChkAckVerifiedBy_0")
	public WebElement LendableNetWorth_VerifiedBy;

	@FindBy(id = "qfnChkAckDate_0")
	public WebElement LendableNetWorth_Date;

	// #2 Background and Criminal Check

	@FindBy(name = "qfnChkAckValue_1")
	public WebElement BackgroundandCriminalCheck_Value;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_1' and @value='Y']")
	public WebElement BackgroundandCriminalCheck_Completed_yes ;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_1' and @value='N']")
	public WebElement BackgroundandCriminalCheck_Completed_No ;

	@FindBy(id = "qfnChkAckCompletionDate_1")
	public WebElement BackgroundandCriminalCheck_CompletionDate;

	@FindBy(id = "qfnChkAssociateDoc_1")
	public WebElement BackgroundandCriminalCheck_associatedDocument;

	@FindBy(name = "qfnChkAckVerifiedBy_1")
	public WebElement BackgroundandCriminalCheck_VerifiedBy;

	@FindBy(id = "qfnChkAckDate_1")
	public WebElement BackgroundandCriminalCheck_Date;

	// #3 Credit Check

	@FindBy(name = "qfnChkAckValue_2")
	public WebElement CreditCheck_Value;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_2' and @value='Y']")
	public WebElement CreditCheck_Completed_yes ;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_2' and @value='N']")
	public WebElement CreditCheck_Completed_No ;

	@FindBy(id = "qfnChkAckCompletionDate_2")
	public WebElement CreditCheck_CompletionDate;

	@FindBy(id = "qfnChkAssociateDoc_2")
	public WebElement CreditCheck_associatedDocument;

	@FindBy(name = "qfnChkAckVerifiedBy_2")
	public WebElement CreditCheck_VerifiedBy;

	@FindBy(id = "qfnChkAckDate_2")
	public WebElement CreditCheck_Date;

	// #4 Territory Approved

	@FindBy(name = "qfnChkAckValue_3")
	public WebElement TerritoryApproved_Value;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_3' and @value='Y']")
	public WebElement TerritoryApproved_Completed_yes ;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_3' and @value='N']")
	public WebElement TerritoryApproved_Completed_No ;

	@FindBy(id = "qfnChkAckCompletionDate_3")
	public WebElement TerritoryApproved_CompletionDate;

	@FindBy(id = "qfnChkAssociateDoc_3")
	public WebElement TerritoryApproved_associatedDocument;

	@FindBy(name = "qfnChkAckVerifiedBy_3")
	public WebElement TerritoryApproved_VerifiedBy;

	@FindBy(id = "qfnChkAckDate_3")
	public WebElement TerritoryApproved_Date;

	// #5 Franchise Agreement on File

	@FindBy(name = "qfnChkAckValue_4")
	public WebElement FranchiseAgreementonFile_Value;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_4' and @value='Y']")
	public WebElement FranchiseAgreementonFile_Completed_yes ;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_4' and @value='N']")
	public WebElement FranchiseAgreementonFile_Completed_No ;

	@FindBy(id = "qfnChkAckCompletionDate_4")
	public WebElement FranchiseAgreementonFile_CompletionDate;

	@FindBy(id = "qfnChkAssociateDoc_4")
	public WebElement FranchiseAgreementonFile_associatedDocument;

	@FindBy(name = "qfnChkAckVerifiedBy_4")
	public WebElement FranchiseAgreementonFile_VerifiedBy;

	@FindBy(id = "qfnChkAckDate_4")
	public WebElement FranchiseAgreementonFile_Date;

	// #6 FDD Receipt on File

	@FindBy(name = "qfnChkAckValue_5")
	public WebElement FDDReceiptonFile_Value;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_5' and @value='Y']")
	public WebElement FDDReceiptonFile_Completed_yes ;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_5' and @value='N']")
	public WebElement FDDReceiptonFile_Completed_No ;

	@FindBy(id = "qfnChkAckCompletionDate_5")
	public WebElement FDDReceiptonFile_CompletionDate;

	@FindBy(id = "qfnChkAssociateDoc_5")
	public WebElement FDDReceiptonFile_associatedDocument;

	@FindBy(name = "qfnChkAckVerifiedBy_5")
	public WebElement FDDReceiptonFile_VerifiedBy;

	@FindBy(id = "qfnChkAckDate_5")
	public WebElement FDDReceiptonFile_Date;

	// #6 FDD Receipt on File

	@FindBy(name = "qfnChkAckValue_6")
	public WebElement Opener_Value;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_6' and @value='Y']")
	public WebElement Opener_Completed_yes ;

	@FindBy(xpath = ".//input[@name='qfnChkAckCompleted_6' and @value='N']")
	public WebElement Opener_Completed_No ;

	@FindBy(id = "qfnChkAckCompletionDate_6")
	public WebElement Opener_CompletionDate;

	@FindBy(id = "qfnChkAssociateDoc_6")
	public WebElement Opener_associatedDocument;

	@FindBy(name = "qfnChkAckVerifiedBy_6")
	public WebElement Opener_VerifiedBy;

	@FindBy(id = "qfnChkAckDate_6")
	public WebElement Opener_Date;

	// Buttons

	@FindBy(xpath = ".//input[@class='cm_new_button' and @value='Save']")
	public WebElement save;

	public QualificationDetailsUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
