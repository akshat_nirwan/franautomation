package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RealEstateUI {

	@FindBy(id = "siteAddress1")
	public WebElement siteAddress1;

	@FindBy(id = "siteAddress2")
	public WebElement siteAddress2;

	@FindBy(id = "siteCity")
	public WebElement siteCity;

	@FindBy(id = "siteCountry")
	public WebElement siteCountry;

	@FindBy(id = "siteState")
	public WebElement siteState;

	@FindBy(id = "buildingSize")
	public WebElement buildingSize;

	@FindBy(id = "buildingDimentionsX")
	public WebElement buildingDimentionsX;

	@FindBy(id = "buildingDimentionsY")
	public WebElement buildingDimentionsY;

	@FindBy(id = "buildingDimentionsZ")
	public WebElement buildingDimentionsZ;

	@FindBy(id = "parkingSpaces")
	public WebElement parkingSpaces;

	@FindBy(id = "dealType")
	public WebElement dealType;

	@FindBy(id = "loiSent")
	public WebElement loiSent;

	@FindBy(id = "loiSigned")
	public WebElement loiSigned;

	@FindBy(id = "approvalDate")
	public WebElement approvalDate;

	@FindBy(id = "leaseCommencement")
	public WebElement leaseCommencement;

	@FindBy(id = "leaseExpiration")
	public WebElement leaseExpiration;

	@FindBy(id = "initialTerm")
	public WebElement initialTerm;

	@FindBy(id = "optionTerm")
	public WebElement optionTerm;

	@FindBy(xpath = ".//input[@id='purchaseOption' and @value='Y']")
	public WebElement purchaseOptionYes;
	@FindBy(xpath = ".//input[@id='purchaseOption' and @value='N']")
	public WebElement purchaseOptionNo;

	@FindBy(id = "projectedOpeningDate")
	public WebElement projectedOpeningDate;

	@FindBy(xpath = ".//input[@id='generalContractorSelector' and @value='Y']")
	public WebElement generalContractorSelectedYes;
	@FindBy(xpath = ".//input[@id='generalContractorSelector' and @value='N']")
	public WebElement generalContractorSelectedNo;

	@FindBy(id = "nameGeneralContractor")
	public WebElement nameGeneralContractor;

	@FindBy(id = "addressGeneralContractor")
	public WebElement addressGeneralContractor;

	@FindBy(id = "permitApplied")
	public WebElement permitApplied;

	@FindBy(id = "permitIssued")
	public WebElement permitIssued;

	@FindBy(id = "certificate")
	public WebElement certificate;

	@FindBy(id = "turnOverDate")
	public WebElement turnOverDate;

	@FindBy(id = "grandOpeningDate")
	public WebElement grandOpeningDate;

	/*
	 * Buttons
	 */

	@FindBy(xpath = ".//input[@name='button' and @value='Save']")
	public WebElement saveBtn ;

	@FindBy(name = "printButton")
	public WebElement printRealEstateBtn;

	@FindBy(linkText = "Modify")
	public WebElement modifyRealEstateBtn;

	public RealEstateUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
