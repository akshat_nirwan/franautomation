package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.builds.uimaps.common.CommonUI;

public class RemarkUI extends CommonUI {

	@FindBy(id = "remarks")
	public WebElement remarks;

	public RemarkUI(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

}
