package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ReportsUI {
	
	@FindBy(xpath=".//*[.='Lead Status Report']")
	public WebElement  leadStatusReport;
	
public ReportsUI(WebDriver driver) {
PageFactory.initElements(driver, this);
}

}
