package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SalesCampaignCenterPage {

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody//div[@class='list-name']")
	public WebElement createBtn ;

	@FindBy(xpath = ".//*[@id='CreateCampaignLink']/a[contains(text(),'Campaign')]")
	public WebElement campaignLnkLeft ;

	@FindBy(xpath = ".//*[@id='CreateCampaignLink']/a[contains(text(),'Template')]")
	public WebElement templateLnkLeft ;

	@FindBy(xpath = ".//*[@id='CreateCampaignLink']/a[contains(text(),'Recipients Group')]")
	public WebElement recipientsGroupLnkLeft ;

	public SalesCampaignCenterPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
