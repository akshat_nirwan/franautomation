package com.builds.uimaps.fs;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SalesTerritoriesUI {

	@FindBy(id = "category")
	public WebElement category;

	@FindBy(name = "regioname")
	public WebElement salesTerritoryName;

	@FindBy(name = "groupBy")
	public WebElement groupBy;

	// CHECKBOX
	public WebElement selectCountry(WebDriver driver , String country) {
		List<WebElement> listElement = driver.findElements(By.xpath(".//input[@name='stateId']/parent::*"));
		WebElement country_checkbox = null;

		for (WebElement webElement : listElement) {

			if (webElement.getText().trim().equalsIgnoreCase(country)) {
				country_checkbox = webElement.findElement(By.xpath("./input[@name='stateId']"));
			}
		}

		return country_checkbox;
	}
	
	// CHECKBOX : Akshat
		public WebElement selectState(WebDriver driver , String country) {
			List<WebElement> listElement = driver.findElements(By.xpath(".//input[@name='stateId']/parent::*"));
			WebElement country_checkbox = null;

			for (WebElement webElement : listElement) {

				if (webElement.getText().trim().equalsIgnoreCase(country)) {
					country_checkbox = webElement.findElement(By.xpath("./input[@name='stateId']"));
				}
			}

			return country_checkbox;
		}
		
		// CHECKBOX : Akshat
	public WebElement selectCounty(WebDriver driver, String county) {
		List<WebElement> listElement = driver.findElements(By.xpath(".//input[@name='countyId']/parent::*"));
		WebElement country_checkbox = null;

		for (WebElement webElement : listElement) {

			if (webElement.getText().trim().equalsIgnoreCase(county)) {
				country_checkbox = webElement.findElement(By.xpath("./input[@name='countyId']"));
			}
		}

		return country_checkbox;
	}
	
	// Akshat Checkbox
	public WebElement select_Country_GroupByCounty(WebDriver driver , String country) {
		List<WebElement> listElement = driver.findElements(By.xpath(".//input[@name='country']/parent::*"));
		WebElement country_checkbox = null;

		for (WebElement webElement : listElement) {

			if (webElement.getText().trim().equalsIgnoreCase(country)) {
				country_checkbox = webElement.findElement(By.xpath("./input[@name='country']"));
			}
		}

		return country_checkbox;
	}

	@FindBy(name = "country")
	public WebElement countryUSA;
	
	// Buttons
	
	@FindBy(xpath=".//input[@class='cm_new_button_link' and @type='button']")
	public WebElement addNewSalesTerritory_Btn;
	
	@FindBy(xpath=".//input[@type='radio' and @value='1']")
	public WebElement zipPostalCodeManually_radioBtn; 
	
	@FindBy(xpath=".//input[@type='radio' and @value='2']")
	public WebElement uploadCsvFile_radioBtn; 
	
	@FindBy(xpath=".//table[@id='ziplist1']//textarea[@name='ziplist']")
	public WebElement zipPostalCodeManually_textBox;
	
	@FindBy(name="zipfile")
	public WebElement csvFileToUpload;
	
	@FindBy(xpath=".//input[@type='button' and @value='Submit']")
	public WebElement submitBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Back']")
	public WebElement backBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value=' Next ']")
	public WebElement NextBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Next']")
	public WebElement NextBtn2;
	
	// .//*[contains(.,"Oklahoma")]/input[@type='checkbox']

	public SalesTerritoriesUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
