package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SalesUI {

	@FindBy(xpath = ".//a[@qat_submodule='Home']")
	public WebElement salesHomePageLnk;

	@FindBy(xpath = ".//a[@qat_submodule='Lead Management']")
	public WebElement leadManagementLnk;

	@FindBy(xpath = ".//a[@qat_submodule='Groups']")
	public WebElement groupsPageLnk;

	@FindBy(xpath = ".//a[@qat_submodule='Search' and @original-title='Search Lead Data Subject to Defined Parameter']")
	public WebElement search;

	@FindBy(xpath = ".//a[@qat_submodule='Campaign Center']")
	public WebElement campaign;

	@FindBy(xpath = ".//a[@qat_submodule='FDD']")
	public WebElement FDD;

	@FindBy(xpath = ".//a[@qat_submodule='Tasks']")
	public WebElement Tasks;

	@FindBy(xpath = ".//a[@qat_submodule='Calendar']")
	public WebElement Calendar;

	@FindBy(xpath = ".//a[@qat_submodule='Import']")
	public WebElement Import;

	@FindBy(xpath = ".//a[@qat_submodule='Export']")
	public WebElement Export;

	@FindBy(xpath = ".//a[@qat_submodule='Mail Merge']")
	public WebElement MailMerge;

	@FindBy(xpath = ".//a[@qat_submodule='Sites']")
	public WebElement Sites;

	@FindBy(xpath = ".//a[@qat_submodule='Brokers']")
	public WebElement Brokers;

	@FindBy(xpath = ".//a[@qat_submodule='Reports']")
	public WebElement Reports;

	@FindBy(xpath = ".//a[@qat_submodule='Dashboard']")
	public WebElement Dashboard;

	// Anukaran
	@FindBy(xpath = ".//a[@qat_submodule='Workflows']")
	public WebElement Workflows;

	@FindBy(id = "searchString")
	public WebElement topSearchRight_TextBox;

	@FindBy(xpath = ".//img[@class='topSearch']")
	public WebElement searchImg;

	@FindBy(xpath = ".//a[@href='fscampaignSummary']")
	public WebElement fscampaignSummaryLnk ;
	@FindBy(xpath = ".//a[@href='fsArchivedCampaignSummary']")
	public WebElement fsArchivedCampaignSummaryLnk ;
	@FindBy(xpath = ".//a[@href='fstemplateSummary']")
	public WebElement fstemplateSummaryLnk ;
	@FindBy(xpath = ".//a[@href='fsInfomercials']")
	public WebElement fsInfomercialsLnk ;
	@FindBy(xpath = ".//a[@href='fsCampaignTriggers']")
	public WebElement fsCampaignTriggersLnk ;
	@FindBy(xpath = ".//a[@href='fsManageMailTemplateFolder?tempStatusType=Public']")
	public WebElement templateFolderLnk ;
	@FindBy(xpath = ".//a[@href='messageSignature?fromWhere=fs']")
	public WebElement modifySignatureLnk ;

	public SalesUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
