package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchUI {

	@FindBy(xpath = ".//input[@name='archivedleads' and @value='yes']")
	public WebElement archivedleadRadio;

	@FindBy(xpath = ".//*[@href='advanceSearch']")
	public WebElement advancesearch ;
	@FindBy(name = "sortedBy1")
	public WebElement sortedBy1;
	@FindBy(name = "resultsPerPage")
	public WebElement resultsPerPage;
	@FindBy(id = "requestDateFrom")
	public WebElement requestDateFrom;
	@FindBy(id = "requestDateTo")
	public WebElement requestDateTo;
	@FindBy(id = "firstName")
	public WebElement firstName;
	@FindBy(id = "lastName")
	public WebElement lastName;
	@FindBy(id = "address")
	public WebElement address;
	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "ms-parentcountry")
	public WebElement countryListBox;

	@FindBy(id = "zip")
	public WebElement zip;
	@FindBy(id = "ms-parentstateID")
	public WebElement stateListBox;
	@FindBy(id = "ms-parentcountyID")
	public WebElement countyListBox;

	@FindBy(id = "phone")
	public WebElement phone;
	@FindBy(id = "emailID")
	public WebElement emailID;

	@FindBy(id = "ms-parentmultpileStatusFrom")
	public WebElement changeStatusFrom;
	@FindBy(id = "ms-parentmultpileStatus")
	public WebElement changeStatusTo;

	@FindBy(id = "fromDate")
	public WebElement statusChangeDateFrom;
	@FindBy(id = "dateStatusChanged")
	public WebElement statusChangeDateTo;

	@FindBy(id = "ms-parentmultpileOwners")
	public WebElement leadOwnerDrp;

	@FindBy(id = "ms-parentleadRatingID")
	public WebElement leadRatingID;
	@FindBy(id = "ms-parentleadSource2ID")
	public WebElement sourceCategory;
	@FindBy(id = "ms-parentleadSource3ID")
	public WebElement sourceDetails;
	@FindBy(id = "nextCallDateFrom")
	public WebElement nextCallDateFrom;
	@FindBy(id = "nextCallDateTo")
	public WebElement nextCallDateTo;
	@FindBy(id = "webSiteLead")
	public WebElement webSiteLead;
	@FindBy(id = "groupId")
	public WebElement groupId;
	@FindBy(id = "captivateVisited")
	public WebElement captivateVisited;
	@FindBy(id = "preferredCity1")
	public WebElement preferredCity1;
	@FindBy(xpath = ".//*[@id='ms-parentpreferredCountry1']/button")
	public WebElement preferredCountry1 ;
	
	// akshat
	@FindBy(id="ms-parentpreferredCountry1")
	public WebElement preferredCountry;
	
	@FindBy(xpath = ".//*[@id='ms-parentpreferredStateId1']/button")
	public WebElement preferredStateId1 ;
	@FindBy(id = "locationId1")
	public WebElement locationId1;
	@FindBy(id = "campaignID")
	public WebElement campaignID;
	@FindBy(id = "LeadsWhoHave")
	public WebElement leadsWhoHave;
	@FindBy(id = "templateID")
	public WebElement templateID;
	@FindBy(id = "Submit")
	public WebElement submit;

	@FindBy(xpath = ".//form[@name='searchLeadInfoForm']//input[@value='Search']")
	public WebElement search;

	@FindBy(xpath = ".//*[@id='ms-parentgroupId']/button")
	public WebElement searchGrpDrpDwn ;

	@FindBy(xpath = ".//*[@id='ms-parentFS_LEAD_MAPPING19_groupID']")
	public WebElement advSearchGrpDrpDwn ;

	@FindBy(xpath = ".//*[@id='ms-parentpreferredCountry1']/div/div/input")
	public WebElement searchMultiple ;

	@FindBy(xpath = ".//form[@id='locationSearchInfoForm']//*[@id='Submit']")
	public WebElement locationbasedSearch ;

	@FindBy(xpath = ".//*[@id='ms-parentmultpileOwners']/div/div/input")
	public WebElement leadOwnerDrpInput ;

	@FindBy(xpath = ".//input[@name='selTables' and @value='fsCampaignEmailLog']")
	public WebElement activityHistoryCampaignEmails ;

	@FindBy(xpath = ".//input[@name='selTables' and @value='fsLeadEmail']")
	public WebElement activityHistoryEmails ;

	@FindBy(xpath = ".//input[@value='Search Data']")
	public WebElement searchData ;

	@FindBy(xpath = ".//input[@value='View Data']")
	public WebElement viewData ;

	@FindBy(name = "FS_LEAD_DETAILS1_firstName")
	public WebElement fName;

	@FindBy(name = "FS_LEAD_DETAILS1_lastName")
	public WebElement lName;

	@FindBy(xpath = ".//*[@id='leadNames']/a[contains(text () , 'Activity History')]")
	public WebElement activityHistory ;

	@FindBy(xpath = ".//*[@id='ms-parentuserCombo']")
	public WebElement selectLeadOwner ;

	@FindBy(id = "searchTermKey")
	public WebElement topSearchField;

	@FindBy(xpath = ".//*[@id='sales']//*[.='Sales']")
	public WebElement moduleClick ;

	@FindBy(xpath = ".//a[contains(text () , 'View Details')]")
	public WebElement viewDetails ;

	@FindBy(xpath = ".//a[@ng-click='viewAll()']")
	public WebElement viewAllBtn;

	@FindBy(xpath = ".//a[contains(text(),'View All')]")
	public WebElement viewAllBtn2 ;

	public SearchUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
