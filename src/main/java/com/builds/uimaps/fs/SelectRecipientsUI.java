package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SelectRecipientsUI {

	@FindBy(xpath = ".//button[.='Continue']")
	public WebElement continueButton;

	@FindBy(xpath = ".//button[.='Associate Later']")
	public WebElement associateLaterButton;

	public SelectRecipientsUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
