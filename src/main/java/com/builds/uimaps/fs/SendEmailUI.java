package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SendEmailUI {

	
	@FindBy(xpath = ".//input[@name='radioCheck' and @value='1']")
	public WebElement Lead_Owner_ID_Radio;

	
	@FindBy(xpath = ".//input[@name='radioCheck' and @value='0']")
	public WebElement Logged_User_ID_Radio;

	
	@FindBy(xpath = ".//input[@name='radioCheck' and @value='2']")
	public WebElement Custom_Radio;

	
	@FindBy(name = "mailTemplateID")
	public WebElement mailTemplateSelect;

	
	@FindBy(name = "customID")
	public WebElement customID;

	
	@FindBy(name = "mailSentTo")
	public WebElement mailSentTo;

	
	@FindBy(name = "subject")
	public WebElement subject;

	public String frame_forbody = "ta_ifr";

	
	@FindBy(id = "tinymce")
	public WebElement body;

	
	@FindBy(xpath = ".//*[@id='sendButton1']/div/img")
	public WebElement sendEmail_Top_Button;

	
	@FindBy(id = "sendButton2")
	public WebElement sendEmail_Bottom_Button;

	
	@FindBy(linkText = "Address Book")
	public WebElement addressBook_Link;

	
	@FindBy(id = "showBcc")
	public WebElement showBcc_checkbox;

	public SendEmailUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
