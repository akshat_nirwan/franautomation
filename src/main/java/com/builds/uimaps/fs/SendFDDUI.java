package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SendFDDUI {
	
	@FindBy(xpath=".//input[@value='0' and @name='radioCheck']")
	public WebElement LoggedUserID_RadioBtn;
	
	@FindBy(xpath=".//input[@value='1' and @name='radioCheck']")
	public WebElement LeadOwnerID_RadioBtn;
	
	@FindBy(xpath=".//input[@value='2' and @name='radioCheck']")
	public WebElement custom_RadioBtn;
	
	@FindBy(id="mailTemplateID")
	public WebElement  fddEmailTemplates;
	
	@FindBy(xpath=".//input[@name='fddToBeSent' and @value='0']")
	public WebElement FDDRegisteredWithLead_RadioBtn;
	
	@FindBy(xpath=".//input[@name='fddToBeSent' and @value='1']")
	public WebElement alternateDivisionState_RadioBtn;
	
	@FindBy(xpath=".//input[@name='fddToBeSent' and @value='2']")
	public WebElement  letMeSelect_RadioBtn;
	
	@FindBy(id="sellerNamesAdded")
	public WebElement defaultSeller;
	
	@FindBy(id="ms-parentseller")
	public WebElement selectSellers;
	
	@FindBy(id="temp")
	public WebElement toEmail;
	
	@FindBy(id="fddCombo")
	public WebElement selectFDD;
	
	@FindBy(name="subject")
	public WebElement subject;
	
	@FindBy(linkText="Attachment")
	public WebElement attachment;
	
	@FindBy(id="fddAdditionalContact")
	public WebElement sendFDDtoCoApplicants_checkBox;
	
	@FindBy(id="tinymce")
	public WebElement emailBody;
	
	@FindBy(xpath=".//input[@value='Send' and @class='cm_new_button']")
	public WebElement sendBtn;
	
	@FindBy(xpath=".//input[@value='Reset' and @class='cm_new_button']")
	public WebElement resetBtn;
	
	@FindBy(xpath=".//input[@value='Cancel' and @class='cm_new_button']")
	public WebElement cancelBtn;
	
	@FindBy(id="asave")
	public WebElement confirmBtn;
	
	@FindBy(xpath=".//p[contains(text(),'Total No. of Leads to whom selected FDD will be sent')]/ancestor::tr[1]/td[2]//a")
	public WebElement noOfLeadsWhomFDDwillBeSent;
	
	@FindBy(name="customID")
	public WebElement customText_fromReply;
	
	@FindBy(id="country")
	public WebElement Country_AlternateState;
	
	@FindBy(id="stateID")
	public WebElement State_AlternateState;
	
	@FindBy(xpath=".//tbody/tr[3]/td/input[@value='Close' and @type='button' and @class='cm_new_button']")
	public WebElement closeBtn_EmailInformation;
		
	public SendFDDUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
