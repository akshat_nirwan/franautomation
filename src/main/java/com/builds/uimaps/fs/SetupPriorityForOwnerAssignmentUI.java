package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SetupPriorityForOwnerAssignmentUI {
	
	public SetupPriorityForOwnerAssignmentUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath=".//select[@class='multiList']")
	public WebElement select_byClass;
	
	@FindBy(xpath=".//*[@title='Move Up']")
	public WebElement upBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Save']")
	public WebElement saveBtn;
	
	@FindBy(xpath=".//input[@type='button' and @value='Cancel']")
	public WebElement cancelBtn;
	
	

}
