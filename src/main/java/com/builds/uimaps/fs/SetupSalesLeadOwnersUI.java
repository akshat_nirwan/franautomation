package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SetupSalesLeadOwnersUI {

	@FindBy(id="allUsers")
	public WebElement allusersasSalesLeadOwners_Radio;
	
	@FindBy(id="selectedUsersForFS")
	public WebElement selectedusersasSalesLeadOwners_Radio;
	
	public SetupSalesLeadOwnersUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
}
