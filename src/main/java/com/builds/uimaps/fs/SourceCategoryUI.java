package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.builds.uimaps.common.CommonUI;

public class SourceCategoryUI extends CommonUI {

	@FindBy(xpath = ".//input[@value = 'Add Lead Source Category']")
	public WebElement addLeadSourceCategory;

	public String getXpathofSourceDetailsLink(String categoryName) {
		return ".//td[contains(text(),'" + categoryName
				+ "')]//ancestor::tr[1]//*[contains(text(),'Lead Source Details')]";
	}

	public SourceCategoryUI(WebDriver driver) {
		super(driver);
	}

}
