package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.builds.uimaps.common.CommonUI;

public class StatusUI extends CommonUI {

	@FindBy(id = "leadStatusID")
	public WebElement leadStatusID_Select_ByID;

	@FindBy(name = "change_status")
	public WebElement change_status_button;

	@FindBy(id = "noOfFieldSold")
	public WebElement noOflocations;

	@FindBy(id = "dateOfOpen")
	public WebElement dateToOpen;

	@FindBy(id = "remarks")
	public WebElement remarks_TextArea_ByID;

	@FindBy(id = "statusChangeDate")
	public WebElement FranchiseAwardedDate;

	public StatusUI(WebDriver driver) {
		super(driver);
	}

}
