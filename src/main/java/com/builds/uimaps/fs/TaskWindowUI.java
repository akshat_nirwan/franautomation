package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.builds.uimaps.common.CommonUI;

public class TaskWindowUI extends CommonUI {

	@FindBy(xpath = ".//input[@name='assignToRadio' and @value='0']")
	public WebElement leadOwnerRadio;

	@FindBy(xpath = ".//input[@name='assignToRadio' and @value='1']")
	public WebElement OtherUserRadio;

	@FindBy(id = "ms-parentassignTo")
	public WebElement OtherUserDropDown_MultiSelect_ByID;

	@FindBy(id = "status")
	public WebElement Status_Select;

	@FindBy(id = "taskType")
	public WebElement TaskType_Select;

	@FindBy(id = "subject")
	public WebElement Subject;

	@FindBy(name = "timelessTaskId")
	public WebElement TimelessTaskId_CheckBox;

	@FindBy(id = "priority")
	public WebElement Priority_Select;

	@FindBy(name = "startDate")
	public WebElement startDate_Date;

	@FindBy(name = "calendarTaskCheckBox")
	public WebElement calendarTaskCheckBox;

	@FindBy(name = "sTime")
	public WebElement startTimeHH;

	@FindBy(name = "sMinute")
	public WebElement startTimeMM;

	@FindBy(name = "APM")
	public WebElement startTimeAMPM;

	@FindBy(name = "eTime")
	public WebElement endTimeHH;

	@FindBy(name = "eMinute")
	public WebElement endTimeMM;

	@FindBy(name = "MPM")
	public WebElement endTimeAMPM;

	@FindBy(id = "comments")
	public WebElement taskDescription;

	@FindBy(xpath = ".//input[@name='schduleTime' and @value='0']")
	public WebElement noReminderRadio;

	@FindBy(xpath = ".//input[@name='schduleTime' and @value='1']")
	public WebElement FifteenMinutesPriorRadio;

	@FindBy(xpath = ".//input[@name='schduleTime' and @value='2']")
	public WebElement SetTimeYourselfRadio;

	@FindBy(name = "taskEmailReminder_checkbox")
	public WebElement taskEmailReminder_checkbox;

	@FindBy(id = "REMINDER_DATE")
	public WebElement reminderDate;

	@FindBy(name = "rTime")
	public WebElement rTimeHH;

	@FindBy(name = "rMinute")
	public WebElement rTimeMM;

	@FindBy(name = "RPM")
	public WebElement rTimeAMPM;

	@FindBy(name = "another")
	public WebElement CreateandCreateAnother;

	public TaskWindowUI(WebDriver driver) {
		super(driver);
	}
}
