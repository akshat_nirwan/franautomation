package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TemplatePageUI {

	@FindBy(id = "createTemplate")
	public WebElement createTemplateButton;

	public TemplatePageUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
