package com.builds.uimaps.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TemplateUI {

	@FindBy(name = "mailTitle")
	public WebElement mailTitle;

	@FindBy(name = "mailSubject")
	public WebElement mailSubject;

	@FindBy(xpath = ".//label[@for='templateStatusType1']")
	public WebElement public_Accessibility;

	@FindBy(xpath = ".//label[@for='templateStatusType2']")
	public WebElement private_Accessibility;

	@FindBy(xpath = ".//label[@for='typeRadio1']")
	public WebElement graphicalTemplate;

	@FindBy(xpath = ".//label[@for='typeRadio2']")
	public WebElement plainTextTemplate;

	@FindBy(xpath = ".//label[@for='typeRadio3']")
	public WebElement uploadHTML_ZIPTemplate;

	@FindBy(xpath = ".//div[@id='fc-drop-parentfolderNo']")
	public WebElement folderName_RadioSelect;

	@FindBy(id = "ta_ifr")
	public WebElement emailBody_Frame_ByID;
	
	@FindBy(id="ta")
	public WebElement emailBody_TextArea;

	@FindBy(id = "tinymce")
	public WebElement emailBody;

	@FindBy(xpath = ".//*[@data-role='ico_AddCircleOutline']")
	public WebElement attachment_Icon;

	@FindBy(id = "captivateTemplateID1")
	public WebElement SetAsEmailType_Select;

	public TemplateUI(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
