package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminConfigurartionConfigureAreaRegionLabelPage {

	public AdminConfigurartionConfigureAreaRegionLabelPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = ".//*[@value='Modify'][@type='button']")
	public WebElement btnModify ;

	@FindBy(name = "areaRegionLabel")
	public WebElement txtAreaRegion;

	@FindBy(xpath = ".//*[@value='Save'][@type='button']")
	public WebElement btnSave ;

}
