package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminInfoMgrConfigureInfoMgrTabsPage {

	public AdminInfoMgrConfigureInfoMgrTabsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "check")
	public WebElement chkSelectAll;

	@FindBy(xpath = ".//*[@value='Save']")
	public WebElement btnSave ;

}
