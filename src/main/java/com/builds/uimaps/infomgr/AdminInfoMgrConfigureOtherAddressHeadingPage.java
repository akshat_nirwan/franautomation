package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminInfoMgrConfigureOtherAddressHeadingPage {

	@FindBy(xpath = ".//input[@value='Add Address Heading']")
	public WebElement addAddressHeading ;

	@FindBy(name = "addressHeadingName")
	public WebElement addressHeadingName;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement add ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement close ;

	@FindBy(name = "templateOrder")
	public WebElement listing;

	public AdminInfoMgrConfigureOtherAddressHeadingPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
