package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminInfoMgrConfigureOwnershipTransferStatusPage {

	@FindBy(xpath = ".//input[@value='Add Ownership Transfer Status']")
	public WebElement addOwnershipTransferStatus ;

	@FindBy(name = "transferStatusName")
	public WebElement transferStatusName;

	/*
	 * @FindBy(name="Add") public WebElement addBtn;
	 */

	@FindBy(xpath = ".//input[@name='Add' or @name='Ajouter']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(name = "templateOrder")
	public WebElement listing;

	public AdminInfoMgrConfigureOwnershipTransferStatusPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
