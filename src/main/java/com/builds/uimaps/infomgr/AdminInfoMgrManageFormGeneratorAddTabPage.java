package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminInfoMgrManageFormGeneratorAddTabPage {

	public AdminInfoMgrManageFormGeneratorAddTabPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "tabDisplayName")
	public WebElement txtDisplayName;

	@FindBy(id = "submoduleName")
	public WebElement drpSubModuleName;

	@FindBy(id = "submitButton")
	public WebElement btnSubmit;

	@FindBy(xpath = ".//input[@value='false' and @name='addMore']")
	public WebElement addMoreFalse ;
	
	@FindBy(xpath = ".//*[@id='containerDiv']//input[@name='addMore' and @value='false']")
	public WebElement multipleInputNo ;

}
