package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminInfoMgrManageFormGeneratorConfigureTabularViewPage {

	public AdminInfoMgrManageFormGeneratorConfigureTabularViewPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "availableField")
	public WebElement lstFieldsTabularView;

	@FindBy(xpath = ".//*[contains(text() , 'Available Fields')]/following ::table[1]//tr[1]/td")
	public WebElement imgAddFields ;

	@FindBy(name = "button")
	public WebElement btnSave;

	@FindBy(id = "selectedField")
	public WebElement lstSelectedFields;

	@FindBy(xpath = ".//*[contains(text() , 'Available Fields')]/following ::table[1]//tr[2]/td")
	public WebElement imgRemoveFields ;

}
