package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminInfoMgrManageFormGeneratorModifyFieldsPositionPage {

	public AdminInfoMgrManageFormGeneratorModifyFieldsPositionPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "0")
	public WebElement tabAgreement;

	@FindBy(id = "1")
	public WebElement tabDateExecuted;

	@FindBy(name = "previewButton")
	public WebElement btnPreview;

}
