package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminInfoMgrManageFormGeneratorPage {

	WebDriver driver;

	public AdminInfoMgrManageFormGeneratorPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	@FindBy(xpath = ".//*[@id='instructiondetail']//input[@value='Continue'][@type='button']")
	public WebElement btnContinue ;

	@FindBy(xpath = "(.//*[@id='siteMainTable']//*[contains(text(), 'Comments')]/Following::img)[2]")
	public WebElement activeImageButton ;

	@FindBy(id = "tabularFieldlink")
	public WebElement btnConfgTabularView;

	@FindBy(id = "formNames")
	public WebElement drpFormNames;

	@FindBy(id = "addsectionlink")
	public WebElement btnAddNewTab;

	@FindBy(linkText = "Multi-Unit / Entity")
	public WebElement lnkMultiUnitEntity ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//table//table//table//table//tr//td[6]/a/img[@title='Click to Activate this Field']")
	public List<WebElement> lnkAllActive;

	@FindBy(xpath = ".//*[@id='siteMainTable']//td[contains(text(), 'State / Province')]/ancestor::tr[1]//img[@title='Click to Activate this Field']")
	public List<WebElement> lnkDeactiveStates;

	@FindBy(xpath = ".//*[@id='siteMainTable']//td[contains(text(), 'Country')][not(contains(text(), '/Country'))]/ancestor::tr[1]//img[@title='Click to Activate this Field']")
	public List<WebElement> lnkDeactiveCountries;

	// Anukaran
	@FindBy(xpath = ".//*[contains[text(),'LeadInfo']]")
	public WebElement LeadInfo ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//td[contains(text(),'Birthdate')]/following-sibling::td/following-sibling::td/following-sibling::td/a/img[@title='Click to De-Activate this Field']")
	public WebElement birthdateActiavte ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//td[contains(text(),'Anniversary Date')]/following-sibling::td/following-sibling::td/following-sibling::td/a/img[@title='Click to De-Activate this Field']")
	public WebElement anniversaryActiavte ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//td[contains(text(),'Sub Status')]/following-sibling::td/following-sibling::td/following-sibling::td/a/img[@title='Click to De-Activate this Field']")
	public WebElement subStatus ;

	public AdminInfoMgrManageFormGeneratorAddTabPage getAddTabPage() {
		return new AdminInfoMgrManageFormGeneratorAddTabPage(driver);
	}

	public AdminInfoMgrManageFormGeneratorTabDetailsPage getAddTabDetailsPage() {
		return new AdminInfoMgrManageFormGeneratorTabDetailsPage(driver);
	}

	public AdminInfoMgrManageFormGeneratorConfigureTabularViewPage getConfigureTabularViewPage() {
		return new AdminInfoMgrManageFormGeneratorConfigureTabularViewPage(driver);
	}

	public AdminInfoMgrManageFormGeneratorModifyFieldsPositionPage getModifyFieldsPositionPage() {
		return new AdminInfoMgrManageFormGeneratorModifyFieldsPositionPage(driver);
	}

}
