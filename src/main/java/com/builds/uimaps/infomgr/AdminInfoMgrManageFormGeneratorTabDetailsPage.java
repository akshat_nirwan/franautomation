package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminInfoMgrManageFormGeneratorTabDetailsPage {

	public AdminInfoMgrManageFormGeneratorTabDetailsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "addsectionlink")
	public WebElement btnAddSection;

	@FindBy(name = "sectionValue")
	public WebElement txtSectionName;

	@FindBy(id = "submitButton")
	public WebElement btnAddSectionName;

	@FindBy(name = "displayName")
	public WebElement txtFieldName;

	@FindBy(name = "dbColumnType")
	public WebElement drpFieldType;
	@FindBy(name = "dependentField")
	public WebElement dependentField;
	
	@FindBy(name = "displayName1")
	public WebElement state;
	
	@FindBy(name = "noOfCols")
	public WebElement noOfCols;
	
	
	@FindBy(name = "noOfRows")
	public WebElement noOfRows;
	
	@FindBy(xpath = ".//*[@id='row_1']/td[2]/input[1]")
	public WebElement option1 ;

	@FindBy(xpath = ".//*[@id='row_2']/td[2]/input[1]")
	public WebElement option2 ;
	
	
	
	@FindBy(xpath = ".//div[@id='dropDownOption']//label[1]")
	public WebElement dropDownOption1;
	
	@FindBy(xpath = ".//div[@id='dropDownOption']//label[2]")
	public WebElement dropDownOption2;
	
	@FindBy(xpath = ".//div[@id='dropDownOption']//label[3]")
	public WebElement dropDownOption3;
	
	@FindBy(xpath = ".//div[@id='dropDownOption']//label[4]")
	public WebElement dropDownOption4;
	
	@FindBy(xpath = ".//div[@id='optionView']//following::td[2]/input[2]")
	public WebElement optionViewHor;
	@FindBy(name = "optviewtype")
	public WebElement optviewtype;
	
	@FindBy(xpath = ".//select[@name='fldValidationType1']")
	public WebElement fldValidationType1;
	
	@FindBy(xpath = ".//select[@id='dependentField'")
	public WebElement dependent;
	@FindBy(xpath = ".//div[@id='dropDownOption']//label[4]//input")
	public WebElement dep;
	
	@FindBy(xpath = ".//*[@id='removeTDForAddMore']")
	public WebElement addOption ;
	
	@FindBy(xpath = ".//*[@name='fldValidationType']")
	public WebElement fldValidationType ;
	
	@FindBy(id = "submitButton")
	public WebElement btnSumbitField;

	/*@FindBy(linkText = "Add New Field")
	public WebElement btnAddField ;
*/
	@FindBy(xpath = "//*[contains(text(),'Add New Field')]")
	public WebElement btnAddField ;
	
	@FindBy(xpath = ".//*[@name='isTabularSection'][2]")
	public WebElement radioTabular ;

	@FindBy(xpath = ".//*[@id='builderForm']//select//option[@value='file']")
	public WebElement documentopt ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='builderForm']//select[@name='dbColumnType']")

	public WebElement FieldType ;
	// Anukaran
	@FindBy(xpath = ".//*[@id='containerDiv']//input[@name='isTabularSection' and @value='yes']")
	public WebElement tabularSection ;

}
