package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminInfoMgrManageWebFormGeneratorPage {

	public AdminInfoMgrManageWebFormGeneratorPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = ".//*[@value='Create New Form'][@type='button']")
	public WebElement btnCreateNewForm ;

	@FindBy(id = "formName")
	public WebElement txtFormName;

	@FindBy(id = "formDisplayTitle")
	public WebElement txtDisplayTitle;

	@FindBy(id = "displayFormTitleCheckBox")
	public WebElement chkDisplayFormTitle;

	@FindBy(id = "formDescription")
	public WebElement txtFormDesc;

	@FindBy(id = "formFormat")
	public WebElement drpFormFormat;

	@FindBy(id = "columnCount")
	public WebElement drpColumnCount;

	@FindBy(id = "filedLabelAlignment")
	public WebElement drpFieldLabelAlignment;

	@FindBy(id = "formLanguage")
	public WebElement drpFormLanguage;

	@FindBy(id = "iframeWidth")
	public WebElement txtFrameWidth;

	@FindBy(id = "iframeHeight")
	public WebElement txtFrameHeight;

	@FindBy(id = "formUrl")
	public WebElement txtFormURL;

	@FindBy(id = "detailsNextBtn")
	public WebElement btnSaveandText;

	@FindBy(id = "page_header")
	public WebElement divPageHeader;

	@FindBy(xpath = ".//*[@id='header_fld']/a")
	public WebElement txtTool ;

	@FindBy(xpath = ".//*[@id='recaptcha_fld']/a")
	public WebElement capchaTool ;

	@FindBy(xpath = ".//*[@id='ui-accordion-subModules-panel-1']/div[1]/input")
	public WebElement txtCenterInfoSearchBox ;

	@FindBy(xpath = ".//*[contains(@id , 'defaultSection')]//p[text() ='Drop here.']")
	public WebElement mainSection ;

	@FindBy(id = "designNextBtn")
	public WebElement btnNext;

	@FindBy(id = "settingsNextBtn")
	public WebElement btnFinish;

	@FindBy(id = "hostCodeBox")
	public WebElement txtHostURL;

	@FindBy(id = "iframeCode")
	public WebElement rdoIFrameEmbedCode;

	@FindBy(id = "hostURL")
	public WebElement rdoHostURL;

	@FindBy(id = "confirmationNextBtn")
	public WebElement btnOk;

	@FindBy(xpath = ".//*[contains(@id ,'defaultSection') and contains(@id ,'Flds')]/li[@class='endEmptyList']")
	public WebElement defaultDropHere ;

	@FindBy(id = "designNextBtn")
	public WebElement saveAndNextBtnDesign;

}
