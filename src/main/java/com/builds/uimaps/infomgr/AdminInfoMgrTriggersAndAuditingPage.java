package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminInfoMgrTriggersAndAuditingPage {

	@FindBy(xpath = ".//input[@type='button' and @value='Actions']")
	public WebElement actionsMenu;

	@FindBy(name = "mailSubject")
	public WebElement mailSubject;

	@FindBy(xpath = ".//*[@id='ms-parentadditionalFields']/button")
	public WebElement emailField;

	@FindBy(xpath = ".//input[@class='searchInputMultiple' and @type='text']")
	public WebElement searchInputMultiple;

	@FindBy(id = "selectAll")
	public WebElement selectAll;

	@FindBy(xpath = ".//iframe[@class='cke_wysiwyg_frame cke_reset' and @title='Rich Text Editor, alertMessage']")
	public WebElement ckEditorFrame;

	@FindBy(xpath = "html/body")
	public WebElement ckEditor;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td")
	public WebElement fieldLevelTriggersAlertEmail;

	@FindBy(xpath = ".//input[@name='Submit' and @value='Save']")
	public WebElement saveBtn;

	@FindBy(xpath = ".//input[@type='checkbox' and @name='auditForm']")
	public WebElement auditForm;
	
	@FindBy(xpath = ".//input[@type='checkbox' and @name='triggerForm']")
	public WebElement triggerForm;

	@FindBy(xpath = ".//div[@id='ms-parenttriggerEvent_0userNo' and @class='ms-parent']")
	public WebElement msParent;

	@FindBy(xpath = ".//table[@id='siteMainTable']//tr//td[4]//a[1]//img[1]")
	public WebElement tickTrigger;

	@FindBy(xpath = ".//textarea[@name='triggerEvent_0emailID']")
	public WebElement recipient;

	@FindBy(xpath = ".//tr//td[5]//a[1]//img[1]")
	public WebElement tickRecepient;

	@FindBy(xpath = ".//tr//td[6]//a[1]//img[1]")
	public WebElement tickOnevent;

	@FindBy(xpath = ".//select[@name='triggerEvent_0event']")
	public WebElement onEvent;
	@FindBy(xpath = ".//input[@value='Submit']")
	public WebElement submit;

	@FindBy(xpath = ".//layer[@id='Actions_dynamicmenu0Bar']//a[@href='javascript:void(0);']//img")
	public WebElement menuBar;
	
	@FindBy(xpath = ".//input[@name='auditForm']")
	public WebElement enableAudit;
	
	@FindBy(xpath = ".//input[@id='selectAllTriggers']")
	public WebElement selectAllTrigger;
	
	@FindBy(xpath = ".//table[@id='siteMainTable']//td//tr//td[4]//a[1]//img[1]")
	public WebElement tickUser;
	
	@FindBy(xpath = ".//div[@id='ms-parenttriggerEvent_0userNo']")
	public WebElement trigerDropDown;
	
	@FindBy(xpath = ".//td//tr//td[5]//a[1]//img[1]")
	public WebElement tickRecipient;
	
	@FindBy(xpath = ".//textarea[@name='triggerEvent_0emailID']")
	public WebElement email;
	
	@FindBy(xpath = ".//select[@name='triggerEvent_0event']")
	public WebElement selectOnEvent;
	
	@FindBy(xpath = ".//layer[@id='Actions_dynamicmenu0Bar']//a[@href='javascript:void(0);']//img")
	public WebElement setting;
	
	@FindBy(xpath = ".//span[(contains(id,'Action_dynamicmenu00']")
	public WebElement fieldLevelConfig;
	
	@FindBy(xpath = "//span[@id='Action_dynamicmenu03']")
	public WebElement fieldLevelConfigC;
	
	@FindBy(xpath = ".//select[@name='triggerEvent_0event']")
	public WebElement selectDD0;
	@FindBy(xpath = ".//select[@name='triggerEvent_1event']")
	public WebElement selectDD1;
	@FindBy(xpath = ".//select[@name='triggerEvent_2event']")
	public WebElement selectDD2;
	@FindBy(xpath = ".//select[@name='triggerEvent_3event']")
	public WebElement selectDD3;
	@FindBy(xpath = ".//select[@name='triggerEvent_4event']")
	public WebElement selectDD4;
	@FindBy(xpath = ".//select[@name='triggerEvent_5event']")
	public WebElement selectDD5;
	@FindBy(xpath = ".//select[@name='triggerEvent_6event']")
	public WebElement selectDD6;
	@FindBy(xpath = ".//select[@name='triggerEvent_7event']")
	public WebElement selectDD7;
	@FindBy(xpath = ".//select[@name='triggerEvent_8event']")
	public WebElement selectDD8;
	@FindBy(xpath = ".//select[@name='triggerEvent_9event']")
	public WebElement selectDD9;
	@FindBy(xpath = ".//select[@name='triggerEvent_10event']")
	public WebElement selectDD10;
	@FindBy(xpath = ".//select[@name='triggerEvent_11event']")
	public WebElement selectDD11;
	@FindBy(xpath = ".//select[@name='triggerEvent_12event']")
	public WebElement selectDD12;
	@FindBy(xpath = ".//select[@name='triggerEvent_13event']")
	public WebElement selectDD13;
	@FindBy(xpath = ".//select[@name='triggerEvent_14event']")
	public WebElement selectDD14;
	@FindBy(xpath = ".//select[@name='triggerEvent_15event']")
	public WebElement selectDD15;
	@FindBy(xpath = ".//select[@name='triggerEvent_16event']")
	public WebElement selectDD16;
	@FindBy(xpath = ".//select[@name='triggerEvent_17event']")
	public WebElement selectDD17;
	@FindBy(xpath = ".//select[@name='triggerEvent_18event']")
	public WebElement selectDD18;
	@FindBy(xpath = ".//select[@name='triggerEvent_19event']")
	public WebElement selectDD19;                   
	@FindBy(xpath = ".//select[@name='triggerEvent_20event']")
	public WebElement selectDD20; 
	@FindBy(xpath = ".//select[@name='triggerEvent_21event']")
	public WebElement selectDD21;
	@FindBy(xpath = ".//select[@name='triggerEvent_22event']")
	public WebElement selectDD22;
	@FindBy(xpath = ".//select[@name='triggerEvent_23event']")
	public WebElement selectDD23;
	@FindBy(xpath = ".//select[@name='triggerEvent_24event']")
	public WebElement selectDD24;
	@FindBy(xpath = ".//select[@name='triggerEvent_25event']")
	public WebElement selectDD25;
	@FindBy(xpath = ".//select[@name='triggerEvent_26event']")
	public WebElement selectDD26;
	@FindBy(xpath = ".//select[@name='triggerEvent_27event']")
	public WebElement selectDD27;
	@FindBy(xpath = ".//select[@name='triggerEvent_28event']")
	public WebElement selectDD28;
	
	@FindBy(xpath = ".//select[@name='triggerEvent_29event']")
	public WebElement selectDD29;
	
	@FindBy(xpath = ".//select[@name='triggerEvent_30event']")
	public WebElement selectDD30;
	@FindBy(xpath = ".//select[@name='triggerEvent_31event']")
	public WebElement selectDD31;
	@FindBy(xpath = ".//select[@name='triggerEvent_32event']")
	public WebElement selectDD32;
	@FindBy(xpath = ".//select[@name='triggerEvent_33event']")
	public WebElement selectDD33;
	@FindBy(xpath = ".//select[@name='triggerEvent_34event']")
	public WebElement selectDD34;
	

	
	@FindBy(xpath = ".//select[@name='triggerEvent_35event']")
	public WebElement selectDD35;
	@FindBy(xpath = ".//select[@name='triggerEvent_36event']")
	public WebElement selectDD36;
	@FindBy(xpath = ".//select[@name='triggerEvent_37event']")
	public WebElement selectDD37;
	
	@FindBy(xpath = ".//select[@name='triggerEvent_38event']")
	public WebElement selectDD38;
	@FindBy(xpath = ".//select[@name='triggerEvent_39event']")
	public WebElement selectDD39;
	
	@FindBy(xpath = ".//select[@name='triggerEvent_40event']")
	public WebElement selectDD40;
	@FindBy(xpath = ".//select[@name='triggerEvent_41event']")
	public WebElement selectDD41;
	@FindBy(xpath = ".//select[@name='triggerEvent_42event']")
	public WebElement selectDD42;
	@FindBy(xpath = ".//select[@name='triggerEvent_43event']")
	public WebElement selectDD43;
	
	
	
	
	//+ select
	@FindBy(xpath = ".//select[@id='triggerEvent_6661event']")
	public WebElement stateSelect;
	
	@FindBy(xpath = ".//select[@id='triggerEvent_7771event']")
	public WebElement otherSelect;
	
	@FindBy(xpath = ".//select[@id='triggerEvent_8881event']")
	public WebElement rightsSelect;
	
	@FindBy(xpath = "//select[@id='triggerEvent_1010101event']")
	public WebElement salesSelect;
	
	@FindBy(xpath = "//select[@id='triggerEvent_1111111event']")
	public WebElement liscenceSelect;
	
	@FindBy(xpath = "//select[@id='triggerEvent_1212121event']")
	public WebElement centerSelect;
	
	@FindBy(xpath = "//select[@id='triggerEvent_1313131event']")
	public WebElement datacenterSelect;
	
	@FindBy(xpath = "//select[@id='triggerEvent_1414141event']")
	public WebElement commentsSelect;
	
	
	@FindBy(xpath = ".//input[@value='Submit']")
	public WebElement submit1;
	
	public AdminInfoMgrTriggersAndAuditingPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
