package com.builds.uimaps.infomgr;




import java.util.List;
import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
public class AdminInfoMgrTriggersAuditingPage {

    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "input.cm_new_button_action.showAction")
    //@CacheLookup
    private WebElement actions;

    @FindBy(css = "a[href='nexturl?nextUrl=administration&menuName=admin']")
    //@CacheLookup
    private WebElement admin1;

    @FindBy(css = "a[href='administration']")
    //@CacheLookup
    private WebElement admin2;

    @FindBy(css = "#moreMenu-options2 div.topaccordion ul li:nth-of-type(5) a")
    //@CacheLookup
    private WebElement ads;

    @FindBy(css = "a[href='nexturl?nextUrl=cmCampaignSummary&menuName=cm&subMenuName=cmCamapign&subMenuURL=cmCampaignSummary&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement campaignManager;

    @FindBy(css = "#moreMenu-options2 div.topaccordion ul li:nth-of-type(2) a")
    //@CacheLookup
    private WebElement campaigns1;

    @FindBy(css = "a[href='fscampaignSummary']")
    //@CacheLookup
    private WebElement campaigns2;

    @FindBy(css = "#popdiv div table tbody tr:nth-of-type(1) td:nth-of-type(4) a.text")
    //@CacheLookup
    private WebElement closeicon;

    @FindBy(css = "#searchResult div:nth-of-type(2) div:nth-of-type(3) a.link-style")
    //@CacheLookup
    private WebElement contactSupport1;

    @FindBy(css = "#dropdown1 div.selecter-options1.scroller div.scroller-content1 span.selecter-item a.bText12")
    //@CacheLookup
    private WebElement contactSupport2;

    @FindBy(css = "a[href='nexturl?nextUrl=cmHome&menuName=cm&subMenuName=first&subMenuURL=cmHome&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement crm;

    @FindBy(css = "a[href='nexturl?nextUrl=marketingDashboard&menuName=md&subMenuName=first&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement dashboard1;

    @FindBy(css = "a[href='fsCampaignDashBoard']")
    //@CacheLookup
    private WebElement dashboard2;

    @FindBy(css = "#AUDITEDIT_1 a")
    //@CacheLookup
    private WebElement edit1;

    @FindBy(css = "#TRIGGEREDIT_5 a")
    //@CacheLookup
    private WebElement edit10;

    @FindBy(css = "#AUDITEDIT_6 a")
    //@CacheLookup
    private WebElement edit11;

    @FindBy(css = "#TRIGGEREDIT_6 a")
    //@CacheLookup
    private WebElement edit12;

    @FindBy(css = "#AUDITEDIT_7 a")
    //@CacheLookup
    private WebElement edit13;

    @FindBy(css = "#TRIGGEREDIT_7 a")
    //@CacheLookup
    private WebElement edit14;

    @FindBy(css = "#AUDITEDIT_8 a")
    //@CacheLookup
    private WebElement edit15;

    @FindBy(css = "#TRIGGEREDIT_8 a")
    //@CacheLookup
    private WebElement edit16;

    @FindBy(css = "#AUDITEDIT_9 a")
    //@CacheLookup
    private WebElement edit17;

    @FindBy(css = "#TRIGGEREDIT_9 a")
    //@CacheLookup
    private WebElement edit18;

    @FindBy(css = "#AUDITEDIT_10 a")
    //@CacheLookup
    private WebElement edit19;

    @FindBy(css = "#TRIGGEREDIT_1 a")
    //@CacheLookup
    private WebElement edit2;

    @FindBy(css = "#TRIGGEREDIT_10 a")
    //@CacheLookup
    private WebElement edit20;

    @FindBy(css = "#AUDITEDIT_11 a")
    //@CacheLookup
    private WebElement edit21;

    @FindBy(css = "#TRIGGEREDIT_11 a")
    //@CacheLookup
    private WebElement edit22;

    @FindBy(css = "#AUDITEDIT_12 a")
    //@CacheLookup
    private WebElement edit23;

    @FindBy(css = "#TRIGGEREDIT_12 a")
    //@CacheLookup
    private WebElement edit24;

    @FindBy(css = "#AUDITEDIT_13 a")
    //@CacheLookup
    private WebElement edit25;

    @FindBy(css = "#TRIGGEREDIT_13 a")
    //@CacheLookup
    private WebElement edit26;

    @FindBy(css = "#AUDITEDIT_14 a")
    //@CacheLookup
    private WebElement edit27;

    @FindBy(css = "#TRIGGEREDIT_14 a")
    //@CacheLookup
    private WebElement edit28;

    @FindBy(css = "#AUDITEDIT_15 a")
    //@CacheLookup
    private WebElement edit29;

    @FindBy(css = "#AUDITEDIT_2 a")
    //@CacheLookup
    private WebElement edit3;

    @FindBy(css = "#TRIGGEREDIT_15 a")
    //@CacheLookup
    private WebElement edit30;

    @FindBy(css = "#AUDITEDIT_16 a")
    //@CacheLookup
    private WebElement edit31;

    @FindBy(css = "#TRIGGEREDIT_16 a")
    //@CacheLookup
    private WebElement edit32;

    @FindBy(css = "#AUDITEDIT_17 a")
    //@CacheLookup
    private WebElement edit33;

    @FindBy(css = "#TRIGGEREDIT_17 a")
    //@CacheLookup
    private WebElement edit34;

    @FindBy(css = "#AUDITEDIT_18 a")
    //@CacheLookup
    private WebElement edit35;

    @FindBy(css = "#TRIGGEREDIT_18 a")
    //@CacheLookup
    private WebElement edit36;

    @FindBy(css = "#AUDITEDIT_19 a")
    //@CacheLookup
    private WebElement edit37;

    @FindBy(css = "#TRIGGEREDIT_19 a")
    //@CacheLookup
    private WebElement edit38;

    @FindBy(css = "#AUDITEDIT_20 a")
    //@CacheLookup
    private WebElement edit39;

    @FindBy(css = "#TRIGGEREDIT_2 a")
    //@CacheLookup
    private WebElement edit4;

    @FindBy(css = "#TRIGGEREDIT_20 a")
    //@CacheLookup
    private WebElement edit40;

    @FindBy(css = "#AUDITEDIT_21 a")
    //@CacheLookup
    private WebElement edit41;

    @FindBy(css = "#TRIGGEREDIT_21 a")
    //@CacheLookup
    private WebElement edit42;

    @FindBy(css = "#AUDITEDIT_22 a")
    //@CacheLookup
    private WebElement edit43;

    @FindBy(css = "#TRIGGEREDIT_22 a")
    //@CacheLookup
    private WebElement edit44;

    @FindBy(css = "#AUDITEDIT_23 a")
    //@CacheLookup
    private WebElement edit45;

    @FindBy(css = "#TRIGGEREDIT_23 a")
    //@CacheLookup
    private WebElement edit46;

    @FindBy(css = "#AUDITEDIT_24 a")
    //@CacheLookup
    private WebElement edit47;

    @FindBy(css = "#TRIGGEREDIT_24 a")
    //@CacheLookup
    private WebElement edit48;

    @FindBy(css = "#AUDITEDIT_3 a")
    //@CacheLookup
    private WebElement edit5;

    @FindBy(css = "#TRIGGEREDIT_3 a")
    //@CacheLookup
    private WebElement edit6;

    @FindBy(css = "#AUDITEDIT_4 a")
    //@CacheLookup
    private WebElement edit7;

    @FindBy(css = "#TRIGGEREDIT_4 a")
    //@CacheLookup
    private WebElement edit8;

    @FindBy(css = "#AUDITEDIT_5 a")
    //@CacheLookup
    private WebElement edit9;

    @FindBy(css = "a[href='nexturl?nextUrl=inspectionHome&menuName=audit&subMenuName=first&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement fieldOps;

    @FindBy(css = "a[href='nexturl?nextUrl=financialSetupAlert&menuName=financials&subMenuName=first&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement finance;

    @FindBy(css = "a.userx")
    //@CacheLookup
    private WebElement franconnectAdministrator;

    @FindBy(css = "#siteMainTable tbody tr td table tbody tr:nth-of-type(2) td:nth-of-type(2) table tbody tr td:nth-of-type(2) a")
    //@CacheLookup
    private WebElement helpForThisPage;

    @FindBy(css = "a[href='nexturl?nextUrl=homeFim?jspname=homeFim&status=1&menuName=fim&subMenuName=first&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement infoMgr1;

    @FindBy(css = "a[href='administration#FIM']")
    //@CacheLookup
    private WebElement infoMgr2;

    @FindBy(css = "a[href='nexturl?nextUrl=biHome&menuName=intelligence&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement insights;

    @FindBy(css = "a[href='nexturl?nextUrl=intelligence&menuName=intelligence2&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement insights20;

    @FindBy(css = "a[href='nexturl?nextUrl=cmLandingPages?view=dashboard&menuName=lp&subMenuName=first&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement landingPages;

    @FindBy(css = "a[href='nexturl?nextUrl=localListings?requestType=localListingsSummary&fromWhere=Zcubator&menuName=ll&subMenuName=first&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement listings;

    @FindBy(css = "a[href='logout']")
    //@CacheLookup
    private WebElement logout;

    @FindBy(css = "a.marketinglabel")
    //@CacheLookup
    private WebElement marketing;

    @FindBy(name = "modify")
    //@CacheLookup
    private WebElement modify;

    @FindBy(css = "#test1 span a.label")
    //@CacheLookup
    private WebElement more;

    @FindBy(css = "#recentSearchresult_1 div.searchTab-content solr-recent-saved.ng-isolate-scope div.searchTab-container.ng-scope div.ng-scope div.category-listing.remove-listing ul li.ng-scope a")
    //@CacheLookup
    private WebElement noRecordsFound1;

    @FindBy(css = "#recentSearchresult_2 div.searchTab-content solr-recent-saved.ng-isolate-scope div.searchTab-container.ng-scope div.ng-scope div.category-listing.remove-listing ul li.ng-scope a")
    //@CacheLookup
    private WebElement noRecordsFound2;

    @FindBy(id = "showNotification")
    //@CacheLookup
    private WebElement notifications;

    @FindBy(css = "a[href='nexturl?nextUrl=ppcHome&menuName=ppc&subMenuName=first&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement onlineAds;

    @FindBy(css = "a[href='nexturl?nextUrl=smStoreList%3FisStoreArchive%3dN&menuName=sm&subMenuName=first&subMenuURL=smStoreList&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement opener;

    @FindBy(css = "a[href='nexturl?nextUrl=changePasswordPage&menuName=options&subMenuName=mySetting']")
    //@CacheLookup
    private WebElement options;

    private final String pageLoadedText = "Unable to save your form information as the data entered exceeded the maximum length";

    private final String pageUrl = "/fc/control/viewTriggersSummary?fromWhere=adminHome&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07";

    @FindBy(css = "a[href='nexturl?nextUrl=cmMpDetails?assign=configureCampaign&menuName=mp&subMenuName=first&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement planner;

    @FindBy(name = "print")
    //@CacheLookup
    private WebElement print;

    @FindBy(css = "a[href='nexturl?nextUrl=amUserHome&menuName=adMaker&subMenuName=first&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement printAds;

    @FindBy(css = "#prntLink a.text")
    //@CacheLookup
    private WebElement printView;

    @FindBy(id = "showQuickLinks")
    //@CacheLookup
    private WebElement quickLinks;

    @FindBy(id = "recentSearch_1")
    //@CacheLookup
    private WebElement recentSavedSearches1;

    @FindBy(id = "recentSearch_2")
    //@CacheLookup
    private WebElement recentSavedSearches2;

    @FindBy(css = "a[href='nexturl?nextUrl=groupSummary&menuName=fs&subMenuName=groups']")
    //@CacheLookup
    private WebElement recipientGroups;

    @FindBy(css = "a[href='nexturl?nextUrl=brandAnalytics&fromWhere=Zcubator&menuName=rm&subMenuName=brandAnalytics&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement reputation;

    @FindBy(css = "a[href='nexturl?nextUrl=fsHome&menuName=fs&subMenuName=first&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement sales;

    @FindBy(css = "a.link-style.white-space")
    //@CacheLookup
    private WebElement saveSearch;

    @FindBy(css = "a[href='nexturl?nextUrl=searchAlert?fromWhere=fromSearch&menuName=admin']")
    //@CacheLookup
    private WebElement search;

    @FindBy(css = "a.bText12.whatnewBtn")
    //@CacheLookup
    private WebElement seeWhatsnew;

    @FindBy(css = "a[href='nexturl?nextUrl=configureQuickLinks&menuName=options&subMenuName=mySetting']")
    //@CacheLookup
    private WebElement settings1;

    @FindBy(css = "a[href='nexturl?nextUrl=configureOptOutWhatsNew&menuName=options&subMenuName=mySetting']")
    //@CacheLookup
    private WebElement settings2;

    @FindBy(css = "a[href='nexturl?nextUrl=supplierCategoryDetails&menuName=supplies&subMenuName=first&subMenuURL=supplierCategoryDetails&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement shop;

    @FindBy(css = "a[href='nexturl?nextUrl=fmScHomeAction&menuName=smartconnect&subMenuName=scHome&subMenuURL=scHomeAction&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement smartconnect;

    @FindBy(css = "a[href='nexturl?nextUrl=socialMedia&menuName=fb&subMenuName=first&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement social;

    @FindBy(css = "a[href='nexturl?nextUrl=suppUserHomeFaq&menuName=support&subMenuName=first&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement support;

    @FindBy(css = "a[href='nexturl?nextUrl=systino']")
    //@CacheLookup
    private WebElement surveys;

    @FindBy(css = "a[href='fstemplateSummary']")
    //@CacheLookup
    private WebElement templates;

    @FindBy(css = "a[href='nexturl?nextUrl=main&menuName=intranet&subMenuName=first&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement theHub;

    @FindBy(css = "a[href='nexturl?nextUrl=trainingUserHome&menuName=training&subMenuName=first&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement training;

    @FindBy(css = "a.btn31yellow.fltRight")
    //@CacheLookup
    private WebElement viewAll;

    @FindBy(css = "a[href='nexturl?nextUrl=wbHome&menuName=website&subMenuName=first&cft=ea5b358c-886b-4fbf-949d-5d32a8d88d07']")
    //@CacheLookup
    private WebElement websites;

    @FindBy(css = "a[href='nexturl?nextUrl=fsWorkFlowSummary&status=0&menuName=fs&subMenuName=fsWorkFlow']")
    //@CacheLookup
    private WebElement workflows;

    @FindBy(id = "searchTermKey")
    //@CacheLookup
    private WebElement yourSearchDidNot1;

    @FindBy(id = "toggle")
    //@CacheLookup
    private WebElement yourSearchDidNot2;

    public AdminInfoMgrTriggersAuditingPage() {
    }

    public AdminInfoMgrTriggersAuditingPage(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public AdminInfoMgrTriggersAuditingPage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public AdminInfoMgrTriggersAuditingPage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on Actions Button.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickActionsButton() {
        actions.click();
        return this;
    }

    /**
     * Click on Admin Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickAdmin1Link() {
        admin1.click();
        return this;
    }

    /**
     * Click on Admin Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickAdmin2Link() {
        admin2.click();
        return this;
    }

    /**
     * Click on Ads Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickAdsLink() {
        ads.click();
        return this;
    }

    /**
     * Click on Campaign Manager Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickCampaignManagerLink() {
        campaignManager.click();
        return this;
    }

    /**
     * Click on Campaigns Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickCampaigns1Link() {
        campaigns1.click();
        return this;
    }

    /**
     * Click on Campaigns Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickCampaigns2Link() {
        campaigns2.click();
        return this;
    }

    /**
     * Click on Closeicon Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickCloseiconLink() {
        closeicon.click();
        return this;
    }

    /**
     * Click on Contact Support Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickContactSupport1Link() {
        contactSupport1.click();
        return this;
    }

    /**
     * Click on Contact Support Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickContactSupport2Link() {
        contactSupport2.click();
        return this;
    }

    /**
     * Click on Crm Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickCrmLink() {
        crm.click();
        return this;
    }

    /**
     * Click on Dashboard Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickDashboard1Link() {
        dashboard1.click();
        return this;
    }

    /**
     * Click on Dashboard Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickDashboard2Link() {
        dashboard2.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit10Link() {
        edit10.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit11Link() {
        edit11.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit12Link() {
        edit12.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit13Link() {
        edit13.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit14Link() {
        edit14.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit15Link() {
        edit15.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit16Link() {
        edit16.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit17Link() {
        edit17.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit18Link() {
        edit18.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit19Link() {
        edit19.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit1Link() {
        edit1.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit20Link() {
        edit20.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit21Link() {
        edit21.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit22Link() {
        edit22.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit23Link() {
        edit23.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit24Link() {
        edit24.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit25Link() {
        edit25.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit26Link() {
        edit26.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit27Link() {
        edit27.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit28Link() {
        edit28.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit29Link() {
        edit29.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit2Link() {
        edit2.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit30Link() {
        edit30.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit31Link() {
        edit31.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit32Link() {
        edit32.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit33Link() {
        edit33.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit34Link() {
        edit34.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit35Link() {
        edit35.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit36Link() {
        edit36.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit37Link() {
        edit37.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit38Link() {
        edit38.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit39Link() {
        edit39.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit3Link() {
        edit3.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit40Link() {
        edit40.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit41Link() {
        edit41.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit42Link() {
        edit42.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit43Link() {
        edit43.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit44Link() {
        edit44.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit45Link() {
        edit45.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit46Link() {
        edit46.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit47Link() {
        edit47.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit48Link() {
        edit48.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit4Link() {
        edit4.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit5Link() {
        edit5.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit6Link() {
        edit6.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit7Link() {
        edit7.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit8Link() {
        edit8.click();
        return this;
    }

    /**
     * Click on Edit Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickEdit9Link() {
        edit9.click();
        return this;
    }

    /**
     * Click on Field Ops Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickFieldOpsLink() {
        fieldOps.click();
        return this;
    }

    /**
     * Click on Finance Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickFinanceLink() {
        finance.click();
        return this;
    }

    /**
     * Click on Franconnect Administrator Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickFranconnectAdministratorLink() {
        franconnectAdministrator.click();
        return this;
    }

    /**
     * Click on Help For This Page Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickHelpForThisPageLink() {
        helpForThisPage.click();
        return this;
    }

    /**
     * Click on Info Mgr Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickInfoMgr1Link() {
        infoMgr1.click();
        return this;
    }

    /**
     * Click on Info Mgr Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickInfoMgr2Link() {
        infoMgr2.click();
        return this;
    }

    /**
     * Click on Insights 2.0 Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickInsights20Link() {
        insights20.click();
        return this;
    }

    /**
     * Click on Insights Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickInsightsLink() {
        insights.click();
        return this;
    }

    /**
     * Click on Landing Pages Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickLandingPagesLink() {
        landingPages.click();
        return this;
    }

    /**
     * Click on Listings Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickListingsLink() {
        listings.click();
        return this;
    }

    /**
     * Click on Logout Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickLogoutLink() {
        logout.click();
        return this;
    }

    /**
     * Click on Marketing Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickMarketingLink() {
        marketing.click();
        return this;
    }

    /**
     * Click on Modify Button.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickModifyButton() {
        modify.click();
        return this;
    }

    /**
     * Click on More Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickMoreLink() {
        more.click();
        return this;
    }

    /**
     * Click on No Records Found. Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickNoRecordsFound1Link() {
        noRecordsFound1.click();
        return this;
    }

    /**
     * Click on No Records Found. Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickNoRecordsFound2Link() {
        noRecordsFound2.click();
        return this;
    }

    /**
     * Click on Notifications Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickNotificationsLink() {
        notifications.click();
        return this;
    }

    /**
     * Click on Online Ads Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickOnlineAdsLink() {
        onlineAds.click();
        return this;
    }

    /**
     * Click on Opener Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickOpenerLink() {
        opener.click();
        return this;
    }

    /**
     * Click on Options Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickOptionsLink() {
        options.click();
        return this;
    }

    /**
     * Click on Planner Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickPlannerLink() {
        planner.click();
        return this;
    }

    /**
     * Click on Print Ads Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickPrintAdsLink() {
        printAds.click();
        return this;
    }

    /**
     * Click on Print Button.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickPrintButton() {
        print.click();
        return this;
    }

    /**
     * Click on Print View Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickPrintViewLink() {
        printView.click();
        return this;
    }

    /**
     * Click on Quick Links Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickQuickLinksLink() {
        quickLinks.click();
        return this;
    }

    /**
     * Click on Recent Saved Searches Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickRecentSavedSearches1Link() {
        recentSavedSearches1.click();
        return this;
    }

    /**
     * Click on Recent Saved Searches Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickRecentSavedSearches2Link() {
        recentSavedSearches2.click();
        return this;
    }

    /**
     * Click on Recipient Groups Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickRecipientGroupsLink() {
        recipientGroups.click();
        return this;
    }

    /**
     * Click on Reputation Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickReputationLink() {
        reputation.click();
        return this;
    }

    /**
     * Click on Sales Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickSalesLink() {
        sales.click();
        return this;
    }

    /**
     * Click on Save Search Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickSaveSearchLink() {
        saveSearch.click();
        return this;
    }

    /**
     * Click on Search Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickSearchLink() {
        search.click();
        return this;
    }

    /**
     * Click on See Whatsnew Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickSeeWhatsnewLink() {
        seeWhatsnew.click();
        return this;
    }

    /**
     * Click on Settings Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickSettings1Link() {
        settings1.click();
        return this;
    }

    /**
     * Click on Settings Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickSettings2Link() {
        settings2.click();
        return this;
    }

    /**
     * Click on Shop Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickShopLink() {
        shop.click();
        return this;
    }

    /**
     * Click on Smartconnect Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickSmartconnectLink() {
        smartconnect.click();
        return this;
    }

    /**
     * Click on Social Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickSocialLink() {
        social.click();
        return this;
    }

    /**
     * Click on Support Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickSupportLink() {
        support.click();
        return this;
    }

    /**
     * Click on Surveys Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickSurveysLink() {
        surveys.click();
        return this;
    }

    /**
     * Click on Templates Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickTemplatesLink() {
        templates.click();
        return this;
    }

    /**
     * Click on The Hub Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickTheHubLink() {
        theHub.click();
        return this;
    }

    /**
     * Click on Training Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickTrainingLink() {
        training.click();
        return this;
    }

    /**
     * Click on View All Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickViewAllLink() {
        viewAll.click();
        return this;
    }

    /**
     * Click on Websites Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickWebsitesLink() {
        websites.click();
        return this;
    }

    /**
     * Click on Workflows Link.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage clickWorkflowsLink() {
        workflows.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage fill() {
        setYourSearchDidNot1CheckboxField();
        setYourSearchDidNot2CheckboxField();
        return this;
    }

    /**
     * Set default value to Your Search Did Not Match Any Items Text field.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage setYourSearchDidNot1CheckboxField() {
        return setYourSearchDidNot1CheckboxField(data.get("YOUR_SEARCH_DID_NOT"));
    }

    /**
     * Set Your Search Did Not Match Any Items Checkbox field.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage setYourSearchDidNot1CheckboxField(String yourSearchDidNotValue) {
        yourSearchDidNot1.sendKeys(yourSearchDidNotValue);
        return this;
    }

    /**
     * Set Your Search Did Not Match Any Items Checkbox field.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage setYourSearchDidNot2CheckboxField() {
        if (!yourSearchDidNot2.isSelected()) {
            yourSearchDidNot2.click();
        }
        return this;
    }

    /**
     * Unset Your Search Did Not Match Any Items Checkbox field.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage unsetYourSearchDidNot2CheckboxField() {
        if (yourSearchDidNot2.isSelected()) {
            yourSearchDidNot2.click();
        }
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the AdminInfoMgrTriggersAuditingPage class instance.
     */
    public AdminInfoMgrTriggersAuditingPage verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getCurrentUrl().contains(pageUrl);
            }
        });
        return this;
    }
}
