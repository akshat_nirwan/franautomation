package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AgreementUI {

	WebDriver driver;
	
	@FindBy(id="fimDdApprovedDate")
	public WebElement ApprovedDate;
	
	@FindBy(id="fimDdDateExecuted")
	public WebElement DateExecuted;

	@FindBy(id="fimDdEffectiveDate")
	public WebElement EffectiveDate;
	
	@FindBy(id="fimDdExpirationDate")
	public WebElement TermExpirationDate;
	
	@FindBy(id="fimDdClosingDate")
	public WebElement ClosingDate;
	
	@FindBy(id="fimTtStateAddendum")
	public WebElement StateAddendum;

	@FindBy(id="fimTtOtherAddendum")
	public WebElement OtherAddendum;
	
	@FindBy(xpath="//html//td[@colspan='1']/input[1]")
	public WebElement RightsofFirstRefusalYes;
	
	@FindBy(xpath="//html//td[@colspan='1']/input[2]")
	public WebElement RightsofFirstRefusalNo;
	
	@FindBy(id="fimTtProtectedTerritory")
	public WebElement ProtectedTerritory;
	
	@FindBy(id="fimTtSalesperson")
	public WebElement Salesperson;
	
	@FindBy(id="fimTtPreviousLicenseNumber")
	public WebElement PreviousLicenseNumber;
	
	@FindBy(id="fimTtRelatedCenter")
	public WebElement RelatedCenter;
	
	@FindBy(id="fimDdStoreSoldDate")
	public WebElement DateCenterSold;
	
	@FindBy(id="fimDdRequiredOpeningDate")
	public WebElement RequiredOpeningDate;
	
	@FindBy(id="fimTaComments")
	public WebElement Comments;

	@FindBy(id="fimTtInitialTerm")
	public WebElement InitialTerm;
	
	@FindBy(id="fimTtRenewalTermFirst")
	public WebElement RenewalTermFirst;
	
	@FindBy(id="fimDdRenewalDueDateFirst")
	public WebElement RenewalDueDateFirst;
	
	@FindBy(id="fimTtRenewalFeeFirst")
	public WebElement RenewalFeeFirst;
	
	@FindBy(id="fimDdRenewalFeePaidFirstDate")
	public WebElement RenewalFeePaidFirstDate;
	
	@FindBy(id="fimTtRenewalTermSecond")
	public WebElement RenewalTermSecond;
	
	@FindBy(id="fimDdRenewalDueDateSecond")
	public WebElement RenewalDueDateSecond;
	
	@FindBy(id="fimTtRenewalFeeSecond")
	public WebElement RenewalFeeSecond;
	
	@FindBy(id="fimDdRenewalFeePaidSecondDate")
	public WebElement RenewalFeePaidSecondDate;
	
	@FindBy(id="fimTtRenewalTermThird")
	public WebElement RenewalTermThird;

	@FindBy(id="fimDdRenewalDueDateThird")
	public WebElement RenewalDueDateThird;
	
	@FindBy(id="fimTtRenewalFeeThird")
	public WebElement RenewalFeeThird;
		
	@FindBy(id="fimDdRenewalFeePaidThirdDate")
	public WebElement RenewalFeePaidThirdDate;
	
	@FindBy(id="fimCbtContactTitle")
	public WebElement ContactTitle;
	
	@FindBy(id="fimTtFirstName")
	public WebElement FirstName;
	
	
	@FindBy(id="fimTtLastName")
	public WebElement LastName;
	
	@FindBy(id="address_0address")
	public WebElement StreetAddress;
	
	@FindBy(id="address_0city")
	public WebElement City;
	
	@FindBy(id="address_0country")
	public WebElement Country;
	
	@FindBy(id="address_0zipcode")
	public WebElement Zipcode;
	
	@FindBy(id="address_0state")
	public WebElement State;
	
	
	@FindBy(id="address_0phoneNumbers")
	public WebElement PhoneNumbers;
	
	@FindBy(id="address_0extn")
	public WebElement PhoneExt;
	
	@FindBy(id="address_0faxNumbers")
	public WebElement FaxNumbers;
	
	@FindBy(id="address_0mobileNumbers")
	public WebElement MobileNumbers;
	
	@FindBy(id="address_0emailIds")
	public WebElement Email;
	
	@FindBy(id="fimTtDescription1")
	public WebElement Description1;
	
	@FindBy(id="fimTtAmount1")
	public WebElement Amount1;

	@FindBy(id="fimTtAmountTerm1")
	public WebElement AmountTerm1;
	
	@FindBy(id="fimTtInterest1")
	public WebElement Interest1;
	
	@FindBy(id="fimTaComment1")
	public WebElement Comment1;
	
	@FindBy(id="fimTtDescription2")
	public WebElement Description2;
	
	@FindBy(id="fimTtAmount2")
	public WebElement Amount2;
	
	@FindBy(id="fimTtAmountTerm2")
	public WebElement AmountTerm2;
	
	@FindBy(id="fimTtInterest2")
	public WebElement Interest2;
	
	@FindBy(id="fimTaComment2")
	public WebElement Comment2;
	
	@FindBy(id="fimTtDescription3")
	public WebElement Description3;
	
	
	@FindBy(id="fimTtAmount3")
	public WebElement Amount3;
	
	@FindBy(id="fimTtAmountTerm3")
	public WebElement AmountTerm3;
	
	@FindBy(id="fimTtInterest3")
	public WebElement Interest3;
	
	@FindBy(id="fimTaComment3")
	public WebElement Comment3;
	
	@FindBy(name="Submit")
	public WebElement Submit;
	
	@FindBy(name="Cancel")
	public WebElement Cancel;
	
	@FindBy(xpath=".//span[contains(text(),'Modify')]")
	public WebElement Modify;
	
	@FindBy(xpath=".//span[contains(text(),'Delete')]")
	public WebElement Delete;
	
	@FindBy(name="Button0")
	public WebElement Print;
	
	public AgreementUI(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	
}
