package com.builds.uimaps.infomgr;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;

public class FIMFranchiseesCenterInfoPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "fimsanity" })

	public void sendEmail() throws Exception {
		Reporter.log("TC_FIM_SendFDDEmail_001: Info Mgr > Franchisees > Center Info > Send FDD Mail.\n");
		Reporter.log("###################################################################");
		String testCaseId = "TC_FIM_SendFDDEmail_001";

		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String franchiseId = dataSet.get("franchiseId");

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fimsanity" })

	public void associateWithCampaign() throws Exception {
		Reporter.log("TC_FIM_Campaign_001: Info Mgr > Franchisees > Center Info > Associate With Campaign.\n");
		Reporter.log("###################################################################");
		String testCaseId = "TC_FIM_Campaign_001";

		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String franchiseId = dataSet.get("franchiseId");

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fimsanity" })

	public void addRemarks() throws Exception {
		Reporter.log("TC_FIM_Remarks_001: Info Mgr > Franchisees > Center Info > Add Remarks.\n");
		Reporter.log("###################################################################");
		String testCaseId = "TC_FIM_Remarks_001";

		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String franchiseId = dataSet.get("franchiseId");

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fimsanity" })

	public void logACall() throws Exception {
		Reporter.log("TC_FIM_LogACall_001: Info Mgr > Franchisees > Center Info > Log A Call.\n");
		Reporter.log("###################################################################");
		String testCaseId = "TC_FIM_LogACall_001";

		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String franchiseId = dataSet.get("franchiseId");

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fimsanity" })

	public void addTask() throws Exception {
		Reporter.log("TC_FIM_Task_001: Info Mgr > Franchisees > Center Info > Add Task.\n");
		Reporter.log("###################################################################");
		String testCaseId = "TC_FIM_Task_001";

		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String franchiseId = dataSet.get("franchiseId");

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fimsanity" })

	public void sendMessage() throws Exception {
		Reporter.log("TC_FIM_SendMessage_001: Info Mgr > Franchisees > Center Info > Message.\n");
		Reporter.log("###################################################################");
		String testCaseId = "TC_FIM_SendMessage_001";

		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String franchiseId = dataSet.get("franchiseId");

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
