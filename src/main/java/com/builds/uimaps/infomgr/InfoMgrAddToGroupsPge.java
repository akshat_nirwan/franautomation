package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrAddToGroupsPge {

	public InfoMgrAddToGroupsPge(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement btnCLose ;

	@FindBy(linkText = "Add to New Group")
	public WebElement lnkAddToNewGroup ;
	
	@FindBy(xpath = "//*[contains(text(),'Add to New Group')]")
	public WebElement AddtoGroup ;

}
