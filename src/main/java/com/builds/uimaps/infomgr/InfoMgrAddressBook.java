package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrAddressBook {

	public InfoMgrAddressBook(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "(.//*[@id='corpDiv']//input[@type='checkbox'])[1]")
	public WebElement chkBoxSelectAll ;

	@FindBy(id = "ms-parentgroupwise")
	public WebElement drpSelectView;

	@FindBy(xpath = ".//*[@name='Submit'][contains(@value ,'OK')]")
	public WebElement btnOK ;

	@FindBy(xpath = "(.//*[@name='Submit'][contains(@value ,'Add')])[1]")
	public WebElement btnAdd ;

	@FindBy(name = "contactlist1")
	public List<WebElement> contactListType;

	@FindBy(name = "searchCorpUsers")
	public WebElement txtCorpUsers;

	@FindBy(xpath = ".//*[@title='Search Corporate Users']")
	public WebElement btnSearchCorporateUser;

	@FindBy(xpath = "(.//*[@id='corpDiv']//input[@type='checkbox'])[2]")
	public WebElement chkCorporateUserName ;

}
