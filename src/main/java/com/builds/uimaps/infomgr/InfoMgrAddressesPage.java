package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrAddressesPage extends InfoMgrBasePage {

	public InfoMgrAddressesPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "(.//*[text()='Center Info - Center Details']/following::select[contains(@id , 'field')])[1]")
	public WebElement drpCenterInfoState ;

	@FindBy(name = "save")
	public WebElement btnSave;

	@FindBy(name = "close")
	public WebElement btnClose;

}
