package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrAgreementPage extends InfoMgrBasePage {

	public InfoMgrAgreementPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	// ************************* Aggreement Page
	// **********************************

	@FindBy(xpath = ".//*[@id='siteMainTable']//a/span[text()='Modify']")
	public WebElement lnkModify ;

	@FindBy(name = "Button0")
	public WebElement btnPrint;

	@FindBy(name = "Submit")
	public WebElement btnsubmit;
	

	// ********************** Locators Specific to Multi-Unit Entity Section
	// *****************************

	@FindBy(id = "muFranRadio")
	public WebElement radioMultiUnitFranchisee;

	// ******************** Common Section ********************************

	
	@FindBy(id = "fimDdApprovedDate")
	public WebElement txtApprovedDate;
	
	@FindBy(xpath = ".//*[@id='fimDdApprovedDate']/ancestor::td[@qat_tableid='addData']/preceding-sibling::td[1]")
	public WebElement labelApprovedDate;
	
	

	@FindBy(id = "fimDdDateExecuted")
	public WebElement txtDateExecuted;

	@FindBy(xpath = ".//*[@id='fimDdDateExecuted']/ancestor::td[@qat_tableid='addData']/preceding-sibling::td[1]")
	public WebElement labelDateExecuted;

	@FindBy(id = "fimDdEffectiveDate")
	public WebElement txtEffectiveDate;
	
	@FindBy(xpath = ".//*[@id='fimDdEffectiveDate']/ancestor::td[@qat_tableid='addData']/preceding-sibling::td[1]")
	public WebElement labelEffectiveDate;
	
	@FindBy(id = "fimDdOpeningDate")
	public WebElement txtOpeningDate;
	
	@FindBy(xpath = ".//*[@id='fimDdOpeningDate']/ancestor::td[@qat_tableid='addData']/preceding-sibling::td[1]")
	public WebElement labelOpeningDate;
	
	@FindBy(id = "fimDdClosingDate")
	public WebElement txtClosingDate;

	@FindBy(xpath = ".//*[@id='fimDdClosingDate']/ancestor::td[@qat_tableid='addData']/preceding-sibling::td[1]")
	public WebElement labelClosingDate;
	
	@FindBy(id = "fimTtInitialFee")
	public WebElement txtInitialFee;

	@FindBy(xpath = ".//*[@id='fimTtInitialFee']/parent::*/preceding-sibling::td[1]")
	public WebElement labelInitialFee;
	
	@FindBy(id = "fimTtRoyaltyRate")
	public WebElement txtRoyaltyRate;

	@FindBy(xpath =  ".//*[@id='fimTtRoyaltyRate']/parent::*/preceding-sibling::td[1]")
	public WebElement labelRoyaltyRate;
	
	@FindBy(id = "fimTtRoyaltyMinimumGallons")
	public WebElement txtRoyaltyMinimumGallons;

	@FindBy(xpath =  ".//*[@id='fimTtRoyaltyMinimumGallons']/parent::*/preceding-sibling::td[1]")
	public WebElement labelRoyaltyMinimumGallons;
	
	@FindBy(id = "fimTtAdvertisingRate")
	public WebElement txtAdvertisingRate;

	@FindBy(id = "fimTtAdvertisingMinimumGallons")
	public WebElement txtAdvertisingMinimumGallons;

	@FindBy(id = "fimTtTransferFee")
	public WebElement txtTransferFee;

	@FindBy(id = "fimTtStateAddendum")
	public WebElement txtStateAddendum;

	@FindBy(id = "fimTtOtherAddendum")
	public WebElement txtOtherAddendum;

	@FindBy(id = "fimRrRightsOfFirstRefusal")
	public List<WebElement> radioRightsOfFirstRefusal;

	@FindBy(xpath = ".//*[@id='fimRrRightsOfFirstRefusal']/parent::*/preceding-sibling::td[1]")
	public WebElement labelRightsOfFirstRefusal;

	@FindBy(id = "fimTtProtectedTerritory")
	public WebElement txtProtectedTerritory;

	@FindBy(id = "fimTtSalesperson")
	public WebElement txtSalesperson;

	@FindBy(id = "fimTtPreviousLicenseNumber")
	public WebElement txtPreviousLicenseNumber;

	@FindBy(id = "fimTtRelatedCenter")
	public WebElement txtRelatedCenter;

	@FindBy(id = "fimDdStoreSoldDate")
	public WebElement txtStoreSoldDate;

	@FindBy(id = "fimDdRequiredOpeningDate")
	public WebElement txtRequiredOpeningDate;

	@FindBy(id = "fimTaComments")
	public WebElement txtComments;

	@FindBy(id = "fimTtInitialTerm")
	public WebElement txtInitialTerm;

	@FindBy(id = "fimTtRenewalTermFirst")
	public WebElement txtRenewalTermFirst;

	@FindBy(id = "fimDdRenewalDueDateFirst")
	public WebElement txtRenewalDueDateFirst;

	@FindBy(id = "fimTtRenewalFeeFirst")
	public WebElement txtRenewalFeeFirst;

	@FindBy(id = "fimDdRenewalFeePaidFirstDate")
	public WebElement txtRenewalFeePaidFirstDate;

	@FindBy(id = "fimTtRenewalTermSecond")
	public WebElement txtRenewalTermSecond;

	@FindBy(id = "fimDdRenewalDueDateSecond")
	public WebElement txtRenewalDueDateSecond;

	@FindBy(id = "fimTtRenewalFeeSecond")
	public WebElement txtRenewalFeeSecond;

	@FindBy(id = "fimDdRenewalFeePaidSecondDate")
	public WebElement txtRenewalFeePaidSecondDate;

	@FindBy(id = "fimTtRenewalTermThird")
	public WebElement txtRenewalTermThird;

	@FindBy(id = "fimDdRenewalDueDateThird")
	public WebElement txtRenewalDueDateThird;

	@FindBy(id = "fimTtRenewalFeeThird")
	public WebElement txtRenewalFeeThird;

	@FindBy(id = "fimDdRenewalFeePaidThirdDate")
	public WebElement txtRenewalFeePaidThirdDate;

	// Contact Details

	@FindBy(id = "fimCbtContactTitle")
	public WebElement drpContactTitle;

	@FindBy(xpath = ".//*[@id='fimCbtContactTitle']/parent::*/preceding-sibling::td[1]")
	public WebElement labelContactTitle;
	
	@FindBy(id = "fimTtFirstName")
	public WebElement txtFirstName;

	@FindBy(id = "fimTtLastName")
	public WebElement txtLastName;

	@FindBy(id = "address_0address")
	public WebElement txtStreetAddress;

	@FindBy(id = "address_0city")
	public WebElement txtCity;

	@FindBy(id = "address_0country")
	public WebElement drpCountry;
	
	@FindBy(id = ".//*[@id='address_0country']/parent::*/preceding-sibling::td[1]")
	public WebElement labelCountry;
	
	@FindBy(id = "address_0zipcode")
	public WebElement txtZipcode;

	@FindBy(id = "address_0state")
	public WebElement drpState;

	@FindBy(id = "address_0phoneNumbers")
	public WebElement txtPhoneNumbers;

	@FindBy(id = "address_0extn")
	public WebElement txtExtn;

	@FindBy(id = "address_0faxNumbers")
	public WebElement txtFaxNumbers;

	@FindBy(id = "address_0mobileNumbers")
	public WebElement txtMobileNumbers;

	@FindBy(id = "address_0emailIds")
	public WebElement txtMailIds;

	// Financial Notes
	@FindBy(id = "fimTtDescription1")
	public WebElement txtDescription1;

	@FindBy(id = "fimTtAmount1")
	public WebElement txtAmount1;

	@FindBy(id = "fimTtAmountTerm1")
	public WebElement txtAmountTerm1;

	@FindBy(id = "fimTtInterest1")
	public WebElement txtInterest1;

	@FindBy(id = "fimTaComment1")
	public WebElement txtComment1;

	@FindBy(id = "fimTtDescription2")
	public WebElement txtDescription2;

	@FindBy(id = "fimTtAmount2")
	public WebElement txtAmount2;

	@FindBy(id = "fimDdExpirationDate")
	public WebElement txtExpirationDate;

	@FindBy(xpath = ".//*[@id='fimDdExpirationDate']/ancestor::td[@qat_tableid='addData']/preceding-sibling::td[1]")
	public WebElement labelExpirationDate;
	
	@FindBy(id = "fimTtAmountTerm2")
	public WebElement txtAmountTerm2;

	@FindBy(id = "fimTtInterest2")
	public WebElement txtInterest2;

	@FindBy(id = "fimTaComment2")
	public WebElement txtComment2;

	@FindBy(id = "fimTtDescription3")
	public WebElement txtDescription3;

	@FindBy(id = "fimTtAmount3")
	public WebElement txtAmount3;

	@FindBy(id = "fimTtAmountTerm3")
	public WebElement txtAmountTerm3;

	@FindBy(id = "fimTtInterest3")
	public WebElement txtInterest3;

	@FindBy(id = "fimTaComment3")
	public WebElement txtComment3;

	@FindBy(id = "fimDdOpeningDate_displayOpeningDate")
	public WebElement lblOpeningDate;
	
	@FindBy(xpath = "//select[@id='selectHistory']")
	public WebElement historyDD;
	

	@FindBy(xpath = ".//*[@id='fimTtAdvertisingRate']/parent::*/preceding-sibling::td[1]")
	public WebElement labelAdvertisingRate;

	@FindBy(xpath = ".//*[@id='fimTtAdvertisingMinimumGallons']/parent::*/preceding-sibling::td[1]")
	public WebElement labelAdvertisingMinimumGallons;

	@FindBy(xpath = ".//*[@id='fimTtTransferFee']/parent::*/preceding-sibling::td[1]")
	public WebElement labelTransferFee;

	@FindBy(xpath = ".//*[@id='fimTtStateAddendum']/parent::*/preceding-sibling::td[1]")
	public WebElement labelStateAddendum;

	@FindBy(xpath = ".//*[@id='fimTtOtherAddendum']/parent::*/preceding-sibling::td[1]")
	public WebElement labelOtherAddendum;



	@FindBy(xpath = ".//*[@id='fimTtProtectedTerritory']/parent::*/preceding-sibling::td[1]")
	public WebElement labelProtectedTerritory;

	@FindBy(xpath = ".//*[@id='fimTtSalesperson']/parent::*/preceding-sibling::td[1]")
	public WebElement labelSalesperson;

	@FindBy(xpath = ".//*[@id='fimTtPreviousLicenseNumber']/parent::*/preceding-sibling::td[1]")
	public WebElement labelPreviousLicenseNumber;

	@FindBy(xpath = ".//*[@id='fimTtRelatedCenter']/parent::*/preceding-sibling::td[1]")
	public WebElement labelRelatedCenter;

	@FindBy(xpath = ".//*[@id='fimDdStoreSoldDate']/ancestor::td[@qat_tableid='addData']/preceding-sibling::td[1]")
	public WebElement labelStoreSoldDate;

	@FindBy(xpath = ".//*[@id='fimDdRequiredOpeningDate']/ancestor::td[@qat_tableid='addData']/preceding-sibling::td[1]")
	public WebElement labelRequiredOpeningDate;

	@FindBy(xpath = ".//*[@id='fimTaComments']/parent::*/preceding-sibling::td[1]")
	public WebElement labelComments;

	@FindBy(xpath = ".//*[@id='fimTtInitialTerm']/parent::*/preceding-sibling::td[1]")
	public WebElement labelInitialTerm;

	@FindBy(xpath = ".//*[@id='fimTtRenewalTermFirst']/parent::*/preceding-sibling::td[1]")
	public WebElement labelRenewalTermFirst;

	@FindBy(xpath = ".//*[@id='fimDdRenewalDueDateFirst']/ancestor::td[@qat_tableid='addData']/preceding-sibling::td[1]")
	public WebElement labelRenewalDueDateFirst;

	@FindBy(xpath = ".//*[@id='fimTtRenewalFeeFirst']/parent::*/preceding-sibling::td[1]")
	public WebElement labelRenewalFeeFirst;

	@FindBy(xpath = ".//*[@id='fimDdRenewalFeePaidFirstDate']/ancestor::td[@qat_tableid='addData']/preceding-sibling::td[1]")
	public WebElement labelRenewalFeePaidFirstDate;

	@FindBy(xpath = ".//*[@id='fimTtRenewalTermSecond']/parent::*/preceding-sibling::td[1]")
	public WebElement labelRenewalTermSecond;

	@FindBy(xpath = ".//*[@id='fimDdRenewalDueDateSecond']/ancestor::td[@qat_tableid='addData']/preceding-sibling::td[1]")
	public WebElement labelRenewalDueDateSecond;

	@FindBy(xpath = ".//*[@id='fimTtRenewalFeeSecond']/parent::*/preceding-sibling::td[1]")
	public WebElement labelRenewalFeeSecond;

	@FindBy(xpath = ".//*[@id='fimDdRenewalFeePaidSecondDate']/ancestor::td[@qat_tableid='addData']/preceding-sibling::td[1]")
	public WebElement labelRenewalFeePaidSecondDate;

	@FindBy(xpath = ".//*[@id='fimTtRenewalTermThird']/parent::*/preceding-sibling::td[1]")
	public WebElement labelRenewalTermThird;

	@FindBy(xpath = ".//*[@id='fimDdRenewalDueDateThird']/ancestor::td[@qat_tableid='addData']/preceding-sibling::td[1]")
	public WebElement labelRenewalDueDateThird;

	@FindBy(xpath = ".//*[@id='fimTtRenewalFeeThird']/parent::*/preceding-sibling::td[1]")
	public WebElement labelRenewalFeeThird;

	@FindBy(xpath = ".//*[@id='fimDdRenewalFeePaidThirdDate']/ancestor::td[@qat_tableid='addData']/preceding-sibling::td[1]")
	public WebElement labelRenewalFeePaidThirdDate;

	// Contact Details

	@FindBy(xpath = ".//*[@id='fimCbtContactTitle']/parent::*/preceding-sibling::td[1]")
	public WebElement labeldrpContactTitle;

	@FindBy(xpath = ".//*[@id='fimTtFirstName']/parent::*/preceding-sibling::td[1]")
	public WebElement labelFirstName;

	@FindBy(xpath = ".//*[@id='fimTtLastName']/parent::*/preceding-sibling::td[1]")
	public WebElement labelLastName;


	@FindBy(xpath = ".//*[@id='address_0address']/parent::*/preceding-sibling::td[1]")
	public WebElement labelStreetAddress;

	@FindBy(xpath = ".//*[@id='address_0city']/parent::*/preceding-sibling::td[1]")
	public WebElement labelCity;

	@FindBy(xpath = ".//*[@id='address_0country']/parent::*/preceding-sibling::td[1]")
	public WebElement labeldrpCountry;

	@FindBy(xpath = ".//*[@id='address_0zipcode']/parent::*/preceding-sibling::td[1]")
	public WebElement labelZipcode;

	@FindBy(xpath = ".//*[@id='address_0state']/parent::*/preceding-sibling::td[1]")
	public WebElement labeldrpState;

	@FindBy(xpath = ".//*[@id='address_0phoneNumbers']/parent::*/preceding-sibling::td[1]")
	public WebElement labelPhoneNumbers;

	@FindBy(xpath = ".//*[@id='address_0extn']/parent::*/preceding-sibling::td[1]")
	public WebElement labelExtn;

	@FindBy(xpath = ".//*[@id='address_0faxNumbers']/parent::*/preceding-sibling::td[1]")
	public WebElement labelFaxNumbers;

	@FindBy(xpath = ".//*[@id='address_0mobileNumbers']/parent::*/preceding-sibling::td[1]")
	public WebElement labelMobileNumbers;

	@FindBy(xpath = ".//*[@id='address_0emailIds']/parent::*/preceding-sibling::td[1]")
	public WebElement labelMailIds;

	// Financial Notes
	@FindBy(xpath = ".//*[@id='fimTtDescription1']/parent::*/preceding-sibling::td[1]")
	public WebElement labelDescription1;

	@FindBy(xpath = ".//*[@id='fimTtAmount1']/parent::*/preceding-sibling::td[1]")
	public WebElement labelAmount1;

	@FindBy(xpath = ".//*[@id='fimTtAmountTerm1']/parent::*/preceding-sibling::td[1]")
	public WebElement labelAmountTerm1;

	@FindBy(xpath = ".//*[@id='fimTtInterest1']/parent::*/preceding-sibling::td[1]")
	public WebElement labelInterest1;

	@FindBy(xpath = ".//*[@id='fimTaComment1']/parent::*/preceding-sibling::td[1]")
	public WebElement labelComment1;

	@FindBy(xpath = ".//*[@id='fimTtDescription2']/parent::*/preceding-sibling::td[1]")
	public WebElement labelDescription2;

	@FindBy(xpath = ".//*[@id='fimTtAmount2']/parent::*/preceding-sibling::td[1]")
	public WebElement labelAmount2;

	
	@FindBy(xpath = ".//*[@id='fimTtAmountTerm2']/parent::*/preceding-sibling::td[1]")
	public WebElement labelAmountTerm2;

	@FindBy(xpath = ".//*[@id='fimTtInterest2']/parent::*/preceding-sibling::td[1]")
	public WebElement labelInterest2;

	@FindBy(xpath = ".//*[@id='fimTaComment2']/parent::*/preceding-sibling::td[1]")
	public WebElement labelComment2;

	@FindBy(xpath = ".//*[@id='fimTtDescription3']/parent::*/preceding-sibling::td[1]")
	public WebElement labelDescription3;

	@FindBy(xpath = ".//*[@id='fimTtAmount3']/parent::*/preceding-sibling::td[1]")
	public WebElement labelAmount3;

	@FindBy(xpath = ".//*[@id='fimTtAmountTerm3']/parent::*/preceding-sibling::td[1]")
	public WebElement labelAmountTerm3;

	@FindBy(xpath = ".//*[@id='fimTtInterest3']/parent::*/preceding-sibling::td[1]")
	public WebElement labelInterest3;

	@FindBy(xpath = ".//*[@id='fimTaComment3']/parent::*/preceding-sibling::td[1]")
	public WebElement labelComment3;

	
	
	
	

	

}
