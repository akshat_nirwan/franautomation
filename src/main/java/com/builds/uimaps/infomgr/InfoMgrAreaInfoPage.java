package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrAreaInfoPage extends InfoMgrBasePage {

	public InfoMgrAreaInfoPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "licenseType")
	public WebElement drpLicenseType;

	@FindBy(id = "agreementType")
	public WebElement drpAgreementType;

	@FindBy(name = "effectiveDate")
	public WebElement txtEffectiveDate;

	@FindBy(name = "expirationDate")
	public WebElement txtExpirationDate;

	@FindBy(id = "email")
	public WebElement txtEmail;

	@FindBy(name = "Submit")
	public WebElement btnSubmit;

}
