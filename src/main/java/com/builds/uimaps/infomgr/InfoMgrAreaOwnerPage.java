package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrAreaOwnerPage extends InfoMgrBasePage {

	public InfoMgrAreaOwnerPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "title")
	public WebElement drpTitle;

	@FindBy(id = "firstName")
	public WebElement txtFirstName;

	@FindBy(id = "lastName")
	public WebElement txtLastName;

	@FindBy(id = "address_0address")
	public WebElement streetAddress;

	@FindBy(id = "address_0city")
	public WebElement city;

	@FindBy(id = "address_0country")
	public WebElement drpCountry;

	@FindBy(id = "address_0state")
	public WebElement drpState;

	@FindBy(id = "address_0zipcode")
	public WebElement txtZip;

	@FindBy(id = "address_0phoneNumbers")
	public WebElement txtPhoneNumbers;

	@FindBy(id = "address_0extn")
	public WebElement txtPhoneExt;

	@FindBy(id = "address_0faxNumbers")
	public WebElement txtFax;

	@FindBy(id = "address_0mobileNumbers")
	public WebElement txtMobileNumbers;

	@FindBy(id = "address_0emailIds")
	public WebElement txtEmailID;

	@FindBy(name = "Submit")
	public WebElement btnAdd;

	@FindBy(id = "areaDeveloperID")
	public WebElement drpExistingOwner;

	@FindBy(id = "owners_0isManager")
	public WebElement drpOwnerTitle;

	@FindBy(name = "ownerType")
	public List<WebElement> rdoAreaOwner;
}
