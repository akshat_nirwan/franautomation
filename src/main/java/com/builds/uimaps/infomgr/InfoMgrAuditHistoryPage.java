package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class InfoMgrAuditHistoryPage {
	
	public InfoMgrAuditHistoryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath = ".//td[@width='100%']//td[@width='100%']//tr//table[@width='100%']//tr//td//table[@width='100%']//td[@colspan='2']//tbody//tr")
	public List<WebElement> formHistoryTable	;
	
	@FindBy(xpath = ".//body[@id='siteMainBody']/div[@id='siteMainDiv']/table[@id='siteMainTable']/tbody/tr/td[@width='100%']/table[@width='100%']/tbody/tr/td[@width='100%']/table[@width='100%']/tbody/tr/td[@colspan='8']/table[@width='98%']/tbody/tr/td/table[@width='100%']/tbody/tr[@qat_maintable='withheader']/td[@colspan='2']/table[1]/tbody/tr")
	public List<WebElement> fieldAuditTable	;
	
	
	
	
	
}
