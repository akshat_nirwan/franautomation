package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrBasePage {

	@FindBy(linkText = "Log a Call")
	public WebElement lnkLogACall ;

	@FindBy(linkText = "Add Task")
	public WebElement lnkAddTask ;

	@FindBy(name = "fimSearchString")
	public WebElement txtFranchiseesSearchBox;

	@FindBy(xpath = ".//img[@title='Search by Area / Region']")
	public WebElement imgAreaRegionSearchButton ;

	@FindBy(linkText = "Center Info")
	public WebElement lnkCenterInfo ;

	@FindBy(linkText = "Agreement")
	public WebElement lnkAgreement ;

	@FindBy(xpath = ".//a[@qat_tabname='Area Info']")
	public WebElement lnkAreaInfo ;

	@FindBy(xpath = ".//a[@qat_tabname='Area Owner']")
	public WebElement lnkAreaOwner ;

	@FindBy(linkText = "Contact History")
	public WebElement lnkContactHistory ;

	@FindBy(linkText = "Contract Signing")
	public WebElement lnkContractSigning ;

	@FindBy(linkText = "Customer Complaints")
	public WebElement lnkCustomerComplaints ;

	@FindBy(linkText = "Documents")
	public WebElement lnkDocuments ;

	@FindBy(linkText = "Employees")
	public WebElement lnkEmployees ;

	@FindBy(linkText = "Entity Detail")
	public WebElement linkEntityDetails ;

	@FindBy(linkText = "Entity Details")
	public WebElement linkEntityDetail ;

	@FindBy(linkText = "Events")
	public WebElement lnkEvents ;

	@FindBy(linkText = "Financial")
	public WebElement lnkFinancial ;

	@FindBy(linkText = "Guarantors")
	public WebElement lnkGuarantors ;

	@FindBy(linkText = "Insurance")
	public WebElement lnkInsurance ;

	@FindBy(linkText = "Legal Violation")
	public WebElement lnkLegalViolation ;

	@FindBy(linkText = "Lenders")
	public WebElement lnkLenders ;

	@FindBy(linkText = "Marketing")
	public WebElement lnkMarketing ;

	@FindBy(linkText = "Mystery Review")
	public WebElement lnkMysteryReview ;

	@FindBy(linkText = "Pictures")
	public WebElement lnkPictures ;

	@FindBy(linkText = "Other Addresses")
	public WebElement lnkOtherAddresses ;

	@FindBy(linkText = "QA History")
	public WebElement lnkQAHistory ;

	@FindBy(linkText = "Real Estate")
	public WebElement lnkRealEstate ;

	@FindBy(linkText = "Renewal")
	public WebElement lnkRenewal ;

	@FindBy(linkText = "Territory")
	public WebElement lnkTerritory ;

	@FindBy(xpath = ".//*[@qat_tabname='Training']")
	public WebElement lnkTraining ;

	/*
	 * @FindBy (linkText = "Owners" ) public WebElement tabOwners;
	 */

	@FindBy(linkText = "Owners")
	public WebElement tabOwners ;

	@FindBy(linkText = "Center Info")
	public WebElement centerInfoTab ;

	@FindBy(linkText = "Contract Signing")
	public WebElement contractSigningTab ;

	@FindBy(linkText = "Customer Complaints")
	public WebElement customerComplaintsTab ;

	@FindBy(linkText = "Default and Termination")
	public WebElement defaultTerminationTab ;

	@FindBy(linkText = "Documents")
	public WebElement documentsTab ;

	@FindBy(linkText = "Employees")
	public WebElement employeesTab ;

	@FindBy(linkText = "Financial")
	public WebElement financialTab ;

	@FindBy(linkText = "Guarantors")
	public WebElement guarantorsTab ;

	@FindBy(linkText = "Insurance")
	public WebElement insuranceTab ;

	@FindBy(linkText = "Legal Violation")
	public WebElement legalViolation ;

	@FindBy(linkText = "Lenders")
	public WebElement lendersTab ;

	@FindBy(linkText = "Marketing")
	public WebElement marketingTab ;

	@FindBy(linkText = "Mystery Review")
	public WebElement mysteryReviewTab ;

	@FindBy(linkText = "Owners")
	public WebElement ownersTab ;

	@FindBy(linkText = "Previous Franchisees")
	public WebElement previousFranchiseesTab ;

	@FindBy(linkText = "Transfer")
	public WebElement transferTab ;

	@FindBy(linkText = "Email Summary")
	public WebElement emailSummary ;

	@FindBy(linkText = "test tab")
	public WebElement testLabTab ;

	/*
	 * @FindBy(linkText="Addresses") public WebElement
	 * lnkAddresses=DDDDDDDDDDDDDDDDDDDDD
	 */

	@FindBy(xpath = ".//*[@qat_tabname='Addresses']")
	public WebElement lnkAddresses;

	@FindBy(linkText = "Users")
	public WebElement lnkUsers ;

	@FindBy(name = "fimSearchString")
	public WebElement txtSearchFranchisee;

	@FindBy(xpath = ".//*[@id='fimReportTable']//a/img[contains(@original-title,'Search')]")
	public WebElement imgFranchiseesSearchButton ;

	// Search box on multiUnitpage

	@FindBy(name = "fimMUSearchString")
	public WebElement txtSearchMultiUnit;

	@FindBy(xpath = ".//*[@id='fimReportTable']//a/img[@title='Search Multi-Unit Owner']")
	public WebElement imgMultiUnitSearchButton ;

	@FindBy(id = "selectHistory")
	public WebElement drpSelectAction;

	@FindBy(linkText = "Multi-Unit Info")
	public WebElement lnkMultiUnitInfo ;

	/* Harish Dwivedi InfoMgr */

	@FindBy(xpath = ".//*[@id='fimUl']/li/a[text()='Marketing']")
	public WebElement lnkMarketingFinTab ;

	
	public InfoMgrBasePage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
}
