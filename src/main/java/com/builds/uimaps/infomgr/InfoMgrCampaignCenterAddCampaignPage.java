package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrCampaignCenterAddCampaignPage {

	@FindBy(id = "campaignTitle")
	public WebElement txtCampaignName;

	@FindBy(id = "campaignDescription")
	public WebElement txtCampaignDescription;

	@FindBy(id = "pramotionTem")
	public WebElement rdoPromotionalCampaign;

	@FindBy(id = "regulerTem")
	public WebElement rdoRegularCampaign;

	@FindBy(id = "NewTem")
	public WebElement rdoNewTemplate;

	@FindBy(id = "existTem")
	public WebElement rdoExistingTemplate;

	@FindBy(id = "templateOrder")
	public WebElement lstExistingTemplate;

	@FindBy(linkText = "Associate Template(s)")
	public WebElement lnkAssociateTemplate ;

	@FindBy(id = "templateSearch")
	public WebElement txtSearch;

	@FindBy(name = "search")
	public WebElement btnSearch;

	@FindBy(name = "button2")
	public WebElement btnAdd;

	@FindBy(linkText = "Remove Template")
	public WebElement lnkRemoveTemplate ;

	@FindBy(id = "mailSubject")
	public WebElement txtMailSubject;

	@FindBy(id = "fck")
	public WebElement rdoGraphical;

	@FindBy(id = "text")
	public WebElement rdoText;

	@FindBy(id = "html")
	public WebElement rdoHTML;
	
	@FindBy(id = "HTML_FILE_ATTACHMENT")
	public WebElement ChooseHTMLFile;
	
	@FindBy(id = "Submit")
	public WebElement UploadHTMLFile;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement mailText ;
	
	@FindBy(xpath = ".//*[@id='tinymce']")
	public WebElement Textbody ;
	
	@FindBy(xpath = "//td[contains(text(),'Attachment :')]")
	public WebElement Attachment ;
	
	@FindBy(xpath = ".//*[@name='attachmentName']")
	public WebElement ChooseFile ;
	
	@FindBy(xpath = "//a[@class='bText12lnk']")
	public WebElement Keywordslink ;
	
	@FindBy(xpath = ".//*[contains(text(),'Keywords List')]")
	public WebElement KeywordsListtext ;
	
	@FindBy(id = "attach1")
	public WebElement attach;
	
	@FindBy(xpath = ".//*[@value='Done']")
	public WebElement Done ;
	
	@FindBy(xpath = ".//*[@value='Close']")
	public WebElement Closebtn ;

	@FindBy(id = "ckEditorTr")
	public WebElement htmlEditor;

	@FindBy(name = "radioCheck")
	public List<WebElement> rdoFromHeaderEmail;

	@FindBy(name = "radioCheck1")
	public List<WebElement> rdoReplyEmail;

	@FindBy(id = "startDate")
	public WebElement txtStartDate;

	@FindBy(id = "endDate")
	public WebElement txtEndDate;

	@FindBy(id = "firstMailDaysOptionsNo")
	public WebElement rdoSendMailNo;

	@FindBy(name = "firstMailDaysOptions")
	public WebElement rdoSendMailYes;

	@FindBy(id = "textAr")
	public WebElement txtMailContent;

	@FindBy(linkText = "Create")
	public WebElement btnCreate ;

	public InfoMgrCampaignCenterAddCampaignPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
