package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrCampaignCenterCampaignDashboardPage {

	public InfoMgrCampaignCenterCampaignDashboardPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "campaignName")
	public WebElement txtCampaignName;

	@FindBy(id = "search")
	public WebElement btnSearch;

}
