package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrCampaignCenterEmailCampaignsPage {

	@FindBy(xpath = ".//*[@id='selAllButton']/a/span")
	public WebElement selectAllLocationLnk ;

	@FindBy(name = "checkBox")
	public WebElement allCheckBox;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr[1]/td/table/tbody/tr[5]/td/table/tbody/tr[1]/td[4]/a/span")
	public WebElement selectLocationAndContinueBtn ;

	@FindBy(xpath = ".//input[@value='Next']")
	public WebElement nextBtn ;

	@FindBy(name = "confirm")
	public WebElement btnConfirm;

	@FindBy(linkText = "Locations by Other Information")
	public WebElement locationByOtherInformationLnk ;

	@FindBy(id = "franchiseeID")
	public WebElement txtFranchiseID;

	@FindBy(name = "button")
	public WebElement btnSearch;

	@FindBy(linkText = "Select Campaign and Continue")
	public WebElement btnSelectandContinue ;

	public InfoMgrCampaignCenterEmailCampaignsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
