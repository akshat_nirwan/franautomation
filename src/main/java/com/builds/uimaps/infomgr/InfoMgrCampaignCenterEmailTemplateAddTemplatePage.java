package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrCampaignCenterEmailTemplateAddTemplatePage {

	@FindBy(name = "mailTitle")
	public WebElement titleTxt;

	@FindBy(name = "mailSubject")
	public WebElement mailSubject;

	@FindBy(xpath = ".//iframe[@id='ta_ifr']")
	public WebElement htmlFrame ;

	@FindBy(xpath = "html/body/p")
	public WebElement mailText ;

	@FindBy(xpath = ".//input[@name='Submit' and @value='Add']")
	public WebElement addBtn ;

	public InfoMgrCampaignCenterEmailTemplateAddTemplatePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
