package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrCampaignCenterEmailTemplatesPage {

	@FindBy(xpath = ".//input[@value='Add Template']")
	public WebElement addTemplateBtn ;

	@FindBy(name = "mailTitle")
	public WebElement titleTxt;

	@FindBy(name = "mailSubject")
	public WebElement mailSubject;

	@FindBy(name = "typeRadio")
	public List<WebElement> rdoTypeEmailTemplate;

	@FindBy(xpath = ".//iframe[@id='ta_ifr']")
	public WebElement htmlFrame ;

	@FindBy(xpath = "html/body/p")
	public WebElement mailText ;
	
	@FindBy(xpath = ".//*[@id='tinymce']")
	public WebElement mailTextbody ;

	@FindBy(id = "ta")
	public WebElement txtContent;

	@FindBy(xpath = ".//input[@name='Submit' and @value='Add']")
	public WebElement addBtn ;
	
	@FindBy(xpath = "//td[@class='bText12']")
	public WebElement attachment ;
	
	@FindBy(xpath = ".//*[@name='attachmentName']")
	public WebElement choosefile ;
	
	@FindBy(xpath = ".//*[@id='attach1']")
	public WebElement attach ;
	
	@FindBy(xpath = ".//*[@value='Done']")
	public WebElement done ;

	@FindBy(xpath = "//input[@value='HTML File Upload']")
	public WebElement htmlrdo ;
	
	@FindBy(xpath = "//input[@id='HTML_FILE_ATTACHMENT']")
	public WebElement choosefilehtml ;
	
	@FindBy(xpath = "//input[@value='Add']")
	public WebElement addhtml ;
	
	@FindBy(xpath = "//input[@id='Submit']")
	public WebElement uploadhtml ;
	
	@FindBy(id = "Actions_dynamicmenu1Bar")
	public WebElement actionButton;
	
	@FindBy(xpath = "//span[@id='Action_dynamicmenu31']")
	public WebElement copy;
	
	@FindBy(xpath = "//span[@id='Action_dynamicmenu11']")
	public WebElement delete;
	
	@FindBy(xpath = "//span[@id='Action_dynamicmenu01']")
	public WebElement modify;
	
	@FindBy(xpath = "//input[@value='Modify']")
	public WebElement modifybtn;
	
	@FindBy(xpath = "//input[@name='mailTitle']")
	public WebElement Title;
	
	@FindBy(xpath = "//input[@name='mailSubject']")
	public WebElement Subject;
	
	@FindBy(xpath = "//span[@id='Action_dynamicmenu21']")
	public WebElement testTemplate;
	
	@FindBy(xpath = "//*[@type='text' and @name='email']")
	public WebElement email;
	
	@FindBy(xpath = "//*[@type='button' and @value='Test']")
	public WebElement testButton;
	
	@FindBy(xpath = "//*[@type='button' and @value='OK']")
	public WebElement okbtn;
	
	@FindBy(xpath = "//input[@value='Text']")
	public WebElement textrdo;
	
	@FindBy(id = "tabEmailLink")
	public WebElement Sendmailbtn;
	
	@FindBy(id = "mailTemplateID")
	public WebElement emaildropdown;
	
	@FindBy(xpath = "//input[@value='Send']")
	public WebElement sendemailbtn;
	
	
	public InfoMgrCampaignCenterEmailTemplatesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
