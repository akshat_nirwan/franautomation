package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrCampaignCenterPage {

	@FindBy(xpath = ".//*[@id='siteMainTable']//span[text()='Create Campaign']//following::span[text()='Create']")
	public WebElement btnCreateCampaign ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//span[text()='Choose Campaign']//following::span[text()='Choose']")
	public WebElement btnChooseCampaign ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//span[text()='Filter Locations']//following::span[text()='Filter']")
	public WebElement btnFilterLocation ;

	@FindBy(linkText = "Campaign Dashboard")
	public WebElement lnkCampaignDashboard ;

	@FindBy(linkText = "Email Templates")
	public WebElement lnkEmailTemplates ;

	public InfoMgrCampaignCenterPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
