package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrCenterInfoPage extends InfoMgrBasePage {

	public InfoMgrCenterInfoPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "centerName")
	public WebElement txtCenterName;

	@FindBy(id = "areaID")
	public WebElement drpAreaID;

	@FindBy(id = "brands")
	public WebElement drpDivision;

	@FindBy(id = "licenseNo")
	public WebElement txtLicenseNo;

	@FindBy(id = "storeTypeId")
	public WebElement drpStoreType;

	@FindBy(id = "status")
	public WebElement drpCorporateLocation;

	@FindBy(id = "reportPeriodStartDate")
	public WebElement txtReportPeriodStartDate;

	@FindBy(id = "fbc")
	public WebElement drpfbc;

	@FindBy(id = "openingDate")
	public WebElement txtStoreOpeningDate;

	@FindBy(id = "address")
	public WebElement txtStreetAddress;

	@FindBy(id = "address2")
	public WebElement txtAddress2;

	@FindBy(id = "city")
	public WebElement txtCity;

	@FindBy(id = "countryID")
	public WebElement drpCountry;

	@FindBy(id = "zipcode")
	public WebElement txtZipCode;

	@FindBy(id = "regionNo")
	public WebElement drpState;

	@FindBy(id = "storePhone")
	public WebElement txtPhone;

	@FindBy(id = "storePhoneExtn")
	public WebElement txtExtension;

	@FindBy(id = "storeFax")
	public WebElement txtStoreFax;

	@FindBy(id = "storeMobile")
	public WebElement txtStoreMobile;

	@FindBy(id = "storeEmail")
	public WebElement txtStoreEmail;

	// Contact Information

	@FindBy(id = "contactTitle")
	public WebElement drpContactSalutation;

	@FindBy(id = "contactFirstName")
	public WebElement txtContactFirstName;

	@FindBy(id = "contactLastName")
	public WebElement txtContactLastName;

	@FindBy(id = "phone1")
	public WebElement txtContactPhone;

	@FindBy(id = "phone1Extn")
	public WebElement txtContactExtension;

	@FindBy(id = "emailID")
	public WebElement txtContactEmail;

	@FindBy(id = "fax")
	public WebElement txtContactFax;

	@FindBy(id = "mobile")
	public WebElement txtContactMobile;

	// Command Buttons

	@FindBy(linkText = "Modify")
	public WebElement lnkModify ;

	@FindBy(name = "Submit")
	public WebElement btnSumbit;

	@FindBy(xpath = ".//*[contains(text(),'Last Contacted')]")
	public WebElement centerInfoLastContactedLabel;

	@FindBy(id = "builderFormsTag")
	public WebElement tblStartRecords;

	@FindBy(id = "selectHistory")
	public WebElement drpActionMenu;

	@FindBy(name = "button")
	public WebElement btnPrint;
	
	@FindBy(id = "vTabsFim")
	public WebElement leftTabView;
	
	@FindBy(xpath = ".//*[@class='uper-leftmenu']")
	public WebElement topTabView;

	/*
	 * // Owner Details - Specific to Multi Unit - Begin
	 * 
	 * @FindBy(id="ownerTitle") public WebElement drpOwnerSalutation;
	 * 
	 * @FindBy(id="firstName") public WebElement txtOwnerFirstName;
	 * 
	 * @FindBy(id="lastName") public WebElement txtOwnerLastName;
	 * 
	 * @FindBy(id="homeAddress") public WebElement txtOwnerHomeAddress;
	 * 
	 * @FindBy(id="homeCity") public WebElement txtOwnerHomeCity;
	 * 
	 * @FindBy(id="homeCountry") public WebElement txtOwnerHomeCountry;
	 * 
	 * @FindBy(id="homeZipCode") public WebElement txtOwnerZipCode;
	 * 
	 * @FindBy(id="homeState") public WebElement txtOwnerHomeState;
	 * 
	 * @FindBy(id="phone") public WebElement txtOwnerPhone;
	 * 
	 * @FindBy(id="extn") public WebElement txtOwnerExtension;
	 * 
	 * @FindBy(id="fax") public WebElement txtOwnerFax;
	 * 
	 * @FindBy(id="mobile") public WebElement txtOwnerMobile;
	 * 
	 * @FindBy(id="email") public WebElement txtOwnerEmail;
	 */

	// Owner Details - Specific to Multi Unit - END

}
