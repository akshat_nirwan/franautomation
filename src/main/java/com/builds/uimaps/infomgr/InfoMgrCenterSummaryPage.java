package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrCenterSummaryPage extends InfoMgrBasePage {

	public InfoMgrCenterSummaryPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = ".//*[@id='actionform']/following::table[1]")
	public WebElement tblCenterSummary ;

}
