package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrContactHistoryPage extends InfoMgrBasePage {

	public InfoMgrContactHistoryPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = ".//*[@value='Add Remark']")
	public WebElement btnAddRemark ;

	@FindBy(xpath = ".//*[@id='cboxIframe']")
	public WebElement frameElement ;

	@FindBy(id = "remarks")
	public WebElement txtRemarks;

	@FindBy(xpath = ".//*[@value='Submit']")
	public WebElement btnSubmit ;

	@FindBy(xpath = ".//*[@value='Close']")
	public WebElement btnClose ;

	@FindBy(xpath = ".//*[@id='callPageId']/a[@title='Show All']")
	public WebElement lnkRemarksShowAll ;

	@FindBy(xpath = ".//*[@id='taskSection']")
	public WebElement tableTaskHistory ;

	@FindBy(xpath = ".//*[@id='taskSection']//input[@value='Log a Task'][@type='button']")
	public WebElement btnLogaTask ;

	@FindBy(xpath = ".//*[@value='Log a Call']")
	public WebElement btnLogaCall ;

	@FindBy(name = "Print")
	public WebElement btnPrint;

	@FindBy(xpath = ".//*[@value='Send Message']")
	public WebElement btnSendMessage ;

	@FindBy(xpath = ".//*[@value='Send Email']")
	public WebElement btnSendEmail ;

	// Harish Dwivedi InfoMgr

	@FindBy(xpath = ".//*[@id='fimReportTable']/tbody/tr[1]/td[2]/form/table/tbody/tr[1]/td[2]/a[2]/span")
	public WebElement logATaskLnk ;
	
	@FindBy(xpath = ".//*[@value='3']")
	public WebElement ticketHistoryPrintCheckox;

}
