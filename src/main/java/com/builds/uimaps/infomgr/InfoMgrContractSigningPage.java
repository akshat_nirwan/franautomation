package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrContractSigningPage extends InfoMgrBasePage {

	public InfoMgrContractSigningPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "fimDdContractRecievedSigned")
	public WebElement txtContractReceivedSignedDate;

	@FindBy(xpath = ".//*[@id='fimRrLicenseAgreementProperlySigned'][@value='Yes']")
	public WebElement rdoAgreementProperlySignedYes ;

	@FindBy(xpath = ".//*[@id='fimRrLicenseAgreementProperlySigned'][@value='No']")
	public WebElement rdoAgreementProperlySignedNo ;

	@FindBy(name = "Submit")
	public WebElement btnSubmit;

}
