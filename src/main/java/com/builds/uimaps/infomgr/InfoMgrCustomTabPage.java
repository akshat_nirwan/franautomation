package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrCustomTabPage extends InfoMgrBasePage {

	public InfoMgrCustomTabPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//input[@value='Add']")
	public WebElement Add; 
	@FindBy(xpath="//input[@value='Add More']")
	public WebElement AddMore; 
	@FindBy(xpath="//input[@value='Save']")
	public WebElement Save; 
	
	@FindBy(id = "moduleCustomTab")
	public WebElement customTabData;
	
	@FindBy(xpath = "//input[@name='closeButton']")
	public WebElement Close;

	@FindBy(xpath = "//a[@class='link-btn-blue']")
	public WebElement addButton;
	
	@FindBy(name = "Cancel")
	public WebElement btnCancel;
	@FindBy(xpath = "//select[@id='selectHistory']")
	public WebElement selectHistory;
	@FindBy(xpath = "//td[contains(text(),'TAB')]/following::tr[3]/td[1]")
	public WebElement date;
	
}
