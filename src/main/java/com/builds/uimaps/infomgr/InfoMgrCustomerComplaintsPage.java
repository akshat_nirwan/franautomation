package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrCustomerComplaintsPage extends InfoMgrBasePage {

	public InfoMgrCustomerComplaintsPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	// Complaints Details Section

	@FindBy(id = "entityFranchiseeCombo")
	public WebElement drpFranchiseID;

	@FindBy(id = "fimTtProductCode")
	public WebElement txtComplaintID;

	@FindBy(id = "fimDdComplaintDate")
	public WebElement txtComplaintDate;

	@FindBy(id = "fimTtComplaint")
	public WebElement txtComplaint;

	@FindBy(id = "fimCbComplaintType")
	public WebElement drpComplaintType;

	@FindBy(id = "fimDdIncidentDate")
	public WebElement txtIncidentDate;

	@FindBy(id = "fimTtMethod")
	public WebElement txtReceivedVia;

	@FindBy(id = "fimCbStatus")
	public WebElement drpStatus;

	@FindBy(id = "fimTaSummary")
	public WebElement txtSummary;

	@FindBy(id = "fimTaActionTaken")
	public WebElement txtActionTaken;

	@FindBy(id = "fimDocuments_0fimDocumentTitle")
	public WebElement txtDocumentTitle;

	@FindBy(id = "fimDocuments_0fimDocumentAttachment")
	public WebElement txtDocumentAttachment;

	// Complainant Details Section

	@FindBy(id = "fimTtComplaintBy")
	public WebElement txtComplaintBy;

	@FindBy(id = "address_0address")
	public WebElement txtStreetAddress;

	@FindBy(id = "address_0city")
	public WebElement txtCity;

	@FindBy(id = "address_0country")
	public WebElement drpCountry;

	@FindBy(id = "address_0zipcode")
	public WebElement txtZipCode;

	@FindBy(id = "address_0state")
	public WebElement drpState;

	@FindBy(id = "address_0phoneNumbers")
	public WebElement txtPhoneNumbers;

	@FindBy(id = "address_0emailIds")
	public WebElement txtEmailIds;

	// Command buttons

	@FindBy(name = "Submit")
	public WebElement btnSubmit;
}
