package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrDashboardPage {

	public InfoMgrDashboardPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	// Dashboard Search Box
	
	
	
	@FindBy(xpath = ".//input[@name='fimSearchString']")
	public WebElement SearchBoxDashboard;
	
	@FindBy(xpath = ".//*[@class='topSearch']")
	public WebElement SearchBoxIconDashboard;
	
	
	//Log a Call Link at  Dashboard
	
	@FindBy(xpath = ".//a[contains(@onclick,'fimFranchiseeCallSearch')]")
	public WebElement LogaCallLinkDashboard;
	
	//Log a Task Link at Dashboard
	
	@FindBy(xpath = ".//a[contains(@onclick,'fimTaskSearch')]")
	public WebElement AddTaskLinkDashboard;
	
	
	//Manage Widget Section
	
	@FindBy(xpath = ".//*[@id='widgets']")
	public WebElement manageWidget;
	
	@FindBy(xpath = ".//*[@id='emailHistoryc']")
	public WebElement mngEmailHistoryWidget;
	
	@FindBy(xpath = ".//*[@id='summaryReportc']")
	public WebElement mngSummaryReportWidget;
	
	@FindBy(xpath = ".//*[@id='totalFranchiseec']")
	public WebElement mngTotalFranchiseeWidget;
	
	@FindBy(xpath = ".//*[@id='recentVisitc']")
	public WebElement mngRecentVisitWidget;
	
	@FindBy(xpath = ".//*[@id='renewalDuesc']")
	public WebElement mngRenewalDueWidget;
	
	@FindBy(xpath = ".//*[@id='taskViewc']")
	public WebElement mngTaskViewWidget;
	
	@FindBy(xpath = ".//*[@name='save']")
	public WebElement mngWidgetSaveButton;
	
	// Main Widget Displayed or Not

	
	@FindBy(id = "summaryReport")
	public WebElement SummaryReportWidgetVisible;
	
	@FindBy(id = "totalFranchisee")
	public WebElement TotalFranchiseeWidgetVisible;
	
	@FindBy(id = "taskView")
	public WebElement TaskViewWidgetVisible;
	
	@FindBy(id = "renewalDues")
	public WebElement RenewalDueWidgetVisible;
	
	@FindBy(id = "emailHistory")
	public WebElement EmailHistoryReportVisible;
	
	@FindBy(id = "recentVisit")
	public WebElement RecentVisitWidgetVisible;
	
	
	//Franchisee System Summary Report at Info Mgr Dashboard
	@FindBy(xpath = ".//*[contains(text(),'Total Active Locations')]")
	public WebElement FranchiseSystemSummaryReportWidget;
	
	@FindBy(xpath = ".//*[@id='summaryReport']//a[2]/img[1]")
	public WebElement FranchiseSystemSummaryReportWidgetMinMax;
	
	@FindBy(xpath = ".//*[@id='summaryReport']")
	public WebElement FranchiseSystemSummaryReportWidgetMinMaxHover;
	
	@FindBy(xpath = ".//*[@id='summaryReport']")
	public WebElement FranchiseSystemSummaryRptWidget;
	
	@FindBy(xpath = ".//*[@id='summaryReport']//a[1]/img[1]")
	public WebElement FranchiseSystemSummaryReportWidgetHideSignX;
	
	//New Franchisee Row
	
	@FindBy(xpath = ".//*[contains(text(),'New Franchisees')]/ancestor::tr[1]/td[3]/a[contains(text(),'1')]")
	public WebElement Franchiseecount30DaysCount1 ;
	
	
	@FindBy(xpath = ".//*[contains(text(),'New Franchisees')]/ancestor::tr[1]/td[5]/a[contains(text(),'1')]")
	public WebElement Franchiseecount6MonthsCount1 ;
	
	@FindBy(xpath = ".//*[contains(text(),'New Franchisees')]/ancestor::tr[1]/td[7]/a[contains(text(),'1')]")
	public WebElement Franchiseecount12MonthsCount1 ;
	
	
	@FindBy(xpath = ".//*[contains(text(),'New Franchisees')]/ancestor::tr[1]/td[3]")
	public WebElement Franchiseecount30DaysCountGetTxt ;
	
	
	@FindBy(xpath = ".//*[contains(text(),'New Franchisees')]/ancestor::tr[1]/td[5]")
	public WebElement Franchiseecount6MonthsCountGetTxt ;
	
	//New Corporate Row
	
	@FindBy(xpath = ".//*[contains(text(),'New Corporate Locations')]/ancestor::tr[1]/td[3]/a[contains(text(),'1')]")
	public WebElement NewCorporateLoc30DaysCount1 ;
	
	
	@FindBy(xpath = ".//*[contains(text(),'New Corporate Locations')]/ancestor::tr[1]/td[5]/a[contains(text(),'1')]")
	public WebElement NewCorporateLoc6MonthsCount1 ;
	
	@FindBy(xpath = ".//*[contains(text(),'New Corporate Locations')]/ancestor::tr[1]/td[7]/a[contains(text(),'1')]")
	public WebElement NewCorporateLoc12MonthsCount1 ;
	
	
	@FindBy(xpath = ".//*[contains(text(),'New Corporate Locations')]/ancestor::tr[1]/td[3]")
	public WebElement NewCorporateLoc30DaysGetTxt ;
	
	
	@FindBy(xpath = ".//*[contains(text(),'New Corporate Locations')]/ancestor::tr[1]/td[5]")
	public WebElement NewCorporateLoc6MonthsGetTxt ;
	
	
	//Total New Location Row
	
	@FindBy(xpath = ".//*[contains(text(),'Total New Locations')]/parent::*/td[3]")
	public WebElement TotalNewLocation30Days ;
	
	@FindBy(xpath = ".//*[contains(text(),'Total New Locations')]/parent::*/td[5]")
	public WebElement TotalNewLocation6Months ;
	
	@FindBy(xpath = ".//*[contains(text(),'Total New Locations')]/parent::*/td[7]")
	public WebElement TotalNewLocation12Months ;
	
	
	//Terminated Row
	
	@FindBy(xpath = ".//*[contains(text(),'Franchise Terminations')]/ancestor::tr[1]/td[3]/a[contains(text(),'1')]")
	public WebElement TerminatedFranchisee30DaysCount1 ;
		
		
	@FindBy(xpath = ".//*[contains(text(),'Franchise Terminations')]/ancestor::tr[1]/td[5]/a[contains(text(),'1')]")
	public WebElement TerminatedFranchisee6MonthsCount1 ;
		
	@FindBy(xpath = ".//*[contains(text(),'Franchise Terminations')]/ancestor::tr[1]/td[7]/a[contains(text(),'1')]")
	public WebElement TerminatedFranchisee12MonthsCount1 ;
		
		
	@FindBy(xpath = ".//*[contains(text(),'Franchise Terminations')]/ancestor::tr[1]/td[3]")
	public WebElement TerminatedFranchisee30DaysCountGetTxt ;
		
		
	@FindBy(xpath = ".//*[contains(text(),'Franchise Terminations')]/ancestor::tr[1]/td[5]")
	public WebElement TerminatedFranchisee6MonthsCountGetTxt ;
	
	//Final 3 Rows
	
	@FindBy(xpath = ".//*[contains(text(),'Total Active Franchisees')]/parent::*/td[3]")
	public WebElement TotalActiveFranchisees ;
	
	@FindBy(xpath = ".//*[contains(text(),'Total Active Locations')]/parent::*/td[3]")
	public WebElement TotalActiveLocations ;
	
	@FindBy(xpath = ".//*[contains(text(),'Total Active Locations')]/parent::*/td[3]")
	public WebElement TotalActiveCorporateLocations ;
	
	
	
	//Franchisee in  Last 5 Years and Total Franchisee
	
	@FindBy(xpath = ".//*[@id='totalFranchisee']//*[contains(text(),'No data available')]")
	public WebElement GraphWidget;
	
	@FindBy(xpath = ".//*[@id='totalFranchisee']//a[2]/img[1]")
	public WebElement GraphWidgetMinMax;
	
	@FindBy(xpath = ".//*[@id='totalFranchisee']")
	public WebElement GraphWidgetMinMaxHover;
	
	@FindBy(xpath = ".//*[@id='totalFranchisee']//a[1]/img[1]")
	public WebElement GraphWidgetHideSignX;
	
	@FindBy(xpath = ".//img[contains(@src,'/fc/temp/')]")
	public WebElement GraphPresent;
	
	
	//Task View Widget
	
	@FindBy(xpath = ".//*[@id='taskView']//*[contains(text(),'No Data Found')]")
	public WebElement TaskViewWidget;
		
	@FindBy(xpath = ".//*[@id='taskView']//a[2]/img[1]")
	public WebElement TaskViewWidgetMinMax;
		
	@FindBy(xpath = ".//*[@id='taskView']")
	public WebElement TaskViewWidgetMinMaxHover;
		
	@FindBy(xpath = ".//*[@id='taskView']//a[1]/img[1]")
	public WebElement TaskViewWidgetHideSignX;
	
	@FindBy(xpath = ".//*[@id='taskView']//*[contains(text(),'More Info')]")
	public WebElement TaskViewMoreInfoLink;
	
	
	//Task Values Match
	@FindBy(xpath= "driver.findElements(By.xpath(\".//*[@id='taskView']//*[contains(text(),'Info Mgr Task Subject for Dashboard Widget Verififcation by Divisional User')]/ancestor::tr[1]/td")
	public WebElement TaskDetails;
	
	
	//Renewal Due Widget
	
	@FindBy(xpath = ".//*[@id='renewalDues']//*[contains(text(),'No Data Found')]  | .//*[@id='renewalDues']//*[contains(text(),'Expiration Date')]") 
	public WebElement RenewalDueWidget;
			
	@FindBy(xpath = ".//*[@id='renewalDues']//a[2]/img[1]")
	public WebElement RenewalDueWidgetMinMax;
			
	@FindBy(xpath = ".//*[@id='renewalDues']")
	public WebElement RenewalDueWidgetMinMaxHover;
			
	@FindBy(xpath = ".//*[@id='renewalDues']//a[1]/img[1]")
	public WebElement RenewalDueWidgetHideSignX;
	
	@FindBy(xpath = ".//*[@id='renewalDues']//*[contains(text(),'Expiration Date')]")
	public WebElement RenewalDueExpirationdateColumnHeader;
	
	
		
	
	//Email History Widget
	
	@FindBy(xpath = ".//*[@id='emailHistory']//*[contains(text(),'No Data Found')] | .//*[@id='emailHistory']//*[contains(text(),'Subject')]")
	public WebElement EmailHistoryWidget;
				
	@FindBy(xpath = ".//*[@id='emailHistory']//a[2]/img[1]")
	public WebElement EmailHistoryWidgetMinMax;
				
	@FindBy(xpath = ".//*[@id='emailHistory']")
	public WebElement EmailHistoryWidgetMinMaxHover;
				
	@FindBy(xpath = ".//*[@id='emailHistory']//a[1]/img[1]")
	public WebElement EmailHistoryWidgetHideSignX;
	
	@FindBy(xpath = ".//*[@id='emailHistory']//*[contains(text(),'Subject')]")
	public WebElement EmailHistoryWidgetSubjectColumnHeader;
	
	
	
	//Recent Qa History Widget
	
	@FindBy(xpath = ".//*[@id='recentVisit']//*[contains(text(),'No Data Found')]")
	public WebElement RecentQaHistoryWidget;
					
	@FindBy(xpath = ".//*[@id='recentVisit']//a[2]/img[1]")
	public WebElement RecentQaHistoryWidgetMinMax;
					
	@FindBy(xpath = ".//*[@id='recentVisit']")
	public WebElement RecentQaHistoryWidgetMinMaxHover;
					
	@FindBy(xpath = ".//*[@id='recentVisit']//a[1]/img[1]")
	public WebElement RecentQaHistoryWidgetHideSignX;
			
	
	@FindBy(xpath = ".//*[@id='recentVisit']//a[contains(text(),'More Info')]")
	public WebElement RecentQaHistoryWidgetMoreInfoLink;
	
	
}
