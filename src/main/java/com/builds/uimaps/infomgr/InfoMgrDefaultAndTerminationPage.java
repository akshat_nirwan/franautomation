package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrDefaultAndTerminationPage extends InfoMgrBasePage {

	public InfoMgrDefaultAndTerminationPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "fimDdApprovedDate")
	public WebElement txtCommitteApprovedDate;

	@FindBy(id = "fimDdTerminatedDate")
	public WebElement txtDateTerminated;

	@FindBy(id = "fimCbReason")
	public WebElement drpReason;

	@FindBy(id = "fimCbTypeOfAction")
	public WebElement drpTypeOfAction;

	@FindBy(name = "Submit")
	public WebElement btnSubmit;

}
