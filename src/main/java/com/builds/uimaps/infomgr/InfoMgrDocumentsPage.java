package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrDocumentsPage extends InfoMgrBasePage {

	public InfoMgrDocumentsPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "fimDocumentTitle")
	public WebElement txtDocumentTitle;

	@FindBy(id = "fimDocumentAttachment")
	public WebElement txtDocumentAttachment;

	@FindBy(name = "Add More")
	public WebElement btnAddMore;

	@FindBy(name = "Submit")
	public WebElement btnAdd;

}
