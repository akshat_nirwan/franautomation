package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrEmailSummaryPage extends InfoMgrBasePage {

	public InfoMgrEmailSummaryPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

}
