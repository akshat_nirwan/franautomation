package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrEmployeesPage extends InfoMgrBasePage {

	public InfoMgrEmployeesPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "entityFranchiseeCombo")
	public WebElement drpFranchiseID;

	@FindBy(id = "firstName")
	public WebElement txtFirstName;

	@FindBy(id = "lastName")
	public WebElement txtLastName;

	@FindBy(id = "entityFranchiseeCombo")
	public WebElement entityFranchiseeCombo;

	@FindBy(id = "salutation")
	public WebElement drpSalutation;

	@FindBy(id = "address")
	public WebElement txtAddress;

	@FindBy(id = "city")
	public WebElement txtCity;

	@FindBy(id = "country")
	public WebElement drpCountry;

	@FindBy(id = "zipcode")
	public WebElement txtZipcode;

	@FindBy(id = "state")
	public WebElement drpState;

	@FindBy(id = "phone1")
	public WebElement txtPhone1;

	@FindBy(id = "phone2")
	public WebElement txtPhone2;

	@FindBy(id = "fax")
	public WebElement txtFax;

	@FindBy(id = "mobile")
	public WebElement txtMobile;

	@FindBy(id = "email")
	public WebElement txtEmail;

	// Command buttons

	@FindBy(name = "Button0")
	public WebElement btnAddMoreEmp;

	@FindBy(id = "Button1")
	public WebElement btnCancel;

	@FindBy(id = "saveCont")
	public WebElement btnAdd;

}
