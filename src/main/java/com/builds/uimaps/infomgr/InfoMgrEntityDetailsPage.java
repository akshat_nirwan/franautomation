package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrEntityDetailsPage extends InfoMgrBasePage {

	public InfoMgrEntityDetailsPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "fimTtEntityName")
	public WebElement txtEntityName;

	@FindBy(id = "ms-parentexOwnerIDs")
	public WebElement drpMultiCheckboxOwners;

	@FindBy(id = "address_0address")
	public WebElement txtAddress;

	@FindBy(id = "address_0city")
	public WebElement txtCity;

	@FindBy(id = "address_0country")
	public WebElement drpCountry;

	@FindBy(id = "address_0zipcode")
	public WebElement txtZipCode;

	@FindBy(id = "address_0state")
	public WebElement drpState;

	@FindBy(id = "address_0phoneNumbers")
	public WebElement txtPhoneNumber;

	@FindBy(id = "address_0extn")
	public WebElement txtExtension;

	@FindBy(id = "address_0faxNumbers")
	public WebElement txtFaxNumber;

	@FindBy(id = "address_0mobileNumbers")
	public WebElement txtMobileNumber;

	@FindBy(id = "address_0emailIds")
	public WebElement txtEmailIds;

	@FindBy(id = "fimCbCountryOfFormation")
	public WebElement drpCountryFormation;

	@FindBy(id = "fimDdDateOfFormation")
	public WebElement txtDateFormation;

	@FindBy(id = "fimCbStateOfFormation")
	public WebElement drpStateFormation;

	@FindBy(id = "fimTtTaxpayer")
	public WebElement txtTaxPayer;

	@FindBy(id = "fimCbEntityType")
	public WebElement drpEntityType;

	@FindBy(id = "fimDocuments_0fimDocumentTitle")
	public WebElement txtFormationDocumentTitle;

	@FindBy(id = "fimDocuments_0fimDocumentAttachment")
	public WebElement txtFormationDocumentAttachment;

	@FindBy(id = "fimDocuments_1fimDocumentTitle")
	public WebElement txtGoverningDocumentTitle;

	@FindBy(id = "fimDocuments_1fimDocumentAttachment")
	public WebElement txtGoverningDocumentAttachment;

	// locators specific to regional tab
	@FindBy(id = "areaDocuments_0fimDocumentTitle")
	public WebElement txtAreaFormationDocumentTitle;

	@FindBy(id = "areaDocuments_0fimDocumentAttachment")
	public WebElement txtAreaFormationDocumentAttachment;

	@FindBy(id = "areaDocuments_1fimDocumentTitle")
	public WebElement txtAreaGoverningDocumentTitle;

	@FindBy(id = "areaDocuments_1fimDocumentAttachment")
	public WebElement txtAreaGoverningDocumentAttachment;

	@FindBy(name = "Submit")
	public WebElement btnAdd;

}
