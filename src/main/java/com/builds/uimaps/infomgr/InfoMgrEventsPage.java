package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrEventsPage extends InfoMgrBasePage {

	public InfoMgrEventsPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "fimDdEventDate")
	public WebElement txtDate;

	@FindBy(id = "fimTtEventAuthor")
	public WebElement txtOrganizer;

	@FindBy(id = "fimTtType")
	public WebElement txtType;

	@FindBy(id = "fimTaEventSummary")
	public WebElement txtSummary;

	@FindBy(name = "Submit")
	public WebElement btnAdd;

}
