package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrExportPage extends InfoMgrBasePage {

	@FindBy(xpath = ".//*[@value='franchiseeCall']")
	public WebElement chkbxActHistoryCall ;

	@FindBy(xpath = ".//*[@value='Search Data']")
	public WebElement srchDataBtn ;

	@FindBy(xpath = ".//*[@name='FS_LEAD_DETAILS1_firstName']")
	public WebElement fnametxt ;

	@FindBy(xpath = ".//*[@name='FS_LEAD_DETAILS1_lastName']")
	public WebElement lnametxt ;

	@FindBy(name = "FRANCHISEE1_centerName")
	public WebElement CenterName;

	@FindBy(name = "FRANCHISEE1_franchiseeName")
	public WebElement FranchiseId;

	@FindBy(xpath = ".//*[@value='View Data']")
	public WebElement viewdata ;

	public InfoMgrExportPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

}
