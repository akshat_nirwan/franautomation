package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrFDDFDDSummaryPage {

	WebDriver driver;

	public InfoMgrFDDFDDSummaryPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "(.//*[@value='Send FDD'])[1]")
	public WebElement btnSendFDD ;

	@FindBy(xpath = ".//*[@value='Print'][@type='button']")
	public WebElement btnPrint;

	public InfoMgrFDDSearchFranchiseePage getFDDSearchFranchiseePage() {
		return new InfoMgrFDDSearchFranchiseePage(driver);
	}

	public InfoMgrFDDSearchFranchiseesResultPage getFDDSearchFranchiseeResultPage() {
		return new InfoMgrFDDSearchFranchiseesResultPage(driver);
	}

	public InfoMgrFDDSendFDDPage getFDDSendFDDPage() {
		return new InfoMgrFDDSendFDDPage(driver);
	}

}
