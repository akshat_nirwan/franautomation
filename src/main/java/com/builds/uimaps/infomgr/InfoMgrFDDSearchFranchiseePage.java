package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrFDDSearchFranchiseePage {

	public InfoMgrFDDSearchFranchiseePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "franchiseID")
	public WebElement txtFranchiseID;

	@FindBy(name = "Search")
	public WebElement btnSearch;

	@FindBy(xpath = ".//*[@id='ms-parentmultipleBrand']")
	public WebElement multiCheckBoxDiv ;

	@FindBy(id = "ms-parentfranchiseeType")
	public WebElement drpFranchiseeType;

}
