package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrFDDSearchFranchiseesResultPage {

	public InfoMgrFDDSearchFranchiseesResultPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = ".//*[@value='Send FDD Email']")
	public WebElement btnSendFDDEmail;

	@FindBy(linkText = "Show All")
	public WebElement lnkShowAll;

}
