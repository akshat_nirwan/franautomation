package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrFDDSendFDDPage {

	public InfoMgrFDDSendFDDPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "mailTemplateID")
	public WebElement drpMailTemplate;

	@FindBy(id = "temp")
	public WebElement txtMailTo;

	@FindBy(id = "fTextBox")
	public WebElement txtSubject;

	@FindBy(xpath = ".//*[@id='form1']//input[@value='Send'][@type='button']")
	public WebElement btnSend;
}
