package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrFinancialsPage extends InfoMgrBasePage {

	public InfoMgrFinancialsPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(linkText = "Modify")
	public WebElement lnkModify;

	@FindBy(linkText = "Delete")
	public WebElement lnkDelete;

	@FindBy(name = "Submit")
	public WebElement btnAdd;

	// Financial Information Section

	@FindBy(id = "fimRrFinancialStatement")
	public List<WebElement> rdoFinStatOnFile;

	@FindBy(id = "fimRrFinanceApproved")
	public List<WebElement> rdoFinStatApproved;

	@FindBy(id = "fimTtCashRevenue")
	public WebElement txtCashRevenue;

	@FindBy(id = "fimTtFranchiseeFee")
	public WebElement txtFranchiseeFee;

	@FindBy(id = "fimTtNoteRevenue")
	public WebElement txtNoteRevenue;

	@FindBy(id = "fimDdPromissoryNotePrepareDate")
	public WebElement txtPNPreparedDate;

	@FindBy(id = "fimDdPaymentPlanDated")
	public WebElement txtPaymentPlanDated;

	@FindBy(id = "fimDdPromissoryNoteDated")
	public WebElement txtPNDated;

	@FindBy(id = "fimTtPaymentPlanAmount")
	public WebElement txtPaymentPlanAmount;

	@FindBy(id = "fimTtPromissoryNoteAmount")
	public WebElement txtPNAmount;

	@FindBy(id = "fimDdPlanFinalPaymentDated")
	public WebElement txtPlanFinalPaymentDated;

	@FindBy(id = "fimDdPromissoryFinalPaymentDated")
	public WebElement txtPromissoryFinalPaymentDated;

	// Billing Contact Details Section

	@FindBy(id = "fimCbtContactTitle")
	public WebElement drpTitle;

	@FindBy(id = "fimTtFirstName")
	public WebElement txtFirstName;

	@FindBy(id = "fimTtLastName")
	public WebElement txtLastName;

	@FindBy(id = "address_0address")
	public WebElement txtStreetAddress;

	@FindBy(id = "address_0city")
	public WebElement txtCity;

	@FindBy(id = "address_0country")
	public WebElement drpCountry;

	@FindBy(id = "address_0zipcode")
	public WebElement txtZipCode;

	@FindBy(id = "address_0state")
	public WebElement drpState;

	@FindBy(id = "address_0phoneNumbers")
	public WebElement txtPhoneNumbers;

	@FindBy(id = "address_0extn")
	public WebElement txtExtension;

	@FindBy(id = "address_0faxNumbers")
	public WebElement txtFaxNumbers;

	@FindBy(id = "address_0mobileNumbers")
	public WebElement txtMobileNumbers;

	@FindBy(id = "address_0emailIds")
	public WebElement txtEmailIds;

	@FindBy(xpath = "(.//*[@id='actionform']/following-sibling::table)[1]")
	public WebElement tblFinancialData;

}
