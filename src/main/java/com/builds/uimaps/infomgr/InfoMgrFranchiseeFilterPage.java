package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrFranchiseeFilterPage {

	public InfoMgrFranchiseeFilterPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "inDevelopmentStatusFilter")
	public WebElement drpStatus;

	@FindBy(id = "ms-parentstoreTypeId")
	public WebElement drpStoreTypeId;

	@FindBy(id = "ms-parentfilterfranchiseeNo")
	public WebElement drpFranchiseeID;

	@FindBy(id = "ms-parentfilterDivision")
	public WebElement drpDivision;

	@FindBy(id = "ms-parentownerId")
	public WebElement drpOwnerId;

	@FindBy(id = "ms-parentcompanyNameId")
	public WebElement drpCompanyId;

	@FindBy(id = "ms-parentareaId")
	public WebElement drpAreaID;

	@FindBy(id = "ms-parentadId")
	public WebElement drpAreaRegionOwnerId;

	@FindBy(id = "ms-parentfilterCountryId")
	public WebElement drpCountryId;

	@FindBy(id = "ms-parentfilterStateId")
	public WebElement drpStateId;

	@FindBy(id = "ms-parentfiltercity")
	public WebElement drpCity;

	@FindBy(id = "contactNameId")
	public WebElement txtContactName;

	@FindBy(id = "spouseNameId")
	public WebElement txtSpouseName;

	@FindBy(id = "search")
	public WebElement btnSearch;

	// Specific to Region section - Begin
	@FindBy(id = "ms-parentagreementId")
	public WebElement drpAgreementType;

}
