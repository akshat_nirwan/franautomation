package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrFranchiseesUsersPage extends InfoMgrBasePage {

	public InfoMgrFranchiseesUsersPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

}
