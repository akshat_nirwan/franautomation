package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrGroupsPage {

	@FindBy(xpath = ".//*[@value='Add Groups']")
	public WebElement btnAddGroups ;

	@FindBy(id = "groupName")
	public WebElement txtGroupName;

	@FindBy(id = "groupDescription")
	public WebElement txtGroupDesc;

	@FindBy(name = "groupType")
	public List<WebElement> rdoGroupType;

	@FindBy(id = "submitButton")
	public WebElement btnAdd;

	@FindBy(name = "Delete")
	public WebElement btnDelete;

	@FindBy(name = "Print")
	public WebElement btnPrint;

	@FindBy(name = "Archive")
	public WebElement btnArchive;

	@FindBy(linkText = "Archived Groups")
	public WebElement lnkArchivedGroups ;

	@FindBy(xpath = ".//a/span[text()='Groups']")
	public WebElement lnkGroups ;

	@FindBy(name = "Restore")
	public WebElement btnRestore;

	@FindBy(id = "searchTemplate")
	public WebElement txtGroupSearch;

	@FindBy(id = "search")
	public WebElement btnSearch;

	public InfoMgrGroupsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
