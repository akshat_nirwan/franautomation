package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrGuarantorsPage extends InfoMgrBasePage {

	public InfoMgrGuarantorsPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "fimCbtGurantorTitle")
	public WebElement drpTitle;

	@FindBy(id = "fimTtFirstName")
	public WebElement txtFirstName;

	@FindBy(id = "fimTtLastName")
	public WebElement txtLastName;

	@FindBy(id = "fimCbEntityType")
	public WebElement drpEntityType;

	@FindBy(id = "fimCbResidency")
	public WebElement drpResidencyState;

	@FindBy(id = "fimTtTaxpayer")
	public WebElement txtTaxPayer;

	// Specific to Regional - Begin
	@FindBy(id = "fimTtTaxpayerID")
	public WebElement txtRegionalTaxPayer;
	// - end
	@FindBy(id = "fimTtPercentage")
	public WebElement txtPercentage;

	@FindBy(id = "address_0address")
	public WebElement txtStreetAddress;

	@FindBy(id = "address_0city")
	public WebElement txtCity;

	@FindBy(id = "address_0country")
	public WebElement drpCountry;

	@FindBy(id = "address_0zipcode")
	public WebElement txtZipCode;

	@FindBy(id = "address_0state")
	public WebElement drpState;

	@FindBy(id = "address_0phoneNumbers")
	public WebElement txtPhoneNumbers;

	@FindBy(id = "address_0extn")
	public WebElement txtExtension;

	@FindBy(id = "address_0faxNumbers")
	public WebElement txtFaxNumbers;

	@FindBy(id = "address_0mobileNumbers")
	public WebElement txtMobileNumbers;

	@FindBy(id = "address_0emailIds")
	public WebElement txtEmailIds;

	@FindBy(id = "fimTaComments")
	public WebElement txtComments;

	// Command buttons

	@FindBy(name = "Submit")
	public WebElement btnAdd;

}
