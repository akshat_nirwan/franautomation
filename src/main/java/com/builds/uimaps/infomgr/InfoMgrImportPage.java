package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrImportPage extends InfoMgrBasePage {

	public InfoMgrImportPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

}
