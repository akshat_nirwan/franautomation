package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrInDevelopmentAddFranchiseLocationPage {

	public InfoMgrInDevelopmentAddFranchiseLocationPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "franchiseeName")
	public WebElement txtFranchiseeName;

	@FindBy(id = "centerName")
	public WebElement txtCenterName;

	@FindBy(id = "areaID")
	public WebElement drpAreaID;

	@FindBy(id = "brands")
	public WebElement drpDivision;

	@FindBy(id = "licenseNo")
	public WebElement txtLicenseNo;

	@FindBy(id = "storeTypeId")
	public WebElement drpStoreType;

	@FindBy(id = "status")
	public WebElement drpCorporateLocation;

	@FindBy(id = "versionID")
	public WebElement drpAgreementVersion;

	@FindBy(id = "reportPeriodStartDate")
	public WebElement txtReportPeriodStartDate;

	@FindBy(id = "taxRateId")
	public WebElement drpTaxRateId;

	@FindBy(id = "fbc")
	public WebElement drpfbc;

	@FindBy(id = "grandStoreOpeningDate")
	public WebElement txtStoreOpeningDate;

	@FindBy(id = "address")
	public WebElement txtStreetAddress;

	@FindBy(id = "address2")
	public WebElement txtAddress2;

	@FindBy(id = "city")
	public WebElement txtCity;

	@FindBy(id = "countryID")
	public WebElement drpCountry;

	@FindBy(id = "zipcode")
	public WebElement txtZipCode;

	@FindBy(id = "regionNo")
	public WebElement drpState;

	@FindBy(id = "storePhone")
	public WebElement txtPhone;

	@FindBy(id = "storePhoneExtn")
	public WebElement txtExtension;

	@FindBy(id = "storeFax")
	public WebElement txtStoreFax;

	@FindBy(id = "storeMobile")
	public WebElement txtStoreMobile;

	@FindBy(id = "storeEmail")
	public WebElement txtStoreEmail;

	// Contact Information

	@FindBy(id = "contactTitle")
	public WebElement drpContactSalutation;

	@FindBy(id = "contactFirstName")
	public WebElement txtContactFirstName;

	@FindBy(id = "contactLastName")
	public WebElement txtContactLastName;

	@FindBy(id = "phone1")
	public WebElement txtContactPhone;

	@FindBy(id = "phone1Extn")
	public WebElement txtContactExtension;

	@FindBy(id = "emailID")
	public WebElement txtContactEmail;

	@FindBy(id = "fax")
	public WebElement txtContactFax;

	@FindBy(id = "mobile")
	public WebElement txtContactMobile;

	// Owners Information

	@FindBy(name = "salutation")
	public WebElement drpOwnerSalutation;

	@FindBy(id = "ownerFirstName")
	public WebElement txtOwnerFirstName;

	@FindBy(id = "ownerLastName")
	public WebElement txtOwnerLastName;

	// Command Buttons

	@FindBy(id = "Submit")
	public WebElement btnSubmit;

}
