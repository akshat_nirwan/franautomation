package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrInsurancePage extends InfoMgrBasePage {

	public InfoMgrInsurancePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	// Insurance Policy Details Section

	@FindBy(id = "fimTtPolicyTitle")
	public WebElement txtPolicyTitle;

	@FindBy(id = "fimDdInsuranceEffDate")
	public WebElement txtInsuranceEffectiveDate;

	@FindBy(id = "fimTtInsuranceCompanyName")
	public WebElement txtInsuranceCompanyName;

	@FindBy(id = "fimTtRating")
	public WebElement txtRating;

	@FindBy(id = "fimTtPolicyNo")
	public WebElement txtPolicyNo;

	@FindBy(id = "fimDdInsuranceExpDate")
	public WebElement txtInsuranceExpDate;

	@FindBy(id = "fimRrWcPresent")
	public List<WebElement> rdoWCPresent;

	@FindBy(id = "fimTtVehicleRequired")
	public WebElement txtVehicleRequired;

	@FindBy(id = "fimRrVehichlePresent")
	public List<WebElement> rdoVehiclePresent;

	@FindBy(id = "fimTtGlRequired")
	public WebElement txtGLRequired;

	@FindBy(id = "fimRrGlPresent")
	public List<WebElement> rdoGLPresent;

	@FindBy(id = "fimTtWcCoverageRequired")
	public WebElement txtWCCoverageRequired;

	@FindBy(id = "fimTtWcCoveragePresent")
	public WebElement txtWCCoveragePresent;

	@FindBy(id = "fimTtGlCoverageRequired")
	public WebElement txtGLCoverageRequired;

	@FindBy(id = "fimTtGlCoveragePresent")
	public WebElement txtGLCoveragePresent;

	@FindBy(id = "fimTtGlEachOccurrence")
	public WebElement txtGLEachOccurrence;

	@FindBy(id = "fimTtGlAggregate")
	public WebElement txtAgreegate;

	@FindBy(id = "fimTtPropertyCoverageRequired")
	public WebElement txtPropertyCoverageRequired;

	@FindBy(id = "fimTtPropertyCoveragePresent")
	public WebElement txtPropertyCoveragePresent;

	@FindBy(id = "fimTtPropertyDeductible")
	public WebElement txPropertyDeductible;

	@FindBy(id = "fimTtVehicleCoverageRequired")
	public WebElement txtVehicleCoverageRequired;

	@FindBy(id = "fimTtVehicleCoveragePresent")
	public WebElement txtVehicleCoveragePresent;

	@FindBy(id = "fimTtOtherCoverageRequired")
	public WebElement txtOtherCoverageRequired;

	@FindBy(id = "fimTtOtherCoveragePresent")
	public WebElement txtOtherCoveragePresent;

	@FindBy(id = "fimTtOtherPolicyDescription")
	public WebElement txtOtherPolicyDescription;

	@FindBy(id = "fimTaOtherComments")
	public WebElement txtOtherComments;

	@FindBy(id = "fimTtOther2CoverageRequired")
	public WebElement txtOther2CoverageRequired;

	@FindBy(id = "fimTtOther2CoveragePresent")
	public WebElement txtOther2CoveragePresent;

	@FindBy(id = "fimTaOther2Comments")
	public WebElement txtOther2Comments;

	@FindBy(id = "fimTtTotalCoverage")
	public WebElement txtTotalCoverage;

	@FindBy(id = "fimTaComments")
	public WebElement txtComments;

	@FindBy(id = "fimDocuments_0fimDocumentTitle")
	public WebElement txtDocumentTitle;

	@FindBy(id = "fimDocuments_0fimDocumentAttachment")
	public WebElement txtDocumentAttachment;

	// Contact Details Section

	@FindBy(id = "fimCbtContactTitle")
	public WebElement drpTitle;

	@FindBy(id = "fimTtContactFirstName")
	public WebElement txtFirstName;

	@FindBy(id = "fimTtContactLastName")
	public WebElement txtLastName;

	@FindBy(id = "address_0address")
	public WebElement txtStreetAddress;

	@FindBy(id = "address_0city")
	public WebElement txtCity;

	@FindBy(id = "address_0country")
	public WebElement drpCountry;

	@FindBy(id = "address_0zipcode")
	public WebElement txtZipCode;

	@FindBy(id = "address_0state")
	public WebElement drpState;

	@FindBy(id = "address_0phoneNumbers")
	public WebElement txtPhoneNumbers;

	@FindBy(id = "address_0extn")
	public WebElement txtExtension;

	@FindBy(id = "address_0faxNumbers")
	public WebElement txtFaxNumbers;

	@FindBy(id = "address_0mobileNumbers")
	public WebElement txtMobileNumbers;

	@FindBy(id = "address_0emailIds")
	public WebElement txtEmailIds;

	// Agency Information Section

	@FindBy(id = "fimTtAgencyName")
	public WebElement txtAgencyName;

	@FindBy(id = "address_1address")
	public WebElement txtAgencyStreetAddress;

	@FindBy(id = "address_1city")
	public WebElement txtAgencyCity;

	@FindBy(id = "address_1country")
	public WebElement drpAgencyCountry;

	@FindBy(id = "address_1zipcode")
	public WebElement txtAgencyZipCode;

	@FindBy(id = "address_1state")
	public WebElement drpAgencyState;

	@FindBy(id = "address_1phoneNumbers")
	public WebElement txtAgencyPhoneNumbers;

	@FindBy(id = "address_1extn")
	public WebElement txtAgencyExtension;

	@FindBy(id = "address_1faxNumbers")
	public WebElement txtAgencyFaxNumbers;

	@FindBy(id = "address_1mobileNumbers")
	public WebElement txtAgencyMobileNumbers;

	@FindBy(id = "address_1emailIds")
	public WebElement txtAgencyEmailIds;

	// Add button
	@FindBy(name = "Submit")
	public WebElement btnAdd;

	// Harish Dwivedi InfoMgr
	@FindBy(id = "fimCbCurrentStatus")
	public WebElement fimCbCurrentStatus;

	@FindBy(id = "fimDdAsOf")
	public WebElement fimDdAsOf;

	@FindBy(id = "fimTtPersonSigning")
	public WebElement fimTtPersonSigning;

	@FindBy(id = "fimTtTitle")
	public WebElement fimTtTitle;

	@FindBy(id = "fimDdNewExpirationDate")
	public WebElement fimDdNewExpirationDate;

	@FindBy(id = "fimTaNotes")
	public WebElement fimTaNotes;

}
