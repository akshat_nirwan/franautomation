package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrLegalViolationPage extends InfoMgrBasePage {

	public InfoMgrLegalViolationPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "fimTtNumber")
	public WebElement txtNumber;

	@FindBy(id = "fimDdIncidentDate")
	public WebElement txtDate;

	@FindBy(id = "fimCbComplaintType")
	public WebElement drpType;

	@FindBy(id = "fimCbStatus")
	public WebElement drpStatus;

	@FindBy(id = "fimDdComplaintDate")
	public WebElement txtCureDate;

	@FindBy(id = "fimDdCuredDate")
	public WebElement txtDateCured;

	@FindBy(id = "fimTaSummary")
	public WebElement txtSummary;

	@FindBy(id = "fimTaActionTaken")
	public WebElement txtActionTaken;

	@FindBy(name = "Submit")
	public WebElement btnAdd;

}
