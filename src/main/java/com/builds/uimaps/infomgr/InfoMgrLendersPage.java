package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrLendersPage extends InfoMgrBasePage {

	public InfoMgrLendersPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	// Lender Details Section

	@FindBy(id = "fimCbtTitle")
	public WebElement drpTitle;

	@FindBy(id = "fimTtFirstName")
	public WebElement txtFirstName;

	@FindBy(id = "fimTtLastName")
	public WebElement txtLastName;

	@FindBy(id = "address_0address")
	public WebElement txtStreetaddress;

	@FindBy(id = "address_0city")
	public WebElement txtCity;

	@FindBy(id = "address_0country")
	public WebElement drpCountry;

	@FindBy(id = "address_0zipcode")
	public WebElement txtZipcCode;

	@FindBy(id = "address_0state")
	public WebElement drpstate;

	@FindBy(id = "fimDdCollateralAssignmentExpirationDate")
	public WebElement txtCollateralAssignmentExpirationDate;

	@FindBy(id = "fimTtComfortLetterForm")
	public WebElement txtComfortLetterForm;

	@FindBy(id = "fimDdComfortLetterDate")
	public WebElement txtComfortLetterDate;

	@FindBy(id = "fimRrComfortAgreement")
	public List<WebElement> rdoComfortAgreement;

	@FindBy(id = "fimDdDateComfortAgreementInfo")
	public WebElement txtDateComfortAgreementInfo;

	@FindBy(id = "fimDocuments_0fimDocumentTitle")
	public WebElement txtDocumentTitle;

	// Regional section - specific

	@FindBy(id = "areaDocuments_0fimDocumentTitle")
	public WebElement txtAreaDocumentTitle;

	@FindBy(id = "areaDocuments_0fimDocumentAttachment")
	public WebElement txtAreaDocumentAttachment;

	// Regional section end

	// Multi Unit specific - Begin fimMuDocuments

	@FindBy(id = "fimMuDocuments_0fimDocumentTitle")
	public WebElement txtMultiUnitDocumentTitle;

	@FindBy(id = "fimMuDocuments_0fimDocumentAttachment")
	public WebElement txtMultiUnitDocumentAttachment;

	// Multi Unit specific - END

	@FindBy(id = "fimDocuments_0fimDocumentAttachment")
	public WebElement txtDocumentAttachment;

	// Contact Details Section

	@FindBy(id = "fimTtLenderContactTitleOne")
	public WebElement txtLenderContactTitleOne;

	@FindBy(id = "fimTtLenderContactTitleTwo")
	public WebElement txtLenderContactTitleTwo;

	@FindBy(id = "fimTtLenderContactOne")
	public WebElement txtLenderContactOne;

	@FindBy(id = "fimTtLenderContactTwo")
	public WebElement txtLenderContactTwo;

	@FindBy(id = "fimTtContact1Phone")
	public WebElement txtContact1Phone;

	@FindBy(id = "fimTtContact2Phone")
	public WebElement txtContact2Phone;

	@FindBy(id = "fimTtContact1PhoneExtn")
	public WebElement txtContact1PhoneExtn;

	@FindBy(id = "fimTtContact2PhoneExtn")
	public WebElement txtContact2PhoneExtn;

	@FindBy(id = "fimTtContact1Fax")
	public WebElement txtContact1Fax;

	@FindBy(id = "fimTtContact2Fax")
	public WebElement txtContact2Fax;

	@FindBy(id = "fimTtContact1Email")
	public WebElement txtContact1Email;

	@FindBy(id = "fimTtContact2Email")
	public WebElement txtContact2Email;

	// Command Buttons

	@FindBy(name = "Submit")
	public WebElement btnAdd;

}
