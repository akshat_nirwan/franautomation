package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrLocalListingPage extends InfoMgrBasePage {

	public InfoMgrLocalListingPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

}
