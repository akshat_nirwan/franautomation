package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrLogACallPage {

	public InfoMgrLogACallPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "category")
	public WebElement drpCategory;

	@FindBy(id = "ms-parentfranchiseeIDs")
	public WebElement drpFranchisee;

	@FindBy(id = "ms-parentfranchiseeOwners")
	public WebElement drpOwner;
	
	@FindBy(xpath = ".//*[@value='Continue']")
	public WebElement btnContinue ;

	@FindBy(id = "subject")
	public WebElement txtSubject;

	@FindBy(id = "date")
	public WebElement txtDate;

	@FindBy(id = "sTime")
	public WebElement txtTimeHour;

	@FindBy(id = "sMinute")
	public WebElement txtTimeMinute;

	@FindBy(id = "APM")
	public WebElement txtAMPM;

	@FindBy(id = "callStatus")
	public WebElement drpStatus;

	@FindBy(id = "callType")
	public WebElement drpCallType;

	@FindBy(id = "comments")
	public WebElement txtComments;

	@FindBy(name = "Submit")
	public WebElement btnSubmit;

	@FindBy(name = "close")
	public WebElement btnClose;

	@FindBy(xpath = ".//*[@name='task']//following::table[1]")
	public WebElement tblConfirmationLogATask;

	@FindBy(xpath = ".//*[@type='button'][@value='No']")
	public WebElement btnNo ;

}
