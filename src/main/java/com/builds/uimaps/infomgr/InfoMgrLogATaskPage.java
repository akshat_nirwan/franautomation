package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrLogATaskPage {

	public InfoMgrLogATaskPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	//Franchisee Selection From Color Box
	
	@FindBy(id = "category")
	public WebElement drpCategory;

	@FindBy(id = "ms-parentfranchiseeIDs")
	public WebElement drpFranchisee;

	@FindBy(id = "ms-parentfranchiseeOwners")
	public WebElement drpOwner;

	@FindBy(xpath = ".//*[@value='Continue']")
	public WebElement btnContinue;

	//Task Elements
	
	
	@FindBy(id = "status")
	public WebElement drpStatus;
	
	@FindBy(id = "taskType")
	public WebElement drpTaskType;

	@FindBy(id = "subject")
	public WebElement txtSubject;

	@FindBy(id = "priority")
	public WebElement drpPriority;

	@FindBy(id = "startDate")
	public WebElement txtStartDate;

	@FindBy(name = "add")
	public WebElement btnAdd;

	@FindBy(id = "ms-parentassignTo")
	public WebElement multiCheckBoxCorporateUsers;
	
	@FindBy(id = "comments")
	public WebElement comments;
	
	@FindBy(name = "timelessTaskId")
	public WebElement TimelessTaskId_CheckBox;


	@FindBy(linkText = "Modify")
	public WebElement lnkModify ;

	@FindBy(id = "schduleTime1")
	public WebElement rdoNoReminder;

	@FindBy(name = "close")
	public WebElement btnClose;

	@FindBy(xpath = ".//*[@name='print'][@value='Process']")
	public WebElement btnProcess ;

}
