package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrMailMergePage extends InfoMgrBasePage {

	public InfoMgrMailMergePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
}
