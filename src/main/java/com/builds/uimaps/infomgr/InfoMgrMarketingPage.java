package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrMarketingPage extends InfoMgrBasePage {

	public InfoMgrMarketingPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "fimCbtContactTitle")
	public WebElement drpTitle;

	@FindBy(id = "fimTtFirstName")
	public WebElement txtFirstName;

	@FindBy(id = "fimTtLastName")
	public WebElement txtLastName;

	@FindBy(id = "address_0address")
	public WebElement txtStreetAddress;

	@FindBy(id = "address_0city")
	public WebElement txtCity;

	@FindBy(id = "address_0country")
	public WebElement drpCountry;

	@FindBy(id = "address_0zipcode")
	public WebElement txtZipCode;

	@FindBy(id = "address_0state")
	public WebElement drpState;

	@FindBy(id = "address_0phoneNumbers")
	public WebElement txtPhoneNumbers;

	@FindBy(id = "address_0extn")
	public WebElement txtExtension;

	@FindBy(id = "address_0faxNumbers")
	public WebElement txtFaxNumbers;

	@FindBy(id = "address_0mobileNumbers")
	public WebElement txtMobileNumbers;

	@FindBy(id = "address_0emailIds")
	public WebElement txtEmailIds;

	// Other Marketing Details Section

	@FindBy(id = "fimRrGrandOpeningRequired")
	public List<WebElement> rdoGrandOpeningRequired;

	@FindBy(id = "fimDdGrandOpeningCompletedDate")
	public WebElement txtGrandOpeningCompletedDate;

	@FindBy(id = "fimRrCouponRedemption")
	public List<WebElement> rdoCouponRedemption;

	@FindBy(id = "fimTtCampaignName")
	public WebElement txtCampaignName;

	@FindBy(id = "fimRrCampaignParticipation")
	public List<WebElement> rdoCampaignParticipation;

	@FindBy(id = "fimTtProgramName")
	public WebElement txtProgramName;

	@FindBy(id = "fimRrProgramParticipation")
	public List<WebElement> rdoProgramParticipation;

	@FindBy(id = "fimTtDMA")
	public WebElement txtDMA;

	@FindBy(id = "fimTaComments")
	public WebElement txtComments;

	// Command Buttons Section

	@FindBy(name = "Submit")
	public WebElement btnSubmit;

}
