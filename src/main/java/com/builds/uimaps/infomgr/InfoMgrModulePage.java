package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrModulePage {

	@FindBy(xpath = ".//a[@original-title='Manage all your campaigns']")
	public WebElement campaignCenterTab ;

	@FindBy(xpath = ".//a[@original-title='Manage Active Locations']")
	public WebElement franchiseesTab ;

	@FindBy(xpath = ".//a[@original-title='Get Quick Overview of Location Metrics']")
	public WebElement dashboardTab ;

	@FindBy(xpath = ".//a[@original-title='Manage In Development Locations']")
	public WebElement inDevelopmentTab ;

	@FindBy(xpath = ".//a[@original-title='Manage Terminated Locations']")
	public WebElement terminatedTab ;

	@FindBy(xpath = ".//a[@original-title='Manage Corporate Locations']")
	public WebElement corporateLocationsTab ;

	// Harish Dwivedi Bugs of having with space between Area and region
	// @FindBy(xpath=".//a[@original-title='Manage Area / Region Info' or
	// @original-title='Manage Area/Region Info']")
	// public WebElement regionalTab=DDDDDDDDDDDDDDDDDDDDD
	// Region Info' or @original-title='Manage Area/Region Info']";

	@FindBy(xpath = ".//a[@original-title='View Multi-Unit/Entity Info']")
	public WebElement multiUnitEntityTab ;

	@FindBy(xpath = ".//a[@original-title='FDD']")
	public WebElement fddTab ;

	@FindBy(xpath = ".//a[@original-title='Group Your Franchisees to Manage Them.' or @original-title='Group Your Franchisees to Manage Them']")
	public WebElement groupsTab ;

	@FindBy(xpath = ".//a[@original-title='Manage Tasks']")
	public WebElement taskTab ;

	@FindBy(xpath = ".//a[@original-title='Manage Appointments/Meetings/Events']")
	public WebElement calendarTab ;

	@FindBy(xpath = ".//a[@original-title='Search Locations Subject to Defined Parameters']")
	public WebElement searchTab ;

	@FindBy(xpath = ".//a[@original-title='Send Personalized Document to Multiple Recipients']")
	public WebElement mailMergeTab ;

	@FindBy(xpath = ".//a[@original-title='Manage all your campaigns']")
	public WebElement campaignTab ;

	@FindBy(xpath = ".//a[@original-title='Export System Data to XML/CSV/Excel']")
	public WebElement exportTab ;

	@FindBy(xpath = ".//a[@original-title='Generate Analytical Reports']")
	public WebElement reportTab ;

	@FindBy(xpath = ".//img[@class='tm']")
	public WebElement moreBtn ;

	public InfoMgrModulePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
