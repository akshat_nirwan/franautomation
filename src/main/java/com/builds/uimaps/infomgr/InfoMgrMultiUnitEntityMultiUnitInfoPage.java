package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrMultiUnitEntityMultiUnitInfoPage extends InfoMgrBasePage {
	@FindBy(xpath = ".//*[@id='fimReportTable']/tbody/tr[1]/td[2]/form/table/tbody/tr[1]/td[2]/a[1]/span")
	public WebElement logACallLnk ;

	@FindBy(xpath = ".//*[@id='fimReportTable']/tbody/tr[1]/td[2]/form/table/tbody/tr[1]/td[2]/a[2]/span")
	public WebElement logATaskLnk ;

	@FindBy(xpath = ".//*[@id='showFilter']/a")
	public WebElement showFiltersLnk ;

	@FindBy(name = "fimMUSearchString")
	public WebElement searchMUOwnerTxt;

	@FindBy(xpath = ".//input[@value='Actions' and @type='button']")
	public WebElement actions ;

	@FindBy(linkText = "Change to Multi-Unit View")
	public WebElement changeToMultiUnitView ;

	@FindBy(linkText = "Change to Entity View")
	public WebElement changeToEntityView ;

	@FindBy(xpath = ".//input[@value='Log a Task' and @type='button']")
	public WebElement logATaskBtn ;

	@FindBy(xpath = ".//input[@value='Log a Call' and @type='button']")
	public WebElement logACallBtn ;

	@FindBy(xpath = ".//input[@value='Send Email' and @type='button']")
	public WebElement sendEmailBtn ;

	@FindBy(linkText = "Modify")
	public WebElement modifyLnk ;

	@FindBy(id = "ownerTitle")
	public WebElement ownerTitle;

	@FindBy(id = "firstName")
	public WebElement firstName;

	@FindBy(id = "homeAddress")
	public WebElement homeAddress;

	@FindBy(id = "homeCity")
	public WebElement homeCity;

	@FindBy(id = "homeCountry")
	public WebElement homeCountry;

	@FindBy(id = "homeZipCode")
	public WebElement homeZipCode;

	@FindBy(id = "homeState")
	public WebElement homeState;

	@FindBy(id = "otherAddress")
	public WebElement otherAddress;

	@FindBy(id = "otherCity")
	public WebElement otherCity;

	@FindBy(id = "otherCountry")
	public WebElement otherCountry;

	@FindBy(id = "otherZipCode")
	public WebElement otherZipCode;

	@FindBy(id = "otherState")
	public WebElement otherState;

	@FindBy(id = "phone")
	public WebElement phone;

	@FindBy(id = "extn")
	public WebElement extn;

	@FindBy(id = "fax")
	public WebElement fax;

	@FindBy(id = "mobile")
	public WebElement mobile;

	@FindBy(id = "email")
	public WebElement email;

	@FindBy(name = "taxpayerID")
	public WebElement taxpayerID;

	@FindBy(id = "typeOfOwnership")
	public WebElement typeOfOwnership;

	@FindBy(id = "spouseFirstName")
	public WebElement spouseFirstName;

	@FindBy(id = "spouseLastName")
	public WebElement spouseLastName;

	@FindBy(id = "spousePhone")
	public WebElement spousePhone;

	@FindBy(id = "spousePhoneExtn")
	public WebElement spousePhoneExtn;

	@FindBy(id = "spouseMobile")
	public WebElement spouseMobile;

	@FindBy(id = "licenseType")
	public WebElement licenseType;

	@FindBy(id = "agreementType")
	public WebElement agreementType;

	@FindBy(id = "initialFee")
	public WebElement initialFee;

	@FindBy(id = "renewalFee")
	public WebElement renewalFee;

	@FindBy(name = "renewalOptions")
	public WebElement renewalOptions;

	@FindBy(name = "consecutive")
	public WebElement consecutive;

	@FindBy(id = "initialFeePercentage")
	public WebElement initialFeePercentage;

	@FindBy(id = "operatingFeePercentage")
	public WebElement operatingFeePercentage;

	@FindBy(id = "renewalFeePercentage")
	public WebElement renewalFeePercentage;

	@FindBy(id = "comments")
	public WebElement comments;

	@FindBy(name = "Submit")
	public WebElement submit;

	public InfoMgrMultiUnitEntityMultiUnitInfoPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

}
