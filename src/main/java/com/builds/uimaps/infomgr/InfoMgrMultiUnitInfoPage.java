package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrMultiUnitInfoPage extends InfoMgrBasePage {

	@FindBy(xpath = ".//*[@id='fimReportTable']/tbody/tr[1]/td[2]/form/table/tbody/tr[1]/td[2]/a[1]/span")
	public WebElement logACallLnk ;

	@FindBy(xpath = ".//*[@id='fimReportTable']/tbody/tr[1]/td[2]/form/table/tbody/tr[1]/td[2]/a[2]/span")
	public WebElement logATaskLnk ;

	@FindBy(xpath = ".//*[@id='showFilter']/a")
	public WebElement showFiltersLnk ;

	@FindBy(name = "fimMUSearchString")
	public WebElement searchMUOwnerTxt;

	@FindBy(xpath = ".//input[@value='Actions' and @type='button']")
	public WebElement actions ;

	@FindBy(linkText = "Change to Multi-Unit View")
	public WebElement changeToMultiUnitView ;

	@FindBy(linkText = "Change to Entity View")
	public WebElement changeToEntityView ;

	@FindBy(xpath = ".//input[@value='Log a Task' and @type='button']")
	public WebElement logATaskBtn ;

	@FindBy(xpath = ".//input[@value='Log a Call' and @type='button']")
	public WebElement logACallBtn ;

	@FindBy(xpath = ".//input[@value='Send Email' and @type='button']")
	public WebElement sendEmailBtn ;

	@FindBy(linkText = "Modify")
	public WebElement modifyLnk ;

	@FindBy(id = "ownerTitle")
	public WebElement drpOwnerTitle;

	@FindBy(id = "firstName")
	public WebElement txtFirstName;

	@FindBy(id = "lastName")
	public WebElement txtLastName;

	@FindBy(id = "homeAddress")
	public WebElement txtHomeAddress;

	@FindBy(id = "homeCity")
	public WebElement txtHomeCity;

	@FindBy(id = "homeCountry")
	public WebElement drpHomeCountry;

	@FindBy(id = "homeZipCode")
	public WebElement txtHomeZipCode;

	@FindBy(id = "homeState")
	public WebElement drpHomeState;

	@FindBy(id = "otherAddress")
	public WebElement txtOtherAddress;

	@FindBy(id = "otherCity")
	public WebElement txtOtherCity;

	@FindBy(id = "otherCountry")
	public WebElement drpOtherCountry;

	@FindBy(id = "otherZipCode")
	public WebElement drpOtherZipCode;

	@FindBy(id = "otherState")
	public WebElement drpOtherState;

	@FindBy(id = "phone")
	public WebElement txtPhone;

	@FindBy(id = "extn")
	public WebElement txtExtension;

	@FindBy(id = "fax")
	public WebElement txtFax;

	@FindBy(id = "mobile")
	public WebElement txtMobile;

	@FindBy(id = "email")
	public WebElement txtEmail;

	@FindBy(name = "taxpayerID")
	public WebElement txtTaxpayerID;

	@FindBy(id = "typeOfOwnership")
	public WebElement drpTypeOfOwnership;

	@FindBy(id = "spouseFirstName")
	public WebElement spouseFirstName;

	@FindBy(id = "spouseLastName")
	public WebElement spouseLastName;

	@FindBy(id = "spousePhone")
	public WebElement spousePhone;

	@FindBy(id = "spousePhoneExtn")
	public WebElement spousePhoneExtn;

	@FindBy(id = "spouseMobile")
	public WebElement spouseMobile;

	@FindBy(id = "licenseType")
	public WebElement licenseType;

	@FindBy(id = "agreementType")
	public WebElement agreementType;

	@FindBy(id = "initialFee")
	public WebElement initialFee;

	@FindBy(id = "renewalFee")
	public WebElement renewalFee;

	@FindBy(name = "renewalOptions")
	public WebElement renewalOptions;

	@FindBy(name = "consecutive")
	public WebElement consecutive;

	@FindBy(id = "initialFeePercentage")
	public WebElement initialFeePercentage;

	@FindBy(id = "operatingFeePercentage")
	public WebElement operatingFeePercentage;

	@FindBy(id = "renewalFeePercentage")
	public WebElement renewalFeePercentage;

	@FindBy(id = "comments")
	public WebElement comments;

	@FindBy(name = "Submit")
	public WebElement submit;

	WebDriver driver;

	public InfoMgrMultiUnitInfoPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

}
