package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrMystryReviewPage extends InfoMgrBasePage {

	public InfoMgrMystryReviewPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	// Mystery Review Details Section

	@FindBy(id = "fimDdInspectionDate")
	public WebElement txtInspectionDate;

	@FindBy(id = "fimDdReportSentToFranchiseeDate")
	public WebElement txtReportSentToFranchiseeDate;

	@FindBy(id = "fimFtTotalScore")
	public WebElement txtTotalScore;

	@FindBy(id = "fimTaGeneralComments")
	public WebElement txtGeneralComments;

	// Performance Section

	@FindBy(id = "fimFtPointValuePerformanceQ1")
	public WebElement txtPointValuePerformanceQ1;

	@FindBy(id = "fimFtPointValuePerformanceQ2")
	public WebElement txtPointValuePerformanceQ2;

	@FindBy(id = "fimFtPointValuePerformanceQ3")
	public WebElement txtPointValuePerformanceQ3;

	@FindBy(id = "fimFtPointValuePerformanceQ4")
	public WebElement txtPointValuePerformanceQ4;

	@FindBy(id = "fimFtPointValuePerformanceQ5")
	public WebElement txtPointValuePerformanceQ5;

	@FindBy(id = "fimFtPointValuePerformanceQ6")
	public WebElement txtPointValuePerformanceQ6;

	@FindBy(id = "fimFtPointValuePerformanceQ7")
	public WebElement txtPointValuePerformanceQ7;

	@FindBy(id = "fimFtPointValuePerformanceQ8")
	public WebElement txtPointValuePerformanceQ8;

	@FindBy(id = "fimFtServiceQuestionScore")
	public WebElement txtPerformanceQuestionScore;

	@FindBy(id = "fimTaCommentsForPerformance")
	public WebElement txtCommentsForPerformance;

	// Service Section

	@FindBy(id = "fimFtPointValueServiceQ1")
	public WebElement txtPointValueServiceQ1;

	@FindBy(id = "fimFtPointValueServiceQ2")
	public WebElement txtPointValueServiceQ2;

	@FindBy(id = "fimFtPointValueServiceQ3")
	public WebElement txtPointValueServiceQ3;

	@FindBy(id = "fimFtPointValueServiceQ4")
	public WebElement txtPointValueServiceQ4;

	@FindBy(id = "fimFtPointValueServiceQ5")
	public WebElement txtPointValueServiceQ5;

	@FindBy(id = "fimFtPointValueServiceQ6")
	public WebElement txtPointValueServiceQ6;

	@FindBy(id = "fimFtPointValueServiceQ7")
	public WebElement txtPointValueServiceQ7;

	@FindBy(id = "fimFtPointValueServiceQ8")
	public WebElement txtPointValueServiceQ8;

	@FindBy(id = "fimFtPointValueServiceQ9")
	public WebElement txtPointValueServiceQ9;

	@FindBy(id = "fimFtPointValueServiceQ10")
	public WebElement txtPointValueServiceQ10;

	@FindBy(id = "fimFtPointValueServiceQ11")
	public WebElement txtPointValueServiceQ11;

	@FindBy(id = "fimFtPointValueServiceQ12")
	public WebElement txtPointValueServiceQ12;

	@FindBy(id = "fimFtServiceQuestionScore")
	public WebElement txtServiceQuestionScore;

	@FindBy(id = "fimTaCommentsForService")
	public WebElement txtCommentsForService;

	// Appearance Section

	@FindBy(id = "fimFtPointValueAppearanceQ1")
	public WebElement txtPointValueAppearanceQ1;

	@FindBy(id = "fimFtPointValueAppearanceQ2")
	public WebElement txtPointValueAppearanceQ2;

	@FindBy(id = "fimFtPointValueAppearanceQ3")
	public WebElement txtPointValueAppearanceQ3;

	@FindBy(id = "fimFtPointValueAppearanceQ4")
	public WebElement txtPointValueAppearanceQ4;

	@FindBy(id = "fimFtPointValueAppearanceQ5")
	public WebElement txtPointValueAppearanceQ5;

	@FindBy(id = "fimFtAppearanceQuestionScore")
	public WebElement txtAppearanceQuestionScore;

	@FindBy(id = "fimTaCommentsForAppearance")
	public WebElement txtCommentsForAppearance;

	// Command buttons

	@FindBy(name = "Submit")
	public WebElement btnSubmit;

}
