package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrOtherAddressesPage extends InfoMgrBasePage {

	public InfoMgrOtherAddressesPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "fimTtAddressHeading")
	public WebElement drpAddressHeading;

	@FindBy(id = "fimCbtContactTitle")
	public WebElement drpTitle;

	@FindBy(id = "fimTtFirstName")
	public WebElement txtFirstName;

	@FindBy(id = "fimTtLastName")
	public WebElement txtLastName;

	@FindBy(id = "address_0address")
	public WebElement txtStreetAddress;

	@FindBy(id = "address_0city")
	public WebElement txtCity;

	@FindBy(id = "address_0country")
	public WebElement drpCountry;

	@FindBy(id = "address_0zipcode")
	public WebElement txtZip;

	@FindBy(id = "address_0state")
	public WebElement drpState;

	@FindBy(id = "address_0phoneNumbers")
	public WebElement txtPhoneNumbers;

	@FindBy(id = "address_0extn")
	public WebElement txtPhoneExt;

	@FindBy(id = "address_0faxNumbers")
	public WebElement txtFaxNumber;

	@FindBy(id = "address_0mobileNumbers")
	public WebElement txtMobileNumber;

	@FindBy(id = "address_0emailIds")
	public WebElement txtEmailID;

	@FindBy(xpath = ".//*[@name='Submit']")
	public WebElement btnSubmit ;

}
