package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrOwnersPage extends InfoMgrBasePage {

	public InfoMgrOwnersPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(linkText = "Add Owner")
	public WebElement lnkAddOwner ;

	@FindBy(xpath = ".//*[@value='Add Remark']")
	public WebElement btnAddRemark ;

	@FindBy(xpath = ".//*[@id='cboxIframe']")
	public WebElement frameElement ;

	@FindBy(id = "remarks")
	public WebElement txtRemarks;

	@FindBy(xpath = ".//*[@value='Submit']")
	public WebElement btnSubmit ;

	@FindBy(xpath = ".//*[@value='Close']")
	public WebElement btnClose ;

	@FindBy(xpath = ".//*[@title='Show All']")
	public WebElement lnkShowAll ;

	@FindBy(id = "entityFranchiseeCombo")
	public WebElement drpFranchiseID;

	// Add Owner Section
	@FindBy(id = "owners_0isManager")
	public WebElement drpOwnerTitle;

	@FindBy(id = "ownerTitle")
	public WebElement chkManagingOwner;

	@FindBy(id = "firstName")
	public WebElement txtFirstName;

	@FindBy(id = "lastName")
	public WebElement txtLastName;

	@FindBy(id = "homeAddress")
	public WebElement txtHomeAddress;

	@FindBy(id = "homeCity")
	public WebElement txtHomeCity;

	@FindBy(id = "homeCountry")
	public WebElement drpHomeCountry;

	@FindBy(id = "homeZipCode")
	public WebElement txtHomeZipCode;

	@FindBy(id = "homeState")
	public WebElement drpHomeState;

	@FindBy(id = "otherAddress")
	public WebElement txtOtherAddress;

	@FindBy(id = "otherCity")
	public WebElement txtOtherCity;

	@FindBy(id = "otherCountry")
	public WebElement txtOtherCountry;

	@FindBy(id = "otherZipCode")
	public WebElement txtOtherZip;

	@FindBy(id = "otherState")
	public WebElement txtOtherState;

	@FindBy(id = "phone")
	public WebElement txtpone;

	@FindBy(id = "extn")
	public WebElement txtExtension;

	@FindBy(id = "fax")
	public WebElement txtFax;

	@FindBy(id = "mobile")
	public WebElement txtMobile;

	@FindBy(id = "email")
	public WebElement txtEmail;

	@FindBy(id = "taxpayerID")
	public WebElement txtTaxPayerID;

	@FindBy(id = "typeOfOwnership")
	public WebElement t;

	@FindBy(id = "owners_0storeOwnedPercentage")
	public WebElement txtStoreOwnedPercentage;

	@FindBy(id = "ms-parentcopyCombo")
	public WebElement drpParentCopyCombo;

	@FindBy(id = "spouseFirstName")
	public WebElement txtSpouseFirstName;

	@FindBy(id = "spouseLastName")
	public WebElement txtSpouseLastName;

	@FindBy(id = "spousePhone")
	public WebElement txtSpousePhone;

	@FindBy(id = "spousePhoneExtn")
	public WebElement txtSpouesPhoneExtension;

	@FindBy(id = "spouseMobile")
	public WebElement txtSpouseMobile;

	@FindBy(name = "ownerType")
	public List<WebElement> rdoExistingOwner;

	@FindBy(name = "Submit")
	public WebElement btnAdd;

	// Multi Unit Page specific

	@FindBy(id = "ms-parentexOwnerIDs")
	public WebElement drpMultiCheckboxOwnerID;

	@FindBy(xpath = ".//*[@id='muId']//input[contains(@id , 'muid')]")
	public WebElement txtMuidName;

}
