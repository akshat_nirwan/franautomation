package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrPagination {

	public InfoMgrPagination(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(name = "pageList")
	public WebElement pageListGoto;

	@FindBy(xpath = ".//optgroup[@label='Go to page']")
	public WebElement gotoPagelabel ;

	@FindBy(xpath = ".//*[contains(text(),'Task Checklist')]")
	public WebElement lnkTaskCheckListy ;

	@FindBy(xpath = ".//*[contains(text(),'Equipment Checklist')]")
	public WebElement lnkequipCheckListy ;

	@FindBy(xpath = ".//*[contains(text(),'Document Checklist')]")
	public WebElement lnkDocumentCheckListy ;

	@FindBy(xpath = ".//*[contains(text(),'Picture Checklist')]")
	public WebElement lnkpictureCheckListy ;

	@FindBy(linkText = "Divisional Users")
	public WebElement DivisnalUserTab ;

	@FindBy(linkText = "Franchise Users")
	public WebElement FranchiseUsersUserTab ;

	@FindBy(linkText = "Regional Users")
	public WebElement RegionalUsersUserTab ;

	@FindBy(linkText = "Suppliers")
	public WebElement SuppliersUsersUserTab ;

	@FindBy(linkText = "Groups")
	public WebElement GroupsUsersUserTab ;

}
