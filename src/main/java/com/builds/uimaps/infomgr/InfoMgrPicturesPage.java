package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrPicturesPage extends InfoMgrBasePage {

	public InfoMgrPicturesPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "pictureTitle")
	public WebElement txtpictureTitle;

	@FindBy(id = "pictureFilename")
	public WebElement txtpictureFilename;

	@FindBy(name = "Submit")
	public WebElement btnSubmit;

	@FindBy(name = "Add More")
	public WebElement btnAddMore;

}
