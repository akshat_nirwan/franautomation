package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrQAHistoryPage extends InfoMgrBasePage {

	WebDriver driver;

	public InfoMgrQAHistoryPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	//When QA Tab integration is ON / Set to Yes
	
	@FindBy(id = "visitNumber")
	public WebElement txtvisitNumber;

	@FindBy(id = "ms-parentmasterVisitId")
	public WebElement drpVisitForm;

	@FindBy(id = "searchButton")
	public WebElement btnView;

	@FindBy(linkText = "Create Visit")
	public WebElement lnkCreateVisit ;

		
	//When QA Tab integration is OFF / Set to No
	
	
	@FindBy(id = "fimDdInspectionDate")
	public WebElement dtinspectionDate ;
	
	@FindBy(id = "fimTtInspectionTypeID")
	public WebElement txtinspectionType ;
	
	@FindBy(id = "fimTtInspectorName")
	public WebElement txtfieldConsultant ;

	@FindBy(id = "fimTtExperienceScore")
	public WebElement txtproductQualityScore ;

	@FindBy(id = "fimTtExperienceGrade")
	public WebElement  txtserviceQualityScore;
	
	@FindBy(id = "fimTtAssuranceScore")
	public WebElement txtfacilityScore ;
	
	@FindBy(id = "fimTtAssuranceGrade")
	public WebElement txtadminScore ;
	
	@FindBy(id = "fimDdReInspectionDate")
	public WebElement dtcureDate ;
	
	@FindBy(id = "fimDdNextInspectionDate")
	public WebElement dtnextInspectionDate ;
	
	@FindBy(xpath = ".//*[@value='Yes']")
	public WebElement rdocriticalYes ;
	
	@FindBy(xpath = ".//*[@value='No']")
	public WebElement rdocriticalNo ;
	
	@FindBy(id = "fimTtMtm")
	public WebElement txtMTM ;
	
	@FindBy (id = "fimDocuments_0fimDocumentTitle")
	public WebElement txtdocumentTitle ;
	
	@FindBy (id = "fimDocuments_0fimDocumentAttachment")
	public WebElement documentAttachment ;
	
	@FindBy (name = "Submit")
	public WebElement btnSubmit ;
	
	@FindBy (name = "Cancel")
	public WebElement btnCancel ;
	
	
	
	public InfoMgrVisitPage getVisitPage() {
    return new InfoMgrVisitPage(driver);
		
		
	}
}
