package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrRealStatePage extends InfoMgrBasePage {

	public InfoMgrRealStatePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	// Real Estate Details Section

	@FindBy(id = "fimCbOwnedLeased")
	public WebElement drpOwnedLeased;

	@FindBy(id = "fimTtSiteAddress1")
	public WebElement txtSiteAddress1;

	@FindBy(id = "fimTtSiteAddress2")
	public WebElement txtSiteAddress2;

	@FindBy(id = "fimTtSiteCity")
	public WebElement txtSiteCity;

	@FindBy(id = "address_0country")
	public WebElement drpCountry;

	@FindBy(id = "address_0state")
	public WebElement drpState;

	@FindBy(id = "fimTtSquareFootage")
	public WebElement txtSquareFootage;

	@FindBy(id = "fimTtBuildingDimentionsX")
	public WebElement txtBuildingDimentionsX;

	@FindBy(id = "fimTtBuildingDimentionsY")
	public WebElement txtBuildingDimentionsY;

	@FindBy(id = "fimTtBuildingDimentionsZ")
	public WebElement txtBuildingDimentionsZ;

	@FindBy(id = "fimTtParkingSpaces")
	public WebElement txtParkingSpaces;

	@FindBy(id = "fimTtDealType")
	public WebElement txtDealType;

	@FindBy(id = "fimCbPremisesType")
	public WebElement drpPremisesType;

	@FindBy(id = "fimDdLoiSent")
	public WebElement txtLoiSent;

	@FindBy(id = "fimDdLoiSent")
	public WebElement txtLoiSigned;

	@FindBy(id = "fimDdLeaseSignedDate")
	public WebElement txtLeaseSignedDate;

	@FindBy(id = "fimDdApprovalDate")
	public WebElement txtApprovalDate;

	@FindBy(id = "fimDdLeaseCommencementDate")
	public WebElement txtLeaseCommencementDate;

	@FindBy(id = "fimDdExpirationDate")
	public WebElement txtExpirationDate;

	@FindBy(id = "fimTtMonthlyRent")
	public WebElement txtMonthlyRent;

	@FindBy(id = "fimDdRentIncreases")
	public WebElement txtRentIncreases;

	@FindBy(id = "fimRrAcal")
	public List<WebElement> rdoAcal;

	@FindBy(id = "fimDocuments_0fimDocumentTitle")
	public WebElement txtDocumentTitle;

	@FindBy(id = "fimDocuments_0fimDocumentAttachment")
	public WebElement txtDocumentAttachment;

	// MultiUnit Section Begin
	@FindBy(id = "fimMuDocuments_0fimDocumentTitle")
	public WebElement txtMultiUnitDocumentTitle;

	@FindBy(id = "fimMuDocuments_0fimDocumentAttachment")
	public WebElement txtMultiUnitDocumentAttachment;

	// MultiUnit Section End

	// Regional Section Begin
	@FindBy(id = "areaDocuments_0fimDocumentTitle")
	public WebElement txtAreaDocumentTitle;

	@FindBy(id = "areaDocuments_0fimDocumentAttachment")
	public WebElement txtAreaDocumentAttachment;

	// Regional Section End

	// Lease Renewal Details Section

	@FindBy(id = "fimTtRenewalTermFirst")
	public WebElement txtRenewalTermFirst;

	@FindBy(id = "fimDdRenewalDueDateFirst")
	public WebElement txtRenewalDueDateFirst;

	@FindBy(id = "fimTtRenewalFeeFirst")
	public WebElement txtRenewalFeeFirst;

	@FindBy(id = "fimDdRenewalFeePaidFirst")
	public WebElement txtRenewalFeePaidFirst;

	@FindBy(id = "fimTtRenewalTermSecond")
	public WebElement txtRenewalTermSecond;

	@FindBy(id = "fimDdRenewalDueDateSecond")
	public WebElement txtRenewalDueDateSecond;

	@FindBy(id = "fimTtRenewalFeeSecond")
	public WebElement txtRenewalFeeSecond;

	@FindBy(id = "fimDdRenewalFeePaidSecond")
	public WebElement txtRenewalFeePaidSecond;

	@FindBy(id = "fimTtRenewalTermThird")
	public WebElement txtRenewalTermThird;

	@FindBy(id = "fimDdRenewalDueDateThird")
	public WebElement txtRenewalDueDateThird;

	@FindBy(id = "fimTtRenewalFeeThird")
	public WebElement txtRenewalFeeThird;

	@FindBy(id = "fimDdRenewalFeePaidThird")
	public WebElement txtRenewalFeePaidThird;

	@FindBy(id = "fimTaRenewalOptions")
	public WebElement txtRenewalOptions;

	@FindBy(id = "fimRrPurchaseOption")
	public List<WebElement> rdoPurchaseOption;

	@FindBy(id = "fimDdProjectedOpeningDate")
	public WebElement txtProjectedOpeningDate;

	@FindBy(id = "fimRrGeneralContractorSelector")
	public List<WebElement> rdoGeneralContractorSelector;

	@FindBy(id = "fimTtNameGeneralContractor")
	public WebElement txtNameGeneralContractor;

	@FindBy(id = "fimTtAddressGeneralContractor")
	public WebElement txtAddressGeneralContractor;

	@FindBy(id = "fimDdPermitApplied")
	public WebElement txtPermitApplied;

	@FindBy(id = "fimDdPermitIssued")
	public WebElement txtPermitIssued;

	@FindBy(id = "fimDdCertificate")
	public WebElement txtCertificate;

	@FindBy(id = "fimDdTurnOverDate")
	public WebElement txtTurnOverDate;

	@FindBy(id = "fimDdGrandOpeningDate")
	public WebElement txtGrandOpeningDate;

	// Lessor Details Section

	@FindBy(id = "fimCbtContactTitle")
	public WebElement drpLessorTitle;

	@FindBy(id = "fimTtFirstName")
	public WebElement txtLessorFirstName;

	@FindBy(id = "fimTtLastName")
	public WebElement txtLessorLastName;

	@FindBy(id = "address_1address")
	public WebElement txtLessorStreetAddress;

	@FindBy(id = "address_1city")
	public WebElement txtLessorCity;

	@FindBy(id = "address_1country")
	public WebElement drpLessorCountry;

	@FindBy(id = "address_1zipcode")
	public WebElement txtLessorZipCode;

	@FindBy(id = "address_1state")
	public WebElement drpLessorState;

	@FindBy(id = "address_1phoneNumbers")
	public WebElement txtLessorPhoneNumbers;

	@FindBy(id = "address_1extn")
	public WebElement txtLessorExtension;

	@FindBy(id = "address_1faxNumbers")
	public WebElement txtLessorFaxNumbers;

	@FindBy(id = "address_1mobileNumbers")
	public WebElement txtLessorMobileNumbers;

	@FindBy(id = "address_1emailIds")
	public WebElement txtLessorEmailIds;

	// SubLessor Details Section

	@FindBy(id = "fimCbtSublessorContactTitle")
	public WebElement drpSubLessorTitle;

	@FindBy(id = "fimTtSublessorFirstName")
	public WebElement txtSublessorFirstName;

	@FindBy(id = "fimTtSublessorLastName")
	public WebElement txtSublessorLastName;

	@FindBy(id = "address_2address")
	public WebElement txtSubLessorStreetAddress;

	@FindBy(id = "address_2city")
	public WebElement txtSubLessorCity;

	@FindBy(id = "address_2country")
	public WebElement drpSubLessorCountry;

	@FindBy(id = "address_2zipcode")
	public WebElement txtSubLessorZipCode;

	@FindBy(id = "address_2state")
	public WebElement drpSubLessorState;

	@FindBy(id = "address_2phoneNumbers")
	public WebElement txtSubLessorPhoneNumbers;

	@FindBy(id = "address_2extn")
	public WebElement txtSubLessorExtension;

	@FindBy(id = "address_2faxNumbers")
	public WebElement txtSubLessorFaxNumbers;

	@FindBy(id = "address_2mobileNumbers")
	public WebElement txtSubLessorMobileNumbers;

	@FindBy(id = "address_2emailIds")
	public WebElement txtSubLessorEmailIds;

	// Primary Tenant Details Section

	@FindBy(id = "fimCbtTenantTitle")
	public WebElement drpTenantTitle;

	@FindBy(id = "fimTtTenantFirstName")
	public WebElement txtTenantFirstName;

	@FindBy(id = "fimTtTenantLastName")
	public WebElement txtTenantLastName;

	@FindBy(id = "address_3address")
	public WebElement txtTenantStreetAddress;

	@FindBy(id = "address_3city")
	public WebElement txtTenantCity;

	@FindBy(id = "address_3country")
	public WebElement drpTenantCountry;

	@FindBy(id = "address_3zipcode")
	public WebElement txtTenantZipCode;

	@FindBy(id = "address_3state")
	public WebElement drpTenantState;

	@FindBy(id = "address_3phoneNumbers")
	public WebElement txtTenantPhoneNumbers;

	@FindBy(id = "address_3extn")
	public WebElement txtTenantExtension;

	@FindBy(id = "address_3faxNumbers")
	public WebElement txtTenantFaxNumbers;

	@FindBy(id = "address_3mobileNumbers")
	public WebElement txtTenantMobileNumbers;

	@FindBy(id = "address_3emailIds")
	public WebElement txtTenantEmailIds;

	// Command Buttons

	@FindBy(name = "Submit")
	public WebElement btnAdd;

}
