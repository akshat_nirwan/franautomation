package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrRenewalPage extends InfoMgrBasePage {

	public InfoMgrRenewalPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "fimCbCurrentStatus")
	public WebElement fimCbCurrentStatus;

	@FindBy(id = "fimDdAsOf")
	public WebElement txtAsOf;

	@FindBy(id = "fimTtPersonSigning")
	public WebElement txtPersonSigning;

	@FindBy(id = "fimTtTitle")
	public WebElement txtTitle;

	@FindBy(id = "fimDdNewExpirationDate")
	public WebElement txtNewExpirationDate;

	@FindBy(id = "fimDdNinetyDayNotice")
	public WebElement txtNinetyDayNotice;

	@FindBy(id = "fimDdRenewalPackageSent")
	public WebElement txtRenewalPackageSent;

	@FindBy(id = "fimTtRenewalMaterials")
	public WebElement txtRenewalMaterials;

	@FindBy(id = "fimTaMissingDocs")
	public WebElement txtMissingDocs;

	@FindBy(id = "fimTaNotes")
	public WebElement txtNotes;

	// Command Button

	@FindBy(name = "Submit")
	public WebElement btnAdd;

}
