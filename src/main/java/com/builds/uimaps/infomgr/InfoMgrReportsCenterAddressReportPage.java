package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrReportsCenterAddressReportPage {

	public InfoMgrReportsCenterAddressReportPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "ms-parentfranchiseeCombo")
	public WebElement drpFranchiseID;

	@FindBy(id = "ms-parentownerId")
	public WebElement drpOwnerName;

	@FindBy(id = "isManager")
	public WebElement drpIsManagingOwner;

	@FindBy(id = "ms-parentfranchiseeType")
	public WebElement drpFranchiseType;

	@FindBy(id = "storeStatus")
	public WebElement drpStatus;

	@FindBy(id = "matchType1")
	public WebElement drpEffectiveDate;

	@FindBy(id = "matchType2")
	public WebElement drpExpirationDate;

	@FindBy(id = "ms-parentcomboDivision")
	public WebElement drpDivision;

	@FindBy(id = "button")
	public WebElement btnViewReport;

	@FindBy(xpath = ".//a[@original-title='Generate Analytical Reports']")
	public WebElement reportsLnk ;

	// Harish Dwivedi

	@FindBy(id = "ms-parentregionCombo")
	public WebElement drpRegionName;

	@FindBy(id = "ms-parentcompanyNameId")
	public WebElement drpEntityName;

	@FindBy(xpath = ".//*[@class='nlist02']/li/a[contains(text(),'Center Address Report')]")
	public WebElement CenterAddressReport ;

	@FindBy(xpath = ".//*[@class='nlist02']/li/a[contains(text(),'Center Summary Report')]")
	public WebElement CenterSumarysReport ;

	@FindBy(xpath = ".//*[@class='nlist02']/li/a[contains(text(),'Franchisee-To-Owner Report')]")
	public WebElement FranchiseeToOwnerReport ;

	@FindBy(xpath = ".//*[@class='nlist02']/li/a[contains(text(),'Owner Details Report')]")
	public WebElement OwnerDetailsReport ;

	@FindBy(xpath = ".//*[@class='nlist02']/li/a[contains(text(),'Entity Details Report')]")
	public WebElement EntityDetailsReport ;

	@FindBy(xpath = ".//*[@class='nlist02']/li/a[contains(text(),'Email Status Report')]")
	public WebElement EmailStatusReport ;

	@FindBy(xpath = ".//*[@class='nlist02']/li/a[contains(text(),'Contact History-Call Report')]")
	public WebElement ContactHistoryCallReport ;

	@FindBy(xpath = ".//*[@class='nlist02']/li/a[contains(text(),'Legal Violation Report')]")
	public WebElement LegalViolationReport ;

	@FindBy(xpath = ".//*[@class='nlist02']/li/a[contains(text(),'Default and Termination Report')]")
	public WebElement DefaultandTerminationReport ;

	@FindBy(xpath = ".//*[@class='nlist02']/li/a[contains(text(),'Franchise / Store Status Report')]")
	public WebElement FranchiseStoreStatusReport ;

	@FindBy(id = "matchType3")
	public WebElement drpmatchType3;
	
	@FindBy(id = "date3")
	public WebElement TerminationDate;

	@FindBy(xpath = ".//*[@class='nlist02']/li/a[contains(text(),'FDD Store / Franchise List')]")
	public WebElement FDDStoreFranchiseList ;

	@FindBy(xpath = ".//*[@class='nlist02']/li/a[contains(text(),'Birthday Report')]")
	public WebElement Birthday_Report ;

	@FindBy(id = "matchType10")
	public WebElement matchType10;
	
	@FindBy(id = "anniversaryMonth2")
	public WebElement BirthMonth;
	
	@FindBy(id = "anniversaryDate2")
	public WebElement BirthDate;
	
	@FindBy(xpath = ".//*[@class='nlist02']/li/a[contains(text(),'Renewal Status Report')]")
	public WebElement RenewalStatusReport ;

	@FindBy(xpath = ".//*[@class='nlist02']/li/a[contains(text(),'Renewals Due Report')]")
	public WebElement RenewalDuesReport ;

	@FindBy(xpath = ".//*[@class='nlist02']/li/a[contains(text(),'Legal Report')]")
	public WebElement LegalReport ;

	@FindBy(xpath = ".//*[@class='nlist02']/li/a[contains(text(),'Transfer Report')]")
	public WebElement TransferReport ;
}
