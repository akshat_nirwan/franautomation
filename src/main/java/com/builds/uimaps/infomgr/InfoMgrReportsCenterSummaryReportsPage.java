package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrReportsCenterSummaryReportsPage {

	public InfoMgrReportsCenterSummaryReportsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "franchiseeCombo")
	public WebElement drpFranchiseID;

	@FindBy(id = "button")
	public WebElement btnViewReport;

}
