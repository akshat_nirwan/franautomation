package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrReportsContactHistoryCallReportPage {

	public InfoMgrReportsContactHistoryCallReportPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "ms-parentfranchiseeCombo")
	public WebElement drpFranchiseId;

	@FindBy(id = "ms-parentownerId")
	public WebElement drpOwnerId;

	@FindBy(id = "ms-parentuserCombo")
	public WebElement drpLoggedBy;

	@FindBy(id = "ms-parentcallStatus")
	public WebElement drpCallStatus;

	@FindBy(id = "ms-parentcallType")
	public WebElement drpCallType;

	@FindBy(id = "ms-parentcomboDivision")
	public WebElement drpDivision;

	@FindBy(name = "subject")
	public WebElement txtSubject;

	@FindBy(name = "remark")
	public WebElement txtRemark;

	@FindBy(id = "matchType1")
	public WebElement drpCallDate;

	@FindBy(id = "button")
	public WebElement btnViewReport;

}
