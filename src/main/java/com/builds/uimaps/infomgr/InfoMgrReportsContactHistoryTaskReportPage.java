package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrReportsContactHistoryTaskReportPage extends InfoMgrBasePage {

	public InfoMgrReportsContactHistoryTaskReportPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = ".//*[@id='recordTable']//table//table[2]//tr")
	public List<WebElement> lstRecords;

	@FindBy(linkText = "Assigned To")
	public WebElement lnkAssignedTo ;

	@FindBy(id = "ms-parentfranchiseeCombo")
	public WebElement drpFranchiseID;

	@FindBy(id = "button")
	public WebElement btnViewReport;

}
