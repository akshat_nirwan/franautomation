package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrReportsCustomerComplaintsReportPage extends InfoMgrBasePage {

	public InfoMgrReportsCustomerComplaintsReportPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "ms-parentcomplainCombo")
	public WebElement complaintIdsDiv;

	@FindBy(id = "button")
	public WebElement btnViewReport;

	@FindBy(name = "fimTtComplaintBy")
	public WebElement txtComplainant;

	@FindBy(id = "recordTable")
	public WebElement tblComplaintsSummary;

	@FindBy(xpath = ".//*[@id='recordTable']//table[2]//tr[2]//td[3]")
	public WebElement tdComplaintID ;

	@FindBy(xpath = ".//*[@id='recordTable']//table[2]//tr[2]//td[7]")
	public WebElement tdComplainantName ;

	@FindBy(xpath = ".//*[@id='recordTable']//table[2]//tr//td[9]")
	public List<WebElement> tdStatus;

	@FindBy(linkText = "Next")
	public WebElement lnkNext ;

	@FindBy(id = "ms-parentfranchiseeCombo")
	public WebElement drpFranchiseID;
	// public WebElement drpFranchiseID=DDDDDDDDDDDDDDDDDDDDD

	@FindBy(id = "ms-parentfimCbStatus")
	public WebElement drpStatus;

	@FindBy(id = "saveViewButton")
	public WebElement btnSaveView;

}
