package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrReportsDefaultandTerminationReportPage {

	public InfoMgrReportsDefaultandTerminationReportPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "ms-parentfranchiseeCombo")
	public WebElement drpFranchiseId;

	@FindBy(id = "ms-parentownerId")
	public WebElement drpOwnerId;

	@FindBy(id = "ms-parentfranchiseeType")
	public WebElement drpFranchiseTypeId;

	@FindBy(id = "ms-parentfimCbReason")
	public WebElement drpReason;

	@FindBy(id = "ms-parentfimCbTypeOfAction")
	public WebElement drpTypeOfAction;

	@FindBy(id = "ms-parentcomboDivision")
	public WebElement drpDivision;

	@FindBy(id = "matchType1")
	public WebElement drpDateSent;

	@FindBy(id = "matchType2")
	public WebElement drpDateCured;

	@FindBy(id = "matchType3")
	public WebElement drpDateTerminated;

	@FindBy(id = "button")
	public WebElement btnViewReport;

}
