package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrReportsEmailStatusReportPage {

	public InfoMgrReportsEmailStatusReportPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "ms-parentuserCombo")
	public WebElement drpUser;

	@FindBy(id = "ms-parentcomboDivision")
	public WebElement drpDivision;

	@FindBy(id = "matchType1")
	public WebElement drpSendDate;

	@FindBy(id = "button")
	public WebElement btnViewReport;

}
