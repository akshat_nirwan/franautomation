package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrReportsFDDItem20ReportPage {

	public InfoMgrReportsFDDItem20ReportPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "reportId")
	public WebElement drpReportType;

	@FindBy(id = "ms-parentcountry")
	public WebElement drpCountry;

	@FindBy(id = "fromDate")
	public WebElement txtFinancialYearFromDate;

	@FindBy(id = "lastDate")
	public WebElement txtFinancialYearToDate;

	@FindBy(id = "ms-parentcomboDivision")
	public WebElement drpDivision;

	@FindBy(id = "button")
	public WebElement btnViewReport;

}
