package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrReportsFranchiseStoreStatusReportPage {

	public InfoMgrReportsFranchiseStoreStatusReportPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "matchType1")
	public WebElement drpTermExpirationDate;

	@FindBy(id = "matchType2")
	public WebElement drpClosingDate;

	@FindBy(id = "matchType3")
	public WebElement drpTerminationDate;

	@FindBy(id = "matchType4")
	public WebElement drpTransferDate;

	@FindBy(id = "button")
	public WebElement btnViewReport;

}
