package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrReportsLegalViolationReportPage {

	public InfoMgrReportsLegalViolationReportPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "ms-parentfranchiseeCombo")
	public WebElement drpFranchiseId;

	@FindBy(id = "ms-parentownerId")
	public WebElement drpOwnerName;

	@FindBy(id = "ms-parentfranchiseeType")
	public WebElement drpFranchiseType;

	@FindBy(id = "ms-parentfimCbComplaintType")
	public WebElement drpComplaintType;

	@FindBy(id = "ms-parentstoreStatus")
	public WebElement drpStoreStatus;

	@FindBy(id = "ms-parentcomboDivision")
	public WebElement drpDivision;

	@FindBy(id = "button")
	public WebElement btnViewReport;

}
