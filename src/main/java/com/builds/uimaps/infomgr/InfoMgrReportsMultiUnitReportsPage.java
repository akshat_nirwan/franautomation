package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrReportsMultiUnitReportsPage extends InfoMgrBasePage {

	public InfoMgrReportsMultiUnitReportsPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);

	}

	@FindBy(id = "ms-parentfranchiseeCombo")
	public WebElement drpMultiCheckBoxRegion;

	@FindBy(id = "ms-parentownerId")
	public WebElement drpMultiCheckBoxMultiUnitOwner;

	@FindBy(id = "ms-parentcomboDivision")
	public WebElement drpMultiCheckBoxDivision;

	@FindBy(id = "ms-parentcountry")
	public WebElement drpMultiCheckBoxCountry;

	@FindBy(id = "ms-parentstate")
	public WebElement drpMultiCheckBoxState;

	@FindBy(id = "button")
	public WebElement btnViewReport;

	@FindBy(id = "saveViewButton")
	public WebElement btnSaveView;

	@FindBy(id = "recordTable")
	public WebElement tblRecord;

}
