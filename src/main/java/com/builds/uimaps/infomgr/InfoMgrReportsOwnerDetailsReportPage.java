package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrReportsOwnerDetailsReportPage {

	public InfoMgrReportsOwnerDetailsReportPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "ms-parentfranchiseeCombo")
	public WebElement drpFranchiseID;

	@FindBy(id = "ms-parentownerId")
	public WebElement drpOwnerID;

	@FindBy(id = "ms-parentcountry")
	public WebElement drpCountry;

	@FindBy(id = "ms-parentstate")
	public WebElement drpState;

}
