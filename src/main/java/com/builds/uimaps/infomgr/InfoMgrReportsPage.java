package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrReportsPage {

	WebDriver driver;

	public InfoMgrReportsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	@FindBy(linkText = "Center Address Report")
	public WebElement lnkCenterAddressReport ;

	@FindBy(linkText = "Center Summary Report")
	public WebElement lnkCenterSummaryReport ;

	@FindBy(linkText = "Franchisee-To-Owner Report")
	public WebElement lnkFranchiseeToOwnerReport ;

	@FindBy(linkText = "Owner Details Report")
	public WebElement lnkOwnerDetailsReport ;

	@FindBy(linkText = "Multi-Unit Report")
	public WebElement lnkMultiUnitReport ;

	@FindBy(linkText = "Entity Details Report")
	public WebElement lnkEntityDetailsReport ;

	@FindBy(linkText = "Birthday Report")
	public WebElement lnkBirthdayReport ;

	@FindBy(linkText = "Email Status Report")
	public WebElement lnkEmailStatusReport ;

	@FindBy(linkText = "Contact History-Call Report")
	public WebElement lnkContactHistoryCallReport ;

	@FindBy(linkText = "Contact History-Task Report")
	public WebElement linkContactHistoryTaskReports ;

	@FindBy(linkText = "Customer Complaints Report")
	public WebElement lnkCustComplaintRpt ;

	@FindBy(linkText = "Legal Violation Report")
	public WebElement lnkLegalViolationReport ;

	@FindBy(linkText = "Legal Report")
	public WebElement lnkLegalReport ;

	@FindBy(linkText = "Renewal Status Report")
	public WebElement lnkRenewalStatusReport ;

	@FindBy(linkText = "Renewal Dues Report")
	public WebElement lnkRenewalDuesReport ;

	@FindBy(linkText = "Transfer Report")
	public WebElement lnkTransferReport ;

	@FindBy(linkText = "Default and Termination Report")
	public WebElement lnkDefaultandTerminationReport ;

	@FindBy(linkText = "Franchise / Store Status Report")
	public WebElement lnkFranchiseStoreStatusReport ;

	@FindBy(linkText = "FDD Store / Franchise List")
	public WebElement lnkFDDStoreFranchiseList ;

	@FindBy(linkText = "FDD Item 20 Report")
	public WebElement lnkFDDItem20Report ;

	@FindBy(linkText = "Number of Emails Sent Report")
	public WebElement lnkNumberofEmailsSentReport ;

	@FindBy(linkText = "Click Through Rate - Number of Clickthroughs / Number of emails opened Report")
	public WebElement lnkNoOfClickthroughsNumberofemailsopenedRpts ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@value='Create New Report']")
	public WebElement lnkNewReport ;

	// Anukaran
	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@name='Next']")
	public WebElement lnkNext;

	// Anukaran
	@FindBy(xpath = ".//*[@id='customReportName']")
	public WebElement reportName;

	// Anukaran
	@FindBy(xpath = ".//*[@id='comments']")
	public WebElement reportDescription;

	// Anukaran
	@FindBy(xpath = ".//*[@id='srcCrtDiv']//input[@name='go']")
	public WebElement lnkGo;

	// Anukaran
	@FindBy(xpath = ".//*[@id='sltFldDiv']//input[@name='go']")
	public WebElement lnkGo1;

	// Anukaran
	@FindBy(xpath = ".//*[@id='ms-parentFRANCHISEE_countryID']")
	public WebElement centerInfoCountry;

	// Anukaran
	@FindBy(xpath = ".//*[@id='ms-parentFRANCHISEE_regionNo']")
	public WebElement centerInfoState;

	// Anukaran
	@FindBy(xpath = ".//*[@id='savRptDiv']//input[@value='Save']")
	public WebElement saveBtn;

}
