package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrReportsRenewalStatusReportPage {

	public InfoMgrReportsRenewalStatusReportPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "ms-parentfranchiseeCombo")
	public WebElement drpFranchiseId;

	@FindBy(id = "ms-parentownerId")
	public WebElement drpOwnerId;

	@FindBy(id = "ms-parentfimCbCurrentStatus")
	public WebElement drpCurrentStatus;

	@FindBy(id = "ms-parentfranchiseeType")
	public WebElement drpFranchiseeType;

	@FindBy(id = "ms-parentstoreStatus")
	public WebElement drpStoreStatus;

	@FindBy(id = "ms-parentcomboDivision")
	public WebElement drpDivision;

	@FindBy(id = "matchType1")
	public WebElement drpExpirationDate;

	@FindBy(id = "matchType2")
	public WebElement drpAsOfDate;

	@FindBy(id = "button")
	public WebElement btnViewReport;

}
