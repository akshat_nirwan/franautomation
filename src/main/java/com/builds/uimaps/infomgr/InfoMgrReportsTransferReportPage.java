package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrReportsTransferReportPage {

	public InfoMgrReportsTransferReportPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "recordTable")
	public WebElement tblRecords;

}
