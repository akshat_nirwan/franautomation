package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrSearchFilterPage {

	public InfoMgrSearchFilterPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "ms-parentadId")
	public WebElement drpAreaOwner;

	@FindBy(id = "search")
	public WebElement btnSearch;

}
