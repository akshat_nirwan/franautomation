package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrSearchPage {
	WebDriver driver;

	public InfoMgrSearchPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "franchiseID")
	public WebElement txtFranchiseID;

	@FindBy(id = "ms-parentfranchiseeType")
	public WebElement drpFranchiseeType;

	@FindBy(name = "centerName")
	public WebElement txtCenterName;

	@FindBy(id = "city")
	public WebElement txtCity;

	@FindBy(id = "ms-parentcountry")
	public WebElement drpCountry;

	@FindBy(id = "zipcode")
	public WebElement txtZipCode;

	@FindBy(id = "ms-parentstate")
	public WebElement drpState;

	@FindBy(id = "phoneNumbers")
	public WebElement txtPhoneNumbers;

	@FindBy(id = "emailIds")
	public WebElement txtEmailIds;

	@FindBy(name = "firstName")
	public WebElement txtFirstName;

	@FindBy(name = "lastName")
	public WebElement txtLastName;

	@FindBy(name = "spouseFirstName")
	public WebElement txtSpouseFirstName;

	@FindBy(name = "spouseLastName")
	public WebElement txtSpouseLastName;

	@FindBy(id = "ms-parentmultipleBrand")
	public WebElement drpDivision;

	@FindBy(name = "Search")
	public WebElement btnSearch;

	@FindBy(xpath = ".//*[@value='Yes'][@type='button']")
	public WebElement btnYes ;

	@FindBy(xpath = ".//*[@value='Close'][@type='button']")
	public WebElement btnClose ;

	public InfoMgrSearchResultPage getSearchResultPage() {
		return new InfoMgrSearchResultPage(driver);
	}

}
