package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrSearchResultPage {

	public InfoMgrSearchResultPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "(.//*[@id='siteMainTable']//table)[9]")
	public WebElement tblFranchisees;

	@FindBy(xpath = "(.//*[@id='siteMainTable']//table)[9]/tbody/tr")
	public List<WebElement> tblFranchiseesRows;

}
