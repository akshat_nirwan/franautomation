package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrSendEmailPage {

	public InfoMgrSendEmailPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "mailTo")
	public WebElement txtToEmail;

	@FindBy(id = "subject")
	public WebElement txtSubject;

	@FindBy(id = "mailMessage")
	public WebElement mailMessage;

	@FindBy(name = "radioCheckMsg")
	public List<WebElement> rdoTxtMail;

	@FindBy(xpath = ".//*[@name='radioCheckMsg' and @value='0']")
	public List<WebElement> rdoTxtMailText;

	@FindBy(xpath = ".//*[@value='Send']")
	public WebElement btnSend ;
	
	@FindBy(xpath = ".//*[@id='tinymce']")
	public WebElement htmltxtArea;
}
