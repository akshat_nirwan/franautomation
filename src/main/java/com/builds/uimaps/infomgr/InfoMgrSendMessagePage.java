package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrSendMessagePage {

	public InfoMgrSendMessagePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "theText")
	public WebElement txtToEmail;

	@FindBy(name = "sSubject")
	public WebElement txtSubject;

	@FindBy(id = "tinymce")
	public WebElement txtMessageHtml;

	@FindBy(name = "Button32")
	public WebElement btnSend;

	@FindBy(name = "radioCheckMsg")
	public List<WebElement> rdoTxtMsg;

	@FindBy(id = "sDetail")
	public WebElement txtMessageText;

	@FindBy(name = "Button")
	public WebElement btnOk;

}
