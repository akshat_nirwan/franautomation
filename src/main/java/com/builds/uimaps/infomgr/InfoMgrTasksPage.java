package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrTasksPage {

	WebDriver driver;

	public InfoMgrTasksPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "category")
	public WebElement drpCategory;

	@FindBy(id = "ms-parentfranchiseeIDs")
	public WebElement drpFranchisee;

	@FindBy(id = "ms-parentfranchiseeOwners")
	public WebElement drpOwner;

	@FindBy(xpath = ".//*[@value='Continue']")
	public WebElement btnContinue ;

	@FindBy(xpath = ".//*[@name='print3'][@value='Delete']")
	public WebElement btnDelete ;

	@FindBy(xpath = ".//*[@name='print2'][@value='Print']")
	public WebElement btnPrint ;

	@FindBy(id = "subject")
	public WebElement txtSubject;

	@FindBy(id = "go")
	public WebElement btnSearch;

	// Filter Section
	@FindBy(id = "category")
	public WebElement drpCategoryFilter;

	@FindBy(id = "subject")
	public WebElement txtSubjectFilter;

	@FindBy(id = "ms-parentstatus")
	public WebElement drpStatus;

	@FindBy(id = "reminder")
	public WebElement drpReminder;

	@FindBy(id = "ms-parentpriority")
	public WebElement drpPriority;

	@FindBy(id = "ms-parentmineall")
	public WebElement drpView;

	@FindBy(id = "ms-parenttaskType")
	public WebElement drpTaskType;
	
	@FindBy(id = "ms-parentassignTo")
	public WebElement AssignTo;
	
	@FindBy(id = "comments")
	public WebElement comments;

	public InfoMgrLogATaskPage getLogaTaskPage() {
		return new InfoMgrLogATaskPage(driver);
	}

}
