package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrTerminatedPage {

	WebDriver driver;

	public InfoMgrTerminatedPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	@FindBy(name = "fimSearchString")
	public WebElement txtSearchFranchisee;

	@FindBy(xpath = ".//*[@title='Show All']")
	public WebElement lnkShowAll ;

	@FindBy(xpath = ".//*[@id='fimReportTable']//a/img[contains(@original-title,'Search')]")
	public WebElement imgFranchiseesSearchButton ;

	@FindBy(xpath = ".//*[@value='Mail Merge']")
	public WebElement btnMailMerge ;

	@FindBy(xpath = ".//*[@value='Log a Task']")
	public WebElement btnLogATask ;

	@FindBy(xpath = ".//*[@value='Log a Call']")
	public WebElement btnLogACall ;

	@FindBy(xpath = ".//*[@value='Send Email']")
	public WebElement btnSendMail ;

	@FindBy(xpath = ".//*[@value='Add To Group']")
	public WebElement btnAddtoGroup ;

	@FindBy(xpath = ".//*[@value='Print']")
	public WebElement btnPrint ;

	@FindBy(xpath = ".//*[@value='Archive']")
	public WebElement btnArchive ;

	@FindBy(xpath = ".//*[@value='Export As Excel']")
	public WebElement btnExportAsExcel ;

	public InfoMgrAddressesPage getAddressPage() {
		return new InfoMgrAddressesPage(this.driver);
	}

	public InfoMgrAgreementPage getAgreementPage() {
		return new InfoMgrAgreementPage(this.driver);
	}

	public InfoMgrCenterInfoPage getCenterInfoPage() {
		return new InfoMgrCenterInfoPage(this.driver);
	}

	public InfoMgrContactHistoryPage getContactHistoryPage() {
		return new InfoMgrContactHistoryPage(this.driver);
	}

	public InfoMgrContractSigningPage getContractSigningPage() {
		return new InfoMgrContractSigningPage(driver);
	}

	public InfoMgrCustomerComplaintsPage getCustomerComplaintsPage() {
		return new InfoMgrCustomerComplaintsPage(driver);
	}

	public InfoMgrDefaultAndTerminationPage getDefaultAndTerminationPage() {
		return new InfoMgrDefaultAndTerminationPage(driver);
	}

	public InfoMgrDocumentsPage InfoMgrDocumentsPage() {
		return new InfoMgrDocumentsPage(driver);
	}

	public InfoMgrEmployeesPage getEmployeesPage() {
		return new InfoMgrEmployeesPage(driver);
	}

	public InfoMgrEntityDetailsPage getEntityDetailsPage() {
		return new InfoMgrEntityDetailsPage(driver);
	}

	public InfoMgrEventsPage getEventsPage() {
		return new InfoMgrEventsPage(driver);
	}

	public InfoMgrFinancialsPage getFinancialsPage() {
		return new InfoMgrFinancialsPage(driver);
	}

	public InfoMgrGuarantorsPage getGuarantorsPage() {
		return new InfoMgrGuarantorsPage(driver);
	}

	public InfoMgrInsurancePage getInsurancePage() {
		return new InfoMgrInsurancePage(driver);
	}

	public InfoMgrLegalViolationPage getLegalViolationPage() {
		return new InfoMgrLegalViolationPage(driver);
	}

	public InfoMgrLendersPage getLendersPage() {
		return new InfoMgrLendersPage(driver);
	}

	public InfoMgrMarketingPage getMarketingPage() {
		return new InfoMgrMarketingPage(driver);
	}

	public InfoMgrMystryReviewPage getMystryReviewPage() {
		return new InfoMgrMystryReviewPage(driver);
	}

	public InfoMgrOwnersPage getOwnersPage() {
		return new InfoMgrOwnersPage(driver);
	}

	public InfoMgrPicturesPage getPicturesPage() {
		return new InfoMgrPicturesPage(driver);
	}

	public InfoMgrPreviousFranchiseesPage getPreviousFranchiseesPage() {
		return new InfoMgrPreviousFranchiseesPage(driver);
	}

	public InfoMgrQAHistoryPage getQAHistoryPage() {
		return new InfoMgrQAHistoryPage(driver);
	}

	public InfoMgrRealStatePage getRealStatePage() {
		return new InfoMgrRealStatePage(driver);
	}

	public InfoMgrRenewalPage getRenewalPage() {
		return new InfoMgrRenewalPage(driver);
	}

	public InfoMgrTerritoryPage getTerritoryPage() {
		return new InfoMgrTerritoryPage(driver);
	}

	public InfoMgrTrainingPage getTrainingPage() {
		return new InfoMgrTrainingPage(driver);
	}

	public InfoMgrTransferPage getTransferPage() {
		return new InfoMgrTransferPage(driver);
	}

	public InfoMgrUsersPage getUsersPage() {
		return new InfoMgrUsersPage(driver);
	}

	public InfoMgrEmailSummaryPage getEmailSummaryPage() {
		return new InfoMgrEmailSummaryPage(driver);
	}

	public InfoMgrCustomTabPage getCustomTabPage() {
		return new InfoMgrCustomTabPage(driver);
	}

	public InfoMgrSearchFilterPage getSearchFilterPage() {
		return new InfoMgrSearchFilterPage(driver);
	}

	public InfoMgrCenterSummaryPage getCenterSummaryPage() {
		return new InfoMgrCenterSummaryPage(driver);
	}

	public InfoMgrLogACallPage getLogACallPage() {
		return new InfoMgrLogACallPage(driver);
	}

	public InfoMgrLogATaskPage getLogATaskPage() {
		return new InfoMgrLogATaskPage(driver);
	}

	public InfoMgrDocumentsPage getDocumentsPage() {
		return new InfoMgrDocumentsPage(driver);
	}

	public InfoMgrSendEmailPage getSendMailPage() {
		return new InfoMgrSendEmailPage(driver);
	}

	public InfoMgrSendMessagePage getSendMessagePage() {
		return new InfoMgrSendMessagePage(driver);
	}

	public InfoMgrFranchiseeFilterPage getFranchiseeFilterPage() {
		return new InfoMgrFranchiseeFilterPage(driver);
	}

}
