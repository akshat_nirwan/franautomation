package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrTerritoryPage extends InfoMgrBasePage {

	public InfoMgrTerritoryPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	// Territory Details Section

	@FindBy(id = "fimTtTypeTerritory")
	public List<WebElement> rdoTypeTerritory;

	@FindBy(id = "fimTtSalesRequirement")
	public WebElement txtSalesRequirement;

	@FindBy(id = "fimTtRestrictions")
	public WebElement txtRestrictions;

	@FindBy(id = "fimTtMarketingObligation")
	public WebElement txtMarketingObligation;

	@FindBy(id = "fimTaOwned")
	public WebElement txtOwned;

	@FindBy(id = "fimTtJuridiction")
	public WebElement txtJuridiction;

	@FindBy(id = "fimTaDisputes")
	public WebElement txtDisputes;

	@FindBy(id = "fimTaNotes")
	public WebElement txtNotes;

	// Geography Section

	@FindBy(id = "fimTtGeoCoordinates")
	public WebElement txtGeoCoordinates;

	@FindBy(id = "fimTtLocation")
	public WebElement txtLocation;

	@FindBy(id = "fimTtCounty")
	public WebElement txtCounty;

	@FindBy(id = "fimTtZip")
	public WebElement txtZip;

	@FindBy(id = "fimTtZipLocatorIdentical")
	public List<WebElement> rdoZipLocatorIdentical;

	@FindBy(id = "fimTtZipLocator")
	public WebElement txtZipOwnedForLocator;

	@FindBy(id = "syncCheckBox")
	public WebElement chkBoxSync;

	@FindBy(id = "fimTtLandBoundaries")
	public WebElement txtLandBoundaries;

	@FindBy(id = "fimTtAraeSize")
	public WebElement txtAraeSize;

	@FindBy(id = "fimTaNaturalHazards")
	public WebElement txtNaturalHazards;

	// Competitors Section

	@FindBy(id = "fimTaCompetitorsFranchise")
	public WebElement txtCompetitorsFranchise;

	@FindBy(id = "fimTaProximity")
	public WebElement txtProximity;

	@FindBy(id = "fimTtCompetitionFive")
	public WebElement txtCompetitionFive;

	@FindBy(id = "fimTtCompetitionTen")
	public WebElement txtCompetitionTen;

	// People Section

	@FindBy(id = "fimTtPopulation")
	public WebElement txtPopulation;

	@FindBy(id = "fimTtMedianIncome")
	public WebElement txtMedianIncome;

	// Transportation Section

	@FindBy(id = "fimTtPortsHarbors")
	public WebElement txtPortsHarbors;

	@FindBy(id = "fimTtAirport")
	public WebElement txtAirport;

	@FindBy(id = "fimTtHeliports")
	public WebElement txtHeliports;

	// Command Buttons

	@FindBy(name = "Submit")
	public WebElement btnSubmit;

}
