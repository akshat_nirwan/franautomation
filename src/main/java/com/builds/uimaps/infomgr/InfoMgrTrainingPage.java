package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrTrainingPage extends InfoMgrBasePage {

	public InfoMgrTrainingPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "entityFranchiseeCombo")
	public WebElement drpFranchiseID;

	@FindBy(id = "fimTtTrainingProgram")
	public WebElement txtTrainingProgName;

	@FindBy(id = "fimTttrainingType")
	public WebElement drpTrainingType;

	@FindBy(id = "fimTtAttendee")
	public WebElement txtAttendee;

	@FindBy(id = "fimDdCompletionDateTr")
	public WebElement txtCompletionDate;

	@FindBy(id = "fimTtGrade")
	public WebElement txtGrade;

	@FindBy(id = "fimTtInstructor")
	public WebElement txtInstructor;

	@FindBy(id = "fimTtLocationTr")
	public WebElement txtLocation;

	@FindBy(id = "fimTtAttendeeTitle")
	public WebElement txtAttendeeTitle;

	@FindBy(id = "fimIiScore")
	public WebElement txtScore;

	@FindBy(id = "fimTtTrainingProgram")
	public WebElement fimTtTrainingProgram;
	@FindBy(id = "fimTtInstructor")
	public WebElement fimTtInstructor;
	@FindBy(id = "fimTttrainingType")
	public WebElement fimTttrainingType;
	@FindBy(id = "fimTtLocationTr")
	public WebElement fimTtLocationTr;
	@FindBy(id = "fimTtAttendee")
	public WebElement fimTtAttendee;
	@FindBy(id = "fimTtAttendeeTitle")
	public WebElement fimTtAttendeeTitle;
	@FindBy(id = "fimDdCompletionDateTr")
	public WebElement fimDdCompletionDateTr;
	@FindBy(id = "fimIiScore")
	public WebElement fimIiScore;
	@FindBy(id = "fimTtGrade")
	public WebElement fimTtGrade;
	@FindBy(name = "Submit")
	public WebElement submit;

	@FindBy(name = "Submit")
	public WebElement btnSubmit;

	/*
	 * @FindBy(xpath = ".//*[@id='calendar']//img[@alt='Close the Calendar']")
	 * public WebElement btCloseCalendar=DDDDDDDDDDDDDDDDDDDDD
	 * .//*[@id='calendar']//img[@alt='Close the Calendar']";
	 */

	@FindBy(name = "Button0")
	public WebElement btnAddMore;

	@FindBy(xpath = ".//*[@id='siteMainTable']")
	public WebElement tblTrainings ;

}
