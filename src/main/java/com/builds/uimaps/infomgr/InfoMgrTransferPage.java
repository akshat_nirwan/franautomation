package com.builds.uimaps.infomgr;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrTransferPage extends InfoMgrBasePage {

	public InfoMgrTransferPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	// Transfer Options Section

	@FindBy(id = "newCenterNo")
	public List<WebElement> rdoNewCenterNo;

	@FindBy(id = "centerNo")
	public WebElement txtCenterNo;

	@FindBy(id = "selectAll")
	public WebElement chkSelectAll;

	@FindBy(id = "reportPeriodStartDate")
	public WebElement txtreportPeriodStartDate;

	@FindBy(id = "transferType")
	public WebElement drpTransferType;

	@FindBy(id = "currentStatus")
	public WebElement txtCurrentStatus;

	@FindBy(id = "txtAsOf")
	public WebElement txtAsOf;

	// Seller/Signer Details Section

	@FindBy(id = "sellerSigner")
	public WebElement txtSellerSigner;

	@FindBy(id = "buyerSigner")
	public WebElement txtBuyerSigner;

	@FindBy(id = "sellerSignerTitle")
	public WebElement txtSellerSignerTitle;

	@FindBy(id = "buyerSignerTitle")
	public WebElement txtBuyerSignerTitle;

	// Buyer Stage 1 Section

	@FindBy(id = "financialPrequalification")
	public WebElement rdoFinancialPrequalification;

	@FindBy(id = "financialPreQualificationApproval")
	public WebElement txtFinancialPreQualificationApproval;

	@FindBy(id = "expirationDate")
	public WebElement txtExpirationDate;

	@FindBy(id = "fddReceipt")
	public WebElement rdoFddReceipt;

	@FindBy(id = "fddDate")
	public WebElement txtfddDate;

	@FindBy(id = "buySellAgreement")
	public WebElement rdoBuySellAgreement;

	@FindBy(id = "corporateDocuments")
	public WebElement txtCorporateDocuments;

	@FindBy(id = "transferFeePd")
	public WebElement rdoTransferFeePd;

	@FindBy(id = "transferFeeNumerical")
	public WebElement txtTransferFeeNumerical;

	@FindBy(id = "financialDemand")
	public WebElement rdoFinancialDemand;

	@FindBy(id = "ptacPaymentDue")
	public WebElement txtPtacPaymentDue;

	@FindBy(id = "sendLegalDocsTo")
	public WebElement drpSendLegalDocsTo;

	@FindBy(id = "sendDocsTo")
	public WebElement txtSendDocsTo;

	// Other Details Section

	@FindBy(id = "otherName")
	public WebElement txtotherName;

	@FindBy(id = "address_0address")
	public WebElement txtStreetAddress;

	@FindBy(id = "address_0city")
	public WebElement txtCity;

	@FindBy(id = "address_0country")
	public WebElement drpCountry;

	@FindBy(id = "address_0zipcode")
	public WebElement txtZipCode;

	@FindBy(id = "address_0state")
	public WebElement drpState;

	@FindBy(id = "address_0phoneNumbers")
	public WebElement txtPhoneNumbers;

	@FindBy(id = "address_0extn")
	public WebElement txtExtension;

	@FindBy(id = "address_0faxNumbers")
	public WebElement txtFaxNumbers;

	@FindBy(id = "address_0mobileNumbers")
	public WebElement txtMobileNumbers;

	@FindBy(id = "address_0emailIds")
	public WebElement txtEmailIds;

	// Buyer Stage 2 Section

	@FindBy(id = "faEffectiveDate")
	public WebElement txtFaEffectiveDate;

	@FindBy(id = "faExpiration")
	public WebElement txtFaExpiration;

	@FindBy(id = "closingDate")
	public WebElement txtClosingDate;

	@FindBy(id = "consentAgreement")
	public WebElement rdoConsentAgreement;

	@FindBy(id = "assignedArea")
	public WebElement rdoAssignedArea;

	@FindBy(id = "guaranty")
	public WebElement rdoGuaranty;

	@FindBy(id = "stateAddendum")
	public WebElement rdoStateAddendum;

	@FindBy(id = "softwareLicense")
	public WebElement rdoSoftwareLicense;

	@FindBy(id = "promissoryNote")
	public WebElement rdoPromissoryNote;

	@FindBy(id = "certOfInsurance")
	public WebElement rdoCertOfInsurance;

	// Buyer Details Section

	@FindBy(id = "entityDetail")
	public WebElement txtEntityDetail;

	@FindBy(id = "ownerType")
	public WebElement rdoOwnerType;

	@FindBy(name = "firstName")
	public WebElement txtFirstName;

	@FindBy(name = "lastName")
	public WebElement txtLastName;

	@FindBy(name = "Submit")
	public WebElement btnSubmit;

	@FindBy(name = "Transfer")
	public WebElement btnTransferComplete;

	// Harish Dwivedi Info Mgr
	@FindBy(name = "comments")
	public WebElement comments;

	@FindBy(xpath = ".//input[@value='Proceed']")
	public WebElement Proceed ;
	
	@FindBy(xpath=".//*[contains(@id,'_muid')]")
	public WebElement muid;

}
