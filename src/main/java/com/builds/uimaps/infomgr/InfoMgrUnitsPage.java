package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrUnitsPage extends InfoMgrBasePage {

	public InfoMgrUnitsPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

}
