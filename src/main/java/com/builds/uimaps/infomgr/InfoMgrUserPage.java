package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrUserPage {

	public InfoMgrUserPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	// User Details Section
	@FindBy(id = "franchiseeNo")
	public WebElement drpFranchiseID;

	@FindBy(name = "addUser")
	public WebElement btnAddUser;

	@FindBy(id = "userName")
	public WebElement txtUserName;

	@FindBy(xpath = ".//*[@id='password'][@type='password']")
	public WebElement txtPassword ;

	@FindBy(id = "confirmPassword")
	public WebElement txtconfirmPassword;

	@FindBy(id = "userType")
	public WebElement drpType;

	@FindBy(id = "expiryDays")
	public WebElement drpExpiryDays;

	@FindBy(id = "ms-parentroleID")
	public WebElement drpRoles;

	@FindBy(id = "timezone")
	public WebElement drpTimezone;

	@FindBy(id = "isDaylight")
	public WebElement chkIsDaylight;

	// Personal Details Section

	@FindBy(id = "users")
	public WebElement drpUserType;

	@FindBy(xpath = ".//*[@id='jobTitle'][@type='text']")
	public WebElement txtJobTitle ;

	@FindBy(id = "ownerName")
	public WebElement drpOwnerName;

	@FindBy(id = "userLanguage")
	public WebElement drpUserLanguage;

	@FindBy(id = "address")
	public WebElement txtAddress;

	@FindBy(id = "city")
	public WebElement txtCity;

	@FindBy(id = "country")
	public WebElement drpCountry;

	@FindBy(id = "zipcode")
	public WebElement txtZipcode;

	@FindBy(id = "state")
	public WebElement drpState;

	@FindBy(id = "phone1")
	public WebElement txtPhone1;

	@FindBy(id = "phoneExt1")
	public WebElement txtPhoneExt1;

	@FindBy(id = "phone2")
	public WebElement txtPhone2;

	@FindBy(id = "phoneExt2")
	public WebElement txtPhoneExt2;

	@FindBy(id = "fax")
	public WebElement txtFax;

	@FindBy(id = "mobile")
	public WebElement txtMobile;

	@FindBy(id = "email")
	public WebElement txtEmail;

	@FindBy(id = "loginUserIp")
	public WebElement txtIpAddress;

	@FindBy(id = "isBillable")
	public WebElement chkIsBillable;

	@FindBy(id = "birthMonth")
	public WebElement drpBirthMonth;

	@FindBy(id = "birthDate")
	public WebElement drpBirthDate;

	@FindBy(id = "userPictureName")
	public WebElement btnUserPictureName;

	@FindBy(id = "sendNotification")
	public WebElement chkSendNotification;

	@FindBy(id = "Submit")
	public WebElement btnSubmit;

}
