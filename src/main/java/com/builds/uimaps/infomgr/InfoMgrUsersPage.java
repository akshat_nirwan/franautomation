package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrUsersPage extends InfoMgrBasePage {

	WebDriver driver;

	public InfoMgrUsersPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(linkText = "Add User")
	public WebElement btnAddUser ;

	public InfoMgrUserPage getUserPage() {
		return new InfoMgrUserPage(driver);
	}

}
