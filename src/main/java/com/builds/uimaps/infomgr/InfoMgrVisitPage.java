package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrVisitPage {

	public InfoMgrVisitPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "ms-parentmasterVisitId")
	public WebElement drpVisitForm;

	@FindBy(id = "ms-parentassignTo")
	public WebElement drpConsultantAssignedTo;

	@FindBy(id = "scheduleDateTime")
	public WebElement txtscheduleDateTime;

	@FindBy(id = "notes")
	public WebElement txtnotes;

	@FindBy(xpath = ".//*[@name='buttonSubmit'][@value='Start Now']")
	public WebElement btnStartNow;

	@FindBy(xpath = ".//*[@name='buttonSubmit'][@value='Schedule']")
	public WebElement btnSchedule;

}
