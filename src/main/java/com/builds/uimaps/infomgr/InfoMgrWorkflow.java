package com.builds.uimaps.infomgr;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrWorkflow {

	public InfoMgrWorkflow(WebDriver driver) {
	    PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = ".//a[@qat_submodule='Workflows']")
	public WebElement WorkflowSubmenu ;

	@FindBy(xpath = ".//*[contains(text(), 'Create Workflow')]")
	public WebElement creatWorkflowbtn;

	@FindBy(id = "workFlowName")
	public WebElement workFlowName;

	@FindBy(xpath = "./html/body/div[2]/table/tbody/tr/td/div/div/div[2]/button[2]")
	public WebElement wfnextbutton;

	@FindBy(id = "when-next")
	public WebElement triggernextbtn;

	@FindBy(xpath = ".//div[@id='fc-drop-parentfranchiseeType']//button[@type='button']//span[@class='placeholder']")
	public WebElement FranchiseeTypedropdown;

	@FindBy(xpath = ".//span[contains(text(),'Select All')]")
	public WebElement selectalldropdown;

	@FindBy(id = "if-next")
	public WebElement conditionnextbtn;

	@FindBy(xpath = ".//div[contains(text(),'Choose Action')]/div[@data-role='ico_AddCircleOutline']")
	public WebElement chooseaction;

	@FindBy(id = "li4")
	public WebElement createtask;

	@FindBy(xpath = ".//div[@id='fc-drop-parentassignTo']//button[@type='button']//span[@class='placeholder']")
	public WebElement assignto;

	@FindBy(xpath = ".//input[@class='searchInputMultiple']")
	public WebElement searchbox;

	@FindBy(xpath=".//span[contains(text(),'Select All')]")
	public WebElement clickselectall;

	@FindBy(xpath = ".//input[@id='subject']")
	public WebElement subject;

	@FindBy(id = "taskDesc")
	public WebElement taskdesc;

	@FindBy(xpath = ".//select[@id='priority']")
	public WebElement priority;

	@FindBy(xpath = ".//select[@id='taskType1']")
	public WebElement tasktypedrpdwn;

	@FindBy(xpath = ".//select[@id='taskType1']")
	public WebElement tasktypedrpdw;

	@FindBy(xpath = ".//select[@id='mins1']")
	public WebElement minutes;

	@FindBy(xpath = "//button[@type='button'][contains(text(),'Create')]")
	public WebElement createbtn;

	@FindBy(xpath = ".//button[@type='button'][contains(text(),'Close')]")
	public WebElement closebtn;

	@FindBy(xpath = ".//button[@id='aasave']")
	public WebElement create;
	}

