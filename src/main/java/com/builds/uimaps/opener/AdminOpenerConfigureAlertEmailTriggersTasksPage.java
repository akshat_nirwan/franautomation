package com.builds.uimaps.opener;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminOpenerConfigureAlertEmailTriggersTasksPage {

	@FindBy(linkText = "Add Alert")
	public WebElement addAlertLnk ;

	@FindBy(id = "fromEmail")
	public WebElement fromEmail;

	@FindBy(id = "frequencyAlertDays")
	public WebElement frequencyAlertDays;

	@FindBy(id = "criticalLevel")
	public WebElement criticalLevel;

	@FindBy(id = "alertEmailSubject")
	public WebElement alertEmailSubject;

	@FindBy(id = "emailContent")
	public WebElement emailContent;

	@FindBy(xpath = ".//*[@id='ms-parentusers']/button")
	public WebElement sendAlertEmailBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentusers']/div/div/input")
	public WebElement sendAlertEmailTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentusers']//input[@id='selectAll']")
	public WebElement sendEmailAlertEmailSelectAll ;

	@FindBy(id = "mailCc")
	public WebElement mailCc;

	@FindBy(id = "mailBcc")
	public WebElement mailBcc;

	@FindBy(id = "saveButton")
	public WebElement saveBtn;

	@FindBy(name = "close")
	public WebElement closeBtn;

	@FindBy(linkText = "Show All")
	public WebElement showAllLink ;

	public AdminOpenerConfigureAlertEmailTriggersTasksPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
