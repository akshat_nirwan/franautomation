package com.builds.uimaps.opener;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminOpenerConfigureChecklistDisplaySettingPage {

	@FindBy(id = "startDay")
	public List<WebElement> startDay;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	public AdminOpenerConfigureChecklistDisplaySettingPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
