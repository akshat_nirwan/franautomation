package com.builds.uimaps.opener;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminOpenerConfigureProjectStatusPage {

	@FindBy(xpath = ".//input[@value='Add Project Status']")
	public WebElement addProjectStatusBtn ;

	@FindBy(name = "cancel")
	public WebElement cancelBtn;

	@FindBy(name = "statusName")
	public WebElement statusNameBtn;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value=' OK ' or @value='  OK  ' or @value='OK' or @value='Ok']")
	public WebElement okBtn ;

	@FindBy(name = "projectOrder")
	public WebElement listing;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	public AdminOpenerConfigureProjectStatusPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
