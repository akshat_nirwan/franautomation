package com.builds.uimaps.opener;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminOpenerCustomizeProfilesPage {

	@FindBy(name = "fieldDisplayName")
	public WebElement fieldDisplayName;

	@FindBy(name = "orderNo")
	public WebElement orderNo;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	public AdminOpenerCustomizeProfilesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
