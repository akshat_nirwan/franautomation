package com.builds.uimaps.opener;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminOpenerDocumentChecklistPage {

	@FindBy(xpath = ".//input[@value='Add More']")
	public WebElement addMoreBtn ;

	@FindBy(id = "smDocumentChecklist_0documentName")
	public WebElement documentNameTxtBox;

	@FindBy(xpath = ".//input[@value='F']")
	public WebElement uploadFileRadioBtn ;

	@FindBy(xpath = ".//input[@value='WL']")
	public WebElement webLinkRadioBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmDocumentChecklist_0responsibilityArea']/button")
	public WebElement responsibilityAreaDropDownBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmDocumentChecklist_0responsibilityArea']")
	public WebElement responsibilitySelect ;

	@FindBy(xpath = ".//*[@id='ms-parentsmDocumentChecklist_0responsibilityArea']/div/div/input")
	public WebElement responsibilityAreaDropDownTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentsmDocumentChecklist_0responsibilityArea']//input[@id='selectAll']")
	public WebElement responsibilityAreaDropDownSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentsmDocumentChecklist_0contact']/button")
	public WebElement ContactDropDown ;

	@FindBy(xpath = ".//*[@id='ms-parentsmDocumentChecklist_0contact']")
	public WebElement selectContact ;

	@FindBy(xpath = ".//*[@id='ms-parentsmDocumentChecklist_0contact']")
	public WebElement contactSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentsmDocumentChecklist_0contact']/div/div/input")
	public WebElement ContactDropDownTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentsmDocumentChecklist_0contact']//input[@id='selectAll']")
	public WebElement ContactDropDownSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentsmDocumentChecklist_0storeTypeId']/button")
	public WebElement StoreTypeDropDown ;

	@FindBy(xpath = ".//*[@id='ms-parentsmDocumentChecklist_0storeTypeId']")
	public WebElement storeTypeSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentsmDocumentChecklist_0storeTypeId']/div/div/input")
	public WebElement StoreTypeDropDownTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentsmDocumentChecklist_0storeTypeId']//input[@id='selectAll']")
	public WebElement StoreTypeDropDownSelectAll ;

	@FindBy(xpath = ".//*[@id='smDocumentChecklist_0groupId']")
	public WebElement groupDropDown ;

	@FindBy(id = "smDocumentChecklist_0franchiseeAccess")
	public WebElement franchiseeAccessDropDown;

	@FindBy(id = "smDocumentChecklist_0priorityID")
	public WebElement priorityDropDown;

	@FindBy(xpath = ".//*[@id='smDocumentChecklist_0criticalLevelId']")
	public WebElement criticalLevelDropDown ;

	@FindBy(name = "smDocumentChecklist_0referenceParent")
	public WebElement dependentOnDropDown;

	@FindBy(xpath = ".//*[@id='ms-parentsmDocumentChecklist_0multitaskCombo']/button")
	public WebElement dependentOnDropDownBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmDocumentChecklist_0multitaskCombo']/div/div/input")
	public WebElement dependentOnDropDownTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentsmDocumentChecklist_0multitaskCombo']//input[@id='selectAll']")
	public WebElement dependentOnDropDownselectAll ;

	@FindBy(name = "smDocumentChecklist_0startDate")
	public WebElement startScheduleDateTxtBox;

	@FindBy(name = "smDocumentChecklist_0scheduleDate")
	public WebElement completionScheduleDateTxtBox;

	@FindBy(name = "smDocumentChecklist_0startAlertDate")
	public WebElement startReminderTxtBox;

	@FindBy(name = "smDocumentChecklist_0alertDate")
	public WebElement completionReminderTxtBox;

	@FindBy(name = "smDocumentChecklist_0startFlag")
	public WebElement scheduleStartDropDown;

	@FindBy(name = "smDocumentChecklist_0scheduleFlag")
	public WebElement scheduleCompletionDropDown;

	@FindBy(name = "smDocumentChecklist_0attachmentName")
	public WebElement attachmentBrowseBox;

	@FindBy(name = "smDocumentChecklist_0urlLink")
	public WebElement webLinkTxBx;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Yes' or @value=' Yes ']")
	public WebElement yesBtn ;

	@FindBy(name = "applyToAll")
	public List<WebElement> applyToAll;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement cboxDeleteBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement cboxCloseBtn ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//input[@value='No' or @value=' No ']")
	public WebElement noBtn ;

	@FindBy(xpath = ".//*[@id='description']")
	public WebElement documentDescriptionArea ;

	@FindBy(xpath = ".//input[@value='Actions']")
	public WebElement actionsBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']//table[@class='text']/tbody/tr[1]/td")
	public WebElement modifyActionBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td")
	public WebElement deleteActionBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td")
	public WebElement modifyContactBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/button")
	public WebElement modifyContactSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/div/div/input")
	public WebElement modifyContactSearchBox ;

	@FindBy(xpath = ".//div[@id='ms-parentcontact']//input[@id='selectAll']")
	public WebElement modifyContactSelectAll ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[6]/td")
	public WebElement orderChecklistBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[7]/td")
	public WebElement configureDefaultValueBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentstoreCombo']/button")
	public WebElement currentOpnerSelectBtn ;

	@FindBy(xpath = ".//div[@id='ms-parentstoreCombo']//label/input[@id='selectAll']")
	public WebElement selectAllOpnerCheck ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement bottomModifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement bottomDeleteBtn ;

	public AdminOpenerDocumentChecklistPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
