package com.builds.uimaps.opener;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminOpenerEquipmentChecklistPage {

	@FindBy(xpath = ".//input[@value='Add More']")
	public WebElement addMoreBtn ;

	@FindBy(name = "smEquipmentChecklist_0equipmentName")
	public WebElement equipmentTextArea;

	@FindBy(id = "smEquipmentChecklist_0quantity")
	public WebElement quantityTextBx;

	@FindBy(id = "smEquipmentChecklist_0vendorID")
	public WebElement supplierSelectDropDown;

	@FindBy(xpath = ".//*[@id='ms-parentsmEquipmentChecklist_0responsibilityArea']/button")
	public WebElement responsibilityAreaSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmEquipmentChecklist_0responsibilityArea']/div/div/input")
	public WebElement responsibilityAreaSearchBx ;

	@FindBy(xpath = ".//div[@id='ms-parentsmEquipmentChecklist_0responsibilityArea']//input[@id='selectAll']")
	public WebElement responsibilityAreaSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentsmEquipmentChecklist_0responsibilityArea']")
	public WebElement responsibilitySelect ;

	@FindBy(xpath = ".//*[@id='ms-parentsmEquipmentChecklist_0contact']")
	public WebElement contactSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentsmEquipmentChecklist_0storeTypeId']")
	public WebElement storeTypeSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentsmEquipmentChecklist_0contact']/button")
	public WebElement contactSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmEquipmentChecklist_0contact']/div/div/input")
	public WebElement contactSearchBx ;

	@FindBy(xpath = ".//div[@id='ms-parentsmEquipmentChecklist_0contact']//input[@id='selectAll']")
	public WebElement contactSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentsmEquipmentChecklist_0storeTypeId']/button")
	public WebElement storeTypeSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmEquipmentChecklist_0storeTypeId']/div/div/input")
	public WebElement storeTypeSearchBx ;

	@FindBy(xpath = ".//div[@id='ms-parentsmEquipmentChecklist_0storeTypeId']//input[@id='selectAll']")
	public WebElement storeTypeSelectAll ;

	@FindBy(id = "smEquipmentChecklist_0groupId")
	public WebElement groupSelectDropDown;

	@FindBy(id = "smEquipmentChecklist_0franchiseeAccess")
	public WebElement franchiseAccessDropDown;

	@FindBy(id = "smEquipmentChecklist_0priorityID")
	public WebElement priorityDropDown;

	@FindBy(id = "smEquipmentChecklist_0criticalLevelId")
	public WebElement criticalLevelDropDown;

	@FindBy(name = "smEquipmentChecklist_0referenceParent")
	public WebElement dependentOnDropDown;

	@FindBy(xpath = ".//input[@value='F']")
	public WebElement uploadFileRadioBtn ;

	@FindBy(xpath = ".//input[@value='WL']")
	public WebElement webLinkRadioBtn ;

	@FindBy(name = "smEquipmentChecklist_0attachmentName")
	public WebElement attachedTxtBx;

	@FindBy(name = "smEquipmentChecklist_0urlLink")
	public WebElement weblinkTxtBx;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(name = "applyToAll")
	public List<WebElement> applyToAll;

	@FindBy(xpath = ".//input[@value='Delete' and @type='button']")
	public WebElement deleteFrameEquipmentBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeFrameEquipmentBtn ;

	@FindBy(xpath = ".//input[@value='No' or @value=' No ']")
	public WebElement noBtn ;

	@FindBy(name = "smEquipmentChecklist_0startDate")
	public WebElement startScheduledTxBx;

	@FindBy(name = "smEquipmentChecklist_0startFlag")
	public WebElement startScheduledSelect;

	@FindBy(name = "smEquipmentChecklist_0scheduleDate")
	public WebElement CompletionScheduledTxBx;

	@FindBy(name = "smEquipmentChecklist_0scheduleFlag")
	public WebElement CompletionScheduledSelect;

	@FindBy(name = "smEquipmentChecklist_0startAlertDate")
	public WebElement startReminderTxBx;

	@FindBy(name = "smEquipmentChecklist_0alertDate")
	public WebElement completiomReminderTxBx;

	// dependency select all
	@FindBy(xpath = ".//*[@id='ms-parentsmEquipmentChecklist_0multitaskCombo']/button")
	public WebElement dependentSelectBtn ;

	@FindBy(xpath = ".//div[@id='ms-parentsmEquipmentChecklist_0multitaskCombo']//input[@id='selectAll']")
	public WebElement dependentSelectAllChckBx ;

	@FindBy(xpath = ".//*[@id='description']")
	public WebElement descriptionArea ;

	@FindBy(xpath = ".//input[@value='Actions']")
	public WebElement actionBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td")
	public WebElement actionModifyBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td")
	public WebElement actionDeleteBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td")
	public WebElement actionModifyContactsBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[7]/td")
	public WebElement actionConfigureDefaultValueBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/button")
	public WebElement contactSelectModifyBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/div/div/input")
	public WebElement contactSelectModifyTxBx ;

	@FindBy(xpath = ".//div[@id='ms-parentcontact']//input[@id='selectAll']")
	public WebElement contactSelectModifySelectAll ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyAtBottomBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteAtBottomBtn ;

	@FindBy(xpath = ".//input[@value='Yes' or @value=' Yes ']")
	public WebElement yesBtn ;

	@FindBy(xpath = ".//input[@value='Actions']")
	public WebElement actionsBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[7]/td")
	public WebElement configureDefaultValueBtn ;

	public AdminOpenerEquipmentChecklistPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
