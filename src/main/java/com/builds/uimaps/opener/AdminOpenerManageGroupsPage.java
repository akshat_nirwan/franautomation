package com.builds.uimaps.opener;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminOpenerManageGroupsPage {

	@FindBy(name = "cancel")
	public WebElement cancelBtn;

	@FindBy(xpath = ".//input[@value='Add Groups']")
	public WebElement addGroupsBtn ;

	@FindBy(id = "groupName")
	public WebElement groupNameBtn;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(name = "groupsOrder")
	public WebElement listing;

	@FindBy(xpath = ".//input[contains(@value , 'OK')]")
	public WebElement okBtn ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	public AdminOpenerManageGroupsPage(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}
}
