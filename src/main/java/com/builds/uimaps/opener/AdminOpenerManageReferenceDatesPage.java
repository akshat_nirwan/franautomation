package com.builds.uimaps.opener;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminOpenerManageReferenceDatesPage {

	@FindBy(xpath = ".//input[@value='Add Reference Date']")
	public WebElement addReferenceDateBtn ;

	@FindBy(id = "displayName")
	public WebElement AddReferenceDateTxtBox;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//input[@value='Back']")
	public WebElement backBtn ;

	public AdminOpenerManageReferenceDatesPage(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}
}
