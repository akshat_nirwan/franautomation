package com.builds.uimaps.opener;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminOpenerMilestoneDateTriggersPage {

	@FindBy(xpath = ".//input[@value='Add Milestone Date Trigger']")
	public WebElement addMilestoneDateTriggerLnk ;

	@FindBy(id = "triggerName")
	public WebElement triggerName;

	@FindBy(id = "milestoneDateId")
	public WebElement milestoneDateId;

	@FindBy(id = "scheduleDate")
	public WebElement scheduleDate;

	@FindBy(id = "scheduleFlag")
	public WebElement scheduleFlag;

	@FindBy(xpath = ".//*[@id='ms-parenttaskId']/button")
	public WebElement actualCompletionBtn ;

	@FindBy(xpath = ".//*[@id='ms-parenttaskId']")
	public WebElement actualCompletionBtn1 ;

	@FindBy(xpath = ".//*[@id='ms-parenttaskId']/div/div/input")
	public WebElement actualCompletionTxBx ;

	@FindBy(xpath = ".//div[@id='ms-parenttaskId']//input[@id='selectAll']")
	public WebElement actualCompletionSelectAll ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	public AdminOpenerMilestoneDateTriggersPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
