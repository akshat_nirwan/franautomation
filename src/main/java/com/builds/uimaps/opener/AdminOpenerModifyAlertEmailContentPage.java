package com.builds.uimaps.opener;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminOpenerModifyAlertEmailContentPage {

	@FindBy(name = "mailFrom")
	public WebElement fromEmailTextBox;

	@FindBy(name = "startReminderAlertMessageValue")
	public WebElement scheduleStartReminderEmailContentTextBox;

	@FindBy(name = "startAlertMessageValue")
	public WebElement scheduleStartAlertEmailContentTextBox;

	@FindBy(name = "scheduleReminderAlertMessageValue")
	public WebElement scheduleCompletionReminderEmailContentTextBox;

	@FindBy(name = "scheduleAlertMessageValue")
	public WebElement scheduleCompletionAlertEmailContentTextBox;

	@FindBy(name = "overdueAlertMessageValue")
	public WebElement overdueAlertEmailContentTextBox;

	@FindBy(xpath = ".//input[@value='Save' or @value='Add']")
	public WebElement saveBtn ;

	public AdminOpenerModifyAlertEmailContentPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
