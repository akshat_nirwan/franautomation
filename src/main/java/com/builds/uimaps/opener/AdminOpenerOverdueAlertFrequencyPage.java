package com.builds.uimaps.opener;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminOpenerOverdueAlertFrequencyPage {

	@FindBy(name = "noOfDays")
	public WebElement overdueAlertFrequencyTextBox;

	@FindBy(xpath = ".//input[@value='Add' or @value='Save']")
	public WebElement addBtn ;

	@FindBy(name = "Cancel")
	public WebElement cancelBtn;

	public AdminOpenerOverdueAlertFrequencyPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
