package com.builds.uimaps.opener;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminOpenerResponsibleDepartmentPage {

	@FindBy(xpath = ".//input[@value='Add More']")
	public WebElement addMoreBtn ;

	@FindBy(id = "responsibilityArea")
	public WebElement responsibilityArea;

	@FindBy(xpath = ".//input[@name='Submit']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	public AdminOpenerResponsibleDepartmentPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
