package com.builds.uimaps.opener;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminOpenerSecondaryChecklistPage {

	@FindBy(xpath = ".//input[@value='Add Secondary Checklists']")
	public WebElement addSecondaryBtn ;

	@FindBy(xpath = ".//input[@value='Configure Default Values']")
	public WebElement configureDefaultValue ;

	@FindBy(id = "checklistType")
	public WebElement checklistNameTxtBox;

	@FindBy(id = "checklistDescription")
	public WebElement checklistDescriptionTxtBox;

	@FindBy(xpath = ".//input[@value='Add More']")
	public WebElement addMoreBtn ;

	@FindBy(id = "smSecondryChecklist_0itemName")
	public WebElement itemNameTxtBox;

	@FindBy(xpath = ".//*[@id='ms-parentsmSecondryChecklist_0responsibilityArea']/button")
	public WebElement responsibilityAreaDropDownBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmSecondryChecklist_0responsibilityArea']")
	public WebElement selectResponsibilityArea ;

	@FindBy(xpath = ".//*[@id='ms-parentsmSecondryChecklist_0responsibilityArea']/div/div/input")
	public WebElement responsibilityAreaDropDownTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentsmSecondryChecklist_0responsibilityArea']//input[@id='selectAll']")
	public WebElement responsibilityAreaDropDownSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentsmSecondryChecklist_0contact']/button")
	public WebElement ContactDropDown ;

	@FindBy(xpath = ".//*[@id='ms-parentsmSecondryChecklist_0contact']")
	public WebElement selectContact ;

	@FindBy(xpath = ".//*[@id='ms-parentsmSecondryChecklist_0contact']/div/div/input")
	public WebElement ContactDropDownTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentsmSecondryChecklist_0contact']//input[@id='selectAll']")
	public WebElement ContactDropDownSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentsmSecondryChecklist_0storeTypeId']/button")
	public WebElement StoreTypeDropDown ;

	@FindBy(xpath = ".//*[@id='ms-parentsmSecondryChecklist_0storeTypeId']")
	public WebElement selectStoreType ;

	@FindBy(xpath = ".//*[@id='ms-parentsmSecondryChecklist_0storeTypeId']/div/div/input")
	public WebElement StoreTypeDropDownTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentsmSecondryChecklist_0storeTypeId']//input[@id='selectAll']")
	public WebElement StoreTypeDropDownSelectAll ;

	@FindBy(xpath = ".//*[@id='smSecondryChecklist_0groupId']")
	public WebElement groupDropDown ;

	@FindBy(xpath = ".//*[@id='smSecondryChecklist_0criticalLevelId']")
	public WebElement criticalLevelDropDown ;

	@FindBy(id = "smSecondryChecklist_0franchiseeAccess")
	public WebElement franchiseeAccessDropDown;

	@FindBy(id = "smSecondryChecklist_0priorityID")
	public WebElement priorityDropDown;

	@FindBy(name = "smSecondryChecklist_0referenceParent")
	public WebElement dependentOnDropDown;

	@FindBy(xpath = ".//*[@id='ms-parentsmSecondryChecklist_0multitaskCombo']/button")
	public WebElement dependentOnDropDownBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmSecondryChecklist_0multitaskCombo']/div/div/input")
	public WebElement dependentOnDropDownTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentsmSecondryChecklist_0multitaskCombo']//input[@id='selectAll']")
	public WebElement dependentOnDropDownselectAll ;

	@FindBy(name = "smSecondryChecklist_0startDate")
	public WebElement startScheduleDateTxtBox;

	@FindBy(name = "smSecondryChecklist_0scheduleDate")
	public WebElement completionScheduleDateTxtBox;

	@FindBy(name = "smSecondryChecklist_0startFlag")
	public WebElement startReminderTxtBox;

	@FindBy(name = "smSecondryChecklist_0alertDate")
	public WebElement completionReminderTxtBox;

	@FindBy(name = "smSecondryChecklist_0startFlag")
	public WebElement scheduleStartDropDown;

	@FindBy(name = "smSecondryChecklist_0scheduleFlag")
	public WebElement scheduleCompletionDropDown;

	@FindBy(xpath = ".//input[@value='F']")
	public WebElement uploadFileRadioBtn ;

	@FindBy(xpath = ".//input[@value='WL']")
	public WebElement webLinkRadioBtn ;

	@FindBy(name = "smSecondryChecklist_0attachmentName")
	public WebElement attachmentBrowseBox;

	@FindBy(name = "smSecondryChecklist_0urlLink")
	public WebElement webLinkTxBx;

	@FindBy(name = "applyToAll")
	public List<WebElement> applyToAll;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement cboxDeleteBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement cboxCloseBtn ;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='No' or @value=' No ']")
	public WebElement noBtn ;

	@FindBy(xpath = ".//input[@value='Yes' or @value=' Yes ']")
	public WebElement yesBtn ;

	@FindBy(xpath = ".//input[@value='Save' or @name='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//*[@id='description']")
	public WebElement secondaryDescriptionArea ;

	@FindBy(xpath = ".//input[@value='Actions']")
	public WebElement actionsBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']//table[@class='text']/tbody/tr[1]/td")
	public WebElement modifyActionBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td")
	public WebElement deleteActionBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td")
	public WebElement modifyContactBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[7]/td")
	public WebElement configureDefaultValueBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/button")
	public WebElement modifyContactSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/div/div/input")
	public WebElement modifyContactSearchBox ;

	@FindBy(xpath = ".//div[@id='ms-parentcontact']//input[@id='selectAll']")
	public WebElement modifyContactSelectAll ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement bottomModifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement bottomDeleteBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmTaskChecklist_0multitaskCombo']/button")
	public WebElement selectDropDownBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmTaskChecklist_0multitaskCombo']/div/div/input")
	public WebElement selectSearchBox ;

	@FindBy(xpath = ".//div[@id='ms-parentsmTaskChecklist_0multitaskCombo']//input[@id='selectAll']")
	public WebElement selectAll ;

	@FindBy(xpath = ".//input[@type='button' and @value='Configure Default Values']")
	public WebElement configureDefaultValueBtnAtHome ;

	public AdminOpenerSecondaryChecklistPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
