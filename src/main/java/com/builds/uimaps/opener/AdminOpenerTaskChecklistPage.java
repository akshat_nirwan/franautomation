package com.builds.uimaps.opener;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminOpenerTaskChecklistPage {

	// #################################

	@FindBy(id = "smTaskChecklist_0task")
	public WebElement taskNameTxtBox;

	@FindBy(xpath = ".//input[@value='Add More']")
	public WebElement addMoreBtn ;

	@FindBy(name = "smTaskChecklist_0groupId")
	public WebElement groupDropDown;

	@FindBy(id = "smTaskChecklist_0criticalLevelId")
	public WebElement criticalLevelDropDown;

	@FindBy(xpath = ".//*[@id='ms-parentsmTaskChecklist_0multitaskCombo']/button")
	public WebElement selectDropDownBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmTaskChecklist_0multitaskCombo']/div/div/input")
	public WebElement selectSearchBox ;

	@FindBy(xpath = ".//div[@id='ms-parentsmTaskChecklist_0multitaskCombo']//input[@id='selectAll']")
	public WebElement selectAll ;

	@FindBy(name = "smTaskChecklist_0startDate")
	public WebElement startScheduleDateTxtBox;

	@FindBy(name = "smTaskChecklist_0scheduleDate")
	public WebElement completionScheduleDateTxtBox;

	@FindBy(name = "smTaskChecklist_0startAlertDate")
	public WebElement startReminderTxtBox;

	@FindBy(name = "smTaskChecklist_0alertDate")
	public WebElement completionReminderTxtBox;

	@FindBy(name = "smTaskChecklist_0startFlag")
	public WebElement scheduleStartDropDown;

	@FindBy(name = "smTaskChecklist_0scheduleFlag")
	public WebElement scheduleCompletionDropDown;

	@FindBy(xpath = ".//input[@value='F']")
	public WebElement uploadFileRadioBtn ;

	@FindBy(name = "smTaskChecklist_0attachmentName")
	public WebElement attachmentBrowseBox;

	@FindBy(name = "smTaskChecklist_0referenceParent")
	public WebElement dependentOnDropDown;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='No' or @value=' No ']")
	public WebElement noBtn ;

	@FindBy(xpath = ".//input[@value='Yes' or @value=' Yes ']")
	public WebElement yesBtn ;

	@FindBy(id = "smTaskChecklist_0priorityID")
	public WebElement priorityDropDown;

	@FindBy(id = "smTaskChecklist_0franchiseeAccess")
	public WebElement franchiseeAccessDropDown;

	@FindBy(xpath = ".//*[@id='ms-parentsmTaskChecklist_0responsibilityArea']/button")
	public WebElement responsibilityAreaSelctBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmTaskChecklist_0responsibilityArea']")
	public WebElement selectResponsibility ;

	@FindBy(xpath = ".//*[@id='ms-parentsmTaskChecklist_0contact']")
	public WebElement selectContact ;

	@FindBy(xpath = ".//*[@id='ms-parentsmTaskChecklist_0storeTypeId']")
	public WebElement selectStoreType ;

	@FindBy(xpath = ".//*[@id='ms-parentsmTaskChecklist_0responsibilityArea']/div/div/input")
	public WebElement responsibilityAreaTextBx ;

	@FindBy(xpath = ".//div[@id='ms-parentsmTaskChecklist_0responsibilityArea']//input[@id='selectAll']")
	public WebElement responsibilityAreaSelectAllBtn ;

	@FindBy(xpath = ".//div[@id='ms-parentsmTaskChecklist_0contact']/button/span")
	public WebElement ContactSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmTaskChecklist_0contact']/div/div/input")
	public WebElement contactTextBox ;

	@FindBy(xpath = ".//div[@id='ms-parentsmTaskChecklist_0contact']//input[@id='selectAll']")
	public WebElement contactSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentsmTaskChecklist_0storeTypeId']/button")
	public WebElement storeTypeSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmTaskChecklist_0storeTypeId']/div/div/input")
	public WebElement storeTypeSearchBox ;

	@FindBy(xpath = ".//*[@id='ms-parentsmTaskChecklist_0storeTypeId']//input[@id='selectAll']")
	public WebElement storeTypeSelctAll ;

	@FindBy(xpath = ".//input[@value='Actions']")
	public WebElement actionBtn ;

	@FindBy(xpath = ".//input[@value='Print']")
	public WebElement printBtn ;

	@FindBy(name = "Modify")
	public WebElement modifyBtn;

	@FindBy(name = "Delete")
	public WebElement deleteBtn;

	@FindBy(name = "Report In Excel")
	public WebElement exportAsExcelBtn;

	@FindBy(xpath = ".//input[@value='Back']")
	public WebElement backBtn ;

	@FindBy(xpath = ".//input[@value='WL']")
	public WebElement webLinkRadioBtn ;

	@FindBy(name = "smTaskChecklist_0urlLink")
	public WebElement webLinkAddressBx;

	@FindBy(name = "applyToAll")
	public List<WebElement> applyToAll;

	@FindBy(xpath = ".//input[@value='Save' or @name='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement cboxDeleteBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement cboxCloseBtn ;

	// Add descrption
	@FindBy(id = "description")
	public WebElement descriptionArea;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addDescBtn ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveDescModBtn ;

	@FindBy(xpath = ".//input[@value='Actions']")
	public WebElement actionsBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']//table[@class='text']/tbody/tr[1]/td")
	public WebElement modifyActionBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td")
	public WebElement deleteActionBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td")
	public WebElement modifyContactBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/button")
	public WebElement modifyContactSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']")
	public WebElement contactSelectM ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/div/div/input")
	public WebElement modifyContactSearchBox ;

	@FindBy(xpath = ".//div[@id='ms-parentcontact']//input[@id='selectAll']")
	public WebElement modifyContactSelectAll ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[6]/td")
	public WebElement orderChecklistBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[7]/td")
	public WebElement configureDefaultValueBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentstoreCombo']/button")
	public WebElement currentOpnerSelectBtn ;

	@FindBy(xpath = ".//div[@id='ms-parentstoreCombo']//label/input[@id='selectAll']")
	public WebElement selectAllOpnerCheck ;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement bottomModifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement bottomDeleteBtn ;

	// franchise Select
	@FindBy(xpath = ".//*[@id='ms-parentstoreCombo']")
	public WebElement franchiseSelect ;

	public AdminOpenerTaskChecklistPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
