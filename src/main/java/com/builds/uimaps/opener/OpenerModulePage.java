package com.builds.uimaps.opener;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OpenerModulePage {

	@FindBy(xpath = ".//a[@original-title='Manage New Locations']")
	public WebElement storeSummaryTab ;

	@FindBy(xpath = ".//a[@original-title='Search Checklist(s) Subject to Defined Parameters']")
	public WebElement searchTab ;

	@FindBy(xpath = ".//a[@original-title='Manage Tasks']")
	public WebElement taskTab ;

	@FindBy(xpath = ".//a[@original-title='Manage Archived Stores']")
	public WebElement archivedTab ;

	@FindBy(xpath = ".//a[@original-title='Generate Analytical Reports about Onboarding/ Store Development']")
	public WebElement reportsTab ;

	public OpenerModulePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
