package com.builds.uimaps.opener;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OpenerSearchPage {

	@FindBy(xpath = ".//*[@id='ms-parenttask']/button")
	public WebElement checklistSearchBtn ;

	@FindBy(xpath = ".//*[@id='ms-parenttask']/div/div/input")
	public WebElement checklistSearchTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parenttask']/div//input[@id='selectAll']")
	public WebElement checklistSearchSelectAll ;

	@FindBy(id = "fName")
	public WebElement storeNumberTxBx;

	@FindBy(xpath = ".//*[@id='ms-parentmultipleDivision']/button")
	public WebElement divisionSearchBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentmultipleDivision']/div/div/input")
	public WebElement divisionSearchTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentmultipleDivision']/div//input[@id='selectAll']")
	public WebElement divisionSearchSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentstoreTypeId']/button")
	public WebElement storeTypeSearchBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentstoreTypeId']/div/div/input")
	public WebElement storeTypeSearchTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentstoreTypeId']/div//input[@id='selectAll']")
	public WebElement storeTypeSearchSelectAll ;

	@FindBy(name = "itemName")
	public WebElement itemNameTxBx;

	@FindBy(xpath = ".//*[@id='ms-parentstatusFilter']/button")
	public WebElement statusSearchBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentstatusFilter']/div/div/input")
	public WebElement statusSearchTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentstatusFilter']/div//input[@id='selectAll']")
	public WebElement statusSearchSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentprojectId']/button")
	public WebElement projectStatusSearchBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentprojectId']/div/div/input")
	public WebElement projectStatusSearchTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentprojectId']/div//input[@id='selectAll']")
	public WebElement projectStatusSearchSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/button")
	public WebElement contactSearchBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/div/div/input")
	public WebElement contactSearchTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/div//input[@id='selectAll']")
	public WebElement contactSearchSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentresponsibilityArea']/button")
	public WebElement responsibilityAreaSearchBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentresponsibilityArea']/div/div/input")
	public WebElement responsibilityAreaSearchTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentresponsibilityArea']/div//input[@id='selectAll']")
	public WebElement responsibilityAreaSearchSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentgroupId']/button")
	public WebElement groupSearchBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentgroupId']/div/div/input")
	public WebElement groupSearchTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentgroupId']/div//input[@id='selectAll']")
	public WebElement groupSearchSelectAll ;

	@FindBy(id = "isStoreArchive")
	public WebElement isStoreArchive;

	@FindBy(name = "Search")
	public WebElement searchBtn;

	@FindBy(xpath = ".//a/u[.='Show All']")
	public WebElement showAllLink ;

	@FindBy(xpath = ".//input[@value='Reset']")
	public WebElement resetBtn ;

	@FindBy(xpath = ".//a[@qat_submodule='Search']")
	public WebElement searchTab ;

	public OpenerSearchPage(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}
}
