package com.builds.uimaps.opener;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OpenerStoreSummaryAddDocumentsPage {

	@FindBy(linkText = "Documents")
	public WebElement documentsTab ;

	@FindBy(id = "fimDocumentTitle")
	public WebElement documentTitleTxBx;

	@FindBy(id = "fimDocumentAttachment")
	public WebElement uploadDocumentBx;

	@FindBy(xpath = ".//input[@name='Submit']")
	public WebElement addBtn ;

	@FindBy(linkText = "Add More")
	public WebElement addMoreLnk ;

	@FindBy(name = "foSearchString")
	public WebElement searchTextBx;

	@FindBy(xpath = ".//img[@title='Search by Store No. / Owner']")
	public WebElement searchImgBtn ;

	public OpenerStoreSummaryAddDocumentsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
