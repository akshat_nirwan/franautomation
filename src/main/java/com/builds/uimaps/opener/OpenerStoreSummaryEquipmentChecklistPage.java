package com.builds.uimaps.opener;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OpenerStoreSummaryEquipmentChecklistPage {

	@FindBy(linkText = "Equipment Checklist")
	public WebElement equipmentChecklistLnk ;

	@FindBy(id = "commentDate")
	public WebElement completionDateTxBx;

	@FindBy(id = "comments")
	public WebElement commentsTxBx;

	@FindBy(id = "description")
	public WebElement descriptionTxBx;

	// @FindBy(xpath=".//input[@value='Complete']")
	@FindBy(xpath = ".//input[@name='Submit' or @value='Complete']")
	public WebElement completeBtn ;

	@FindBy(name = "foSearchString")
	public WebElement searchTextBx;

	@FindBy(xpath = ".//img[@title='Search by Store No. / Owner']")
	public WebElement searchImgBtn ;

	@FindBy(xpath = ".//input[@class='fTextBox' and @name='checklistNameToBeMatched']")
	public WebElement equipmentSearchBx ;

	@FindBy(id = "search")
	public WebElement searchBtn;


	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(linkText = "Store Info")
	public WebElement storeInfoLnk ;

	// modify equipment
	@FindBy(name = "smUserEquipmentChecklist_0equipmentName")
	public WebElement equipmentNameTxtBox;

	@FindBy(name = "smUserEquipmentChecklist_0quantity")
	public WebElement quantityTxBx;

	@FindBy(name = "smUserEquipmentChecklist_0vendorID")
	public WebElement supplierDropDown;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserEquipmentChecklist_0contact']/button")
	public WebElement ContactSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserEquipmentChecklist_0contact']")
	public WebElement selectContact ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserEquipmentChecklist_0contact']/div/div/input")
	public WebElement contactTextBox ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserEquipmentChecklist_0contact']//input[@id='selectAll']")
	public WebElement contactSelectAll ;

	@FindBy(name = "smUserEquipmentChecklist_0referenceParent")
	public WebElement dependentOnDropDown;

	@FindBy(name = "smUserEquipmentChecklist_0startDate")
	public WebElement startScheduleDateTxtBox;

	@FindBy(name = "smUserEquipmentChecklist_0scheduleDate")
	public WebElement completionScheduleDateTxtBox;

	@FindBy(name = "smUserEquipmentChecklist_0startAlertDate")
	public WebElement startReminderTxtBox;

	@FindBy(name = "smUserEquipmentChecklist_0alertDate")
	public WebElement completionReminderTxtBox;

	@FindBy(name = "smUserEquipmentChecklist_0startFlag")
	public WebElement scheduleStartDropDown;

	@FindBy(name = "smUserEquipmentChecklist_0scheduleFlag")
	public WebElement scheduleCompletionDropDown;

	@FindBy(xpath = ".//input[@value='WL']")
	public WebElement webLinkRadioBtn ;

	@FindBy(name = "smUserEquipmentChecklist_0urlLink")
	public WebElement webLinkAddressBx;

	@FindBy(name = "applyToAll")
	public List<WebElement> applyToAll;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentstoreCombo']/button")
	public WebElement currentOpnerSelectBtn ;

	@FindBy(xpath = ".//div[@id='ms-parentstoreCombo']//label/input[@id='selectAll']")
	public WebElement selectAllOpnerCheck ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserEquipmentChecklist_0responsibilityArea']/button")
	public WebElement responsibilityAreaSelctBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserEquipmentChecklist_0responsibilityArea']")
	public WebElement selectResponsibilityArea ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserEquipmentChecklist_0responsibilityArea']/div/div/input")
	public WebElement responsibilityAreaTextBx ;

	@FindBy(xpath = ".//div[@id='ms-parentsmUserEquipmentChecklist_0responsibilityArea']//input[@id='selectAll']")
	public WebElement responsibilityAreaSelectAllBtn ;

	// Actions Btn
	@FindBy(xpath = ".//input[@value='Actions']")
	public WebElement actionsBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td")
	public WebElement overdueEquipmentLink ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td")
	public WebElement completeLink ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td")
	public WebElement deleteLink ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td")
	public WebElement modifyContactLink ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[6]/td")
	public WebElement configureDefaultValueLink ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[7]/td")
	public WebElement groupViewLink ;

	// modify Contact

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/button")
	public WebElement modifyContactSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/div/div/input")
	public WebElement modifyContactSearchBx ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']//input[@id='selectAll']")
	public WebElement modifyContactSelectAll ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement bottomDeleteBtn ;

	@FindBy(name = "modifyContact")
	public WebElement modifyContactBoottomBtn;

	@FindBy(linkText = "Add More")
	public WebElement addMoreLink ;

	@FindBy(name = "smUserEquipmentChecklist_0groupId")
	public WebElement groupDropDown;

	@FindBy(name = "smUserEquipmentChecklist_0franchiseeAccess")
	public WebElement franchiseeAccessDropDown;

	@FindBy(name = "smUserEquipmentChecklist_0docType")
	public WebElement uploadFileRadioBtn;

	@FindBy(name = "smUserEquipmentChecklist_0attachmentName")
	public WebElement attachmentBrowseBox;

	@FindBy(xpath = ".//input[@value='No' or @value=' No ']")
	public WebElement noBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentstoreNameCombo']/button")
	public WebElement storeFilterSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentstoreNameCombo']/div//input[@id='selectAll']")
	public WebElement storeFilterSelectAll ;

	@FindBy(xpath = ".//*[@id='ms-parentstoreNameCombo']/div/div/input")
	public WebElement storeFilterSelectBx ;

	// filter update
	@FindBy(xpath = ".//*[@id='ms-parentstatusFilter']")
	public WebElement statusSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']")
	public WebElement contactSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentgroupId']")
	public WebElement groupSelect ;

	@FindBy(id = "matchType1")
	public WebElement scheduleStartDateDropDown;

	public OpenerStoreSummaryEquipmentChecklistPage(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}

}
