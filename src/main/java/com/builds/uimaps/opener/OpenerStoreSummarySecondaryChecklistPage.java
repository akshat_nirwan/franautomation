package com.builds.uimaps.opener;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OpenerStoreSummarySecondaryChecklistPage {

	@FindBy(linkText = "Secondary Checklists")
	public WebElement secondaryChecklistLnk ;

	@FindBy(id = "commentDate")
	public WebElement completionDateTxBx;

	@FindBy(id = "comments")
	public WebElement commentsTxBx;

	@FindBy(id = "description")
	public WebElement descriptionTxBx;

	@FindBy(xpath = ".//input[@value='Complete']")
	public WebElement completeBtn ;

	@FindBy(name = "foSearchString")
	public WebElement searchTextBx;

	@FindBy(xpath = ".//img[@title='Search by Store No. / Owner']")
	public WebElement searchImgBtn ;

	@FindBy(xpath = ".//input[@class='fTextBox' and @name='checklistNameToBeMatched']")
	public WebElement secondarySearchBx ;

	@FindBy(id = "search")
	public WebElement searchBtn;



	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(linkText = "Store Info")
	public WebElement storeInfoLnk ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	@FindBy(xpath = ".//input[@type='checkbox' and @name='checkAll']")
	public WebElement checkAllchckBx ;

	// modify
	@FindBy(name = "smUserSecondryChecklist_0itemName")
	public WebElement secondaryNameTxtBox;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserSecondryChecklist_0contact']/button")
	public WebElement ContactSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserSecondryChecklist_0contact']")
	public WebElement selectContact ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserSecondryChecklist_0contact']/div/div/input")
	public WebElement contactTextBox ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserSecondryChecklist_0contact']//input[@id='selectAll']")
	public WebElement contactSelectAll ;

	@FindBy(name = "smUserSecondryChecklist_0referenceParent")
	public WebElement dependentOnDropDown;

	@FindBy(name = "smUserSecondryChecklist_0startDate")
	public WebElement startScheduleDateTxtBox;

	@FindBy(name = "smUserSecondryChecklist_0scheduleDate")
	public WebElement completionScheduleDateTxtBox;

	@FindBy(name = "smUserSecondryChecklist_0startAlertDate")
	public WebElement startReminderTxtBox;

	@FindBy(name = "smUserSecondryChecklist_0alertDate")
	public WebElement completionReminderTxtBox;

	@FindBy(name = "smUserSecondryChecklist_0startFlag")
	public WebElement scheduleStartDropDown;

	@FindBy(name = "smUserSecondryChecklist_0scheduleFlag")
	public WebElement scheduleCompletionDropDown;

	@FindBy(xpath = ".//input[@value='WL']")
	public WebElement webLinkRadioBtn ;

	@FindBy(name = "smUserSecondryChecklist_0urlLink")
	public WebElement webLinkAddressBx;

	@FindBy(name = "applyToAll")
	public List<WebElement> applyToAll;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentstoreCombo']/button")
	public WebElement currentOpnerSelectBtn ;

	@FindBy(xpath = ".//div[@id='ms-parentstoreCombo']//label/input[@id='selectAll']")
	public WebElement selectAllOpnerCheck ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserSecondryChecklist_0responsibilityArea']/button")
	public WebElement responsibilityAreaSelctBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserSecondryChecklist_0responsibilityArea']")
	public WebElement selectResponsibilityArea ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserSecondryChecklist_0responsibilityArea']/div/div/input")
	public WebElement responsibilityAreaTextBx ;

	@FindBy(xpath = ".//div[@id='ms-parentsmUserSecondryChecklist_0responsibilityArea']//input[@id='selectAll']")
	public WebElement responsibilityAreaSelectAllBtn ;

	@FindBy(xpath = ".//input[@type='button' and @value='Configure Default Values']")
	public WebElement configureDefaultValueBtn ;

	// Actions Btn
	@FindBy(xpath = ".//input[@value='Actions']")
	public WebElement actionsBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td")
	public WebElement completeLink ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td")
	public WebElement deleteLink ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td")
	public WebElement modifyContactLink ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[6]/td")
	public WebElement groupViewLink ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/button")
	public WebElement modifyContactSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/div/div/input")
	public WebElement modifyContactSearchBx ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']//input[@id='selectAll']")
	public WebElement modifyContactSelectAll ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement bottomDeleteBtn ;

	@FindBy(name = "modifyContact")
	public WebElement modifyContactBoottomBtn;

	@FindBy(linkText = "Add More")
	public WebElement addMoreLink ;

	@FindBy(name = "smUserSecondryChecklist_0groupId")
	public WebElement groupDropDown;

	@FindBy(name = "smUserSecondryChecklist_0franchiseeAccess")
	public WebElement franchiseeAccessDropDown;

	@FindBy(name = "smUserSecondryChecklist_0docType")
	public WebElement uploadFileRadioBtn;

	@FindBy(name = "smUserSecondryChecklist_0attachmentName")
	public WebElement attachmentBrowseBox;

	@FindBy(xpath = ".//input[@value='No']")
	public WebElement noBtn ;

	@FindBy(xpath = ".//*[@id='smUserSecondryChecklist_0scheduleStartDate']")
	public WebElement scheduledStartDate ;

	@FindBy(xpath = ".//*[@id='smUserSecondryChecklist_0scheduleCompletionDate']")
	public WebElement scheduledCompletionDate ;

	@FindBy(name = "smUserSecondryChecklist_0startAlertDate")
	public WebElement reminderStart;

	@FindBy(name = "smUserSecondryChecklist_0alertDate")
	public WebElement reminderCompletion;

	public OpenerStoreSummarySecondaryChecklistPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
