package com.builds.uimaps.opener;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OpenerStoreSummaryStoreListPage {

	@FindBy(linkText = "Add New Franchise Location")
	public WebElement addNewFranchiseLocation ;

	@FindBy(id = "franchiseeName")
	public WebElement StoreNoFranchiseID;

	@FindBy(id = "centerName")
	public WebElement centerName;

	@FindBy(id = "areaID")
	public WebElement areaRegionDrp;

	@FindBy(xpath = ".//select[@id='division' or @id='brands']")
	public WebElement divisionName;

	@FindBy(id = "ms-parentbrands")
	public WebElement selectDivision;

	@FindBy(name = "licenseNo")
	public WebElement licenseNo;

	@FindBy(id = "storeTypeId")
	public WebElement storeType;

	@FindBy(id = "status")
	public WebElement corporateLocation;

	@FindBy(id = "versionID")
	public WebElement agreementVersion;

	@FindBy(id = "reportPeriodStartDate")
	public WebElement royaltyReportingStartDate;

	@FindBy(id = "fbc")
	public WebElement fbc;

	@FindBy(id = "projectId")
	public WebElement projectStatus;

	@FindBy(id = "grandStoreOpeningDate")
	public WebElement expectedOpeningDate;

	@FindBy(name = "grandStoreOpeningDate")
	public WebElement expectedOpeningDate1;

	@FindBy(name = "address")
	public WebElement streetAddressTextBox;

	@FindBy(name = "address2")
	public WebElement address2TextBox;

	@FindBy(name = "city")
	public WebElement cityTextBox;

	@FindBy(id = "countryID")
	public WebElement countryDropdown;

	@FindBy(name = "zipcode")
	public WebElement zipcodeTextBox;

	@FindBy(id = "regionNo")
	public WebElement stateProvinceDropdown;

	@FindBy(name = "storePhone")
	public WebElement PhoneTextBox;

	@FindBy(id = "storePhoneExtn")
	public WebElement phoneExtension;

	@FindBy(id = "storeFax")
	public WebElement faxTextbox;

	@FindBy(id = "storeMobile")
	public WebElement mobileTextBox;

	@FindBy(id = "storeEmail")
	public WebElement EmailTextBox;

	@FindBy(id = "contactTitle")
	public WebElement salutation;

	@FindBy(id = "contactFirstName")
	public WebElement firstName;

	@FindBy(id = "contactLastName")
	public WebElement lastName;

	@FindBy(id = "ownerFirstName")
	public WebElement ownerFirstName;

	@FindBy(id = "ownerLastName")
	public WebElement ownerLastName;

	@FindBy(id = "entityDetail")
	public WebElement ownersType;

	@FindBy(id = "ownerType")
	public WebElement owner;

	@FindBy(id = "Submit")
	public WebElement addBtn;

	@FindBy(name = "foSearchString")
	public WebElement searchTextBx;

	@FindBy(xpath = ".//img[@title='Search by Store No. / Owner']")
	public WebElement searchImgBtn ;

	@FindBy(name = "Reset")
	public WebElement resetBtn;

	@FindBy(id = "Button1")
	public WebElement cancelBtn;

	@FindBy(id = "searchCountry")
	public WebElement country;

	

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(id = "storeStatus")
	public WebElement storeStatus;

	@FindBy(id = "transactionType")
	public WebElement licenseType;

	@FindBy(xpath = ".//*[@id='cboxClose']")
	public WebElement cboxClose ;

	@FindBy(xpath = "html/body/div[2]/table/tbody/tr/td/table[2]/tbody/tr/td/input")
	public WebElement okBtn ;

	@FindBy(name = "fimSearchString")
	public WebElement fimSearchString;

	@FindBy(xpath = ".//a/img[@alt='Search FIM on Name']")
	public WebElement fimSearchBtn ;

	@FindBy(xpath = ".//input[@value='Actions']")
	public WebElement actionBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td")
	public WebElement moveToInfoMgrBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[4]/td")
	public WebElement archiveBtn ;

	@FindBy(xpath = ".//input[@value='Move to Info Mgr']")
	public WebElement moveToInfoMgrBottomBtn ;

	@FindBy(xpath = ".//input[@value='Archive']")
	public WebElement archiveBottomBtn ;

	@FindBy(linkText = "Modify")
	public WebElement modifyLink ;

	@FindBy(id = "commentDate")
	public WebElement completionDateTxBx;

	@FindBy(id = "comments")
	public WebElement commentsTxBx;

	@FindBy(id = "description")
	public WebElement descriptionTxBx;

	@FindBy(xpath = ".//input[@value='Complete']")
	public WebElement completeBtn ;

	@FindBy(linkText = "Primary Checklist")
	public WebElement primaryChecklistLink ;

	@FindBy(name = "Close")
	public WebElement closeBtn;

	// filter
	@FindBy(linkText = "Show Filters")
	public WebElement showFilter ;

	@FindBy(xpath = ".//*[@id='ms-parentsearchStoreNo']")
	public WebElement storeNumber ;

	@FindBy(xpath = ".//*[@id='ms-parentsearchCountry']")
	public WebElement countrySelect ;

	@FindBy(xpath = ".//*[@id='ms-parentsearchState']")
	public WebElement stateSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentcompanyNameId']")
	public WebElement entitySelect ;

	@FindBy(xpath = ".//*[@id='ms-parentownerId']")
	public WebElement ownerNameSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentprojectId1']")
	public WebElement projectStatusSelect ;

	@FindBy(id = "matchType1")
	public WebElement expectedOpeningDateDropDown;

	@FindBy(id = "saveView")
	public WebElement saveViewBtn;

	@FindBy(linkText = "Hide Filters")
	public WebElement hideFilter ;

	@FindBy(id = "search")
	public WebElement searchBtn;

	@FindBy(id = "searchStoreNo")
	public WebElement selectStoreNumber;

	public OpenerStoreSummaryStoreListPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
