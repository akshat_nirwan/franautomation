package com.builds.uimaps.opener;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OpenerStoreSummaryTaskChecklistPage {

	@FindBy(linkText = "Task Checklist")
	public WebElement taskChecklistLnk ;

	@FindBy(id = "commentDate")
	public WebElement completionDateTxBx;

	@FindBy(id = "comments")
	public WebElement commentsTxBx;

	@FindBy(id = "description")
	public WebElement descriptionTxBx;

	// @FindBy(xpath=".//input[@value='Complete']")
	@FindBy(xpath = ".//input[@name='Submit' or @value='Complete']")
	public WebElement completeBtn ;

	@FindBy(name = "foSearchString")
	public WebElement searchTextBx;

	@FindBy(xpath = ".//img[@title='Search by Store No. / Owner']")
	public WebElement searchImgBtn ;

	@FindBy(xpath = ".//input[@class='fTextBox' and @name='checklistNameToBeMatched']")
	public WebElement taskSearchBx ;

	@FindBy(id = "search")
	public WebElement searchBtn;

	

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(linkText = "Store Info")
	public WebElement storeInfoLnk ;

	// modify task
	@FindBy(xpath = ".//*[@id='smUserTaskChecklist_0task']")
	public WebElement taskNameTxtBox ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserTaskChecklist_0contact']/button")
	public WebElement ContactSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserTaskChecklist_0contact']")
	public WebElement selectContact ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserTaskChecklist_0contact']/div/div/input")
	public WebElement contactTextBox ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserTaskChecklist_0contact']//input[@id='selectAll']")
	public WebElement contactSelectAll ;

	@FindBy(name = "smUserTaskChecklist_0referenceParent")
	public WebElement dependentOnDropDown;

	@FindBy(name = "smUserTaskChecklist_0startDate")
	public WebElement startScheduleDateTxtBox;

	@FindBy(name = "smUserTaskChecklist_0scheduleDate")
	public WebElement completionScheduleDateTxtBox;

	@FindBy(name = "smUserTaskChecklist_0startAlertDate")
	public WebElement startReminderTxtBox;

	@FindBy(name = "smUserTaskChecklist_0alertDate")
	public WebElement completionReminderTxtBox;

	@FindBy(name = "smUserTaskChecklist_0startFlag")
	public WebElement scheduleStartDropDown;

	@FindBy(name = "smUserTaskChecklist_0scheduleFlag")
	public WebElement scheduleCompletionDropDown;

	@FindBy(xpath = ".//input[@value='WL']")
	public WebElement webLinkRadioBtn ;

	@FindBy(name = "smUserTaskChecklist_0urlLink")
	public WebElement webLinkAddressBx;

	@FindBy(name = "applyToAll")
	public List<WebElement> applyToAll;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentstoreCombo']/button")
	public WebElement currentOpnerSelectBtn ;

	@FindBy(xpath = ".//div[@id='ms-parentstoreCombo']//label/input[@id='selectAll']")
	public WebElement selectAllOpnerCheck ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserTaskChecklist_0responsibilityArea']/button")
	public WebElement responsibilityAreaSelctBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserTaskChecklist_0responsibilityArea']")
	public WebElement selectResponsibilityArea ;

	@FindBy(xpath = ".//*[@id='ms-parentsmUserTaskChecklist_0responsibilityArea']/div/div/input")
	public WebElement responsibilityAreaTextBx ;

	@FindBy(xpath = ".//div[@id='ms-parentsmUserTaskChecklist_0responsibilityArea']//input[@id='selectAll']")
	public WebElement responsibilityAreaSelectAllBtn ;

	// Actions Btn
	@FindBy(xpath = ".//input[@value='Actions']")
	public WebElement actionsBtn ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td")
	public WebElement overdueTaskLink ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td")
	public WebElement completeLink ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td")
	public WebElement deleteLink ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[4]/td")
	public WebElement modifyContactLink ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[7]/td")
	public WebElement configureDefaultValueLink ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[8]/td")
	public WebElement groupViewLink ;

	// modify Contact

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/button")
	public WebElement modifyContactSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/div/div/input")
	public WebElement modifyContactSearchBx ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']//input[@id='selectAll']")
	public WebElement modifyContactSelectAll ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement bottomDeleteBtn ;

	@FindBy(name = "modifyContact")
	public WebElement modifyContactBoottomBtn;

	@FindBy(linkText = "Add More")
	public WebElement addMoreLink ;

	@FindBy(name = "smUserTaskChecklist_0groupId")
	public WebElement groupDropDown;

	@FindBy(name = "smUserTaskChecklist_0franchiseeAccess")
	public WebElement franchiseeAccessDropDown;

	@FindBy(name = "smUserTaskChecklist_0docType")
	public WebElement uploadFileRadioBtn;

	@FindBy(name = "smUserTaskChecklist_0attachmentName")
	public WebElement attachmentBrowseBox;

	@FindBy(xpath = ".//input[@value='No']")
	public WebElement noBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/button")
	public WebElement contactFilterSelectBtn ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/div/div/input")
	public WebElement contactFilterSearchBx ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']/div//input[@id='selectAll']")
	public WebElement contactFilterSelectAll ;

	public OpenerStoreSummaryTaskChecklistPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
