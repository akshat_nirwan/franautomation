package com.builds.uimaps.opener;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OpenerTasksPage {

	@FindBy(linkText = "Show Filters")
	public WebElement showFilter ;

	@FindBy(xpath = ".//*[@id='ms-parentstoreNameCombo']")
	public WebElement storeNumberSelect ;

	@FindBy(xpath = ".//*[@id='ms-parenttask']")
	public WebElement checkListSelect ;

	@FindBy(xpath = ".//*[@id='filterDiv']//input[@name='checklistNameToBeMatched']")
	public WebElement taskTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentstatusFilter']")
	public WebElement statusSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentcontact']")
	public WebElement contactSelect ;

	@FindBy(xpath = ".//*[@id='ms-parentgroupId']")
	public WebElement groupSelect ;

	@FindBy(id = "matchType1")
	public WebElement scheduleStartDateDropDown;

	@FindBy(id = "reset")
	public WebElement saveViewBtn;

	@FindBy(linkText = "Hide Filters")
	public WebElement hideFilter ;

	@FindBy(id = "search")
	public WebElement searchBtn;

	public OpenerTasksPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
