package com.builds.uimaps.support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminSupportManageContactInformationAddNewContact {

	@FindBy(linkText = "Add Contact Address")
	public WebElement addContactAddressLnk ;

	@FindBy(name = "departmentName")
	public WebElement departmentName;
	@FindBy(name = "firstName")
	public WebElement firstName;
	@FindBy(name = "lastName")
	public WebElement lastName;
	@FindBy(name = "address")
	public WebElement address;
	@FindBy(name = "address2")
	public WebElement address2;
	@FindBy(name = "city")
	public WebElement city;
	@FindBy(name = "countryId")
	public WebElement countryId;
	@FindBy(name = "zipCode")
	public WebElement zipCode;
	@FindBy(name = "regionNo")
	public WebElement state;
	@FindBy(name = "phone1")
	public WebElement phone1;
	@FindBy(name = "email")
	public WebElement email;
	@FindBy(name = "fax")
	public WebElement fax;
	@FindBy(name = "phone2")
	public WebElement phone2;
	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	public AdminSupportManageContactInformationAddNewContact(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
