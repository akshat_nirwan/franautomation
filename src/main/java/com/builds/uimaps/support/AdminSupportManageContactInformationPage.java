package com.builds.uimaps.support;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminSupportManageContactInformationPage {

	@FindBy(linkText = "Add Contact Address")
	public WebElement addContactAddressLnk ;

	@FindBy(name = "departmentName")
	public WebElement departmentName;

	@FindBy(name = "firstName")
	public WebElement firstName;

	@FindBy(name = "lastName")
	public WebElement lastName;

	@FindBy(name = "address")
	public WebElement address;

	@FindBy(name = "address2")
	public WebElement address2;

	@FindBy(name = "city")
	public WebElement city;

	@FindBy(name = "countryId")
	public WebElement countryId;

	@FindBy(name = "zipCode")
	public WebElement zipCode;

	@FindBy(name = "zipcode")
	public WebElement zipCode1;

	@FindBy(name = "regionNo")
	public WebElement state;

	@FindBy(name = "phone1")
	public WebElement phone1;

	@FindBy(name = "email")
	public WebElement email;

	@FindBy(name = "fax")
	public WebElement fax;

	@FindBy(name = "phone2")
	public WebElement phone2;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;
	
	@FindBy(xpath = ".//input[@value='Cancel']")
	public WebElement CancelBtn ;
	
	@FindBy(xpath=".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[5]/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[2]")
	public List<WebElement> ContactAddressDiv;

	public AdminSupportManageContactInformationPage(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}
}
