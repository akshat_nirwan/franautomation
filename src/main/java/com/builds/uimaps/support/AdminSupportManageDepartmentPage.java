package com.builds.uimaps.support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminSupportManageDepartmentPage {

	@FindBy(linkText = "Add Department")
	public WebElement addDepartementLnk ;

	@FindBy(id = "departmentName")
	public WebElement departmentName;

	@FindBy(id = "departmentAbbr")
	public WebElement departmentAbbr;

	@FindBy(css = "button.ms-choice")
	public WebElement mschoice;

	@FindBy(css = "input.searchInputMultiple")
	public WebElement searchInput;

	@FindBy(id = "selectAll")
	public WebElement selectAll;

	@FindBy(id = "defaultUser")
	public WebElement defaultOwner;

	@FindBy(id = "departmentEmail")
	public WebElement departmentEmail;

	@FindBy(id = "Submit")
	public WebElement submit;

	@FindBy(name = "Submit")
	public WebElement close;

	@FindBy(linkText = "User-Departments View")
	public WebElement userDepartmentViewLink ;

	@FindBy(linkText = "Department-Users View")
	public WebElement departmentUserViewLink ;

	@FindBy(id = "ms-parentassignedTo")
	public WebElement assignUserSelectBtn;

	@FindBy(xpath = ".//*[@id='pageid']/a/u[.='Show All']")
	public WebElement showAllLnk ;

	@FindBy(xpath = ".//input[@value='Assign']")
	public WebElement assignBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeAtCbox ;

	public AdminSupportManageDepartmentPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
