package com.builds.uimaps.support;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminSupportManageFAQsPage {

	@FindBy(linkText = "Add New Category")
	public WebElement addNewCategoryLink ;

	@FindBy(id = "categoryName")
	public WebElement categoryName;

	@FindBy(id = "description")
	public WebElement description;

	@FindBy(name = "roleBase")
	public List<WebElement> roleBase;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//*[@id='startRecords']//input[@value='3']")
	public WebElement defaultCorporateRole ;

	@FindBy(xpath = ".//*[@id='startRecords']//input[@value='2']")
	public WebElement defaultFranchiseRole ;
	
	@FindBy(xpath = ".//*[@id='startRecords']//input[@value='5']")
	public WebElement defaultDivisionalRole ;

	@FindBy(xpath = ".//*[@id='startRecords']//input[@value='4']")
	public WebElement defaultRegionalRole ;

	@FindBy(name = "sSearch")
	public WebElement searchTxBx;

	@FindBy(xpath = ".//*[@name='sSearch']/ancestor::tr/td[@class='sBgSq']/a/img")
	public WebElement searchImgBtn ;

	@FindBy(id = "faqQuestion")
	public WebElement faqQuestion;

	@FindBy(id = "faqAnswer")
	public WebElement faqAnswer;

	@FindBy(xpath = ".//a/span[.='Add']")
	public WebElement addUpdateBtn ;

	@FindBy(name = "attachmentName")
	public WebElement attachmentName;

	@FindBy(xpath = ".//input[@value='Attach']")
	public WebElement attachBtn ;

	@FindBy(xpath = ".//input[@value='Done']")
	public WebElement doneBtn ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(linkText = "Add FAQ")
	public WebElement addFAQLink ;

	@FindBy(xpath = ".//a[.='Show All']")
	public WebElement showAll ;

	public AdminSupportManageFAQsPage(WebDriver driver) {

		PageFactory.initElements(driver, this);

	}

}
