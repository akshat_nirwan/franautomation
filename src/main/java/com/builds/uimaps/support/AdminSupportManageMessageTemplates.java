package com.builds.uimaps.support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminSupportManageMessageTemplates {

	@FindBy(linkText = "Add Message Template")
	public WebElement addMessageTemplate ;

	@FindBy(id = "subject")
	public WebElement subject;

	@FindBy(id = "description")
	public WebElement description;

	public AdminSupportManageMessageTemplates(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
