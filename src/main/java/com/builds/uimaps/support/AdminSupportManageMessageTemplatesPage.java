package com.builds.uimaps.support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminSupportManageMessageTemplatesPage {

	@FindBy(linkText = "Add Message Template")
	public WebElement addMessageTemplate ;

	@FindBy(id = "subject")
	public WebElement title;

	@FindBy(id = "description")
	public WebElement description;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	public AdminSupportManageMessageTemplatesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
