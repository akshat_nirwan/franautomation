package com.builds.uimaps.support;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AdminSupportTicketStatus {
	@FindBy(linkText = "Add Ticket Status")
	public WebElement addTicketStatus ;

	@FindBy(name = "statusName")
	public WebElement statusName;

	@FindBy(name = "Add")
	public WebElement add;

	@FindBy(name = "Submit")
	public WebElement submit;

	public AdminSupportTicketStatus() {

	}

}
