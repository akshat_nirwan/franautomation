package com.builds.uimaps.support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminSupportTicketStatusPage {
	@FindBy(xpath = ".//*[.='Add Ticket Status']")
	public WebElement addTicketStatus ;

	@FindBy(name = "statusName")
	public WebElement statusName;

	@FindBy(name = "Add")
	public WebElement add ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement close ;

	@FindBy(name = "templateOrder")
	public WebElement listing;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	@FindBy(name = "cancel")
	public WebElement cancelBtn;

	public AdminSupportTicketStatusPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
