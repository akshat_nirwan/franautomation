package com.builds.uimaps.support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InfoMgrFranchiseesContactHistoryPage {

	@FindBy(name = "fimSearchString")
	public WebElement fimSearchString;

	@FindBy(xpath = ".//img[@alt='Search FIM on Name']")
	public WebElement fimSearchBtn ;

	@FindBy(linkText = "Contact History")
	public WebElement contactHistoryTab ;

	public InfoMgrFranchiseesContactHistoryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
