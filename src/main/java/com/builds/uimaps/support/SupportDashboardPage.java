package com.builds.uimaps.support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SupportDashboardPage {

	@FindBy(xpath = ".//div[@id='ms-parentownerCombo']/button/span")
	public WebElement ownersDropDown ;

	@FindBy(id = "selectAll")
	public WebElement selectAllOwnersDropDown;

	@FindBy(id = "matchType")
	public WebElement reportingDateDropDown;

	@FindBy(id = "view")
	public WebElement viewBtn;

	@FindBy(id = "save")
	public WebElement saveViewBtn;
	
	@FindBy(xpath = ".//*[@id='ms-parentownerCombo']")
	public WebElement ownersSelectBtn;

	@FindBy(xpath = " .//*[@id='ms-parentownerCombo']/div/div/input")
	public WebElement ownersSelectTxBx ;

	@FindBy(xpath = ".//*[@id='ms-parentownerCombo']/div//input[@id='selectAll']")
	public WebElement ownersSelectAll ;

	@FindBy(id = "matchType")
	public WebElement reportingDate;

	@FindBy(id = "view")
	public WebElement viewReport;

	public SupportDashboardPage(WebDriver driver) {

		PageFactory.initElements(driver, this);

	}

}
