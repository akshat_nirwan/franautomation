package com.builds.uimaps.support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SupportFAQsPage {

	@FindBy(name = "sSearch")
	public WebElement searchFAQTxBx1;

	@FindBy(xpath = ".//img[@alt='Keyword Search on FAQ Question / Answer']")
	public WebElement searchBtn1 ;

	public SupportFAQsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
