package com.builds.uimaps.support;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SupportReportedTicketsPage {

	@FindBy(xpath = ".//span[contains(text () , 'Create Ticket')]")
	public WebElement createTicketLink ;

	@FindBy(id = "departmentId")
	public WebElement departmentDropDown;

	@FindBy(id = "priorityID")
	public WebElement priorityDropDown;

	@FindBy(id = "franchiseeNo")
	public WebElement franchiseIdDropDown;

	@FindBy(id = "phoneNumber")
	public WebElement phoneTextBox;

	@FindBy(id = "subject")
	public WebElement subjectTextBox;

	@FindBy(id = "description")
	public WebElement descriptionTextBox;

	@FindBy(id = "attachedFileName")
	public WebElement attachFileBrowseBox;

	@FindBy(id = "addButton")
	public WebElement submitButton;

	@FindBy(name = "reset")
	public WebElement resetButton;

	@FindBy(name = "cancel")
	public WebElement cancelButton;

	@FindBy(xpath = ".//input[@value='Ok' or @value='OK']")
	public WebElement okBtn ;

	@FindBy(name = "supportTopSearch")
	public WebElement supportTopSearch;

	@FindBy(xpath = ".//img[@title='Search Tickets on Number / Subject']")
	public WebElement searchImgBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//span[contains(text () , 'More Actions')]")
	public WebElement moreActionBtn ;

	@FindBy(xpath = ".//a[.='Archived Ticket' or .='Archived Tickets']")
	public WebElement archiveTicketsLink ;

	@FindBy(id = "checkFlagView")
	public WebElement assignToMeChckBx;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(id = "radiobutton")
	public List<WebElement> radioButton;

	@FindBy(id = "withinUsers")
	public WebElement selectOwnerDropDown;

	@FindBy(id = "acrossDepartID")
	public WebElement selectDepartmentDropDown;

	@FindBy(id = "acrossUsers")
	public WebElement selectOwnerAcross;

	@FindBy(name = "categoryID")
	public WebElement categoryName;

	@FindBy(id = "faqQuestion")
	public WebElement faqQuestion;

	@FindBy(id = "faqAnswer")
	public WebElement faqAnswer;

	@FindBy(name = "attachmentFileName")
	public WebElement attacheFile;

	@FindBy(xpath = ".//input[@value='Submit']")
	public WebElement submitBtn ;

	@FindBy(xpath = ".//input[@value='OK']")
	public WebElement okFaqBtn ;

	@FindBy(id = "statusID")
	public WebElement stausDropDown;

	@FindBy(linkText = "Locked Tickets")
	public WebElement lockedTicketLink ;

	@FindBy(linkText = "Tickets")
	public WebElement ticketsLink ;

	@FindBy(linkText = "Reported Tickets")
	public WebElement reportedTickets ;

	public SupportReportedTicketsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
