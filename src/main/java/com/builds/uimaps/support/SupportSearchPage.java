package com.builds.uimaps.support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SupportSearchPage {

	@FindBy(id = "ST:TICKET_NAME:TEXT")
	public WebElement ticketTextBox;

	@FindBy(id = "ST:SUBJECT:TEXT")
	public WebElement subjectTextBox;

	@FindBy(id = "ST:DESCRIPTION:TEXT")
	public WebElement descriptionTextBox;

	@FindBy(xpath = ".//div[@id='ms-parentST:STATUS_ID:COMBOMULTIPLE")
	public WebElement statusDropDown ;

	@FindBy(id = "selectAll")
	public WebElement selectAllStatusCheckBox;

	@FindBy(xpath = "ms-parentST:PRIORITY_ID:COMBOMULTIPLE")
	public WebElement priorityDropDown ;

	@FindBy(id = "selectAll")
	public WebElement selectAllPriorityCheckBox;

	@FindBy(xpath = "ms-parentST:FRANCHISEE_NO:COMBOMULTIPLE")
	public WebElement franchiseIdDropDown ;

	@FindBy(id = "selectAll")
	public WebElement selectAllFranchiseIdCheckBox;

	@FindBy(xpath = "ms-parentST:DEPARTMENT_ID:COMBOMULTIPLE")
	public WebElement departmentDropDown ;

	@FindBy(id = "selectAll")
	public WebElement selectAllDepartmentCheckBox;

	@FindBy(xpath = "ms-parentST:REPORTED_BY:COMBOMULTIPLE")
	public WebElement reportedByDropDown ;

	@FindBy(id = "selectAll")
	public WebElement selectAllReportedByCheckBox;

	@FindBy(xpath = "ms-parentST:ASSIGNED_TO:COMBOMULTIPLE")
	public WebElement assignedToDropDown ;

	@FindBy(id = "selectAll")
	public WebElement selectAllAssignedToCheckBox;

	@FindBy(id = "DATEFROM")
	public WebElement dateFromTextBox;

	@FindBy(id = "DATETO")
	public WebElement dateToTextBox;

	@FindBy(id = "ST:IS_ARCHIVE:CHECKBOX")
	public WebElement searchInArchivedTicketsCheckBox;

	@FindBy(xpath = ".//input[@value='Search']")
	public WebElement searchBtn ;

	@FindBy(xpath = ".//*[@id='module_support']//a[@qat_submodule='Search']")
	public WebElement searchTabLnk ;

	public SupportSearchPage(WebDriver driver) {

		PageFactory.initElements(driver, this);

	}

}
