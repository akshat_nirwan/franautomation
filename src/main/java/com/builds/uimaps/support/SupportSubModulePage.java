package com.builds.uimaps.support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SupportSubModulePage {

	@FindBy(xpath = ".//a[@href='nexturl?nextUrl=supportDashBoard&menuName=support&subMenuName=dashboard']")
	public WebElement dashboardPage ;

	@FindBy(xpath = ".//a[@href='nexturl?nextUrl=troubleTickets&menuName=support&subMenuName=tickets&fromPage=fromSearch']")
	public WebElement troubleTicketPage ;

	@FindBy(xpath = ".//a[@href='nexturl?nextUrl=supportTroubleTicketSearch&menuName=support&subMenuName=search']")
	public WebElement searchPage ;

	@FindBy(linkText = "FAQ")
	public WebElement faqPage ;

	@FindBy(linkText = "Contact Info")
	public WebElement contactInfoPage ;

	@FindBy(linkText = "Reports")
	public WebElement reportsPage ;

	@FindBy(linkText = "Home")
	public WebElement homePage ;

	public SupportSubModulePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
