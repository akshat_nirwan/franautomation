package com.builds.uimaps.support;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SupportTicketsPage {

	@FindBy(xpath = ".//span[contains(text () , 'Create Ticket')]")
	public WebElement createTicketLink ;

	@FindBy(id = "departmentId")
	public WebElement departmentDropDown;

	@FindBy(id = "priorityID")
	public WebElement priorityDropDown;

	@FindBy(id = "franchiseeNo")
	public WebElement franchiseIdDropDown;

	@FindBy(id = "phoneNumber")
	public WebElement phoneTextBox;

	@FindBy(id = "subject")
	public WebElement subjectTextBox;

	@FindBy(name = "subject")
	public WebElement subjectTextBox1;

	@FindBy(id = "description")
	public WebElement descriptionTextBox;

	@FindBy(id = "attachedFileName")
	public WebElement attachFileBrowseBox;

	@FindBy(id = "addButton")
	public WebElement submitButton;

	@FindBy(name = "reset")
	public WebElement resetButton;

	@FindBy(name = "cancel")
	public WebElement cancelButton;

	@FindBy(xpath = ".//input[@value='Ok' or @value='OK']")
	public WebElement okBtn ;

	@FindBy(xpath = ".//input[@class='sBginputSq' and @name='supportTopSearch']")
	public WebElement supportTopSearch;

	@FindBy(xpath = ".//img[@title='Search Tickets on Number / Subject']")
	public WebElement searchImgBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//span[contains(text(),'More Actions')]")
	public WebElement moreActionBtn ;

	@FindBy(linkText = "Archived Tickets")
	public WebElement archiveTicketsLink ;

	@FindBy(id = "checkFlagView")
	public WebElement assignToMeChckBx;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(id = "radiobutton")
	public List<WebElement> radioButton;

	@FindBy(id = "withinUsers")
	public WebElement selectOwnerDropDown;

	@FindBy(xpath = ".//select[@id='acrossDepartID']")
	public WebElement selectDepartmentDropDown ;

	@FindBy(id = "acrossUsers")
	public WebElement selectOwnerAcross;

	@FindBy(name = "categoryID")
	public WebElement categoryName;

	@FindBy(id = "faqQuestion")
	public WebElement faqQuestion;

	@FindBy(id = "faqAnswer")
	public WebElement faqAnswer;

	@FindBy(name = "attachmentFileName")
	public WebElement attacheFile;

	@FindBy(xpath = ".//input[@value='Submit']")
	public WebElement submitBtn ;

	@FindBy(xpath = ".//input[@value='OK']")
	public WebElement okFaqBtn ;

	@FindBy(id = "statusID")
	public WebElement stausDropDown;

	@FindBy(linkText = "Locked Tickets")
	public WebElement lockedTicketLink ;

	@FindBy(id = "addButton")
	public WebElement submitBtnAsk;

	@FindBy(xpath = ".//div[@id='addDIV']//select[@id='departmentId']")
	public WebElement selectDepartmentAskCorporate ;

	@FindBy(linkText = "Reported Tickets")
	public WebElement reportedTickets ;

	@FindBy(xpath = ".//a[contains(@href ,'isArchivedTickets') and @rel='tab3']")
	public WebElement archivedTickets ;

	@FindBy(name = "description")
	public WebElement descriptionTextBoxAskCo;

	@FindBy(xpath = ".//a[@href='#addMessage']")
	public WebElement postMessage ;

	@FindBy(id = "templateID")
	public WebElement templateID;

	@FindBy(id = "add")
	public WebElement addBtn;

	@FindBy(id = "showFilter")
	public WebElement showFilter;

	@FindBy(xpath = ".//*[@id='ms-parentdepartmentId']")
	public WebElement selectDepartment ;

	@FindBy(id = "search")
	public WebElement searchBtn;

	@FindBy(xpath = ".//*[@id='pettabs']//a[.='Tickets']")
	public WebElement ticketTab ;

	@FindBy(xpath = ".//*[@id='ms-parentstatusID']")
	public WebElement selectStatus ;

	public SupportTicketsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
