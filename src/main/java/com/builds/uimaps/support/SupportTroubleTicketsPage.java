package com.builds.uimaps.support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SupportTroubleTicketsPage {

	@FindBy(id = "departmentId")
	public WebElement departmentDropDown;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table[2]/tbody/tr[1]/td[2]/table/tbody/tr/td/table/tbody/tr[2]/td/a[2]/span")
	public WebElement createTicketLink ;

	@FindBy(id = "priorityID")
	public WebElement priorityDropDown;

	@FindBy(id = "franchiseeNo")
	public WebElement franchiseIdDropDown;

	@FindBy(id = "phoneNumber")
	public WebElement phoneTextBox;

	@FindBy(id = "subject")
	public WebElement subjectTextBox;

	@FindBy(id = "description")
	public WebElement descriptionTextBox;

	@FindBy(id = "attachedFileName")
	public WebElement attachFileBrowseBox;

	@FindBy(id = "addButton")
	public WebElement submitButton;

	@FindBy(name = "reset")
	public WebElement resetButton;

	@FindBy(name = "cancel")
	public WebElement cancelButton;

	public SupportTroubleTicketsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
