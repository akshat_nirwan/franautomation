package com.builds.uimaps.support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TheHubPage {

	@FindBy(xpath = ".//*[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(name = "supportTopSearch")
	public WebElement supportTopSearch;

	@FindBy(xpath = ".//img[@title='Search Tickets on Number / Subject']")
	public WebElement searchImgBtn ;

	@FindBy(id = "checkFlagView")
	public WebElement assignToMeChckBx;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	public TheHubPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
