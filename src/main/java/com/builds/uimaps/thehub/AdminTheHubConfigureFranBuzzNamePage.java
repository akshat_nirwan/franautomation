package com.builds.uimaps.thehub;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTheHubConfigureFranBuzzNamePage {

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='FranBuzz' or @name='franbuzzName']")
	public WebElement franBuzzFieldBx ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	///////
	public AdminTheHubConfigureFranBuzzNamePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
