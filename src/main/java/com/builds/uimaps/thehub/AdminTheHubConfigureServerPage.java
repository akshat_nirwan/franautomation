package com.builds.uimaps.thehub;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTheHubConfigureServerPage {

	@FindBy(xpath=".//input[@id='flag' and @value='2']")
	public WebElement FranConnectS3;
	
	//FTP server
	@FindBy(xpath=".//input[@id='flag' and @value='0']")
	public WebElement FTPServer;
	
	@FindBy(id="ftpHostName")
	public WebElement FTPServerIP;
	
	@FindBy(id="ftpUserName")
	public WebElement FTPServerUN;
	
	@FindBy(id="ftpPassword")
	public WebElement FTPServerP;
	
	//SFTP server
	@FindBy(xpath=".//input[@id='flag' and @value='5']")
	public WebElement SFTPServer;
	
	@FindBy(id="sftpHostName")
	public WebElement SFTPServerIP;
	
	@FindBy(id="sftpUserName")
	public WebElement SFTPServerUN;
	
	@FindBy(id="sftpPassword")
	public WebElement SFTPServerP;
	
	//OneDrive 
	@FindBy(xpath=".//input[@id='flag' and @value='3']")
	public WebElement OneDrive;
	
	@FindBy(id="skyUserName")
	public WebElement OneDriveUN;
	
	@FindBy(id="skyPassword")
	public WebElement OneDriveP;
	
	@FindBy(id="skyClientId")
	public WebElement OneDriveClientID;
	
	@FindBy(id="skyClientId")
	public WebElement OneDriveClientSecretID;
	
	// Customer S3
	@FindBy(xpath=".//input[@id='flag' and @value='1']")
	public WebElement CustomerS3;
	
	@FindBy(id="amazonUserName")
	public WebElement CustomerS3UN;
	
	@FindBy(id="amazonPassword")
	public WebElement CustomerS3P;
	
	@FindBy(id="accessKey")
	public WebElement CustomerS3AccessKey;
	
	@FindBy(id="secretKey")
	public WebElement CustomerS3SecretKey;
	
	//Google Drive
	@FindBy(xpath=".//input[@id='flag' and @value='4']")
	public WebElement GoogleDrive;
	
	@FindBy(id="googleUserName")
	public WebElement GoogleDriveUN;
	
	@FindBy(id="googlePassword")
	public WebElement GoogleDriveP;
	
	@FindBy(id="googleClientId")
	public WebElement GoogleDriveClientID;
	
	@FindBy(id="googleClientSecret")
	public WebElement GoogleDriveClientSecretID;
	
	@FindBy(name="Submit")
	public WebElement submitBtn;
	
	@FindBy(xpath=".//input[@value='Ok']")
	public WebElement okayBtn;
	
	public AdminTheHubConfigureServerPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	
	
}
