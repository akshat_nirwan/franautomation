package com.builds.uimaps.thehub;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTheHubEPOllPage {

	@FindBy(id = "ms-parentpostedBy")
	public WebElement postedBySelectBtn;

	@FindBy(id = "startDate")
	public WebElement pollStartDate;

	@FindBy(id = "endDate")
	public WebElement pollEndDate;

	@FindBy(id = "search")
	public WebElement searchBtn;

	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@class='cm_new_button_link' and contains(@value,'New Poll')]")
	public WebElement postNewPollBtn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//input[@class='cm_new_button_link' and contains(@value,'Archive')]")
	public WebElement viewArchiveBtn ;

	@FindBy(id = "epollTitle")
	public WebElement epollTitle;

	@FindBy(name = "isMainPoll")
	public WebElement isMainPoll;

	@FindBy(id = "fromDate")
	public WebElement dateOfPublishingStartDate;

	@FindBy(id = "lastDate")
	public WebElement dateOfPublishingEndDate;

	@FindBy(name = "allUsers")
	public WebElement allUsers;

	@FindBy(name = "corporateUsers")
	public WebElement corporateUsers;

	@FindBy(name = "divisionUsers")
	public WebElement divisionUsers;

	@FindBy(name = "regionalUsers")
	public WebElement regionalUsers;

	@FindBy(name = "franchiseeUsers")
	public WebElement franchiseeUsers;

	@FindBy(name = "flagView")
	public List<WebElement> flafView;

	@FindBy(name = "epollQuestion_0question")
	public WebElement epollQuestion;

	@FindBy(name = "epollQuestion_0optionType")
	public WebElement epollQuestionType;

	@FindBy(name = "epollOptions_0answers")
	public WebElement option1;

	@FindBy(xpath = ".//a[contains(text () , 'Add Options')]")
	public WebElement addOptions ;

	@FindBy(xpath = ".//input[@id='epollOptions_0answers' or @id='epollOptions_2answers']")
	public WebElement option2 ;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath=".//input[@name='hideprint' and @value='Print']")
	public WebElement printBtn;

	@FindBy(xpath=".//input[@name='hideprint' and @value='Close']")
	public WebElement closeBtn;
	
	public AdminTheHubEPOllPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
