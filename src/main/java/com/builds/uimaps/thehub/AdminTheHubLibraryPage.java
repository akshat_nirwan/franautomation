package com.builds.uimaps.thehub;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTheHubLibraryPage {

	@FindBy(linkText = "Add Folder")
	public WebElement addFolder ;

	@FindBy(linkText = "Add Document")
	public WebElement addDocument ;

	@FindBy(xpath = ".//*[@id='display']//span[contains(text () , 'More')]")
	public WebElement moreActions ;

	@FindBy(linkText = "Move Folders")
	public WebElement moveFolders ;

	@FindBy(linkText = "Move Documents")
	public WebElement moveDocuments ;

	@FindBy(linkText = "Configure Document Subtype")
	public WebElement configureDocumentSubtype ;

	@FindBy(linkText = "Add Document Subtype")
	public WebElement addDocumentSubType ;

	@FindBy(xpath = ".//input[@name='typeName' and @type='text']")
	public WebElement documentSubTypeTxBx ;

	@FindBy(xpath = ".//a[@class='catelist bText12bl' and .='Home']")
	public WebElement homeTab ;

	@FindBy(id = "folderName")
	public WebElement folderName;

	@FindBy(name = "isWebPage")
	public List<WebElement> summaryForamte;

	@FindBy(id = "folderSummary")
	public WebElement folderSummary;

	@FindBy(name = "roleBase")
	public List<WebElement> roleBase;

	@FindBy(xpath = ".//input[@name='roleBase' and @value='no']")
	public WebElement accesToAll ;

	@FindBy(xpath = ".//input[@name='roleBase' and @value='yes']")
	public WebElement accesToAllNo ;

	@FindBy(name = "addDocument")
	public List<WebElement> addDocumentFolder;

	@FindBy(xpath = ".//input[@value='Add' and @type='button']")
	public WebElement addBtn1 ;

	@FindBy(name = "templateOrder")
	public WebElement templateOrder;

	@FindBy(id = "add1")
	public WebElement addBtn;
	
	@FindBy(id = "add2")
	public WebElement multiDocAddBtn;;

	@FindBy(id = "libraryDocuments_0documentTitle")
	public WebElement documentTitle1;

	@FindBy(id = "libraryDocuments_0summary")
	public WebElement briefSummary1;

	@FindBy(id = "libraryDocuments_0displayDate")
	public WebElement startDisplayDate1;

	@FindBy(id = "libraryDocuments_0expirationDate")
	public WebElement expirationDate1;

	@FindBy(name = "showWebDocument")
	public List<WebElement> documentType1;

	@FindBy(id = "libraryDocuments_0typeID")
	public WebElement documentSubType1;

	@FindBy(name = "libraryDocuments_0docFileClick")
	public WebElement selectFile1;

	@FindBy(name = "libraryDocuments_0thumbFileUploaded")
	public List<WebElement> thumbnailImage1;

	@FindBy(name = "tempRecommendedDoc")
	public WebElement recommmendedDocumnet;

	@FindBy(xpath = ".//input[@id='tempPrivateDoc' and @value='N']")
	public WebElement accessibilityPublic ;

	@FindBy(xpath = ".//input[@id='tempPrivateDoc' and @value='Y']")
	public WebElement accessibilityPrivate ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;
	
	@FindBy(xpath=".//input[@name='SubmitButton']")
	public WebElement closeConfirmation;

	@FindBy(name = "multipleDoc")
	public List<WebElement> multipleDoc;

	@FindBy(id = "folderNoHome")
	public WebElement folderSelect;

	@FindBy(id = "documentTitle")
	public WebElement documentTitle;

	@FindBy(id = "summary")
	public WebElement briefSummary;

	@FindBy(id = "displayDate")
	public WebElement displayDate;

	@FindBy(id = "expirationDate")
	public WebElement expirationDate;

	@FindBy(name = "isWebDocument")
	public List<WebElement> documentType;

	@FindBy(id = "typeID")
	public WebElement documentSubType;

	@FindBy(id = "fileClick")
	public WebElement fileClick;

	@FindBy(name = "thumbFileUploaded")
	public List<WebElement> thumbFileUploaded;

	@FindBy(xpath = ".//input[@name='linkDoc' and @type='text']")
	public WebElement linkUrl ;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement editorTextArea ;

	@FindBy(id = "movefrom")
	public WebElement moveFromSelect;

	@FindBy(xpath = ".//*[@id='folderNo' or @id='moveto']")
	public WebElement moveToSelect ;

	@FindBy(xpath = ".//input[@value='Move' and @type='submit']")
	public WebElement moveBtn ;

	@FindBy(xpath = ".//input[@value='OK']")
	public WebElement okayBtn ;

	@FindBy(xpath = ".//input[@value='Continue' and @type='submit']")
	public WebElement continueBtn ;

	@FindBy(id = "documentNo")
	public WebElement documentItemSelect;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	@FindBy(linkText = "Index")
	public WebElement indexTab ;

	@FindBy(linkText = "Recommended Documents")
	public WebElement recommendedDocumentsLink ;

	@FindBy(linkText = "All Documents")
	public WebElement allDocumentsLink ;

	@FindBy(id = "recommendedExpDate")
	public WebElement recommendedExpDate;

	@FindBy(xpath = ".//*[@id='th03' and @title='Detail View']")
	public WebElement detailView ;

	@FindBy(xpath = ".//*[@id='th02']")
	public WebElement thumbnailView ;

	@FindBy(xpath = ".//input[@value='Save' and @type='button']")
	public WebElement saveBtn ;

	@FindBy(linkText = "Comment")
	public WebElement comment ;

	@FindBy(xpath = ".//input[@value='Post' and @id='btnReply']")
	public WebElement post ;

	@FindBy(xpath = ".//img[@alt='Delete']")
	public WebElement deleteComment ;

	@FindBy(linkText = "Delete")
	public WebElement deleteLink ;

	@FindBy(xpath = ".//a[@class='catelist bText12bl' and .='Drafts']")
	public WebElement draftsTab ;

	@FindBy(xpath = ".//*[@id='th01']")
	public WebElement listView ;

	@FindBy(linkText = "Modify")
	public WebElement modifyFolderLink ;

	@FindBy(linkText = "Delete")
	public WebElement deleteFolderLink ;

	@FindBy(xpath = ".//*[contains(text () , 'Add Sub Folder')]")
	public WebElement addSubFolder ;

	@FindBy(xpath = ".//*[@id='docType']")
	public WebElement docTypeSelect ;

	@FindBy(xpath = ".//*[@id='selectMenu']")
	public WebElement docSubTypeSelect ;

	@FindBy(xpath = ".//button[contains(text () , 'Search')]")
	public WebElement searchBtn ;

	@FindBy(xpath = ".//*[@id='searchDocuments']")
	public WebElement searchDoc ;

	@FindBy(xpath = ".//img[@alt='Search Library Documents']")
	public WebElement searcDocIcon ;

	@FindBy(xpath = ".//input[@name = 'chkRole' and @value='3']")
	public WebElement corporateUserAccess ;

	@FindBy(xpath = ".//input[@name = 'chkRole' and @value='5']")
	public WebElement divisionalUserAccess ;

	@FindBy(xpath = ".//input[@name = 'chkRole' and @value='2']")
	public WebElement franchiseUserAccess ;

	@FindBy(xpath = ".//input[@name = 'chkRole' and @value='4']")
	public WebElement regionalUserAccess ;

	// @FindBy(id="searchSystem")
	@FindBy(id = "searchDocuments")
	public WebElement searchSystem;

	@FindBy(xpath = ".//img[@alt='Search Library Documents']")
	public WebElement systemSearchBtn ;
	
	@FindBy(xpath=".//*[@id='moreLink']/a")
	public WebElement moreLink;
	
	@FindBy(id="sortBySelect")
	public WebElement sortBy;
	
	@FindBy(xpath="//*[@id='librarybody']//button[@class='yellow-button1']")
	public WebElement searchYellowBtn;
	
	@FindBy(name="back")
	public WebElement backBtn;
	
	@FindBy(linkText="Archived Documents")
	public WebElement archivedDocumentsTab;
	
	@FindBy(xpath=".//input[@name='hideprint' and  @value='Print']")
	public WebElement printButton;
	
	@FindBy(xpath=".//input[@value='Export As Excel']")
	public WebElement exportAsExcelButton;
	
	@FindBy(xpath=".//*[@title='Modify']")
	public WebElement modifyDocument;
	
	@FindBy(xpath=".//*[@title='Delete']")
	public WebElement deleteDocument;
	
	@FindBy(xpath=".//*[contains(text () , 'More Actions')]")
	public WebElement moreActionsUnderFolder;
	
	
	public AdminTheHubLibraryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
