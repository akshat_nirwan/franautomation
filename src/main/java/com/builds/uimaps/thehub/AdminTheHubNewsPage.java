package com.builds.uimaps.thehub;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTheHubNewsPage {

	@FindBy(xpath = ".//a[@class='catelist bText12bl' and .='Home']")
	public WebElement homeTab ;

	@FindBy(xpath = ".//*[contains(text(),'Add Folder')]")
	public WebElement addFolderLink ;

	@FindBy(id = "folderName")
	public WebElement folderName;

	@FindBy(id = "folderSummary")
	public WebElement folderSummary;

	@FindBy(name = "roleBase")
	public List<WebElement> accessibleToAll;

	@FindBy(name = "addDocument")
	public List<WebElement> addNewsItemFolder;

	@FindBy(xpath = ".//*[@id='documentTitle' or @id='newsDocuments_0documentTitle']")
	public WebElement newsItemTitle ;

	@FindBy(xpath = ".//*[@id='summary' or @id='newsDocuments_0summary']")
	public WebElement briefSummary ;

	@FindBy(xpath = ".//*[@name='newsDocuments_0docFile' or @name='docFile']")
	public WebElement newsItemFile ;

	@FindBy(id = "completeNews_ifr")
	public WebElement editorFrameId;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement editorTextArea ;

	@FindBy(xpath = ".//input[@value='Add' and @type='button']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Preview' and @type='button']")
	public WebElement previewBtn ;

	@FindBy(xpath = ".//input[@value='Close' or @name='SubmitButton']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//*[@id='newsdropdown']/span/a/span")
	public WebElement moreActions ;

	@FindBy(xpath = ".//*[@id='newsdropdown']//a[.='Move Folders']")
	public WebElement moveFoldersLink ;

	@FindBy(id = "movefrom")
	public WebElement moveFromSelect;

	@FindBy(xpath = ".//*[@id='folderNo' or @id='moveto']")
	public WebElement moveToSelect ;

	@FindBy(xpath = ".//input[@value='Move' and @type='submit']")
	public WebElement moveBtn ;

	@FindBy(xpath = ".//input[@value='OK']")
	public WebElement okayBtn ;

	@FindBy(linkText = "Add News Item")
	public WebElement addNewsItemLink ;

	@FindBy(xpath = ".//input[@value='Continue' and @type='submit']")
	public WebElement continueBtn ;

	@FindBy(id = "documentNo")
	public WebElement newsItemSelect;

	@FindBy(xpath = ".//*[@id='newsdropdown']//a[.='Move News Items']")
	public WebElement moveNewsItemsLink ;
	
	@FindBy(xpath = ".//*[@id='newsdropdown']//a[.='Define Folder Sequence']")
	public WebElement defineFolderSequence ;

	@FindBy(linkText = "Modify")
	public WebElement modifyFolder ;

	@FindBy(xpath = ".//input[@value='Save' and @type='button']")
	public WebElement saveBtn ;

	@FindBy(linkText = "Delete")
	public WebElement deleteFolder ;

	@FindBy(linkText = "Archive")
	public WebElement archiveLink ;
	
	@FindBy(linkText = "Define Sub Folder Sequence")
	public WebElement defineSubFolderSequenceLink;
	
	@FindBy(linkText = "Define News Item Sequence")
	public WebElement defineNewsItemSequenceLink;

	@FindBy(linkText = "Archived News")
	public WebElement archivedNewsTab ;

	@FindBy(linkText = "Add Sub Folder")
	public WebElement addSubFolder ;

	@FindBy(xpath = ".//input[@placeholder='Search News' and @name='searchDocuments']")
	public WebElement searchNewsTextBx ;

	@FindBy(xpath = ".//img[@alt='Search News']")
	public WebElement searchNewsImg ;

	@FindBy(linkText = "Top Stories")
	public WebElement topStoriesTab ;

	@FindBy(name = "mediaType")
	public List<WebElement> mediaType;

	@FindBy(name = "videoFile")
	public WebElement videoFile;

	@FindBy(name = "embedCode")
	public WebElement embedCode;

	@FindBy(linkText = "View All Stories")
	public WebElement viewAllStories ;

	public AdminTheHubNewsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
