package com.builds.uimaps.thehub;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTheHubRSSFeedsPage {

	@FindBy(linkText = "RSS Feeds")
	public WebElement rssFeedsLink ;

	@FindBy(linkText = "Add RSS Feed")
	public WebElement addRSSFeedsLink ;

	@FindBy(name = "rssFeedName")
	public WebElement rssFeedName;

	@FindBy(name = "rssDesc")
	public WebElement rssDesc;

	@FindBy(name = "roleBase")
	public List<WebElement> roleBase;

	@FindBy(id = "ms-parentroleID")
	public WebElement roles;

	@FindBy(xpath = ".//input[@value='Submit']")
	public WebElement submitBtn ;

	public AdminTheHubRSSFeedsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
