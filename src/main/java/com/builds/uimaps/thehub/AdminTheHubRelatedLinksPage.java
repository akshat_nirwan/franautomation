package com.builds.uimaps.thehub;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTheHubRelatedLinksPage {

	@FindBy(xpath = ".//input[@value='Add New Link' and @type='button']")
	public WebElement addNewLink ;

	@FindBy(id = "linkUrl")
	public WebElement linkUrl;

	@FindBy(id = "linkTitle")
	public WebElement linkTitle;

	@FindBy(id = "description")
	public WebElement description;

	@FindBy(name = "haspass")
	public List<WebElement> roleBaseAccessLink;

	@FindBy(xpath = ".//input[@value='3']")
	public WebElement defaultCorporateRoleCheckBox ;

	@FindBy(xpath = ".//input[@value='4']")
	public WebElement defaultRegionalRoleCheckBox ;

	@FindBy(xpath = ".//input[@value='2']")
	public WebElement defaultFranchiseRoleCheckBox ;

	@FindBy(xpath = ".//input[@value='5']")
	public WebElement defaultDivisionalRoleCheckBox ;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	public AdminTheHubRelatedLinksPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
