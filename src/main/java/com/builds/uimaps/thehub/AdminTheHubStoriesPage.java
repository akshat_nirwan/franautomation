package com.builds.uimaps.thehub;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTheHubStoriesPage {

	@FindBy(linkText = "Stories")
	public WebElement storiesTab ;

	@FindBy(linkText = "Add")
	public WebElement addStoriesLink ;

	@FindBy(xpath = ".//a[@qat_tabname='Home Page View']")
	public WebElement homePageView ;

	@FindBy(xpath = ".//input[@name='storyView' and @value='S']")
	public WebElement singleStoryRadioBtn ;

	@FindBy(xpath = ".//input[@name='storyView' and @value='M']")
	public WebElement multipleStoryRadioBtn ;

	@FindBy(linkText = "Archive Stories")
	public WebElement archiveStoriesLink ;

	@FindBy(linkText = "Archived Stories")
	public WebElement archivedStoriesLink ;

	@FindBy(linkText = "Define Top Story Sequence")
	public WebElement defineTopStorySequenceLink ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBottomBtn ;

	@FindBy(xpath = ".//input[@value='Archive']")
	public WebElement archiveBottomBtn ;

	@FindBy(id = "title")
	public WebElement title;

	@FindBy(id = "summary")
	public WebElement summary;

	@FindBy(id = "startDate")
	public WebElement startDate;

	@FindBy(id = "endDate")
	public WebElement endDate;

	

	@FindBy(name = "accessibleToAll")
	public List<WebElement> storyAccessibleAll;

	@FindBy(id = "ta_ifr")
	public WebElement editorFrameId;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement editorTextArea ;
	
	@FindBy(id="tinymce")
	public WebElement tinymce;

	@FindBy(name = "mediaType")
	public List<WebElement> mediaType;

	@FindBy(name = "imageFile1")
	public WebElement imageFile1;

	@FindBy(name = "selectedFile")
	public List<WebElement> selectedFile;

	@FindBy(name = "topStory")
	public List<WebElement> topStoryAppearsHomePage;

	@FindBy(name = "satisfied")
	public WebElement satisfiedWithPreview;

	@FindBy(xpath = ".//input[@value='Preview' and @type='button']")
	public WebElement previewBtn ;

	@FindBy(id = "ms-parentrolesStory")
	public WebElement userSelect;

	@FindBy(xpath = ".//*[@id='ms-parentrolesStory']/div/div/input")
	public WebElement searchBoxSelectRole ;
	
	@FindBy(xpath=".//input[@class='search-btn on' and @type='button']")
	public WebElement searchIcon;

	@FindBy(xpath = ".//input[@value='Add' and @type='button']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Save' and @type='button']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//input[@value='Save' and @type='submit']")
	public WebElement saveBtnAtHPV ;

	@FindBy(name = "videoFile")
	public WebElement videoFile;

	@FindBy(id = "embedCode")
	public WebElement embedCode;

	@FindBy(linkText = "Show All")
	public WebElement showAll ;

	@FindBy(xpath = ".//input[@value='Restore' and @type='button']")
	public WebElement restoreBottomBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(name = "homeActiveValue_S")
	public WebElement homeActiveCheckBox;

	@FindBy(name = "homeActiveValue_H")
	public WebElement franbuzzCheckBox;

	public AdminTheHubStoriesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
