package com.builds.uimaps.thehub;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TheHubAlertsPage {

	@FindBy(linkText = "Send Alert")
	public WebElement sendAlertLink ;

	@FindBy(id = "theText")
	public WebElement toEmailTextArea;

	@FindBy(xpath = ".//*[@id='sushant']")
	public WebElement autoComplete ;

	@FindBy(xpath = ".//input[@name='subject']")
	public WebElement subject ;

	@FindBy(name = "radioCheckMsg")
	public List<WebElement> texthtmlOption;

	@FindBy(id = "text")
	public WebElement alertMessages;

	@FindBy(xpath = ".//input[@value='Send']")
	public WebElement sendBtn ;

	@FindBy(linkText = "Address book")
	public WebElement addressBook ;

	@FindBy(xpath = ".//*[@id='ms-parentgroupwise']")
	public WebElement selectUsers ;

	@FindBy(xpath = ".//input[@name='Submit']")
	public WebElement submitOk ;

	@FindBy(xpath = ".//input[@placeholder='Search']")
	public WebElement searchTextBx ;

	@FindBy(xpath = ".//td[@id='buttonsTd']/input[contains(@value, 'Add')]")
	public WebElement addUserBtn ;

	@FindBy(linkText = "View Alerts")
	public WebElement viewAlertsLink ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteAlertBottomBtn ;

	@FindBy(id = "searchButton")
	public WebElement searchBtn;

	@FindBy(id = "searchUser")
	public WebElement searchByUser;

	@FindBy(name = "search")
	public WebElement searchAlertWithSelect;

	@FindBy(name = "subtext")
	public WebElement searchAlertWithTxBx;

	@FindBy(id = "view")
	public WebElement viewSelect;

	@FindBy(linkText = "Reports")
	public WebElement reportsTab ;

	@FindBy(xpath = ".//input[@value='Archive']")
	public WebElement archiveBtn ;

	@FindBy(linkText = "Archived Alerts")
	public WebElement archiveAlertLink ;

	@FindBy(xpath = ".//a[@href='archiveAlertsReport']")
	public WebElement archiveAlertReportsLink ;

	@FindBy(xpath = ".//*[@id='addressbook']//input[@name='Submit' and contains(@value ,'Add')]")
	public WebElement addBottmBtn ;

	@FindBy(xpath = ".//input[@placeholder='Search']")
	public WebElement searchTxBx ;

	@FindBy(xpath = ".//img[contains(@title, 'Search')]")
	public WebElement searchImgBtn ;

	@FindBy(name = "search")
	public WebElement selectSubject;

	@FindBy(name = "subtext")
	public WebElement subjectFilter;

	@FindBy(id = "matchType")
	public WebElement selectSendDate;

	@FindBy(name = "Reminder")
	public WebElement reminderBtn;

	@FindBy(name = "signatures")
	public WebElement selectSignatures;

	@FindBy(id = "fromDateD")
	public WebElement fromDateD;

	@FindBy(id = "toDateD")
	public WebElement toDateD;

	public TheHubAlertsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
