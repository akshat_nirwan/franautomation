package com.builds.uimaps.thehub;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TheHubDirectoryPage {

	@FindBy(linkText = "Corporate Users")
	public WebElement corporateUsersTab ;

	@FindBy(xpath = ".//*[@id='ms-parentcountryId']")
	public WebElement selectByCountry ;

	@FindBy(id = "search")
	public WebElement searchTxBx;

	@FindBy(id = "searchButton")
	public WebElement searchBtn;

	@FindBy(xpath = ".//input[@value='Send Message']")
	public WebElement sendMessages ;

	@FindBy(xpath = ".//input[@value='Add To Group']")
	public WebElement addToGroupBtn ;

	@FindBy(xpath = ".//input[@name='sSubject']")
	public WebElement subject ;

	@FindBy(name = "radioCheckMsg")
	public List<WebElement> texthtmlOption;

	@FindBy(id = "sDetail")
	public WebElement messages;

	@FindBy(xpath = ".//input[@value='Send']")
	public WebElement sendBtn ;

	@FindBy(xpath = ".//input[@value='OK' and @name='Button']")
	public WebElement OkBtn ;

	@FindBy(xpath = ".//input[@value='Create New Group']")
	public WebElement createNewGroupLink ;

	@FindBy(linkText = "Groups")
	public WebElement groupsTab ;

	@FindBy(xpath = ".//input[@value='GlobalAddressBook']")
	public WebElement internalAddressBook ;

	@FindBy(xpath = ".//input[@value='PersonalAddressBook']")
	public WebElement externalAddressBook ;

	@FindBy(xpath = ".//input[@placeholder='Search']")
	public WebElement searchTxBxCU ;

	@FindBy(xpath = ".//img[@title='Search Corporate Users']")
	public WebElement searchCUImgIcon ;

	@FindBy(xpath = ".//input[@value='Select']")
	public WebElement selectUser ;
	
	@FindBy(id="selectcorporate")
	public WebElement selectcorporate;

	@FindBy(id = "groupName")
	public WebElement grouopName;

	@FindBy(xpath = ".//input[@value='Create']")
	public WebElement createBtn ;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(linkText = "Divisional Users")
	public WebElement divisionalUserTab ;

	@FindBy(linkText = "Franchise Users")
	public WebElement franchiseUsersTab ;

	@FindBy(xpath = ".//*[@id='ms-parentfranchiseMenu']")
	public WebElement searchByFranchiseId ;

	@FindBy(linkText = "Regional Users")
	public WebElement regionalUserTab ;

	@FindBy(linkText = "Suppliers")
	public WebElement suppliers ;

	@FindBy(xpath = ".//input[@name='searchName']")
	public WebElement suppliersName ;

	@FindBy(xpath = ".//*[contains(text () , 'Internal Address book')]")
	public WebElement internalAddressBookLink ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(name = "searchregionalUsers")
	public WebElement searchregionalUsers;

	@FindBy(xpath = ".//img[@title='Search Regional Users']")
	public WebElement searchRegUserImgIcon ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBottomBtn ;

	@FindBy(id = "text")
	public WebElement alertMessages;

	@FindBy(linkText = "View Alerts")
	public WebElement viewAlertsLink ;

	@FindBy(id = "view")
	public WebElement viewSelect;

	@FindBy(name = "view")
	public WebElement viewSelectAtSentPage;

	@FindBy(linkText = "Inbox")
	public WebElement inboxTab ;

	/*@FindBy(id = "folder")
	public WebElement folderSelect;*/
	
	@FindBy(xpath = ".//select[@id='folder' or @var='folder']")
	public WebElement folderSelect ;

	@FindBy(name = "subject")
	public WebElement subjectAlert;

	@FindBy(xpath = ".//input[@name='displayType' and @value='PersonalAddressBook' and @type='radio']")
	public WebElement externalLnkRadio ;

	public TheHubDirectoryPage(WebDriver driver) {
		PageFactory.initElements(driver,this);
	}
}
