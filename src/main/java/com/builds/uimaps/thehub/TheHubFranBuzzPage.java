package com.builds.uimaps.thehub;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TheHubFranBuzzPage {

	@FindBy(xpath = ".//div[@id='fbz_Information']//*[@id='scrapText']")
	public WebElement scrapText ;

	@FindBy(xpath = ".//*[@id='fbz_post']/a")
	public WebElement shareBtn ;

	@FindBy(xpath = ".//a[.='Wall']")
	public WebElement wallTab ;

	@FindBy(xpath = ".//a[.='Community']")
	public WebElement communityTab ;

	@FindBy(xpath = ".//a[@class='newStoriesPost']")
	public WebElement newPost ;

	@FindBy(id = "title")
	public WebElement title;

	@FindBy(id = "addButton")
	public WebElement postBtn;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement editorTextArea ;

	@FindBy(xpath = ".//select[@name='select']")
	public WebElement selectPost ;

	@FindBy(xpath = ".//a[contains(text () ,'Modify')]")
	public WebElement modifyBtn ;

	@FindBy(linkText = "Create New Group")
	public WebElement createGroupLink ;

	@FindBy(id = "groupTitle")
	public WebElement groupTitle;

	@FindBy(xpath = ".//textarea[@name='groupDescription']")
	public WebElement groupDescription ;

	@FindBy(id = "publicGroup")
	public WebElement publicGroup;

	@FindBy(id = "publicRGroup")
	public WebElement publicRGroup;

	@FindBy(id = "privateGroup")
	public WebElement privateGroup;

	@FindBy(id = "selectAllC")
	public WebElement selectAllCorporateUser;

	@FindBy(xpath = ".//*[@id='Submit2']")
	public WebElement createGroupBtn ;

	@FindBy(xpath = ".//*[@id='BrowserHidden3']")
	public WebElement groupImgeUpload ;

	@FindBy(xpath = ".//a[.='Modify Group']")
	public WebElement modifyGroupBtn ;
	
	@FindBy(id="viewScrap")
	public WebElement shareTo;
	
	@FindBy(id="slick-toggle1")
	public WebElement photoLink;
	
	//Create Album
	@FindBy(xpath=".//a[contains(@onclick,'addAlbum')]")
	public WebElement createAlbum;
	
	@FindBy(id="htitle")
	public WebElement albumtitle;
	
	@FindBy(id="title")
	public WebElement cNewAlbumTitle;
	
	@FindBy(id="description")
	public WebElement albumDescription;
	
	@FindBy(id="photoTitle1")
	public WebElement cPhotoTitle;
	
	@FindBy(id="fileName1")
	public WebElement cPhotoUpload;
	
	@FindBy(xpath=".//a[.='Create']")
	public WebElement createBtn;
	
	@FindBy(xpath=".//div[@class='fbz_asideleft2 pt']//a[.='Cancel']")
	public WebElement cancleBtn;
	
	//Upload Photo
	@FindBy(className="fbz_uploadPhoto")
	public WebElement uploadPhoto;
	
	@FindBy(name="singlePhotoTitle")
	public WebElement photoTitle;
	
	@FindBy(id="BrowserHidden3")
	public WebElement browsePhoto;
	
	@FindBy(id="scrapDescription")
	public WebElement photoDescription;
	
	@FindBy(xpath=".//a[contains(@onclick,'addSinglePhotograph')]")
	public WebElement shareSinglePhoto;
	
	@FindBy(xpath=".//a[contains(@onclick,'addSinglePhotograph')]/parent::*/select[@id='viewScrap']")
	public WebElement photoShareTo;

	//Search Post
	@FindBy(id="searchText")
	public WebElement searchWallPosts;
	
	@FindBy(xpath=".//a[@title='Search Wall Posts']")
	public WebElement searchWallPostsIcon;
	
	@FindBy(xpath=".//img[@alt='Google']")
	public WebElement lnkVerify;
	
	//Video
	@FindBy(id="slick-Video")
	public WebElement videoLnk;

	@FindBy(id="embedVideoTitle")
	public WebElement videoTitle;
	
	@FindBy(name="scrapVideoText")
	public WebElement textAreaVideo;
	
	@FindBy(xpath=".//div[@id='fbz_video']//a[contains(text(),'Share')]")
	public WebElement ShareVideoBtn;
	
	//Comment
	@FindBy(className="fbz_addcomment")
	public WebElement moreCommentLink;
	
	//Create Gallery Album
	@FindBy(className="fbz_Gallery")
	public WebElement galleryTab;
	
	@FindBy(xpath=".//a[@class='fbz_albumsIco fbzmenu']")
	public WebElement albumTab;
	
	@FindBy(xpath=".//a[@class='fbz_editIcon inline']")
	public WebElement createAnAlbum;
	
	//Add Photos
	@FindBy(linkText="Add Photos")
	public WebElement addPhotosLnk;
	
	@FindBy(id="franPhotos_0photoTitle")
	public WebElement apTitle0;
	
	@FindBy(id="fileName0")
	public WebElement apBrowse0;
	
	@FindBy(id="franPhotos_1photoTitle")
	public WebElement apTitle1;
	
	@FindBy(id="fileName1")
	public WebElement apBrowse1;
	
	@FindBy(linkText="Upload")
	public WebElement apUploadBtn;
	
	//Upload photos
	@FindBy(className="fbz_uploadIcon")
	public WebElement uploadPhotosInGallery;
	
	@FindBy(name="singlePhotoUpload1")
	public WebElement singlePhotoUpload1;
	
	//Search Members
	@FindBy(xpath=".//*[@id='searchString' and not(@type='hidden')]")
	public WebElement searchString;
	
	@FindBy(xpath=".//a[contains(text(),'Photos')]")
	public WebElement userPhotosTab;
	
	@FindBy(xpath=".//a[@class='fbz_addcomment']")
	public WebElement addCommentLink;
	
	@FindBy(id="photoComment")
	public WebElement commentTextArea;
	
	@FindBy(name="commentButton")
	public WebElement addCommentBtn;
	
	@FindBy(id="backToAlbums")
	public WebElement backToAlbumsBtn;
	
	@FindBy(id="title0")
	public WebElement uPhotoTitleTxBx;
	
	@FindBy(id="title1")
	public WebElement uPhotoTitleTxBx1;
	
	@FindBy(linkText="Update")
	public WebElement uPhotoTitleUpdateBtn;
	
	@FindBy(xpath=".//a[@href='userProfile']")
	public WebElement userProfileLink;
	
	//Documents
	@FindBy(id="slick-Document")
	public WebElement documentLink;
	
	@FindBy(id="documentTitle")
	public WebElement documentTitle;
	
	@FindBy(id="documentUpload")
	public WebElement uploadDocumentTxBx;
	
	@FindBy(xpath=".//*[@id='fbz_Document']//*[@id='viewScrap']")
	public WebElement docShareTo;
	
	@FindBy(xpath=".//div[@id='fbz_Document']//a[@class='fbz_btn' and contains(text(),'Share')]")
	public WebElement shareDocumentBtn;
	
	@FindBy(linkText="Edit")
	public WebElement editUserProfile;
	
	@FindBy(id="birthMonth")
	public WebElement birthMonth;
	
	@FindBy(id="birthDate")
	public WebElement birthDate;
	
	@FindBy(linkText="Save Changes")
	public WebElement saveChangesBtn;
	
	@FindBy(xpath=".//a[@class='bithday_calendar' and .='Birthday Calendar']")
	public WebElement birthDateCalendarLink;
	
	@FindBy(xpath=".//*[@class='arrow_right']")
	public WebElement nextArrow;
	
	@FindBy(xpath=".//*[@class='arrow_left']")
	public WebElement prevArrow;
	
	@FindBy(xpath=".//*[@class='subTitleText']")
	public WebElement currentMonth;
	
	@FindBy(id="textfield2")
	public WebElement birthDayTextField;
	
	@FindBy(xpath=".//div[@id='cboxLoadedContent']//*[@id='textfield2']/following-sibling::a[.='Send']")
	public WebElement sendBWishesBtn;
	
	@FindBy(xpath=".//a[@class='fbz_Inbox' and .='Inbox']")
	public WebElement inboxTab;
	
	@FindBy(id="startDate")
	public WebElement startDate;
	
	@FindBy(id="expDate")
	public WebElement expDate;
	
	@FindBy(xpath=".//a[@class='fbz_Link' and contains(text(),'Link')]")
	public WebElement linkTab;
	
	@FindBy(id="shareLink")
	public WebElement linkTxBx;
	
	@FindBy(xpath=".//div[@id='fbz_Link']//a[contains(text(),'Share')]")
	public WebElement shareLinkBtn;
	
	@FindBy(xpath=".//*[@id='fbz_Link']//*[@id='viewScrap']")
	public WebElement shareToLinkDD;
	
	@FindBy(linkText="Add Image")
	public WebElement addImage;
	
	@FindBy(id="franCommunityImages_1imageFile")
	public WebElement fcImageUpload;
	
	public TheHubFranBuzzPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
