package com.builds.uimaps.thehub;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TheHubHomePage {

	@FindBy(xpath = ".//a[@original-title='Take a Quick Overview of Latest Updates']")
	public WebElement homeLink ;

	@FindBy(id = "widgets")
	public WebElement manageWidgetsLink;

	@FindBy(id = "messagesHomePagec")
	public WebElement messagesHomePageWidgets;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//input[@value='Cancel']")
	public WebElement cancelBtn ;

	@FindBy(xpath = ".//*[@id='alertsHomePagec']")
	public WebElement alertsHomePagec ;

	@FindBy(xpath = ".//*[@id='eventCalenderc']")
	public WebElement eventCalenderc ;

	@FindBy(xpath = ".//*[@id='todaysEventc']")
	public WebElement todaysEventc ;
	
	@FindBy(xpath = ".//*[@id='libraryDocumentsc']")
	public WebElement libraryDocumentsc ;

	@FindBy(linkText = "Home")
	public WebElement homeTab ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(linkText = "All Ongoing Polls")
	public WebElement allOngoingPoll ;

	@FindBy(xpath = ".//*[@id='epollHomePageContent']//a[contains(text(),'Previous Results')]")
	public WebElement previousResults ;

	@FindBy(xpath = ".//*[@id='right' and @title='Next Story']")
	public WebElement nextStoryLnk ;

	@FindBy(linkText = "Restore default settings")
	public WebElement restoredefaultsettings ;

	@FindBy(xpath = ".//*[@id='askCorporatec']")
	public WebElement askCorporatec ;

	@FindBy(id = "departmentId")
	public WebElement departmentId;

	@FindBy(xpath = ".//*[@id='askCorporateContent']//input[@name='subject']")
	public WebElement askCorporateSubject ;

	@FindBy(xpath = ".//*[@id='askCorporateContent']//textarea[@name='description']")
	public WebElement askCorporateDescription ;

	@FindBy(id = "addButton")
	public WebElement addButton;

	@FindBy(id = "troubleTicketsHomePagec")
	public WebElement troubleTicketsHomePagec;
	
	@FindBy(xpath=".//a[@class='next1']")
	public WebElement nextMultiStory;
	
	@FindBy(xpath=".//*[@id='downloadedDocumentsAnchor']//*[.='Most Downloaded']")
	public WebElement mostDownloadedLink;

	public TheHubHomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
