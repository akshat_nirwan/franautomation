package com.builds.uimaps.thehub;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TheHubLibraryPage {

	@FindBy(xpath = ".//a[@class='catelist bText12bl' and contains(text () , 'Home')]")
	public WebElement homeTab1 ;

	@FindBy(linkText = "Add Folder")
	public WebElement addFolder ;

	@FindBy(linkText = "Add Document")
	public WebElement addDocument;

	@FindBy(xpath = ".//*[@id='display']//span[contains(text () , 'More')]")
	public WebElement moreActions ;

	@FindBy(linkText = "Move Folders")
	public WebElement moveFolders ;

	@FindBy(linkText = "Move Documents")
	public WebElement moveDocuments ;

	@FindBy(linkText = "Configure Document Subtype")
	public WebElement configureDocumentSubtype ;

	@FindBy(linkText = "Add Document Subtype")
	public WebElement addDocumentSubType ;

	@FindBy(xpath = ".//input[@name='typeName' and @type='text']")
	public WebElement documentSubTypeTxBx ;

	@FindBy(id = "folderName")
	public WebElement folderName;

	@FindBy(name = "isWebPage")
	public List<WebElement> summaryForamte;

	@FindBy(id = "folderSummary")
	public WebElement folderSummary;

	@FindBy(name = "roleBase")
	public List<WebElement> roleBase;

	@FindBy(xpath = ".//input[@value='4']")
	public WebElement defaultRegionalRole;

	@FindBy(name = "addDocument")
	public List<WebElement> addDocumentFolder;

	@FindBy(xpath = ".//input[@value='Add' and @type='button']")
	public WebElement addBtn1 ;

	@FindBy(name = "templateOrder")
	public WebElement templateOrder;

	@FindBy(id = "add1")
	public WebElement addBtn;

	@FindBy(id = "libraryDocuments_0documentTitle")
	public WebElement documentTitle1;

	@FindBy(id = "libraryDocuments_0summary")
	public WebElement briefSummary1;

	@FindBy(id = "libraryDocuments_0displayDate")
	public WebElement startDisplayDate1;

	@FindBy(id = "libraryDocuments_0expirationDate")
	public WebElement expirationDate1;

	@FindBy(name = "showWebDocument")
	public List<WebElement> documentType1;

	@FindBy(id = "libraryDocuments_0typeID")
	public WebElement documentSubType1;

	@FindBy(name = "libraryDocuments_0docFileClick")
	public WebElement selectFile1;

	@FindBy(name = "libraryDocuments_0thumbFileUploaded")
	public List<WebElement> thumbnailImage1;

	@FindBy(name = "tempRecommendedDoc")
	public WebElement recommmendedDocumnet;

	@FindBy(xpath = ".//input[@id='tempPrivateDoc' and @value='N']")
	public WebElement accessibilityPublic ;

	@FindBy(xpath = ".//input[@id='tempPrivateDoc' and @value='Y']")
	public WebElement accessibilityPrivate ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn;

	@FindBy(name = "multipleDoc")
	public List<WebElement> multipleDoc;

	@FindBy(id = "folderNoHome")
	public WebElement folderSelect;

	@FindBy(id = "documentTitle")
	public WebElement documentTitle;

	@FindBy(id = "summary")
	public WebElement briefSummary;

	@FindBy(id = "displayDate")
	public WebElement displayDate;

	@FindBy(id = "expirationDate")
	public WebElement expirationDate;

	@FindBy(name = "isWebDocument")
	public List<WebElement> documentType;

	@FindBy(id = "typeID")
	public WebElement documentSubType;

	@FindBy(id = "fileClick")
	public WebElement fileClick;

	@FindBy(name = "thumbFileUploaded")
	public List<WebElement> thumbFileUploaded;

	@FindBy(xpath = ".//input[@name='linkDoc' and @type='text']")
	public WebElement linkUrl ;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement editorTextArea ;

	@FindBy(id = "movefrom")
	public WebElement moveFromSelect;

	@FindBy(xpath = ".//*[@id='folderNo' or @id='moveto']")
	public WebElement moveToSelect ;

	@FindBy(xpath = ".//input[@value='Move' and @type='submit']")
	public WebElement moveBtn ;

	@FindBy(xpath = ".//input[@value='OK']")
	public WebElement okayBtn ;

	@FindBy(xpath = ".//input[@value='Continue' and @type='submit']")
	public WebElement continueBtn ;

	@FindBy(id = "documentNo")
	public WebElement documentItemSelect;

	@FindBy(xpath = ".//input[@value='Modify']")
	public WebElement modifyBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	@FindBy(linkText = "Index")
	public WebElement indexTab ;

	@FindBy(linkText = "Recommended Documents")
	public WebElement recommendedDocumentsLink ;

	@FindBy(linkText = "All Documents")
	public WebElement allDocumentsLink ;

	@FindBy(id = "recommendedExpDate")
	public WebElement recommendedExpDate;

	@FindBy(xpath = ".//*[@id='th03']")
	public WebElement detailView ;

	@FindBy(xpath = ".//*[@id='th02']")
	public WebElement thumbnailView ;

	@FindBy(xpath = ".//input[@value='Save' and @type='button']")
	public WebElement saveBtn ;

	@FindBy(linkText = "Comment")
	public WebElement comment ;

	@FindBy(xpath = ".//input[@value='Post' and @id='btnReply']")
	public WebElement post ;

	@FindBy(xpath = ".//img[@alt='Delete']")
	public WebElement deleteComment ;

	@FindBy(xpath = ".//*[@id='librarybody']//a[.='Delete']")
	public WebElement deleteLink ;

	@FindBy(linkText = "Drafts")
	public WebElement draftsTab ;

	@FindBy(xpath = ".//*[@id='th01']")
	public WebElement listView ;

	@FindBy(linkText = "Modify")
	public WebElement modifyFolderLink;

	@FindBy(linkText = "Delete")
	public WebElement deleteFolderLink ;

	@FindBy(xpath = ".//*[contains(text () , 'Add Sub Folder')]")
	public WebElement addSubFolder ;

	@FindBy(xpath = ".//*[@id='docType']")
	public WebElement docTypeSelect ;

	@FindBy(xpath = ".//*[@id='selectMenu']")
	public WebElement docSubTypeSelect ;

	@FindBy(xpath = ".//button[contains(text () , 'Search')]")
	public WebElement searchBtn ;

	@FindBy(id = "sortBySelect")
	public WebElement sortBySelect;
	
	@FindBy(id = "searchAllDocument")
	public WebElement searchAllDocument;

	@FindBy(xpath = ".//img[@title='Search All Documents']")
	public WebElement searchItem ;

	@FindBy(id = "searchCriteria")
	public WebElement searchCriteria;

	@FindBy(id = "searchButton")
	public WebElement searchButton;
	
	@FindBy(id="searchExact")
	public WebElement searchExact;
	
	@FindBy(id="recommendedDocumentsAnchor")
	public WebElement recommendedDocumentsAnchor;
	
	//Global Search
	@FindBy(id="searchTermKey")
	public WebElement searchKeyTextBx;
	
	
	private AdminTheHubLibraryPage adminLibraryObj=null;
	
	public AdminTheHubLibraryPage adminLibraryPage(WebDriver driver){
		if (adminLibraryObj==null) {
			return new AdminTheHubLibraryPage(driver);
		}
		return adminLibraryObj;
	}
	public TheHubLibraryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
