package com.builds.uimaps.thehub;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.builds.uimaps.common.CommonUI;

public class TheHubMessagesPage {

	@FindBy(linkText = "Compose")
	public WebElement composeLink ;

	@FindBy(id = "theText")
	public WebElement toEmailTextArea;

	@FindBy(id = "sCcAutoUser")
	public WebElement ccMailTextArea;

	@FindBy(id = "sBCCAutoUser")
	public WebElement bccMailTextArea;

	@FindBy(xpath = ".//*[@id='sushant']")
	public WebElement autoComplete ;

	@FindBy(name = "sSubject")
	public WebElement subject;

	@FindBy(name = "radioCheckMsg")
	public List<WebElement> texthtmlOption;

	@FindBy(id = "sDetail")
	public WebElement messages;

	@FindBy(xpath = ".//input[@value='Send']")
	public WebElement sendBtn ;

	@FindBy(xpath = ".//input[@value='OK' and @name='Button']")
	public WebElement OkBtn ;

	@FindBy(xpath = ".//*[@id='addressLinkDivTo']/a")
	public WebElement addressBookTo;

	@FindBy(xpath = ".//*[@id='addressLinkDivCc']/a")
	public WebElement addressBookCC;

	@FindBy(xpath = ".//*[@id='addressLinkDivBcc']/a")
	public WebElement addressBookBCC;

	@FindBy(xpath = ".//*[@id='ms-parentgroupwise']")
	public WebElement selectUsers ;

	@FindBy(xpath = ".//input[@name='Submit']")
	public WebElement submitOk ;

	@FindBy(xpath = ".//input[@placeholder='Search' and @type='text' and @value='Search']")
	public WebElement searchTextBx ;

	@FindBy(xpath = ".//td[@id='buttonsTd']/input[contains(@value, 'Add') and @name='Submit']")
	public WebElement addUserBtn;

	@FindBy(xpath = "//*[@id='userLists']//a/img[contains(@title , 'Search')]")
	public WebElement searchImgBtn ;

	@FindBy(xpath = ".//input[@placeholder='Search']")
	public WebElement searchTxBx ;

	@FindBy(xpath = ".//input[@value='Global' and @name='contactlist1']")
	public WebElement internalContact ;

	@FindBy(id = "isSaved")
	public WebElement isSavedCheck;

	@FindBy(id = "isSentMail")
	public WebElement isSentMailCheck;

	/*@FindBy(id = "folder")
	public WebElement folderSelect;*/

	@FindBy(xpath = ".//select[@name='view']")
	public WebElement viewSelect ;

	@FindBy(linkText = "Inbox")
	public WebElement inboxTab ;

	@FindBy(xpath = ".//*[@id='buttonsTd']/input[@name='Submit']")
	public WebElement addBtnAtTop ;

	@FindBy(linkText = "Folders")
	public WebElement foldersLink ;

	@FindBy(xpath = ".//input[@value='Add Folder']")
	public WebElement addFolder ;

	@FindBy(xpath = ".//input[@type='text' and @name='folderName']")
	public WebElement folderTxBx ;

	@FindBy(xpath = ".//input[@value='Save' and @name='Button']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//select[@id='folder' or @var='folder']")
	//@FindBy(xpath = ".//select[@var='folder']")
	public WebElement currentFolderSelect ;

	@FindBy(linkText = "Address Book")
	public WebElement addressBookTab ;

	@FindBy(xpath = ".//input[@value='Add New Contact']")
	public WebElement addNewContactLink ;

	@FindBy(id = "firstname")
	public WebElement firstname;

	@FindBy(id = "lastName")
	public WebElement lastName;

	@FindBy(id = "nickname")
	public WebElement nickName;

	@FindBy(id = "phoneNumber")
	public WebElement phoneNumber;

	@FindBy(id = "phone1Extn")
	public WebElement phoneNumberExten;

	@FindBy(id = "faxNumber")
	public WebElement faxNumber;

	@FindBy(id = "mobileNumber")
	public WebElement mobileNumber;

	@FindBy(id = "defmailid")
	public WebElement emailId;

	@FindBy(id = "othermailid")
	public WebElement otherEmailId;

	@FindBy(id = "organization")
	public WebElement organization;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "countryid")
	public WebElement country;

	@FindBy(id = "stateid")
	public WebElement state;

	@FindBy(id = "zip")
	public WebElement zip;

	@FindBy(id = "otherInformation")
	public WebElement otherInformation;

	@FindBy(xpath = ".//input[@value='Add Contact']")
	public WebElement addConatactBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBtn ;

	@FindBy(xpath = ".//input[@value='Send Email']")
	public WebElement sendEmailBtn ;

	@FindBy(xpath = ".//input[@value='Save Contact']")
	public WebElement saveContactBtn ;

	@FindBy(xpath = ".//select[@name='select']")
	public WebElement viewContactSelect ;

	@FindBy(id = "corpResultsPerPage")
	public WebElement corpResultsPerPage;

	@FindBy(xpath = ".//input[@value='Send Message']")
	public WebElement sendMessageBtn ;

	@FindBy(xpath = ".//select[@name='globalUserType']")
	public WebElement userTypeSelect ;

	@FindBy(xpath = ".//*[.='Next']")
	public WebElement nextLink ;

	@FindBy(id = "divResultsPerPage")
	public WebElement divResultsPerPage;

	@FindBy(id = "regResultsPerPage")
	public WebElement regResultsPerPage;

	@FindBy(id = "franResultsPerPage")
	public WebElement franResultsPerPage;

	@FindBy(linkText = "My Signatures")
	public WebElement mySignaturesTab ;

	@FindBy(xpath = ".//input[@value='Add Signature']")
	public WebElement addSignatureLink ;

	@FindBy(xpath = ".//input[@name='signatureName']")
	public WebElement signaturesTitle ;

	@FindBy(xpath = ".//input[@value='Add Signature']")
	public WebElement addSignaturebtn ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveSignatureBtn ;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement editorTextArea ;

	@FindBy(xpath = ".//a[@qat_tabname='Search']")
	public WebElement searchTab ;

	@FindBy(xpath = ".//a[@qat_tabname='Export']")
	public WebElement exportTab ;

	@FindBy(id = "folderNo")
	public WebElement searchSelectFolder;

	@FindBy(id = "from")
	public WebElement searchFrom;

	@FindBy(id = "to")
	public WebElement searchTo;

	@FindBy(id = "subject")
	public WebElement searchSubject;

	@FindBy(id = "text")
	public WebElement searchText;

	@FindBy(id = "date")
	public WebElement searchFromDate;

	@FindBy(id = "datefrom")
	public WebElement searchToDate;

	@FindBy(xpath = ".//input[@name='Button322']")
	public WebElement saveAsDraft ;

	@FindBy(linkText = "Auto Reply for Out of Office")
	public WebElement autoReplyForOutOfOffice;

	@FindBy(xpath = ".//*[@id='oooRadio']/input[@value='1' and @type='radio']")
	public WebElement outOfOfficeStatusOut ;

	@FindBy(id = "userMessage")
	public WebElement userMessage;

	@FindBy(xpath = ".//input[@name='ok']")
	public WebElement submitAutoReplyPage ;

	@FindBy(name = "category")
	public WebElement searchMessageswithSelect;

	@FindBy(xpath = ".//*[@name='subject' and @type='text']")
	public WebElement searchMessagesWithTextBx ;

	@FindBy(name = "mark")
	public WebElement markSelect;

	@FindBy(id = "deleteButton")
	public WebElement deleteButtonInbox;

	@FindBy(id = "toFolderNo")
	public WebElement moveToFolder;

	@FindBy(xpath = ".//*[@id='addressLinkDivTo']/a")
	public WebElement addressBookToLnk;

	@FindBy(xpath = ".//*[@id='addressLinkDivCc']/a")
	public WebElement addressBookCCLnk;

	@FindBy(xpath = ".//*[@id='addressLinkDivBcc']/a")
	public WebElement addressBookBCCLnk;

	@FindBy(id = "keywords-button")
	public WebElement keywordsbutton;

	@FindBy(xpath = ".//input[@name='contactlist1' and @value='Personal']")
	public WebElement externalContact;

	@FindBy(xpath = ".//*[@id='attachmentTable']//a[contains(text(),'Add')]")
	public WebElement addAttachament;

	@FindBy(name = "attachmentName")
	public WebElement attachmentName;

	@FindBy(id = "attach1")
	public WebElement attachBtn;

	@FindBy(xpath = ".//input[@value='Done' and @type='button']")
	public WebElement doneBtn;

	@FindBy(xpath = ".//*[contains(text(),'Attachment')]/parent::*/following-sibling::td//a[contains(text(),'Save')]")
	public WebElement saveFileBtn;

	@FindBy(xpath = ".//input[@name='msgaction' and @value='Reply']")
	public WebElement replyBtn;

	@FindBy(xpath = ".//input[@name='msgaction' and @value='Reply All']")
	public WebElement replyAllBtn;

	@FindBy(xpath = ".//input[@name='msgaction' and @value='Forward']")
	public WebElement forwardBtn;

	@FindBy(xpath = ".//input[@name='msgaction' and @value='Add Sender']")
	public WebElement addSenderBtn;

	@FindBy(xpath = ".//input[@name='msgaction' and @value='Print']")
	public WebElement printBtn;

	private CommonUI CommonUi = null;

	public CommonUI commonUi(WebDriver driver) {

		if (CommonUi == null) {
			CommonUi = new CommonUI(driver);
		}
		return CommonUi;
	}

	@FindBy(xpath = ".//select[@id='toFolderNo']/parent::td/following-sibling::td/input[@value='OK']")
	public WebElement OKBtn;
	
	@FindBy(id="isSignatured")
	public WebElement isSignatured;
	
	@FindBy(id="select")
	public WebElement keyWordSelect;

	public TheHubMessagesPage(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}
}
