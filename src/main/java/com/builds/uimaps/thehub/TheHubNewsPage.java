package com.builds.uimaps.thehub;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TheHubNewsPage {

	@FindBy(xpath = ".//a[@class='catelist bText12bl' and .='Home']")
	public WebElement homeTab ;

	@FindBy(linkText = "Top Stories")
	public WebElement topStoriesTab ;

	@FindBy(linkText = "Add News Item")
	public WebElement addNewsItemLink ;

	@FindBy(xpath = ".//*[@id='documentTitle' or @id='newsDocuments_0documentTitle']")
	public WebElement newsItemTitle ;

	@FindBy(xpath = ".//*[@id='summary' or @id='newsDocuments_0summary']")
	public WebElement briefSummary ;

	@FindBy(name = "mediaType")
	public List<WebElement> mediaType;

	@FindBy(xpath = ".//*[@name='newsDocuments_0docFile' or @name='docFile']")
	public WebElement newsItemFile ;

	@FindBy(id = "completeNews_ifr")
	public WebElement editorFrameId;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement editorTextArea ;

	@FindBy(xpath = ".//input[@value='Add' and @type='button']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(name = "videoFile")
	public WebElement videoFile;

	@FindBy(name = "embedCode")
	public WebElement embedCode;

	@FindBy(linkText = "View All Stories")
	public WebElement viewAllStories ;

	@FindBy(xpath = ".//*[@id='newsdropdown']/span/a/span")
	public WebElement moreActions ;

	@FindBy(linkText = "Modify")
	public WebElement modifyFolder ;

	@FindBy(id = "folderName")
	public WebElement folderName;

	@FindBy(id = "folderSummary")
	public WebElement folderSummary;

	@FindBy(name = "roleBase")
	public List<WebElement> accessibleToAll;

	@FindBy(xpath = ".//input[@value='Save' and @type='button']")
	public WebElement saveBtn ;

	@FindBy(linkText = "Archived News")
	public WebElement archivedNewsTab ;

	@FindBy(linkText = "Add Sub Folder")
	public WebElement addSubFolder ;

	@FindBy(name = "addDocument")
	public List<WebElement> addNewsItemFolder;

	@FindBy(linkText = "Delete")
	public WebElement deleteTab ;

	@FindBy(xpath = ".//input[@value='OK']")
	public WebElement okBtn ;

	@FindBy(linkText = "Archive")
	public WebElement archiveLink ;

	@FindBy(id = "searchAllDocument")
	public WebElement searchAllDocument;

	@FindBy(xpath = ".//img[@title='Search All Documents']")
	public WebElement searchItem ;

	@FindBy(id = "searchCriteria")
	public WebElement searchCriteria;

	@FindBy(id = "searchButton")
	public WebElement searchButton;

	@FindBy(xpath=".//*[@id='newsDocument']//input[@value='Preview']")
	public WebElement previewBtn;
	
	public TheHubNewsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
