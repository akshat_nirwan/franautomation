package com.builds.uimaps.thehub;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TheHubRelatedLinksPage {

	@FindBy(xpath = ".//input[@value='Add New Link']")
	public WebElement addNewLink ;

	@FindBy(id = "linkUrl")
	public WebElement linkUrl;

	@FindBy(id = "linkTitle")
	public WebElement linkTitle;

	@FindBy(id = "description")
	public WebElement description;

	@FindBy(xpath = ".//input[@value='Add']")
	public WebElement addBtn ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//*[@id='GeneralLinks']/a[.='Next']")
	public WebElement generalLinkNext ;

	public TheHubRelatedLinksPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
