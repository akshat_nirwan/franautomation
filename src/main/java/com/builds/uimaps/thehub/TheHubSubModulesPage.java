package com.builds.uimaps.thehub;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TheHubSubModulesPage {

	@FindBy(linkText = "Home")
	public WebElement homePage ;

	@FindBy(linkText = "Directory")
	public WebElement directoryPage ;

	@FindBy(linkText = "Alerts")
	public WebElement alertsPage ;

	@FindBy(linkText = "Messages")
	public WebElement messagesPage ;

	@FindBy(linkText = "Calendar")
	public WebElement calendarPage ;

	@FindBy(linkText = "News")
	public WebElement newsPage ;

	@FindBy(linkText = "Library")
	public WebElement libraryPage ;

	@FindBy(linkText = "Forums")
	public WebElement forumsPage ;

	@FindBy(xpath = ".//a[@qat_submodule='Franbuzz']")
	public WebElement franBuzzPage ;

	@FindBy(linkText = "Related Links")
	public WebElement relatedLinksPage ;

	@FindBy(linkText = "What's New")
	public WebElement whatsNewPage ;

	public TheHubSubModulesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
