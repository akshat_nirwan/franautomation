package com.builds.uimaps.thehub;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TheHubTasksPage {

	@FindBy(linkText = "Add Task")
	public WebElement addTaskLink ;

	@FindBy(id = "franchiseeId")
	public WebElement franchiseeId;

	@FindBy(xpath = ".//*[@id='ms-parentassignTo']")
	public WebElement assignToSelect ;

	@FindBy(id = "status")
	public WebElement statusSelect;

	@FindBy(id = "taskType")
	public WebElement taskTypSelect;

	@FindBy(id = "subject")
	public WebElement subject;

	@FindBy(xpath = ".//input[@name='timelessTaskId']")
	public WebElement timelessTaskIdCheck ;

	@FindBy(id = "priority")
	public WebElement prioritySelect;

	@FindBy(id = "startDate")
	public WebElement startDate;

	@FindBy(name = "calendarTaskCheckBox")
	public WebElement calendarTaskCheckBox;

	@FindBy(id = "comments")
	public WebElement comments;

	@FindBy(name = "schduleTime")
	public List<WebElement> schduleTime;

	@FindBy(xpath = ".//*[@id='taskEmailReminder_checkbox']")
	public WebElement sendReminderNotification ;

	@FindBy(xpath = ".//*[@id='REMINDER_DATE']")
	public WebElement reminderDate ;

	@FindBy(xpath = ".//input[@value='Create']")
	public WebElement createBtn ;

	@FindBy(linkText = "Show Filters")
	public WebElement showFilter ;

	@FindBy(id = "category")
	public WebElement categorySelect;

	@FindBy(xpath = ".//*[@id='ms-parentstatus']")
	public WebElement taskStatus ;

	@FindBy(id = "endDate")
	public WebElement endDate;

	@FindBy(id = "reminder")
	public WebElement reminderSelect;

	@FindBy(xpath = ".//*[@id='ms-parentpriority']")
	public WebElement prioritySelectFilter ;

	@FindBy(xpath = ".//*[@id='ms-parentmineall']")
	public WebElement viewFilterUser ;

	@FindBy(xpath = ".//*[@id='ms-parenttaskType']")
	public WebElement taskTypeFilter ;

	@FindBy(xpath = ".//input[@value='Save View']")
	public WebElement saveViewBtn ;

	@FindBy(xpath = ".//input[@value='Search']")
	public WebElement searchBtn;

	@FindBy(linkText = "Hide Filters")
	public WebElement hideFilters ;

	@FindBy(xpath = ".//input[@value='Save']")
	public WebElement saveBtn ;

	@FindBy(xpath = ".//input[@value='Close']")
	public WebElement closeBtn ;

	@FindBy(xpath = ".//input[@value='Process']")
	public WebElement processBtn ;

	@FindBy(linkText = "Modify")
	public WebElement modifyLink ;

	@FindBy(linkText = "Complete")
	public WebElement completeLink ;

	@FindBy(xpath = ".//input[@value='Actions']")
	public WebElement actionsBtn ;

	@FindBy(xpath = ".//input[@value='Change']")
	public WebElement changeBtn ;

	@FindBy(xpath = ".//input[@value='Change Status']")
	public WebElement statusChangedBtmBtn ;

	@FindBy(xpath = ".//input[@value='Delete']")
	public WebElement deleteBottomBtn ;

	@FindBy(id = "search")
	public WebElement searchCorUser;

	@FindBy(xpath = ".//*[@id='searchButton']")
	public WebElement searchBtnAtCorPage ;

	@FindBy(xpath = ".//input[@value='Reassign']")
	public WebElement reassignBtn ;

	@FindBy(id = "intranetTaskCountlist")
	public WebElement intranetTaskCountlist;

	@FindBy(xpath = ".//*[@name='Close']")
	public WebElement closeBtnAtCPage ;

	@FindBy(name = "sTime")
	public WebElement sTime;

	@FindBy(name = "sMinute")
	public WebElement sMinute;

	@FindBy(name = "APM")
	public WebElement sAPM;

	@FindBy(name = "eTime")
	public WebElement eTime;

	@FindBy(name = "eMinute")
	public WebElement eMinute;

	@FindBy(name = "MPM")
	public WebElement eAPM;

	public TheHubTasksPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
