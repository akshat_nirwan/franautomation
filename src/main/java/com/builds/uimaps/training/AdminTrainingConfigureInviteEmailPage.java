package com.builds.uimaps.training;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTrainingConfigureInviteEmailPage {

	@FindBy(id = "TemplateName")
	public WebElement fromMail;

	@FindBy(xpath = ".//*[@id='SenderNameRadioGroup']/label[1]")
	public WebElement returnPath ;

	@FindBy(id = "mailReturnpath")
	public WebElement customMail;

	@FindBy(id = "mailSubject")
	public WebElement mailSubject;

	@FindBy(id = "Submit")
	public WebElement modify;

	@FindBy(name = "Submit")
	public WebElement close;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement editorTextArea ;

	public AdminTrainingConfigureInviteEmailPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
