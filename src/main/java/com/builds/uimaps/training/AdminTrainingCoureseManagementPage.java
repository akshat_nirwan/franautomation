package com.builds.uimaps.training;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTrainingCoureseManagementPage extends AdminTrainingCourseManagementAddSectionrPage {

	@FindBy(xpath = "//label[@for='corporateDashboard']")
	public WebElement OverAllDashBoard;
	
	@FindBy(xpath = "//label[@for='franchiseDashboard']")
	public WebElement MyDashBoard = null;

	
	@FindBy(xpath = "//button[contains(text(),'Manage Categories')]")
	public WebElement manageCategory ;

	@FindBy(xpath = ".//*[@id='dialogV']//a[contains(text (),'Modify ')]")
	public WebElement modify ;

	@FindBy(xpath = ".//button[contains(text (),' Modify Course')]")
	public WebElement modifycourse ;

	@FindBy(xpath = ".//*[@id='dialogV']//span[contains(text (), 'General Category')]")
	public WebElement GeneralCategory ;

	@FindBy(xpath = "//button[contains(text(),'Add Category')]")
	public WebElement addCategory ;

	@FindBy(xpath = ".//*[@class='textbox-drop']//*[.='All']")
	public WebElement LastModifiedDateAll ;

	@FindBy(xpath = ".//*[@id='date-box1']")
	public WebElement LastModifiedDate ;

	@FindBy(xpath = "name=\"customizeCourseName\"")
	public WebElement CopyandCustomizeCourseName ;

	@FindBy(xpath = ".//*[@name='categoryName']")
	public WebElement categoryName;

	@FindBy(id = "description")
	public WebElement categoryDescription;

	@FindBy(xpath = ".//*[@id='siteMainDiv']//*[@type='submit'] ")
	public WebElement addCategoryButton ;

	@FindBy(xpath = ".//*[@id='siteMainDiv']//label[@data-target='#roleBassedAccess']")
	public WebElement allowRoleBasedAccess ;

	@FindBy(xpath = ".//*[contains(text(),'Save')]")
	public WebElement saveCategoryButtonInModify ;

	@FindBy(css = "input[value='Close']")
	public WebElement closeCategoryButton;

	@FindBy(name = "courseName")
	public WebElement courseName;

	@FindBy(id = "summary")
	public WebElement courseObjective;

	@FindBy(id = "fc-drop-parentuserCombo")
	public WebElement instructor;

	@FindBy(xpath = ".//*[@id='instruction']")
	public WebElement instructions;

	@FindBy(id = "courseCombo")
	public WebElement courseDependency;

	@FindBy(name = "haspass")
	public List<WebElement> roleBasedAccess;
	// .//*[@id='siteMainDiv']//button[contains(text(),'Add Course')]
	@FindBy(xpath = ".//*[@id='siteMainDiv']//form//button[contains(text(),'Add Course')]")
	public WebElement addCourseButton ;

	@FindBy(xpath = ".//*/li[contains(text(),'Add Course')]")
	public WebElement addCourse ;

	@FindBy(css = "input[value='Back']")
	public WebElement backFromCourse;

	// Move Course
	@FindBy(linkText = "Move Courses")
	public WebElement moveCourses ;

	@FindBy(id = "categoryID")
	public WebElement fromCategory;

	@FindBy(name = "Submit")
	public WebElement continueButton;

	@FindBy(id = "courseId")
	public WebElement courseId;

	@FindBy(id = "Submit")
	public WebElement move;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[5]/td[2]")
	public WebElement confirmation ;

	@FindBy(name = "SubmitButton")
	public WebElement ok;

	@FindBy(xpath = ".//input[(@value='Back'  or @value='Cancel') and @type='button']")
	public WebElement backfromSectionPage ;

	// Send Invite Email
	@FindBy(css = ".cm_new_button[value='Back']")
	public WebElement backButton;

	@FindBy(linkText = "Address book")
	public WebElement AddressBook ;

	@FindBy(xpath = ".//*[@id='searchCorpUsers']")
	public WebElement corporateSearchBox ;

	@FindBy(xpath = ".//td[@class='sBgSq']/a/img[@title='Search Corporate Users']")
	public WebElement searchIcon ;

	@FindBy(css = ".cm_new_button[value='Add']")
	public WebElement Add;

	@FindBy(name = "mailSubject")
	public WebElement mailSubject;

	@FindBy(css = ".cm_new_button[value='Send Invite']")
	public WebElement sendInvite;

	@FindBy(css = ".Fbtn[value='Close']")
	public WebElement close;

	@FindBy(name = "submit3362")
	public WebElement closeCopyUrlBtn;

	// Admin Verify Search

	@FindBy(xpath = ".//input[@value='Back']")
	public WebElement backFromSectionBtn ;

	@FindBy(xpath = ".//input[@value='Search on Course Title / Objective']")
	public WebElement searchTextBox ;

	@FindBy(xpath = "//img[@title='Search Training Items']")
	public WebElement searchBoxSearchBtn ;

	// At Home page Search
	@FindBy(id = "searchDocuments")
	public WebElement searchTextBoxHome;

	@FindBy(xpath = ".//input[@title='Search Document']")
	public WebElement searchboxBtnAtHome ;

	@FindBy(xpath = ".//*[@id='dialogV']//a[contains(text (),'Invite Users')]")
	public WebElement inviteUser ;

	@FindBy(xpath = ".//*[contains(text(),'Copy URL')]")
	public WebElement copyUrlLinkText ;

	@FindBy(xpath = ".//*[contains(text(),'Copy URL')]")
	public WebElement copyUrlLinkText1 ;

	// Add multiple question
	@FindBy(xpath = ".//input[@value='Actions']")
	public WebElement Action ;

	@FindBy(xpath = ".//*/li[contains(text(),'Delete')]")
	public WebElement delete ;

	@FindBy(xpath = ".//*[@id='dialogV']//a[contains(text(),'Delete')]")
	public WebElement delete1 ;

	@FindBy(id = "fieldID_coverImage")
	public WebElement uploadcoverimage;

	@FindBy(xpath = ".//*[@id='fc-drop-parentbrands']/button")
	public WebElement divisionCourse;

	@FindBy(xpath = ".//*[@id='fc-drop-parentuserCombo']/div/div/input")
	public WebElement instrutorsearch;

	@FindBy(xpath = ".//*[@id='siteMainDiv']/div/form/div[1]/div[2]/div[3]/div[2]/div/div/select")
	public WebElement selectboxinstructor;

	@FindBy(xpath = "//div[@class='full-width']//table[@class='tbl-style tbl-hover']//tbody//tr//td[@class='text-right']//button[@type='button']")
	public WebElement resume;

	
	@FindBy(xpath = "//input[@class='nSearch bText12']")
	public WebElement Search;

	@FindBy(xpath = "//div[@class='selectize-input items not-full']")
	public WebElement sendmail;
	
	@FindBy(id="addressLinkDiv")
	public WebElement addressLink;
	
	@FindBy(id="searchCorpUsers")
	public WebElement searchCorpUsers;
	
	@FindBy(id="corpCheckBox")
	public WebElement corpCheckBox;
	
	@FindBy(name="ok")
	public WebElement addUser;
	
	@FindBy(xpath=".//button[contains(@onclick ,'submitInviteForm')]")
	public WebElement sendInviteButton;
	
	@FindBy(xpath=".//*[@id='dialogV']//span[@class='ellipsis']")
	public WebElement searchCategoryDropArrow;

	public AdminTrainingCoureseManagementPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

}
