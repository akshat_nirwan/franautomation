package com.builds.uimaps.training;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTrainingCourseManagementAddQuizPage {

	@FindBy(name = "quizName")
	public WebElement quizTitle;

	@FindBy(id = "questionOrderIng")
	public WebElement questionOrdering;

	@FindBy(id = "timeAllotedHour")
	public WebElement hourSelect;

	@FindBy(id = "timeAllotedMin")
	public WebElement minSelect;

	@FindBy(name = "passingMarks")
	public WebElement passingScore;

	@FindBy(name = "isMandatory")
	public WebElement quizOption;

	@FindBy(name = "canViewAnswer1")
	public WebElement canViewAnswer;

	@FindBy(name = "quizSummary")
	public WebElement quizSummary;

	@FindBy(name = "Submit")
	public WebElement Next;

	@FindBy(id = "new")
	public WebElement addNewQuestion;

	@FindBy(id = "old")
	public WebElement addQuestionFromLibrary;

	@FindBy(name = "text")
	public WebElement questionText;

	@FindBy(id = "responseType")
	public WebElement responseType;

	@FindBy(name = "scorable")
	public List<WebElement> scorable;

	@FindBy(id = "questionMarks")
	public WebElement questionScore;

	@FindBy(name = "publish")
	public List<WebElement> publish;

	@FindBy(id = "questionMandantory")
	public WebElement responseIsMandatory;

	@FindBy(name = "insertInLibrary")
	public List<WebElement> addInLibraryAccess;

	@FindBy(name = "submit2")
	public WebElement submitAndNext;

	@FindBy(id = "minRating")
	public WebElement minRangeForRating;

	@FindBy(id = "maxRating")
	public WebElement maxRating;

	@FindBy(xpath = ".//*[@id='row_1']/td[2]/textarea")
	public WebElement option1 ;

	@FindBy(xpath = ".//*[@id='row_2']/td[2]/textarea")
	public WebElement option2 ;

	@FindBy(name = "submit1")
	public WebElement submit;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[1]/td[2]")
	public WebElement addQuestionConfirmationPage ;

	@FindBy(linkText = "Modify")
	public WebElement modifyQuiz ;

	@FindBy(linkText = "Delete")
	public WebElement deleteQuiz ;

	// Delete Question
	@FindBy(xpath = ".//a[@class='showAction']/img")
	public WebElement Action ;

	@FindBy(xpath = ".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody/tr[4]/td")
	public WebElement Delete ;

	public AdminTrainingCourseManagementAddQuizPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
