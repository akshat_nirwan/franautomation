package com.builds.uimaps.training;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTrainingCourseManagementAddSectionrPage {

	@FindBy(xpath = "//a[@href='#in-progress']")
	public  WebElement InProgress;

	@FindBy(id = "nameSection")
	public WebElement sectionTitle;
	
	@FindBy(xpath = ".//span[@class='print-active-name']/a")
	public WebElement viewperpage ;

	@FindBy(xpath = ".//a[contains(text(),'500')]")
	public WebElement recordsperpage ;

	@FindBy(xpath = ".//*[@id='dialogV']//a[contains(text (),'Invite Users')]")
	public WebElement inviteUser ;

	@FindBy(id = "quizName")
	public WebElement quizName;
	
	@FindBy(id = "question")
	public WebElement question;

	@FindBy(id = "quizSummary")
	public WebElement quizSummary;

	@FindBy(id = "createQuizButton")
	public WebElement createQuizButton;
	
	@FindBy(xpath = ".//*[@id='template_1']")
	public WebElement Template1;

	@FindBy(id = "text1")
	public WebElement text1;

	@FindBy(id = "saveQuiz")
	public WebElement saveQuiz;

	@FindBy(xpath = ".//*[contains(text(),'View Quiz')]")
	public WebElement viewquiz1 ;

	@FindBy(xpath = ".//*[contains(text(),'Copy URL')]")
	public WebElement copyurl ;

	@FindBy(xpath = ".//*[@id='dialogV']//button[contains(text(),'Back')]")
	public WebElement back;
	
	@FindBy(xpath = ".//*[@id='dialogV']//button[contains(text(),'Print')]")
	public WebElement print ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//a[contains(text(),'Modify ')]")
	public WebElement modify ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//a[contains(text(),'Delete')]")
	public WebElement delete ;

	@FindBy(xpath = ".//*[@id='dialogV']//a[contains(text(),'Publish')]")
	public WebElement Publish ;

	@FindBy(xpath = ".//*/button[contains(text(),'Confirm')]")
	public WebElement Confirm ;
	
	@FindBy(xpath=".//div[@id='publishPopup']//button[contains(text(),'Confirm')]")
	public WebElement publishPopupConfirm;

	@FindBy(xpath = ".//*[@id='siteMainDiv']//button[contains(text(),'Save')]")
	public WebElement ModifyLesson ;

	@FindBy(xpath = ".//*[@id='siteMainDiv1111']//button[contains(text(),'Add Quiz')]")
	public WebElement addquiz ;

	@FindBy(xpath = ".//*[contains(text(),'Add Quiz')]")
	public WebElement addquiz1 ;
	
	@FindBy(xpath = "//ul[@class='dropdown-list direction open-menu']//li//a[@href='javascript:void(0);'][contains(text(),'Add Quiz')]")
	public WebElement addquiz2 ;

	@FindBy(xpath = ".//button[contains(text(),'Add New Question')]")
	public WebElement AddNewQuestion ;
	
	@FindBy(xpath = ".//*[@for=\"insertInLibrary1\"]")
	public WebElement AddtoLibrary ;

	@FindBy(xpath = ".//*/button[contains(text(),'Add')]")
	public WebElement Add ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//button[contains(text(),'Send Invite')]")
	public WebElement sendInvite ;

	@FindBy(xpath = ".//*[@id='responseType1']/option[@value='date']")
	public WebElement ResponseTypeDate ;

	@FindBy(id = "regular")
	public WebElement contentOptionRegular;

	@FindBy(xpath = ".//*[@id='OpenPreviewDialog']")
	public WebElement previewOncreateYourOwn ;

	@FindBy(xpath = ".//*[@id='ContinueEditing']")
	public WebElement continueediting ;

	@FindBy(xpath = ".//*[@id='AddAttachments-dialogLink']")
	public WebElement attachment ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//div[@data-role=\"ico_Content\"]")
	public WebElement uploadFile ;
	
	@FindBy(xpath = ".//*[@id='fileClick']")
	public WebElement fileuploader ;

	// Advance type

	@FindBy(xpath = ".//*[@id='siteMainTable']//div[@data-role=\"ico_Text\"]")
	public WebElement useAdvanceEditor ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//div[contains(text(),'Create your own')]")
	public WebElement createYourOwn ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//div[@data-role=\"ico_Video\"]")
	public WebElement embedVideo ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//div[@data-role=\"ico_Scorm\"]")
	public WebElement scormContent ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//div[@data-role=\"ico_PageContent\"]")
	public WebElement articulateContent ;

	@FindBy(xpath = ".//*[@id='saveButton']")
	public WebElement SaveAdvanceEditorButton ;

	@FindBy(xpath = ".//*[@id='dialogV']//span[contains(text(),'General Category')]")
	public WebElement GeneralCategory ;

	@FindBy(xpath = ".//*[@id='dialogV']//input")
	public WebElement input ;

	@FindBy(id = "ta_ifr")
	public WebElement Iframe;

	@FindBy(xpath = ".//html/body")
	public WebElement editorContent ;
	@FindBy(xpath = ".//*[@id='tinymce']")
	public WebElement tinuMCEditor ;

	@FindBy(id = "publish-course")
	public WebElement publishcourse;

	// Embed Video

	@FindBy(id = "embed")
	public WebElement contentOptionEmbed;

	@FindBy(id = "subSectionContent")
	public WebElement embedCode;

	// Scorm Type

	@FindBy(id = "scorm")
	public WebElement contentOptionScorm;

	// Articulate Type

	@FindBy(id = "articulate")
	public WebElement contentOptionArticulate;

	@FindBy(name = "mainFile")
	public WebElement mainFile;

	// Basic

	@FindBy(name = "checkDays")
	public WebElement resetNotComplete;

	@FindBy(name = "sectionCombo")
	public WebElement sectionDependency;

	@FindBy(id = "summary")
	public WebElement summary;

	@FindBy(xpath = ".//*[@id='siteMainDiv']//button[contains( text(),'Create Lesson')]")
	public WebElement addLesson ;

	@FindBy(xpath = ".//*[@id='attachmentName']")
	public WebElement attachmentName ;

	@FindBy(xpath = ".//*[@id='groupAddressTable']//a/img[contains(@title,'Search Corporate Users')]")
	public WebElement searchCorpUser ;

	@FindBy(xpath = ".//td/input[@value='Back']")
	public WebElement backToCategoryPage ;

	@FindBy(xpath = ".//*/div[@data-role=\"ico_TextWithImage\"]")
	public WebElement sourceSelector ;

	@FindBy(xpath = ".//*[@id='formFlds']")
	public WebElement destinationSelector ;

	@FindBy(xpath = ".//*[@id='fieldID_coverImage']")
	public WebElement uploadcoverimage ;
	
	@FindBy(xpath = ".//*[@class=\"checkbox-lbl\"]")
	public WebElement checkAll ;
	
	@FindBy(xpath = ".//button[contains(text(),'Select')]")
	public WebElement select ;

	@FindBy(id = "questionName")
	public WebElement QuestionName;
	
	@FindBy(id = "search")
	public WebElement search;
	
	@FindBy(xpath = ".//*[contains(text(),'My Notes')]")
	public WebElement mynotes;
	
	@FindBy(xpath = "//input[@placeholder='Search on Course Title / Objective']")
	public WebElement searchcoursetitle;
	
	@FindBy(xpath = ".//*[contains(text(),'Add Notes')]")
	public WebElement addnotes;
	
	@FindBy(xpath = ".//button[contains(text(),'Add')]")
	public WebElement add;

	@FindBy(xpath=".//button[contains(@onclick ,'start')]")
	public WebElement startLearningBtn;
	
	@FindBy(xpath = ".//*[@id='siteMainDiv1111']//textarea")
	public WebElement txtarea;

	@FindBy(xpath = ".//*[@data-role='ico_Delete']")
	public WebElement deletenote;

	@FindBy(xpath = ".//*[@id='siteMainDiv1111']/div/div/div[3]/button")
	public WebElement close;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/div/div[3]/div/div[3]/div[3]/div/div/div/ul/li[1]/a")
	public WebElement viewquiz2nd;

	
	@FindBy(xpath = "//a[@href='#most-popular']")
	public WebElement mostPopular;

	@FindBy(xpath = "//a[@href='#Recently-added-plans']")
	public WebElement recentlyAddedPlan;
	
	@FindBy(xpath = "//a[@href='#plans-to-be-expired']")
	public WebElement plantobeExpired;

	@FindBy(xpath = ".//[contains(@data-role,\\\"ico_Preview\\\")]")
	public WebElement certificate;


	public AdminTrainingCourseManagementAddSectionrPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
