package com.builds.uimaps.training;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTrainingCourseManagementTakeQuizPage {

	@FindBy(id = "user_id")
	public WebElement userName;

	@FindBy(id = "password")
	public WebElement password;

	@FindBy(id = "ulogin")
	public WebElement loginButton;

	@FindBy(linkText = "Training")
	public WebElement Training ;

	@FindBy(xpath = ".//span[contains(text () , 'Start Quiz')]")
	public WebElement startquiz;

	@FindBy(xpath = ".//table[@class='result']/tbody/tr/td[contains(text(),'Result')]/following-sibling::td")
	public WebElement result;

	public AdminTrainingCourseManagementTakeQuizPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
