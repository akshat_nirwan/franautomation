package com.builds.uimaps.training;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTrainingLinksPage {

	@FindBy(xpath = ".//*[@id='dropdown']/span/a")
	public WebElement aDropdown ;

	@FindBy(xpath = ".//*[@id='dropdown']/div/div/span[1]/a")
	public WebElement admin1 ;

	@FindBy(xpath = ".//*[@id='section1']/ul/li[15]/a/span")
	public WebElement training ;

	@FindBy(xpath = ".//*[@id='Training']/ul/li")
	public WebElement tBox ;

	@FindBy(className = ".text_b")
	public WebElement confirm;

	public AdminTrainingLinksPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
