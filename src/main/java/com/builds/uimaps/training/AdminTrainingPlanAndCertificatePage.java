package com.builds.uimaps.training;


import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTrainingPlanAndCertificatePage {

	@FindBy(xpath = ".//*[@id='siteMainTable']//div[contains(text(),'Create')]")
	public WebElement Create ;

	@FindBy(xpath = ".//*[@id='publishPopup']//button[contains(text(),'Confirm')]")
	public WebElement Confirm ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//a[contains(text(),'Plan')]")
	public WebElement CreatePlan ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//a[contains(text(),'Certificate')]")
	public WebElement CreateCertificate ;

	@FindBy(xpath = ".//a[contains(text(),'Publish')]")
	public WebElement Publish ;

	@FindBy(xpath = ".//div[@data-role='ico_Filter']")
	public WebElement FilterBtn ;

	@FindBy(id = "search")
	public WebElement applyFilters;

	@FindBy(id = "planName")
	public WebElement planfieldtabinsearch;

	@FindBy(name = "groupName")
	public WebElement groupName;

	@FindBy(name = "groupDescription")
	public WebElement groupDescription;

	@FindBy(name = "description")
	public WebElement Description;

	@FindBy(xpath = ".//*[@id='pageid']/a/u[contains(text(),'Show All')]")

	public WebElement showAll ;

	@FindBy(xpath = ".//a[.='Question Library']")
	public WebElement questionLibraryButton ;

	@FindBy(xpath = ".//*[@id='FilterTabShowHide']//a[contains(text(),'Plans')]")
	public WebElement planTab ;

	@FindBy(xpath = ".//input[@name='planName' and @type='text']")
	public WebElement planName ;

	@FindBy(xpath = ".//input[@name='certificateName' and @type='text']")
	public WebElement certificateName ;
	
	
	@FindBy(xpath = ".//input[@name='courseName' and @type='text']")
	public WebElement courseName1 ;

	@FindBy(xpath = ".//*[@id='template_1']/div/div/div[2]")
	public WebElement certificateTemplate ;
	
	@FindBy(xpath = ".//*[@id='template_1']")
	public WebElement Template1;

	@FindBy(id = "groupId")
	public WebElement groupNameId;

	@FindBy(id = "courseName")
	public WebElement courseNamefilter;

	@FindBy(id = "search")
	public WebElement searchfilter;

	@FindBy(name = "question")
	public WebElement questionText;

	@FindBy(id = "responseType")
	public WebElement responseType;

	@FindBy(name = "scorable")
	public List<WebElement> scorable;

	@FindBy(id = "questionMandantory")
	public WebElement resoponseMandatory;

	@FindBy(id = "questionMarks")
	public WebElement questionMark;

	@FindBy(xpath = ".//button[contains(text(),'Add Courses')]")
	public WebElement addCourses ;

	@FindBy(xpath = ".//*[@id='addButton']")
	public WebElement addinCourse ;
	//

	@FindBy(xpath = ".//button[contains(text(),'Add Certificates')]")
	public WebElement addCertificate ;

	@FindBy(xpath = ".//*[@id='saveButton']")
	public WebElement saveCourse ;

	@FindBy(xpath = ".//*[@id='changeForm']//button[contains(text(),'Save')]")
	public WebElement savePlan ;
	
	@FindBy(xpath = ".//*[@id='changeForm']//button[contains(text(),'Cancel')]")
	public WebElement cancelPlan ;

	@FindBy(xpath = ".//button[@type=\"submit\"]")
	public WebElement saveCertificate ;

	// Add Question In Group
	@FindBy(xpath = "//a[.='Question Library']")
	public WebElement questionLibraryAfterQuestionAdd ;

	@FindBy(xpath = ".//*[@id='fieldID_Heading_2']")
	public WebElement certificateHeading ;

	@FindBy(xpath = ".//*[@id='fieldID_Sub$Heading_3']")
	public WebElement certificateSubHeading ;

	@FindBy(xpath = ".//*[@id='fieldID_Message_5']")
	public WebElement certificateMessage ;

	@FindBy(xpath = ".//*[@id='fieldID_Signature_8']")
	public WebElement UploadcertificateSignature ;

	@FindBy(xpath = ".//*[@id='fieldID_Logo_1']")
	public WebElement UploadcertificateLogo ;

	@FindBy(xpath = ".//*[@id='FilterTabShowHide']//a[contains(text(),'Certificates')]")
	public WebElement CertificateTab ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//tr/th[1]/label[@for='checkBox']")
	public WebElement deleteall ;

	@FindBy(xpath = ".//*[@for='check']")
	public WebElement selectallChkbx ;

	@FindBy(xpath = ".//a[contains(text(),'Available')]")
	public WebElement availablePlan ;

	@FindBy(xpath = ".//a[contains(text(),'Completed')]")
	public WebElement CompletedPlan ;

	@FindBy(xpath = ".//a[contains(text(),'In progress')]")
	public WebElement InProgressPlan ;
	
	@FindBy(xpath = ".//a[contains(text(),'Expired')]")
	public WebElement Expired ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//button[contains(text(),'SUBSCRIBE')]")
	public WebElement subscribe ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//button[contains(text(),'Start')]")
	public WebElement start ;

	@FindBy(xpath = ".//button[contains(text(),'Start Learning')]")
	public WebElement startLearning ;

	@FindBy(xpath = ".//*[@id='siteMainTable']/tbody/tr/td/div/div[3]/div/div[2]/div[3]/div/button")
	public WebElement startLearningN ;

	@FindBy(xpath = ".//button[contains(text () , 'Start Quiz')]")
	public WebElement startquiz ;

	@FindBy(xpath = ".//button[contains(text () , 'Close')]")
	public WebElement closequiz ;

	@FindBy(xpath = ".//*[contains(@placeholder,'mm/dd/yyyy')]")
	public WebElement insertDate ;

	@FindBy(xpath = ".//*[@id='btnFinish']")
	public WebElement finishQuiz ;

	@FindBy(xpath = ".//*[@id='SubmitQuizBtn']")
	public WebElement SubmitQuiz ;

	@FindBy(xpath = ".//button[contains(text (),'Close')]")
	public WebElement Close ;

	@FindBy(xpath = ".//*[@id='siteMainTable']//button[contains(text(),'Resume')]")
	public WebElement resume ;
	
	@FindBy(xpath=".//button[contains(@onclick , 'startT')]")
	public WebElement startTAgain;

	@FindBy(xpath = ".//*[@id='courseName']")
	public WebElement coursename ;

	@FindBy(xpath = ".//*[contains(text(),'Start Evaluation')]")
	public WebElement startevaluation ;

	@FindBy(xpath = ".//button[contains(@onclick,'goToReviewScoring')]")
	public WebElement reviewScoring ;

	@FindBy(xpath = ".//*[contains(@id ,'marks_')]")
	public WebElement slider ;

	@FindBy(xpath = ".//*[contains(text(),'Submit Assessment')]")
	public WebElement submitassessment ;

	@FindBy(xpath = ".//*[@id='quizName']")
	public WebElement quizName ;

	@FindBy(xpath = ".//*[@id='addEvent']")
	public WebElement addevent ;

	@FindBy(xpath = ".//*[@id='eventName']")
	public WebElement eventname ;

	@FindBy(xpath = ".//*[@id='eventDescription']")
	public WebElement eventdiscription ;

	@FindBy(xpath = ".//*[@id='eventLocation']")
	public WebElement eventlocation ;

	@FindBy(xpath = ".//*[@id='dateScheduled']")
	public WebElement eventdateScheduled ;

	@FindBy(xpath = ".//button[contains(text(),'Create')]")
	public WebElement createevent ;

	@FindBy(xpath = ".//button[contains(text(),'Modify')]")
	public WebElement modify ;

	@FindBy(xpath = ".//button[contains(text(),'Save')]")
	public WebElement save;

	@FindBy(xpath = ".//span[@class='print-active-name']/a")
	public WebElement viewperpage ;

	@FindBy(xpath = ".//*[@id='days']")
	public WebElement days;

	@FindBy(xpath = ".//*[@id='fc-drop-parentbrands']/div/div[1]/input[1]")
	public WebElement divisionName;

	@FindBy(xpath = ".//*[@id='StartDate']")
	public WebElement startdate;
	
	@FindBy(xpath = ".//*[@id='EndDate']")
	public WebElement enddate;

	@FindBy(xpath = ".//*[contains(text(),'Print')]")
	public WebElement print;

	@FindBy(xpath = "//div[@class='svg-icon cursor-pointer']")
	public WebElement threedots;

	@FindBy(xpath = "//a[@onclick=\"javascript:void(0);go('23','modifyPlan');\"]")
	public WebElement modifyplan;

	public AdminTrainingPlanAndCertificatePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
