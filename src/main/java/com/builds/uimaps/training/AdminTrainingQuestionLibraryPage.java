package com.builds.uimaps.training;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTrainingQuestionLibraryPage {

	@FindBy(xpath = ".//*[.='Manage Groups']")
	public WebElement manageGroup ;

	@FindBy(xpath = ".//*[.='Add Group']")
	public WebElement addGroup ;

	@FindBy(name = "groupName")
	public WebElement groupName;

	@FindBy(name = "groupDescription")
	public WebElement groupDescription;

	@FindBy(id = "submitButton")
	public WebElement save;

	@FindBy(xpath = ".//*[@id='pageid']/a/u[contains(text(),'Show All')]")

	public WebElement showAll ;

	@FindBy(xpath = ".//a[.='Question Library']")
	public WebElement questionLibraryButton ;

	@FindBy(xpath = ".//*/button[contains(text(),'Add Question')]")
	public WebElement addQuestion ;

	@FindBy(id = "groupId")
	public WebElement groupNameId;

	@FindBy(name = "question")
	public WebElement questionText;

	@FindBy(name = "minRating")
	public WebElement minRating;

	@FindBy(name = "maxRating")
	public WebElement maxRating;

	@FindBy(id = "responseType")
	public WebElement responseType;

	@FindBy(name = "questionMarks")
	public List<WebElement> questionMarks;

	@FindBy(xpath = ".//*[@for='questionMandantory']")
	public WebElement resoponseMandatory ;

	@FindBy(id = "questionMarks")
	public WebElement questionMark;

	@FindBy(xpath = ".//*[@id='siteMainDiv']//div[3]/button[2]")
	public WebElement saveQuestion ;

	@FindBy(xpath = ".//*[@id='siteMainDiv']//div[3]/button[3]")
	public WebElement saveandAddnextQuestion ;

	// Add Question In Group
	@FindBy(xpath = "//a[.='Question Library']")
	public WebElement questionLibraryAfterQuestionAdd ;

	@FindBy(xpath = ".//span[@class='print-active-name']/a")
	public WebElement viewperpage ;

	@FindBy(xpath = ".//a[contains(text(),'500')]")
	public WebElement recordsperpage ;
	
	/*@FindBy(xpath=".//div[@class='dropdown-action']//ul//a")
	public List<WebElement> listOfPages; */
	

	public AdminTrainingQuestionLibraryPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
