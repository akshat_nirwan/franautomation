package com.builds.uimaps.training;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TrainingCourseAddQuestionsPage {

	@FindBy(id = "addCourse")
	public WebElement addCourseBtn;

	@FindBy(id = "courseName")
	public WebElement courseName;

	@FindBy(name = "userCombo")
	public WebElement selectInstructor;

	@FindBy(id = "summary")
	public WebElement summary;

	@FindBy(xpath = ".//button[contains(text(),'Create Lesson')]")
	public WebElement createLessonLnk ;

	@FindBy(id = "nameSection")
	public WebElement nameLesson;

	@FindBy(xpath = ".//button[contains(text(),'Add Course')]")
	public WebElement addcourseConfirmBtn ;

	@FindBy(xpath = ".//button[contains(text(),'Create Lesson')]")
	public WebElement createLessonBtn ;

	@FindBy(xpath = ".//div[contains(text(),'Use Advance Editor')]")
	public WebElement lessonContent ;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement advanceEditorText ;

	@FindBy(id = "createQuizButton")
	public WebElement createQuizButton;

	@FindBy(id = "quizName")
	public WebElement quizName;

	@FindBy(id = "quizSummary")
	public WebElement quizSummary;

	@FindBy(xpath = ".//button[contains(text(),'Add Quiz')]")
	public WebElement addQuizBtn ;

	@FindBy(xpath = ".//button[contains(text(),'Add New Question')]")
	public WebElement addNewQuestion ;

	@FindBy(id = "responseType1")
	public WebElement selectResposeType;

	@FindBy(id = "text1")
	public WebElement textTypeQuestion;

	@FindBy(xpath = ".//*[@id='fc-drop-parentuserCombo']")
	public WebElement selctInstructorBtn ;

	@FindBy(xpath = ".//*[@id='tinymce']/p")
	public WebElement editorTextArea ;

	@FindBy(xpath = ".//button[contains(text(),'Save Quiz')]")
	public WebElement saveQuizBtn ;

	// upload File
	@FindBy(xpath = ".//div[contains(text(),'Upload file (including video)')]")
	public WebElement uploadFileType ;

	@FindBy(xpath = ".//input[@class='fileLoader']")
	public WebElement fileLoader ;

	public TrainingCourseAddQuestionsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
