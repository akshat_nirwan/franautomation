package com.builds.uimaps.training;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TrainingEventsPage {

	@FindBy(xpath = ".//input[@value='Add New Event']")
	public WebElement addNewEvent ;

	@FindBy(id = "eventName")
	public WebElement eventName;

	@FindBy(id = "eventDescription")
	public WebElement eventDescription;

	@FindBy(id = "eventLocation")
	public WebElement eventLocation;

	@FindBy(id = "personResponsible")
	public WebElement personResponsible;

	@FindBy(id = "dateScheduled")
	public WebElement dateScheduled;

	@FindBy(xpath = ".//input[@class='cm_new_button'][@value='Save']")
	public WebElement save ;

	@FindBy(xpath = ".//*[@id='pageid']/a/u[contains(text(),'Show All')]")
	public WebElement showAll ;

	public TrainingEventsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
