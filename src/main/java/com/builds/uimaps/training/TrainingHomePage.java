package com.builds.uimaps.training;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TrainingHomePage {

	// Add Role

	@FindBy(name = "roleType")
	public WebElement roleTypeSelect;

	@FindBy(name = "roleName")
	public WebElement roleName;

	@FindBy(name = "Submit")
	public WebElement submit;

	@FindBy(css = ".cm_new_button[value='Assign Later']")
	public WebElement assignLater;

	// User Detail..............................

	@FindBy(id = "userName")
	public WebElement loginId;

	@FindBy()
	public WebElement password;

	@FindBy(id = "confirmPassword")
	public WebElement confirmPassword;

	@FindBy(id = "userType")
	public WebElement userTypeSelect;

	@FindBy(id = "roleID")
	public WebElement roleSelect;

	@FindBy(xpath = "//div[@id='ms-parentroleID']/div[@class='ms-drop bottom']/div")
	public WebElement roleSelectSearch ;

	@FindBy(xpath = ".//*[@id='ms-parentroleID']/div[@class='ms-drop bottom']/ul/li/label[contains(text () , 'Select All')]/input")
	public WebElement selectAll ;

	@FindBy(id = "timezone")
	public WebElement timeZone;

	@FindBy(id = "isDaylight")
	public WebElement allowDST;

	// Personal Detail..........................

	@FindBy(id = "jobTitle")
	public WebElement jobTitle;

	@FindBy(id = "firstName")
	public WebElement firstName;

	@FindBy(id = "lastName")
	public WebElement lastName;

	@FindBy(id = "address")
	public WebElement address;

	@FindBy(id = "city")
	public WebElement city;

	@FindBy(id = "country")
	public WebElement countrySelect;

	@FindBy(id = "zipcode")
	public WebElement postalCode;

	@FindBy(id = "state")
	public WebElement stateSelect;

	@FindBy(id = "phone1")
	public WebElement phone1;

	@FindBy(id = "phoneExt1")
	public WebElement phone1EXt;

	@FindBy(id = "phone2")
	public WebElement phone2;

	@FindBy(id = "phoneExt2")
	public WebElement phone2EXt;

	@FindBy(id = "fax")
	public WebElement fax;

	@FindBy(id = "mobile")
	public WebElement mobile;

	@FindBy(id = "email")
	public WebElement email;

	@FindBy(id = "loginUserIp")
	public WebElement ipAddress;

	@FindBy(id = "isBillable")
	public WebElement isBillable;

	@FindBy(id = "sendNotification")
	public WebElement sendNotofication;

	@FindBy(id = "userPictureName")
	public WebElement uploadUserPicture;

	@FindBy(id = "Submit")
	public WebElement add;

	@FindBy(id = "Button1")
	public WebElement cancle;

	// verify recent Completed
	@FindBy(xpath = ".//tr/td[@class='btnBG']")
	public WebElement markAsCompleted ;

	@FindBy(xpath = ".//*[@id='dataTable']/tbody/tr[1]/td[2]/table/tbody/tr/td[2]/table/tbody/tr/td[4]/a/img")
	public WebElement closeSectionWindow ;

	@FindBy(xpath = "//a[@class='rowText12 nBtn26 fltRight']/span[contains(text(),'Notes')]")
	public WebElement notes ;

	@FindBy(xpath = "//td/a[@class='link-btn-blue']/span")
	public WebElement addNotes ;

	@FindBy(name = "notes")
	public WebElement textArea;

	@FindBy(xpath = "//input[@class='cm_new_button'][@value='Add']")
	public WebElement addButtonNotes ;

	@FindBy(xpath = ".//input[@class='cm_new_button'][@value='Close']")
	public WebElement closeNotes ;

	@FindBy(linkText = "Copy URL")
	public WebElement copyUrlLinkText ;

	@FindBy(xpath = ".//input[@placeholder='Search by Title / Description']")
	public WebElement searchTextBox ;

	@FindBy(name = "submit3362")
	public WebElement closeCopyUrlBtn;

	@FindBy(xpath = "//input[@title='Search Document']")
	public WebElement searchBoxSearchBtn ;

	public TrainingHomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

}
