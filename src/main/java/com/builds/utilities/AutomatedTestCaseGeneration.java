package com.builds.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class AutomatedTestCaseGeneration {

	@Parameters("moduleName")
	@Test
	public void ReadAllClass(@Optional String moduleName)
			throws EncryptedDocumentException, InvalidFormatException, InterruptedException, IOException {

		String filePath = FranconnectUtil.config.get("inputDirectory") + "\\TestCaseSteps.xlsx";

		File fileForTest = new File(filePath);

		if (fileForTest.exists() == true) {

			FileInputStream fis = new FileInputStream(fileForTest);
			Workbook wb = WorkbookFactory.create(fis);
			Sheet sh = wb.getSheetAt(0);
			int rowNum = sh.getLastRowNum();

			for (int i = 0; i <= rowNum; i++) {

				Row row = sh.getRow(i);

				if (row != null) {
					sh.removeRow(row);
				}
			}

			FileOutputStream fout = new FileOutputStream(fileForTest);
			wb.write(fout);
			fout.flush();
			fout.close();
			wb.close();
		}

		File file = new File(".");
		String newFile = file.getAbsolutePath();
		if (newFile.endsWith(".")) {
			newFile = newFile.substring(0, newFile.lastIndexOf("\\."));
		}

		System.out.println(newFile);
		file = new File(newFile);

		File[] directory = file.listFiles();

		for (File tempFile : directory) {
			if (tempFile.isDirectory()) {
				readFilesInDirectory(tempFile, moduleName, filePath);
			} else {
				if (tempFile.getAbsolutePath().endsWith(".java")) {
					System.out.println(tempFile.getAbsolutePath());
					readProgramLines(tempFile.getAbsolutePath(), moduleName, filePath);
				}
			}
		}
	}

	public void readFilesInDirectory(File fileName, String moduleName, String filePath)
			throws FileNotFoundException, EncryptedDocumentException, InvalidFormatException, InterruptedException {
		File[] dir = new File(fileName.getAbsolutePath()).listFiles();
		for (File tempFile : dir) {
			if (tempFile.isDirectory()) {
				readFilesInDirectory(tempFile, moduleName, filePath);
			} else {
				if (tempFile.getAbsolutePath().endsWith(".java")) {
					System.out.println(tempFile.getAbsolutePath());
					readProgramLines(tempFile.getAbsolutePath(), moduleName, filePath);
				}
			}
		}
	}

	public void readProgramLines(String filePath, String moduleName, String filePath1)
			throws FileNotFoundException, EncryptedDocumentException, InvalidFormatException, InterruptedException {

		String absPath = new File(filePath).getAbsolutePath();

		if (!filePath.contains("ACC.java") && !filePath.contains("TestJAVARead.java") && !filePath.contains("uimaps")
				&& absPath.contains("\\" + moduleName + "\\")) {

			int checked = 0;

			int count = 0;
			try {
				FileReader fr = new FileReader(filePath);
				LineNumberReader lnr = new LineNumberReader(fr);
				int startLineNo = 0;
				int endLineNo = 0;
				boolean print = false;
				String fileTc = "";
				boolean testBegins = false;
				boolean tcIDFound = false;

				try {
					for (String line; (line = lnr.readLine()) != null;) {
						String testCaseId = line;

						count++;
						if (checked > 0) {

							// System.out.println("Line : "+count+" "+line);
							/*
							 * if( (line.contains("public") ||
							 * line.contains("private")) && (
							 * line.contains("(")||(line.contains(")")) ) &&
							 * !line.contains(";") && line.contains("@Optional")
							 * )
							 */
							if (line.contains("@TestCase") || testBegins == true) {
								startLineNo = count;
								testBegins = true;

								if (line.contains("testCaseId") && !line.contains(";") && tcIDFound == false) {

									System.err.println("Line + " + line);

									testCaseId = testCaseId.substring(testCaseId.indexOf("testCaseId"));
									testCaseId = testCaseId.substring(testCaseId.indexOf("\"") + 1);
									testCaseId = testCaseId.substring(0, testCaseId.indexOf("\""));
									fileTc = testCaseId;
									tcIDFound = true;
									print = true;
								}
							}

							if (line.contains("logoutAndQuitBrowser") && startLineNo != 0) {
								endLineNo = count;
								// System.out.println("End at "+endLineNo +
								// "---------------------------------------------------------------------");
								print = false;
								testBegins = false;
								tcIDFound = false;
							}
							checked = 0;

							if (print == true && line.contains("printTestStep")) {
								String steps = line;
								try {
									steps = steps.substring(steps.indexOf(","));
									steps = steps.replace(",", "");
									steps = steps.replaceAll("\"", "");
									steps = steps.replace(");", "");
								} catch (Exception e) {
									steps = "PLEASE CONTACT AUTOMATION TEAM (WITH TEST CASE ID)IF THIS LINE IS FOUND";
								}
								System.out.println("Print Line : " + count + " " + steps);

								System.out.println(fileTc);
								printTestSteps(fileTc, steps, filePath1);
							}
						}
						checked += 1;
					}
					fr.close();
					lnr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	public void printTestSteps(String testCaseId, String testCaseSteps, String filePath)
			throws EncryptedDocumentException, InvalidFormatException, IOException {

		// String filePath =
		// config.get("inputDirectory")+"\\TestCaseSteps.xlsx";

		File file = new File(filePath);

		FileInputStream fis = new FileInputStream(file);
		Workbook wb = WorkbookFactory.create(fis);

		Sheet sh = wb.getSheetAt(0);

		Row row = sh.createRow(sh.getLastRowNum() + 1);
		Cell cell = row.createCell(0);
		cell.setCellValue(testCaseId);

		Cell cell2 = row.createCell(1);
		cell2.setCellValue(testCaseSteps);

		FileOutputStream fout = new FileOutputStream(file);
		wb.write(fout);
		fout.flush();
		fout.close();
		wb.close();

	}

	public String readTestCaseStepsFromExcel(String testCaseId) throws IOException {
		String filePath = FranconnectUtil.config.get("inputDirectory") + "\\TestCaseSteps.xlsx";
		File file = new File(filePath);
		FileInputStream fis = new FileInputStream(file);

		Workbook wb = new XSSFWorkbook(fis);
		Sheet sh = wb.getSheetAt(0);
		String testSteps = "";

		for (int x = 0; x < sh.getLastRowNum() + 1; x++) {
			Row row = null;

			try {
				row = sh.getRow(x);
			} catch (Exception e) {
				System.out.println(e.toString());
			}
			if (row != null) {
				String cellVal = "";
				Cell cell1 = null;
				Cell cell2 = null;
				try {
					cell1 = row.getCell(0);
					cell2 = row.getCell(1);

					cellVal = cell2.getStringCellValue();
					if (cell1.getStringCellValue().equalsIgnoreCase(testCaseId) && cellVal != null
							&& cellVal.length() > 0) {
						testSteps = testSteps.concat(cellVal.trim().concat("\n"));
					}
				} catch (Exception e) {
					System.out.println(e.toString());
				}
			}
		}
		wb.close();
		return testSteps;

	}
}
