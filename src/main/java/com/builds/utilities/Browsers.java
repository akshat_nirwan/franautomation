package com.builds.utilities;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Reporter;
import org.testng.SkipException;

import gridpackage.ActiveNodeDeterminer;


public class Browsers {
	private FranconnectUtil fc = new FranconnectUtil();
	public WebDriver openBrowser() throws Exception {

		fc.utobj().printTestStep("Open Browser");

		//boolean isBuildAccessible = cu.checkBuildStatus(FranconnectUtil.config, FranconnectUtil.config.get("buildUrl")); // First

		boolean isBuildAccessible = true;
		
		WebDriver driver = null;
		try {
			if (isBuildAccessible == true)// System.out.println("openDriver-----------------------------------"+this.config);
			{
				if (FranconnectUtil.config.get("browserName").equalsIgnoreCase("gecko")
						|| FranconnectUtil.config.get("browserName").equalsIgnoreCase("firefox")) {

					File file = new File(FranconnectUtil.config.get("inputDirectory") + "//exe//geckodriver.exe");
					System.out.println(file.getAbsolutePath());
					System.setProperty("webdriver.gecko.driver", file.getAbsolutePath());
					driver = new FirefoxDriver();

				} else if (FranconnectUtil.config.get("browserName").equalsIgnoreCase("chrome")) {
					if (System.getProperty("os.name").toLowerCase().contains("win")) {
						System.setProperty("webdriver.chrome.driver",
								FranconnectUtil.config.get("inputDirectory") + "\\exe\\chromedriver.exe");
					} else {
						System.setProperty("webdriver.chrome.driver",
								FranconnectUtil.config.get("inputDirectory") + "//exe//chromedriver.exe");
					}
					
					String downloadFilepath="";
					
					if (FranconnectUtil.config.get("jspHit").equalsIgnoreCase("yes")) {
						//downloadFilepath="//NODE49/download_File";
						downloadFilepath="//192.168.8.162/Download";
					}else{
						downloadFilepath=System.getProperty("java.io.tmpdir");
					}
					
					FranconnectUtil.config.put("downloadFilepath", downloadFilepath);
					ChromeOptions options = new ChromeOptions();
					
					if(FranconnectUtil.config.get("jspHit").equalsIgnoreCase("yes"))
					{
						if(FranconnectUtil.config.get("binary").equalsIgnoreCase("yes"))
						{
							options.setBinary("C:\\Users\\fran\\AppData\\Local\\Google\\Chrome SxS\\Application\\chrome.exe");
						}
					}else
					{
						if(FranconnectUtil.config.get("binary").equalsIgnoreCase("yes"))
						{
							options.setBinary(FranconnectUtil.config.get("binaryPath"));
						}
					}

					
					if(FranconnectUtil.config.get("userProfile")!=null && !FranconnectUtil.config.get("userProfile").isEmpty() &&!FranconnectUtil.config.get("jspHit").equalsIgnoreCase("yes"))
					{
						options.addArguments("user-data-dir="+FranconnectUtil.config.get("userProfile"));
					}
					
					options.addArguments("disable-popup-blocking");
					//options.addArguments("--disable-extensions");// change by
					options.addArguments("disable-infobars");// change by ravi
					options.addArguments("--start-maximized");
					options.addArguments("--dns-prefetch-disable");
					options.addArguments("--disable-application-cache"); // akshat
					options.addArguments("--disable-dev-shm-usage");
					//options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
					//options.addArguments("incognito");
					
					
					if(FranconnectUtil.config.get("extension").equalsIgnoreCase("yes"))
					{
						options.addExtensions(new File("C:\\Selenium_Test_Input\\exe\\FranConnect-Sky-for-Gmail_v1.9.0.crx")); // added by Akshat
					}
					
					

					HashMap<String, Object> prefs = new HashMap<String, Object>();
					prefs.put("credentials_enable_service", false);
					prefs.put("profile.default_content_settings.popups", 0);// change
					prefs.put("download.prompt_for_download", "false");
					prefs.put("download.directory_upgrade", "true");
					prefs.put("download.default_directory", downloadFilepath);
					options.setExperimentalOption("prefs", prefs);
					options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);// change
					
					options.setProxy(null);
					
					/*Proxy proxy = new Proxy();
					try {
						proxy.setHttpProxy(
								FranconnectUtil.config.get("PROXYIP") + ":" + FranconnectUtil.config.get("PROXYPORT"));
						options.setCapability("proxy", proxy);
					} catch (Exception e) {
						Thread.sleep(15000);
						Reporter.log(e.getMessage());
					}*/
					
					
					try {
						try {
							FranconnectUtil.config.get("jspHit");
						} catch (Exception e) {
							Reporter.log(e.getMessage());
						}

						if (FranconnectUtil.config.get("jspHit").equalsIgnoreCase("yes")) {
							URL url = new URL("http://" + FranconnectUtil.config.get("GRIDSERVERIP") + ":"
									+ FranconnectUtil.config.get("GRIDSERVERPORT") + "/wd/hub");

							RemoteWebDriver rmDriver = new RemoteWebDriver(url, options);
							rmDriver.setFileDetector(new LocalFileDetector());
							driver = rmDriver;
							
							//Get Node Ip : 09/09/2018
							try {
								ActiveNodeDeterminer active=new ActiveNodeDeterminer(FranconnectUtil.config.get("GRIDSERVERIP"), 
										Integer.parseInt(FranconnectUtil.config.get("GRIDSERVERPORT")));
								System.out.println(active.getNodeInfoForSession(((RemoteWebDriver) driver).getSessionId()));
							} catch (Exception e) {
								// TODO: handle exception
							}
							
							
						} else {
							driver = new ChromeDriver(options);
						}
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						Thread.sleep(15000);
					}

				} else if (FranconnectUtil.config.get("browserName").equalsIgnoreCase("internetexplorer")) {
					//TODO : Code to run on IE
				} else if (FranconnectUtil.config.get("browserName").equalsIgnoreCase("edge")) {
					//TODO : Code to run on Edge
				} else {
					throw new SkipException("Browser not available");
				}
				
				try {
					driver.manage().window().maximize();
				} catch (Exception e) {
					Reporter.log("Trouble maximizing the Browser / Or it is already Maximized.");
				}
				
				try {
					// Akshat - Browser Resolution
					if(FranconnectUtil.config.get("resolution").equalsIgnoreCase("yes"))
					{
						// options.setCapability("screen-resolution", FranconnectUtil.config.get("resolution"));
						driver.manage().window().setSize(new Dimension(Integer.parseInt(FranconnectUtil.config.get("resolutionWidth")),Integer.parseInt(FranconnectUtil.config.get("resolutionHeight"))));
					}
				}
				catch(Exception e)
				{
					Reporter.log("Trouble changing the resolution of the browser");
				}
			}
		} catch (Exception e) {
			driver.quit();
			throw new Exception("Not able to initialize the WebDriver - " + e.getMessage());
		}
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(180, TimeUnit.SECONDS);
		return driver;
	}
}
