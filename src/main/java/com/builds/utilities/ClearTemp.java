package com.builds.utilities;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.testng.annotations.Test;

public class ClearTemp {
	@Test
	public void clear() throws Exception {
		ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", "rd /s /q %temp%");
		builder.redirectErrorStream(true);
		Process p = builder.start();
		BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;
		while (true) {
			line = r.readLine();
			if (line == null) {
				break;
			}
			System.out.println(line);
		}
	}

}
