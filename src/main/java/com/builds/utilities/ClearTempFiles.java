package com.builds.utilities;

import java.io.File;
import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.Test;

public class ClearTempFiles {

	@Test
	public void deleteTempFiles() throws IOException {
		try {
			File temp = File.createTempFile("automationtempfile", ".tmp");
			String absFileName = temp.getAbsolutePath();
			String folderPath = absFileName.substring(0, absFileName.indexOf("automationtempfile"));

			if (folderPath.endsWith("\\")) {
				folderPath = folderPath.substring(0, folderPath.length() - 1);
			}

			System.out.println("Temp file : " + folderPath);
			File file = new File(folderPath);

			deleteDir(file);
		} catch (Exception e) {
			Reporter.log(e.getMessage());
		}
	}

	void deleteDir(File file) {
		File[] contents = file.listFiles();
		if (contents != null) {
			for (File f : contents) {
				System.out.println(f);
				deleteDir(f);
				System.out.println(f.canWrite());
			}
		}

		long fileupdateTime = file.lastModified();
		long currentTime = System.currentTimeMillis();
		long gap = ((currentTime - fileupdateTime) / 1000) / 60;
		if (gap > 180) {
			file.delete();
		}

	}
}
