package com.builds.utilities;

/*
 * import java.lang.reflect.Method; import java.util.HashMap; import
 * java.util.Map; import java.util.Map.Entry; import java.util.Set; import
 * java.util.TreeMap;
 * 
 * import org.reflections.Reflections; import
 * org.reflections.scanners.MethodAnnotationsScanner; import
 * org.reflections.util.ClasspathHelper; import
 * org.reflections.util.ConfigurationBuilder; import
 * org.testng.annotations.Optional; import org.testng.annotations.Parameters;
 * import org.testng.annotations.Test;
 * 
 * public class CreatedOnUpdatedOnCount {
 * 
 * @Parameters({"moduleName" , "toMonth" , "fromMonth"})
 * 
 * @Test public void countCreatedOn(@Optional String moduleName , @Optional
 * String toMonth , String fromMonth) throws Exception{
 * 
 * Map<String, String> config=new HashMap<String , String>();
 * config.put("SMTP_HOST_NAME", "192.168.9.1"); config.put("SMTP_PORT", "25");
 * 
 * Reflections reflections = new Reflections(new ConfigurationBuilder()
 * .setUrls(ClasspathHelper.forPackage("com.builds.test")) .setScanners(new
 * MethodAnnotationsScanner()));
 * 
 * Set<Method> methods =
 * reflections.getMethodsAnnotatedWith(org.testng.annotations.Test.class);
 * String testCaseId=null; String gr=null; String createdOn=null; int
 * testCaseCount=0;
 * 
 * HashMap<String, String> testCaseDuplicate=new HashMap<String,String>();
 * 
 * for (Method meth : methods) {
 * if(meth.isAnnotationPresent(com.builds.utilities.TestCase.class)){ String[]
 * modulegroup = meth.getAnnotation(org.testng.annotations.Test.class).groups();
 * 
 * for (int i = 0; i < modulegroup.length; i++) { gr=modulegroup[i].trim();
 * if(gr.equalsIgnoreCase(moduleName)){ break; }
 * 
 * }
 * 
 * if (gr!=null && gr.length()>0 && gr.equalsIgnoreCase(moduleName)) {
 * testCaseId=meth.getAnnotation(com.builds.utilities.TestCase.class).testCaseId
 * ().trim();
 * createdOn=meth.getAnnotation(com.builds.utilities.TestCase.class).createdOn()
 * .trim(); testCaseDuplicate.put(testCaseId, createdOn); } } }
 * 
 * Map<String, String> treeMap = new TreeMap<String, String>(testCaseDuplicate);
 * 
 * for(Entry<String, String> entry : treeMap.entrySet()){
 * 
 * if (entry.getValue()!=null && (entry.getValue().contains(toMonth) ||
 * entry.getValue().contains(fromMonth))) { testCaseCount++; } }
 * 
 * System.out.println("Total Number Of Test Cases Created For The "+moduleName+
 * "  To "+toMonth+" from "+fromMonth+": "+testCaseCount +"\n"); }
 * 
 * @Parameters({"moduleName" , "toMonth" , "fromMonth"})
 * 
 * @Test public void countUpdatedOn(@Optional String moduleName , @Optional
 * String toMonth, String fromMonth) throws Exception{ Map<String, String>
 * config=new HashMap<String , String>(); config.put("SMTP_HOST_NAME",
 * "192.168.9.1"); config.put("SMTP_PORT", "25");
 * 
 * Reflections reflections = new Reflections(new ConfigurationBuilder()
 * .setUrls(ClasspathHelper.forPackage("com.builds.test")) .setScanners(new
 * MethodAnnotationsScanner()));
 * 
 * Set<Method> methods =
 * reflections.getMethodsAnnotatedWith(org.testng.annotations.Test.class);
 * String testCaseId=null; String gr=null; String updatedOn=null; int
 * testCaseCount=0;
 * 
 * HashMap<String, String> testCaseDuplicate=new HashMap<String,String>();
 * 
 * for (Method meth : methods) {
 * if(meth.isAnnotationPresent(com.builds.utilities.TestCase.class)){ String[]
 * modulegroup = meth.getAnnotation(org.testng.annotations.Test.class).groups();
 * 
 * for (int i = 0; i < modulegroup.length; i++) { gr=modulegroup[i].trim();
 * if(gr.equalsIgnoreCase(moduleName)){ break; }
 * 
 * }
 * 
 * if (gr!=null && gr.length()>0 && gr.equalsIgnoreCase(moduleName)) {
 * testCaseId=meth.getAnnotation(com.builds.utilities.TestCase.class).testCaseId
 * ().trim();
 * updatedOn=meth.getAnnotation(com.builds.utilities.TestCase.class).updatedOn()
 * .trim(); testCaseDuplicate.put(testCaseId, updatedOn); } } }
 * 
 * Map<String, String> treeMap = new TreeMap<String, String>(testCaseDuplicate);
 * 
 * for(Entry<String, String> entry : treeMap.entrySet()){
 * 
 * if (entry.getValue()!=null && (entry.getValue().contains(toMonth) ||
 * entry.getValue().contains(fromMonth))) { testCaseCount++; } }
 * 
 * System.out.println("Total Number Of Test Cases Updated For The "+moduleName+
 * "  To "+toMonth+" From "+fromMonth+" : "+testCaseCount +"\n"); }
 * 
 * }
 * 
 */
