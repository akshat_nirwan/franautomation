package com.builds.utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.testng.annotations.Test;

public class DateManipulator {
	public static String getSysDate() {
		SimpleDateFormat date = new SimpleDateFormat("yyyy.MM.dd.h.mm.s");
		Date date1 = new Date();
		String sysdate = date.format(date1);
		System.out.println(sysdate);
		return sysdate;
	}

	@Test
	public static String futureDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date()); // Now use today date.
		c.add(Calendar.DATE, 7); // Adding 5 days
		String output = sdf.format(c.getTime());
		System.out.println(output);
		return output;
	}

}
