package com.builds.utilities;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.testng.annotations.Test;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.FileList;

public class DeleteGoogleFiles {
	/** Application name. */
	private final String APPLICATION_NAME = "Drive API Java Quickstart";

	public Drive getDriveService() throws Exception {
		Credential credential = authorize();
		return new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
	}

	@Test
	public void deleteFiles() throws Exception {
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
		Date now = DateUtils.addDays(new Date(), -30);
		String strDate = sdfDate.format(now).trim();
		System.out.println("Month : " + strDate);

		Drive service = getDriveService();
		List<com.google.api.services.drive.model.File> mainFolder = new ArrayList<com.google.api.services.drive.model.File>();
		List<com.google.api.services.drive.model.File> dateFolder = new ArrayList<com.google.api.services.drive.model.File>();

		FileList files = service.files().list()
				.setQ(" 'root' in parents and name='Selenium_Test_Output' and mimeType = 'application/vnd.google-apps.folder'")
				.execute();
		mainFolder.addAll(files.getFiles());

		try {

			for (int i = 0; i < mainFolder.size(); i++) {
				String fileName = mainFolder.get(i).get("name").toString();
				System.out.println("Main Folder Name : " + fileName);
				System.out.println("Main Folder Id : " + mainFolder.get(i).getId());

				if (fileName.equalsIgnoreCase("Selenium_Test_Output")) {
					FileList files1 = service.files().list()
							.setQ(" '" + mainFolder.get(i).getId()
									+ "' in parents and mimeType = 'application/vnd.google-apps.folder' and modifiedTime < '"
									+ strDate + "T12:00:00' and trashed=false")
							.execute();
					dateFolder.addAll(files1.getFiles());
					System.out.println("Size Of Date Folder: " + dateFolder.size());

					for (int l = 0; l < dateFolder.size(); l++) {
						System.out.println("Deleted : " + dateFolder.get(l).getName());
						service.files().delete(dateFolder.get(l).getId()).execute();
					}
				}
			}

		} catch (Exception e) {
			System.out.println("Error : " + e.getMessage().toString());
			// TODO: handle exception
		}
	}

	// private static final java.io.File DATA_STORE_DIR = new
	// java.io.File(System.getProperty("user.home"),
	// ".credentials/drive-java-quickstart");

	private final static java.io.File DATA_STORE_DIR = new java.io.File(
			/* FranconnectUtil.config.get("inputDirectory") */"C:\\Selenium_Test_Input" + "\\exe");

	private static HttpTransport HTTP_TRANSPORT;
	// drive scope all
	private final java.util.Collection<String> SCOPES = DriveScopes.all();
	/** Global instance of the {@link FileDataStoreFactory}. */
	private static FileDataStoreFactory DATA_STORE_FACTORY;

	static {
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
		} catch (Throwable t) {
			t.printStackTrace();
			System.exit(1);
		}
	}

	public Credential authorize() throws Exception {
		// Load client secrets.
		InputStream in;
		if (System.getProperty("os.name").toLowerCase().contains("win")) {
			in = new FileInputStream(
					/* FranconnectUtil.config.get("inputDirectory") */"C:\\Selenium_Test_Input"
							+ "\\exe\\client_secret.json");
		} else {
			in = new FileInputStream(
					/* FranconnectUtil.config.get("inputDirectory") */"C:\\Selenium_Test_Input"
							+ "//exe//client_secret.json");
		}
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES).setDataStoreFactory(DATA_STORE_FACTORY).setAccessType("offline").build();

		Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");

		// System.out.println("Credentials saved to " +
		// DATA_STORE_DIR.getAbsolutePath());

		return credential;
	}

	/** Global instance of the JSON factory. */
	private final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	/** Global instance of the HTTP transport. */
}
