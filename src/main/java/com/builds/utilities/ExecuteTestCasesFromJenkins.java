package com.builds.utilities;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ExecuteTestCasesFromJenkins {

	@Test
	@Parameters({ "buildUrl", "userName", "password", "emailIds", "moduleList" })
	public void runAlt(@Optional String buildUrl, @Optional String userName, @Optional String password,
			@Optional String emailIds, @Optional String moduleList) throws Exception {

		/*
		 * 1. This method is called through eclipse directly. 2. config is used
		 * to carry all the configuration related info 3. testDataFolderPath is
		 * where we store our test data (documents and test cases excel sheet).
		 * 4. baseDirFolder is where all the logs of test will be available
		 * after execution.
		 */
		FranconnectUtil.config.put("buildUrl", buildUrl);
		FranconnectUtil.config.put("userName", userName);
		FranconnectUtil.config.put("password", password);
		FranconnectUtil.config.put("emailIds", emailIds);
		FranconnectUtil.config.put("moduleList", moduleList);
		FranconnectUtil.config.put("browserName", "chrome");
		FranconnectUtil.config.put("SMTP_HOST_NAME", "192.168.9.1");
		FranconnectUtil.config.put("SMTP_PORT", "25");
		FranconnectUtil.config.put("SMTP_FROM_ADDRESS", "automation@franconnect.net");
		FranconnectUtil.config.put("SMTP_REPLY_TO", "automationteam@franconnect.net");
		FranconnectUtil.config.put("SMTP_CC_ADDRESS", "");
		FranconnectUtil.config.put("SMTP_BCC_ADDRESS", "");
		FranconnectUtil.config.put("MAIL_SUBJECT", "Automation Report");
		FranconnectUtil.config.put("REPORT_SUBJECT", "Report");
		FranconnectUtil.config.put("SUITE_NAME", "FC Sky");
		FranconnectUtil.config.put("TEST_NAME", "Polaris");
		FranconnectUtil.config.put("THREAD_COUNT", "10");
		FranconnectUtil.config.put("IPADDRESS", "192.168.8.199");
		FranconnectUtil.config.put("ALTERNATE_PATH_SCREENSHOT", "");
		FranconnectUtil.config.put("GRIDSERVERIP", "192.168.8.199");
		FranconnectUtil.config.put("GRIDSERVERPORT", "4444");
		FranconnectUtil.config.put("PROXYIP", "192.168.9.11");
		FranconnectUtil.config.put("PROXYPORT", "3128");
		FranconnectUtil.config.put("inputDirectory", "C://Selenium_Test_Input");
		FranconnectUtil.config.put("outputDirectory", "D://Selenium_Test_Output");
		FranconnectUtil.config.put("i18Files", "");
		FranconnectUtil.config.put("jspHit", "Yes");
		FranconnectUtil.config.put("inserttoDB", "No");
		FranconnectUtil.config.put("binary", "Yes");

		/// Settings up the base directory
		FranconnectUtil fc = new FranconnectUtil();

		fc.codeUtil().runTestScriptFromXML();
	}

}
