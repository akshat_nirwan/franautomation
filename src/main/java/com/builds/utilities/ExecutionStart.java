package com.builds.utilities;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ExecutionStart {

	@Test
	@Parameters({ "buildUrl", "userName", "password", "emailIds", "moduleList", "browserName", "SMTP_HOST_NAME",
			"SMTP_PORT", "SMTP_FROM_ADDRESS", "SMTP_REPLY_TO", "SMTP_CC_ADDRESS", "SMTP_BCC_ADDRESS", "MAIL_SUBJECT",
			"REPORT_SUBJECT", "SUITE_NAME", "TEST_NAME", "THREAD_COUNT", "IPADDRESS", "ALTERNATE_PATH_SCREENSHOT",
			"GRIDSERVERIP", "GRIDSERVERPORT", "PROXYIP", "PROXYPORT", "inputDirectory", "outputDirectory", "i18Files",
			"jspHit", "inserttoDB" ,"binary","binaryPath" , "extension","userProfile","hubServerName" ,"jwPath" , "resolution","resolutionWidth","resolutionHeight"})
	public void runAlt(@Optional String buildUrl, @Optional String userName, @Optional String password,
			@Optional String emailIds, @Optional String moduleList, @Optional String browserName,
			@Optional String SMTP_HOST_NAME, @Optional String SMTP_PORT, @Optional String SMTP_FROM_ADDRESS,
			@Optional String SMTP_REPLY_TO, @Optional String SMTP_CC_ADDRESS, @Optional String SMTP_BCC_ADDRESS,
			@Optional String MAIL_SUBJECT, @Optional String REPORT_SUBJECT, @Optional String SUITE_NAME,
			@Optional String TEST_NAME, @Optional String THREAD_COUNT, @Optional String IPADDRESS,
			@Optional String ALTERNATE_PATH_SCREENSHOT, @Optional String GRIDSERVERIP, @Optional String GRIDSERVERPORT,
			@Optional String PROXYIP, @Optional String PROXYPORT, @Optional String inputDirectory,
			@Optional String outputDirectory, @Optional String i18Files, @Optional String jspHit, String inserttoDB,
			@Optional String binary,@Optional String binaryPath, @Optional String extension, @Optional String userProfile,
			@Optional String hubServerName , @Optional String jwPath , @Optional String resolution , @Optional String resolutionWidth, @Optional String resolutionHeight)
			throws Exception {

		/*
		 * 1. This method is called through eclipse directly. 2. config is used
		 * to carry all the configuration related info 3. testDataFolderPath is
		 * where we store our test data (documents and test cases excel sheet).
		 * 4. baseDirFolder is where all the logs of test will be available
		 * after execution.
		 */

		FranconnectUtil.config.put("buildUrl", buildUrl);
		FranconnectUtil.config.put("userName", userName);
		FranconnectUtil.config.put("password", password);
		FranconnectUtil.config.put("emailIds", emailIds);
		FranconnectUtil.config.put("moduleList", moduleList);
		FranconnectUtil.config.put("browserName", browserName);
		FranconnectUtil.config.put("SMTP_HOST_NAME", SMTP_HOST_NAME);
		FranconnectUtil.config.put("SMTP_PORT", SMTP_PORT);
		FranconnectUtil.config.put("SMTP_FROM_ADDRESS", SMTP_FROM_ADDRESS);
		FranconnectUtil.config.put("SMTP_REPLY_TO", SMTP_REPLY_TO);
		FranconnectUtil.config.put("SMTP_CC_ADDRESS", SMTP_CC_ADDRESS);
		FranconnectUtil.config.put("SMTP_BCC_ADDRESS", SMTP_BCC_ADDRESS);
		FranconnectUtil.config.put("MAIL_SUBJECT", MAIL_SUBJECT);
		FranconnectUtil.config.put("REPORT_SUBJECT", REPORT_SUBJECT);
		FranconnectUtil.config.put("SUITE_NAME", SUITE_NAME);
		FranconnectUtil.config.put("TEST_NAME", TEST_NAME);
		FranconnectUtil.config.put("THREAD_COUNT", THREAD_COUNT);
		FranconnectUtil.config.put("IPADDRESS", IPADDRESS);
		FranconnectUtil.config.put("ALTERNATE_PATH_SCREENSHOT", ALTERNATE_PATH_SCREENSHOT);
		FranconnectUtil.config.put("GRIDSERVERIP", GRIDSERVERIP);
		FranconnectUtil.config.put("GRIDSERVERPORT", GRIDSERVERPORT);
		FranconnectUtil.config.put("PROXYIP", PROXYIP);
		FranconnectUtil.config.put("PROXYPORT", PROXYPORT);
		FranconnectUtil.config.put("inputDirectory", inputDirectory);
		FranconnectUtil.config.put("outputDirectory", outputDirectory);
		FranconnectUtil.config.put("i18Files", i18Files);
		FranconnectUtil.config.put("jspHit", jspHit);
		FranconnectUtil.config.put("inserttoDB", inserttoDB);
		FranconnectUtil.config.put("binary", binary);
		FranconnectUtil.config.put("binaryPath", binaryPath);
		FranconnectUtil.config.put("extension", extension);
		FranconnectUtil.config.put("userProfile", userProfile);
		FranconnectUtil.config.put("hubServerName", hubServerName);
		FranconnectUtil.config.put("jwPath", jwPath);
		FranconnectUtil.config.put("resolution", resolution);
		FranconnectUtil.config.put("resolutionWidth", resolutionWidth);
		FranconnectUtil.config.put("resolutionHeight", resolutionHeight);

		
		/// Settings up the base directory
		FranconnectUtil fc = new FranconnectUtil();
		fc.codeUtil().runTestScriptFromXML();
	}
}
