package com.builds.utilities;

import java.util.HashMap;
import java.util.Map;

import com.builds.test.admin.AdminPageTest;
import com.builds.test.common.CRMModule;
import com.builds.test.common.CommonMethods;
import com.builds.test.common.FCHomePageTest;
import com.builds.test.common.FieldOpsModule;
import com.builds.test.common.FinanceModule;
import com.builds.test.common.LoginPageTest;
import com.builds.test.common.OpenerModule;
import com.builds.test.common.SalesModule;
import com.builds.test.common.SmartConnectModule;
import com.builds.test.common.SupportModule;
import com.builds.test.common.TheHubModule;
import com.builds.test.common.TrainingModule;
import com.builds.test.infomgr.InfoMgrModule;

public class FranconnectUtil {

	public static Map<String, String> config = new HashMap<String, String>();
	public static String path;
	public static String executingModule;
	public static long timeofBuildFailure = 0;
	public static final String RyverNextLineChar = "\\n";
	public static final String RyverNextLineCharWithBullet = RyverNextLineChar+"* ";
	
	
	private InfoMgrModule infomgrobj = null;

	public InfoMgrModule infomgr() {
		if (infomgrobj == null) {
			infomgrobj = new InfoMgrModule();
		}
		return infomgrobj;
	}

	private SalesModule fsmoduleObj = null;

	public SalesModule sales() {
		if (fsmoduleObj == null) {
			fsmoduleObj = new SalesModule();
		}
		return fsmoduleObj;
	}

	private OpenerModule openerObj = null;

	public OpenerModule opener() {
		if (openerObj == null) {
			openerObj = new OpenerModule();
		}
		return openerObj;
	}

	private TheHubModule hubobj = null;

	public TheHubModule hub() {
		if (hubobj == null) {
			hubobj = new TheHubModule();
		}
		return hubobj;
	}

	private TrainingModule trainingobj = null;

	/**
	 * This is my summary. Hope it is helpful.
	*/
	public TrainingModule training() {
		if (trainingobj == null) {
			trainingobj = new TrainingModule();
		}
		return trainingobj;
	}

	private FieldOpsModule FieldOpsobj = null;

	public FieldOpsModule fieldops() {
		if (FieldOpsobj == null) {
			FieldOpsobj = new FieldOpsModule();
		}
		return FieldOpsobj;
	}

	private FinanceModule financeobj = null;

	public FinanceModule finance() {
		if (financeobj == null) {
			financeobj = new FinanceModule();
		}
		return financeobj;
	}

	private SupportModule supportobj = null;

	public SupportModule support() {
		if (supportobj == null) {
			supportobj = new SupportModule();
		}
		return supportobj;
	}

	private SmartConnectModule smartconnectobj = null;

	public SmartConnectModule smartconnect() {
		if (smartconnectobj == null) {
			smartconnectobj = new SmartConnectModule();
		}
		return smartconnectobj;
	}

	private CRMModule crmobj = null;

	public CRMModule crm() {
		if (crmobj == null) {
			crmobj = new CRMModule();
		}
		return crmobj;
	}

	private BaseUtil BaseUtilObj = null;
	public BaseUtil utobj(){
		if (BaseUtilObj == null) {
			BaseUtilObj = new BaseUtil();
		}
		return BaseUtilObj;
	}

	private CodeUtility codeUtil = null;

	public CodeUtility codeUtil() {
		if (codeUtil == null) {
			codeUtil = new CodeUtility();
		}
		return codeUtil;
	}

	private FCHomePageTest UserHomePageTestObj = null;

	public FCHomePageTest home_page() {
		if (UserHomePageTestObj == null) {
			UserHomePageTestObj = new FCHomePageTest();
		}
		return UserHomePageTestObj;
	}

	private LoginPageTest LoginPageTestObj = null;

	public LoginPageTest loginpage() {
		if (LoginPageTestObj == null) {
			LoginPageTestObj = new LoginPageTest();
		}
		return LoginPageTestObj;
	}

	private AdminPageTest AdminPageTestObj = null;
	public AdminPageTest adminpage() {
		if (AdminPageTestObj == null) {
			AdminPageTestObj = new AdminPageTest();
		}
		return AdminPageTestObj;
	}

	private GoogleDrive googleDriveObj = null;

	public GoogleDrive googleDrive() {
		if (googleDriveObj == null) {
			googleDriveObj = new GoogleDrive();
		}
		return googleDriveObj;
	}

	private CommonMethods commonMethodObj = null;

	public CommonMethods commonMethods() {
		if (commonMethodObj == null) {
			commonMethodObj = new CommonMethods();
		}
		return commonMethodObj;
	}

}
