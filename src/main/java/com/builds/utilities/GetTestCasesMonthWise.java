/*
 * package com.builds.utilities;
 * 
 * import java.io.File; import java.io.FileInputStream; import
 * java.io.FileOutputStream; import java.io.IOException; import
 * java.lang.reflect.Method; import java.util.HashMap; import java.util.Map;
 * import java.util.Map.Entry; import java.util.Set; import java.util.TreeMap;
 * 
 * import org.apache.poi.ss.usermodel.Cell; import
 * org.apache.poi.ss.usermodel.CellStyle; import
 * org.apache.poi.ss.usermodel.FillPatternType; import
 * org.apache.poi.ss.usermodel.Font; import
 * org.apache.poi.ss.usermodel.IndexedColors; import
 * org.apache.poi.ss.usermodel.Row; import org.apache.poi.ss.usermodel.Sheet;
 * import org.apache.poi.ss.usermodel.Workbook; import
 * org.apache.poi.ss.usermodel.WorkbookFactory; import
 * org.apache.poi.xssf.usermodel.XSSFWorkbook; import
 * org.reflections.Reflections; import
 * org.reflections.scanners.MethodAnnotationsScanner; import
 * org.reflections.util.ClasspathHelper; import
 * org.reflections.util.ConfigurationBuilder; import
 * org.testng.annotations.Optional; import org.testng.annotations.Parameters;
 * import org.testng.annotations.Test;
 * 
 * public class GetTestCasesMonthWise {
 * 
 * @Parameters({"moduleName" , "startmonth"})
 * 
 * @Test public void getTestCases(@Optional String moduleName , @Optional String
 * startMonth){
 * 
 * try {
 * 
 * Reflections reflections = new Reflections(new ConfigurationBuilder()
 * .setUrls(ClasspathHelper.forPackage("com.builds.test")) .setScanners(new
 * MethodAnnotationsScanner()));
 * 
 * Set<Method> methods =
 * reflections.getMethodsAnnotatedWith(org.testng.annotations.Test.class);
 * 
 * System.out.println("Size of total method : "+methods.size());
 * 
 * String testCaseId=null; String testCaseDescription=null; String
 * createdOn=null; String updatedOn=null; String gr=null;
 * 
 * HashMap<String, MyClass> testCaseDuplicate = new HashMap<String, MyClass>();
 * 
 * 
 * int count = 0; for (Method meth : methods) {
 * if(meth.isAnnotationPresent(com.builds.utilities.TestCase.class)){ String[]
 * modulegroup = meth.getAnnotation(org.testng.annotations.Test.class).groups();
 * 
 * for (int i = 0; i < modulegroup.length; i++) { gr=modulegroup[i].trim();
 * if(gr.equalsIgnoreCase(moduleName)){ break; } }
 * 
 * if (gr!=null && gr.length()>0 && gr.equalsIgnoreCase(moduleName)) {
 * 
 * testCaseId =
 * meth.getAnnotation(com.builds.utilities.TestCase.class).testCaseId().trim();
 * testCaseDescription =
 * meth.getAnnotation(com.builds.utilities.TestCase.class).testCaseDescription()
 * .trim();
 * createdOn=meth.getAnnotation(com.builds.utilities.TestCase.class).createdOn()
 * .trim();
 * updatedOn=meth.getAnnotation(com.builds.utilities.TestCase.class).updatedOn()
 * .trim();
 * 
 * if (createdOn.contains(startMonth) || updatedOn.contains(startMonth)) {
 * testCaseDuplicate.put(testCaseId, new MyClass(testCaseDescription, createdOn,
 * updatedOn));
 * 
 * } } } }
 * 
 * //For Update Month Test Cases ! String fileName =
 * FranconnectUtil.config.get("inputDirectory")+
 * "\\TestCases\\"+moduleName+"TestCases.xlsx"; File file = new File(fileName);
 * 
 * if(file.exists()) { boolean isSheetPresent = false;
 * 
 * FileInputStream fis = new FileInputStream(file); Workbook wb =
 * WorkbookFactory.create(fis);
 * 
 * CellStyle style = wb.createCellStyle(); Font font = wb.createFont();
 * font.setFontName("Courier New"); style.setFont(font);
 * 
 * Sheet sh = wb.getSheetAt(0); String sheetName = sh.getSheetName();
 * 
 * 
 * if(startMonth.contains(sheetName)){ isSheetPresent = true; sheetName=
 * sh.getSheetName(); }
 * 
 * if(isSheetPresent==true){
 * 
 * int a=sh.getLastRowNum();
 * 
 * for (int i = 1; i <= a; i++) { sh.removeRow(sh.getRow(i)); } } }
 * 
 * Map<String, MyClass> treeMap = new TreeMap<String,
 * MyClass>(testCaseDuplicate); for(Entry<String, MyClass> entry :
 * treeMap.entrySet()){
 * 
 * System.out.println("Test Case Id : "+entry.getKey()); System.out.println(
 * "Test Case Description : "+entry.getValue().getTestDescription());
 * System.out.println("Test Case Created On : "
 * +entry.getValue().getCreatedOn()); System.out.println(
 * "Test Case Updated On : "+entry.getValue().getUpdatedOn());
 * 
 * printTestInExcel_Annotation(fileName, startMonth, entry.getKey(),
 * entry.getValue().getTestDescription(), entry.getValue().getCreatedOn(),
 * entry.getValue().getUpdatedOn()); System.out.println(
 * "----------------Count : "+count); count++;
 * 
 * updatedTestStepsInXls(fileName, entry.getKey()); }
 * 
 * 
 * } catch (Exception e) { System.err.println(e.getMessage().toString()); } }
 * 
 * 
 * private void printTestInExcel_Annotation(String fileName , String startMonth
 * , String testCaseId,String testCaseDescription , String createdOn , String
 * updatedOn) throws Exception{
 * 
 * //String fileName =
 * config.get("inputDirectory")+"\\TestCases\\"+moduleName+"TestCases.xlsx";
 * 
 * File file = new File(fileName);
 * 
 * if(file.exists()==false){
 * 
 * FileOutputStream fileOut = new FileOutputStream(file.getAbsolutePath());
 * Workbook workbook = new XSSFWorkbook(); CellStyle style =
 * workbook.createCellStyle();
 * style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
 * style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
 * 
 * Font font = workbook.createFont();
 * 
 * font.setBold(true); font.setFontName("Courier New"); style.setFont(font);
 * 
 * Sheet worksheet = null;
 * 
 * worksheet = workbook.createSheet(startMonth); Row rHeader =
 * worksheet.createRow(0); Cell cHeader = rHeader.createCell(0);
 * cHeader.setCellValue("Test_WorkFlow_Id"); cHeader.setCellStyle(style);
 * 
 * cHeader = rHeader.createCell(1);
 * cHeader.setCellValue("Test_Case_Description"); cHeader.setCellStyle(style);
 * 
 * cHeader = rHeader.createCell(2); cHeader.setCellValue("Test Steps");
 * cHeader.setCellStyle(style);
 * 
 * cHeader = rHeader.createCell(3); cHeader.setCellValue("Created On");
 * cHeader.setCellStyle(style);
 * 
 * cHeader = rHeader.createCell(4); cHeader.setCellValue("Updated On");
 * cHeader.setCellStyle(style);
 * 
 * worksheet.createFreezePane(0, 1);
 * 
 * Row rHeader_space = worksheet.createRow(1);
 * 
 * workbook.write(fileOut); fileOut.flush(); fileOut.close(); workbook.close();
 * }else{ FileInputStream fis = new FileInputStream(file); Workbook wb =
 * WorkbookFactory.create(fis);
 * 
 * CellStyle style = wb.createCellStyle(); Font font = wb.createFont();
 * font.setFontName("Courier New"); style.setFont(font);
 * 
 * Sheet sh = wb.getSheetAt(0); String sheetName = sh.getSheetName();
 * 
 * boolean isSheetPresent = false;
 * 
 * if(startMonth.contains(sheetName)){ isSheetPresent = true; sheetName=
 * sh.getSheetName(); }
 * 
 * if(isSheetPresent==true){ Row row = sh.createRow(sh.getLastRowNum()+1); Cell
 * celltestCaseId = row.createCell(0);
 * 
 * if(testCaseId!=null && testCaseId.length()>0){
 * celltestCaseId.setCellValue(testCaseId); }else{ celltestCaseId.setCellValue(
 * "Test Case Id Missing!"); } celltestCaseId.setCellStyle(style);
 * 
 * Cell celltestCaseDescription = row.createCell(1);
 * celltestCaseDescription.setCellValue(testCaseDescription);
 * celltestCaseDescription.setCellStyle(style);
 * 
 * Cell cellcreatedOn = row.createCell(3);
 * cellcreatedOn.setCellValue(createdOn); cellcreatedOn.setCellStyle(style);
 * 
 * Cell cellUpdatedOn = row.createCell(4);
 * cellUpdatedOn.setCellValue(updatedOn); cellUpdatedOn.setCellStyle(style);
 * 
 * sh.autoSizeColumn(0); sh.autoSizeColumn(1); sh.autoSizeColumn(2);
 * sh.autoSizeColumn(3); sh.autoSizeColumn(4); }
 * 
 * FileOutputStream fout = new FileOutputStream(file); wb.write(fout);
 * fout.flush(); fout.close(); wb.close(); } }
 * 
 * 
 * public void updatedTestStepsInXls(String fileName , String testCaseId) throws
 * IOException{ try {
 * 
 * AutomatedTestCaseGeneration testCaseG=new AutomatedTestCaseGeneration();
 * String testCaseSteps=testCaseG.readTestCaseStepsFromExcel(testCaseId);
 * 
 * File file=new File(fileName);
 * 
 * FileInputStream fis = new FileInputStream(file); Workbook wb =
 * WorkbookFactory.create(fis);
 * 
 * CellStyle style = wb.createCellStyle(); Font font = wb.createFont();
 * font.setFontName("Courier New"); style.setFont(font);
 * 
 * Sheet sh = wb.getSheetAt(0);
 * 
 * String cellData=null;
 * 
 * for (int i = 1; i <=sh.getLastRowNum(); i++) {
 * 
 * Row rowTestCaseId=sh.getRow(i); Cell data=rowTestCaseId.getCell(0);
 * 
 * if (data!=null) { cellData=data.getStringCellValue();
 * 
 * System.out.println("Cell Data : "+cellData);
 * 
 * } if (cellData!= null && cellData.equalsIgnoreCase(testCaseId)) {
 * 
 * Row row=sh.getRow(i); Cell celltestSteps = row.createCell(2);
 * celltestSteps.setCellValue(testCaseSteps); celltestSteps.setCellStyle(style);
 * } }
 * 
 * sh.autoSizeColumn(2);
 * 
 * FileOutputStream fout = new FileOutputStream(file); wb.write(fout);
 * fout.flush(); fout.close(); wb.close();
 * 
 * } catch (Exception e) { System.out.println(e.getMessage().toString()); } } }
 */
