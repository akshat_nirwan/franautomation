
package com.builds.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.AddSheetRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetResponse;
import com.google.api.services.sheets.v4.model.CellData;
import com.google.api.services.sheets.v4.model.CellFormat;
import com.google.api.services.sheets.v4.model.DimensionProperties;
import com.google.api.services.sheets.v4.model.DimensionRange;
import com.google.api.services.sheets.v4.model.ExtendedValue;
import com.google.api.services.sheets.v4.model.GridCoordinate;
import com.google.api.services.sheets.v4.model.GridProperties;
import com.google.api.services.sheets.v4.model.Request;
import com.google.api.services.sheets.v4.model.RowData;
import com.google.api.services.sheets.v4.model.Sheet;
import com.google.api.services.sheets.v4.model.SheetProperties;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.SpreadsheetProperties;
import com.google.api.services.sheets.v4.model.TextFormat;
import com.google.api.services.sheets.v4.model.UpdateCellsRequest;
import com.google.api.services.sheets.v4.model.UpdateDimensionPropertiesRequest;
import com.google.api.services.sheets.v4.model.UpdateSheetPropertiesRequest;
import com.google.api.services.sheets.v4.model.ValueRange;

public class GoogleDrive {
	/** Application name. */
	private final String APPLICATION_NAME = "Drive API Java Quickstart";

	/** Directory to store user credentials for this application. */

	// private static final java.io.File DATA_STORE_DIR = new
	// java.io.File(System.getProperty("user.home"),
	// ".credentials/drive-java-quickstart");

	private final static java.io.File DATA_STORE_DIR = new java.io.File(
			FranconnectUtil.config.get("inputDirectory") + "\\exe");

	/** Global instance of the {@link FileDataStoreFactory}. */
	private static FileDataStoreFactory DATA_STORE_FACTORY;

	/** Global instance of the JSON factory. */
	private final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	/** Global instance of the HTTP transport. */
	private static HttpTransport HTTP_TRANSPORT;

	/**
	 * Global instance of the scopes required by this quickstart.
	 *
	 * If modifying these scopes, delete your previously saved credentials at
	 * ~/.credentials/drive-java-quickstart
	 */
	/*
	 * private static final List<String> SCOPES =
	 * Arrays.asList(DriveScopes.DRIVE_METADATA_READONLY);
	 */

	// drive scope all
	private final java.util.Collection<String> SCOPES = DriveScopes.all();

	static {
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
		} catch (Throwable t) {
			t.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @return an authorized Credential object.
	 * @throws Exception
	 */
	public Credential authorize() throws Exception {
		// Load client secrets.
		InputStream in;
		if (System.getProperty("os.name").toLowerCase().contains("win")) {
			in = new FileInputStream(FranconnectUtil.config.get("inputDirectory") + "\\exe\\client_secret.json");
		} else {
			in = new FileInputStream(FranconnectUtil.config.get("inputDirectory") + "//exe//client_secret.json");
		}
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES).setDataStoreFactory(DATA_STORE_FACTORY).setAccessType("offline").build();

		Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");

		// System.out.println("Credentials saved to " +
		// DATA_STORE_DIR.getAbsolutePath());

		return credential;
	}

	/**
	 * Build and return an authorized Drive client service.
	 * 
	 * @return an authorized Drive client service
	 * @throws Exception
	 */
	public Drive getDriveService() throws Exception {
		Credential credential = authorize();
		return new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
	}

	/**
	 * This Method is used for create folder in google drive > My Drive
	 */
	public String createFolder(Drive service, String folderName) throws Exception {
		File fileMetadata = new File();
		fileMetadata.setName(folderName);
		fileMetadata.setMimeType("application/vnd.google-apps.folder");

		File file = service.files().create(fileMetadata).setFields("id").execute();
		return file.getId();
	}

	/**
	 * This Method is used for upload file in specified folderId
	 *
	 */
	public String insertFileInFolder(Drive service, String folderId, String fileName, String absFilePath,
			String fileType) throws Exception {

		// Drive service = getDriveService();
		// String folderId = "0BwwA4oUTeiV1TGRPeTVjaWRDY1E";
		File file = null;
		try {
			File fileMetadata = new File();
			fileMetadata.setName(fileName);
			fileMetadata.setParents(Collections.singletonList(folderId));

			String absFilePath1 = absFilePath + "/" + fileName;
			java.io.File filePath = new java.io.File(absFilePath1);
			FileContent mediaContent = new FileContent(/* "image/jpeg" */fileType, filePath);
			file = service.files().create(fileMetadata, mediaContent)
					.setFields("id, parents,webContentLink,webViewLink").execute();

			permission(service, file.getId());

		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return file.getWebViewLink();
	}

	/**
	 * 
	 * This is the method is for give permission to end user
	 *
	 */
	public void permission(Drive service, String fileId) throws IOException {
		// String fileId = "1sTWaJ_j7PkjzaBWtNc3IzovK5hQf21FbOw9yLeeLPNQ";
		Permission userPermission = new Permission().setType("anyone").setRole("reader");
		service.permissions().create(fileId, userPermission).setFields("id").execute();
	}

	/********************* Google Sheets Api **********************/

	/**
	 * Build and return an authorized Sheets API client service.
	 * 
	 * @return an authorized Sheets API client service
	 * @throws Exception
	 */

	public Sheets getSheetsService() throws Exception {
		Credential credential = authorize();
		return new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME)
				.build();
	}

	/**
	 * Create New Sheet In Google Sheet Api
	 */

	public Sheets createSheetsService(Credential credential) throws IOException, GeneralSecurityException {
		HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();

		// TODO: Change placeholder below to generate authentication
		// credentials. See
		// https://developers.google.com/sheets/quickstart/java#step_3_set_up_the_sample
		//
		// Authorize using one of the following scopes:
		// "https://www.googleapis.com/auth/drive"
		// "https://www.googleapis.com/auth/spreadsheets"
		// GoogleCredential credential = null;

		return new Sheets.Builder(httpTransport, jsonFactory, credential)
				.setHttpRequestInitializer(new HttpRequestInitializer() {

					@Override
					public void initialize(HttpRequest httpRequest) throws IOException {

						credential.initialize(httpRequest);
						httpRequest.setConnectTimeout(300 * 60000); // 300
																	// minutes
																	// connect
																	// timeout
						httpRequest.setReadTimeout(300 * 60000); // 300 minutes
																	// read
																	// timeout

					}
				}).setApplicationName("Google-SheetsSample/0.1").build();
	}

	public String createSpreadSheet(Drive service, Credential credential, Sheets sheetsService)
			throws IOException, GeneralSecurityException {

		String id = null;
		Spreadsheet requestBody = new Spreadsheet();
		com.google.api.services.drive.model.File file = null;

		// make List Of Spreadsheet

		List<com.google.api.services.drive.model.File> result = new ArrayList<com.google.api.services.drive.model.File>();
		String spreadSheetName = null;
		boolean isSpreadSheetPresent = false;
		Spreadsheet response = null;

		try {
			FileList files = service.files().list()
					.setQ(" 'root' in parents and mimeType = 'application/vnd.google-apps.spreadsheet'and trashed = false")
					.execute();
			result.addAll(files.getFiles());

			for (int i = 0; i < result.size(); i++) {

				spreadSheetName = result.get(i).get("name").toString();

				if (spreadSheetName != null && spreadSheetName.equalsIgnoreCase("Automation Test Cases")) {
					isSpreadSheetPresent = true;
					file = result.get(i);
					id = file.getId();
				}
			}

			if (isSpreadSheetPresent == true) {
				// copy File To NewlY Created Folder If SpreadSheetPresent
				copySpreadSheetToFolder("TestCaseSheetBackUpFolder", id);
			}

			if (isSpreadSheetPresent == false) {

				SpreadsheetProperties sp = new SpreadsheetProperties();
				sp.setTitle("Automation Test Cases");
				requestBody.setProperties(sp);

				Sheets.Spreadsheets.Create request = sheetsService.spreadsheets().create(requestBody);
				response = request.execute();
				System.out.println(response.getProperties().getTitle());

				id = response.getSpreadsheetId();

				// Grant Permission
				permission(service, id);

			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return id;
	}

	// get All Sheet Of File If File Exists

	public List<Sheet> getAllSheets(String spreadSheetId, Sheets sheetsService) throws IOException {

		List<Sheet> sheetList = new ArrayList<Sheet>();

		try {
			Spreadsheet request = sheetsService.spreadsheets().get(spreadSheetId).execute();
			sheetList = request.getSheets();
			// System.out.println(request.getSheets().size());
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return sheetList;
	}

	// create Sheet Inside
	public BatchUpdateSpreadsheetResponse createNewSheetInsideSpreadSheet(String spreadSheetId, Sheets sheetsService,
			String title) throws IOException {

		List<Request> requests = new ArrayList<>();
		BatchUpdateSpreadsheetResponse response = null;

		// for place holder value
		List<Request> headerFormatRequest = new ArrayList<Request>();
		List<CellData> values = new ArrayList<CellData>();
		List<CellData> values1 = new ArrayList<CellData>();
		List<CellData> values2 = new ArrayList<CellData>();
		List<CellData> values3 = new ArrayList<CellData>();
		List<CellData> values4 = new ArrayList<CellData>();
		List<CellData> values5 = new ArrayList<CellData>();

		try {

			requests.add(new Request().setAddSheet(
					new AddSheetRequest().setProperties(new SheetProperties().setTitle(/* "scstc" */title))));

			BatchUpdateSpreadsheetRequest body = new BatchUpdateSpreadsheetRequest().setRequests(requests);
			response = sheetsService.spreadsheets().batchUpdate(spreadSheetId, body).execute();

			// update The First Row With Default Value

			List<Sheet> googleSheets = getAllSheets(spreadSheetId, sheetsService);
			String sheetName = null;
			int sheetId = 0;
			boolean isSheetFound = false;
			for (int i = 0; i < googleSheets.size(); i++) {

				sheetName = googleSheets.get(i).getProperties().getTitle();

				if (sheetName.equalsIgnoreCase(title)) {
					sheetId = googleSheets.get(i).getProperties().getSheetId();
					isSheetFound = true;
				}
			}

			if (isSheetFound == true) {

				values.add(new CellData().setUserEnteredValue(new ExtendedValue().setStringValue("Test_WorkFlow_Id"))
						.setUserEnteredFormat(new CellFormat().setTextFormat(new TextFormat().setBold(true))));

				headerFormatRequest
						.add(new Request()
								.setUpdateCells(new UpdateCellsRequest()
										.setStart(new GridCoordinate().setSheetId(sheetId).setRowIndex(0)
												.setColumnIndex(0))
										.setRows(Arrays.asList(new RowData().setValues(values)))
										.setFields("userEnteredValue , userEnteredFormat.textFormat")));

				values1.add(
						new CellData().setUserEnteredValue(new ExtendedValue().setStringValue("Test_Case_Description"))
								.setUserEnteredFormat(new CellFormat().setTextFormat(new TextFormat().setBold(true))));

				headerFormatRequest
						.add(new Request()
								.setUpdateCells(new UpdateCellsRequest()
										.setStart(new GridCoordinate().setSheetId(sheetId).setRowIndex(0)
												.setColumnIndex(1))
										.setRows(Arrays.asList(new RowData().setValues(values1)))
										.setFields("userEnteredValue ,userEnteredFormat.textFormat")));

				values2.add(new CellData().setUserEnteredValue(new ExtendedValue().setStringValue("Test Steps"))
						.setUserEnteredFormat(new CellFormat().setTextFormat(new TextFormat().setBold(true))));

				headerFormatRequest
						.add(new Request()
								.setUpdateCells(new UpdateCellsRequest()
										.setStart(new GridCoordinate().setSheetId(sheetId).setRowIndex(0)
												.setColumnIndex(2))
										.setRows(Arrays.asList(new RowData().setValues(values2)))
										.setFields("userEnteredValue , userEnteredFormat.textFormat")));
				// Edit By Inzmam

				values3.add(new CellData().setUserEnteredValue(new ExtendedValue().setStringValue("Created On"))
						.setUserEnteredFormat(new CellFormat().setTextFormat(new TextFormat().setBold(true))));

				headerFormatRequest
						.add(new Request()
								.setUpdateCells(new UpdateCellsRequest()
										.setStart(new GridCoordinate().setSheetId(sheetId).setRowIndex(0)
												.setColumnIndex(3))
										.setRows(Arrays.asList(new RowData().setValues(values3)))
										.setFields("userEnteredValue , userEnteredFormat.textFormat")));

				values4.add(new CellData().setUserEnteredValue(new ExtendedValue().setStringValue("Updated On"))
						.setUserEnteredFormat(new CellFormat().setTextFormat(new TextFormat().setBold(true))));

				headerFormatRequest
						.add(new Request()
								.setUpdateCells(new UpdateCellsRequest()
										.setStart(new GridCoordinate().setSheetId(sheetId).setRowIndex(0)
												.setColumnIndex(4))
										.setRows(Arrays.asList(new RowData().setValues(values4)))
										.setFields("userEnteredValue , userEnteredFormat.textFormat")));

				values5.add(new CellData().setUserEnteredValue(new ExtendedValue().setStringValue("Remarks"))
						.setUserEnteredFormat(new CellFormat().setTextFormat(new TextFormat().setBold(true))));

				headerFormatRequest
						.add(new Request()
								.setUpdateCells(new UpdateCellsRequest()
										.setStart(new GridCoordinate().setSheetId(sheetId).setRowIndex(0)
												.setColumnIndex(5))
										.setRows(Arrays.asList(new RowData().setValues(values5)))
										.setFields("userEnteredValue , userEnteredFormat.textFormat")));

				headerFormatRequest.add(new Request().setUpdateSheetProperties(new UpdateSheetPropertiesRequest()
						.setProperties(new SheetProperties().setSheetId(sheetId)
								.setGridProperties(new GridProperties().setFrozenRowCount(1)))
						.setFields("gridProperties.frozenRowCount")));

				headerFormatRequest.add(
						new Request().setUpdateDimensionProperties(new UpdateDimensionPropertiesRequest()
								.setRange(new DimensionRange().setSheetId(sheetId).setDimension("COLUMNS")
										.setStartIndex(0).setEndIndex(1))
								.setProperties(new DimensionProperties().setPixelSize(300)).setFields("pixelSize")));

				headerFormatRequest.add(
						new Request().setUpdateDimensionProperties(new UpdateDimensionPropertiesRequest()
								.setRange(new DimensionRange().setSheetId(sheetId).setDimension("COLUMNS")
										.setStartIndex(1).setEndIndex(2))
								.setProperties(new DimensionProperties().setPixelSize(450)).setFields("pixelSize")));

				headerFormatRequest.add(
						new Request().setUpdateDimensionProperties(new UpdateDimensionPropertiesRequest()
								.setRange(new DimensionRange().setSheetId(sheetId).setDimension("COLUMNS")
										.setStartIndex(2).setEndIndex(3))
								.setProperties(new DimensionProperties().setPixelSize(450)).setFields("pixelSize")));

				headerFormatRequest.add(
						new Request().setUpdateDimensionProperties(new UpdateDimensionPropertiesRequest()
								.setRange(new DimensionRange().setSheetId(sheetId).setDimension("COLUMNS")
										.setStartIndex(3).setEndIndex(4))
								.setProperties(new DimensionProperties().setPixelSize(300)).setFields("pixelSize")));

				headerFormatRequest.add(
						new Request().setUpdateDimensionProperties(new UpdateDimensionPropertiesRequest()
								.setRange(new DimensionRange().setSheetId(sheetId).setDimension("COLUMNS")
										.setStartIndex(4).setEndIndex(5))
								.setProperties(new DimensionProperties().setPixelSize(300)).setFields("pixelSize")));

				headerFormatRequest.add(
						new Request().setUpdateDimensionProperties(new UpdateDimensionPropertiesRequest()
								.setRange(new DimensionRange().setSheetId(sheetId).setDimension("COLUMNS")
										.setStartIndex(5).setEndIndex(6))
								.setProperties(new DimensionProperties().setPixelSize(300)).setFields("pixelSize")));

				if (headerFormatRequest != null && headerFormatRequest.size() > 0) {

					BatchUpdateSpreadsheetRequest batchUpdateRequest = new BatchUpdateSpreadsheetRequest()
							.setRequests(headerFormatRequest);

					sheetsService.spreadsheets().batchUpdate(spreadSheetId, batchUpdateRequest).execute();

				}
			}

		} catch (Exception e) {
			System.out.println(e.toString());
		}

		return response;
	}

	// update sheet

	public BatchUpdateSpreadsheetResponse updateSheetWithTestSteps(String spreadSheetId, Sheets sheetsService,
			int sheetId, int rowNum, int culmNum, String testCaseSteps) throws IOException {

		List<Request> insertDataRequest = new ArrayList<Request>();
		List<CellData> values = new ArrayList<CellData>();
		BatchUpdateSpreadsheetResponse reponse = null;

		values.add(new CellData().setUserEnteredValue(new ExtendedValue().setStringValue(testCaseSteps))
				.setUserEnteredFormat(new CellFormat().setWrapStrategy("WRAP")));

		insertDataRequest
				.add(new Request()
						.setUpdateCells(new UpdateCellsRequest()
								.setStart(new GridCoordinate().setSheetId(sheetId).setRowIndex(rowNum)
										.setColumnIndex(culmNum))
								.setRows(Arrays.asList(new RowData().setValues(values)))
								.setFields("userEnteredValue , userEnteredFormat.wrapStrategy")));

		if (insertDataRequest != null && insertDataRequest.size() > 0) {

			BatchUpdateSpreadsheetRequest batchUpdateRequest = new BatchUpdateSpreadsheetRequest()
					.setRequests(insertDataRequest);

			reponse = sheetsService.spreadsheets().batchUpdate(spreadSheetId, batchUpdateRequest).execute();

		}

		return reponse;

	}

	// insert Data
	public void insertDataInSheet(Sheets sheetsService, List<Sheet> allSheets, String moduleGroupName,
			String testCaseId, String spreadSheetId, String testCaseDescription) throws Exception {

		// get The Id Of Sheet
		List<Request> requests1 = new ArrayList<>();
		List<CellData> values = new ArrayList<>();
		List<CellData> values1 = new ArrayList<>();

		String currentSheetName = null;
		int sheetId = 0;

		for (int i = 0; i < allSheets.size(); i++) {

			currentSheetName = allSheets.get(i).getProperties().getTitle();

			if (currentSheetName.equalsIgnoreCase(moduleGroupName)) {
				sheetId = allSheets.get(i).getProperties().getSheetId();

				// Deleted All Values From Sheet

				/*
				 * int lastRow1=getRowNum(sheetsService, spreadSheetId,
				 * currentSheetName);
				 * 
				 * try {
				 * 
				 * values3.add(new CellData() .setUserEnteredValue(new
				 * ExtendedValue() .setStringValue("")));
				 * 
				 * requests2.add(new Request().setUpdateCells(new
				 * UpdateCellsRequest().setRange(new
				 * GridRange().setSheetId(sheetId).setStartRowIndex(0)
				 * .setEndRowIndex(lastRow1).setStartColumnIndex(0).
				 * setEndColumnIndex(2)).setRows(Arrays.asList(new
				 * RowData().setValues(values3))).setFields("userEnteredValue"))
				 * );
				 * 
				 * 
				 * if (requests2.size()>0) {
				 * 
				 * BatchUpdateSpreadsheetRequest batchUpdateRequest = new
				 * BatchUpdateSpreadsheetRequest() .setRequests(requests2);
				 * sheetsService.spreadsheets().batchUpdate(spreadSheetId,
				 * batchUpdateRequest) .execute(); }
				 * 
				 * } catch (Exception e) {
				 * 
				 * }
				 */

				// inserting data Into It after cleainig this
				int lastRow = getRowNum(sheetsService, spreadSheetId, currentSheetName);

				for (int j = lastRow; j <= lastRow; j++) {

					// entered test case id
					try {

						values.add(new CellData().setUserEnteredValue(new ExtendedValue().setStringValue(testCaseId)));

						requests1
								.add(new Request().setUpdateCells(new UpdateCellsRequest()
										.setStart(new GridCoordinate().setSheetId(sheetId).setRowIndex(lastRow)
												.setColumnIndex(0))
										.setRows(Arrays.asList(new RowData().setValues(values)))
										.setFields("userEnteredValue")));

						values1.add(new CellData()
								.setUserEnteredValue(new ExtendedValue().setStringValue(testCaseDescription)));

						requests1
								.add(new Request().setUpdateCells(new UpdateCellsRequest()
										.setStart(new GridCoordinate().setSheetId(sheetId).setRowIndex(lastRow)
												.setColumnIndex(1))
										.setRows(Arrays.asList(new RowData().setValues(values1)))
										.setFields("userEnteredValue")));

					} catch (Exception e) {
						System.out.println(e.toString());
					}
				}

				if (requests1.size() > 0) {
					BatchUpdateSpreadsheetRequest batchUpdateRequest = new BatchUpdateSpreadsheetRequest()
							.setRequests(requests1);
					sheetsService.spreadsheets().batchUpdate(spreadSheetId, batchUpdateRequest).execute();
				}
			}
		}
	}

	// get Last Row Number
	public int getRowNum(Sheets sheetsService, String spreadsheetId, String sheetName) throws Exception {
		String range = sheetName + "!A:A";
		ValueRange response1 = sheetsService.spreadsheets().values().get(spreadsheetId, range).execute();
		List<List<Object>> values1 = response1.getValues();

		int size = 0;

		if (values1 == null) {
			return size;
		} else {
			return values1.size();
		}
	}

	public void copySpreadSheetToFolder(String folderName, String spreadSheetId) throws Exception {

		GoogleDrive qStart = new GoogleDrive();
		Drive service = getDriveService();
		CodeUtility codeutil = new CodeUtility();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd.HHmmss");
		Date date = new Date();
		String timeStamp = dateFormat.format(date);
		String curDate = timeStamp.substring(0, timeStamp.lastIndexOf("."));
		String curTime = timeStamp.substring(timeStamp.lastIndexOf(".") + 1, timeStamp.length());
		String fileName = curDate.concat(curTime);

		File file = new File();
		file.setName(fileName);

		// copy Spreadsheet To NewLy Created Folder
		File FileCop = service.files().copy(spreadSheetId, file).execute();
		System.out.println(FileCop.getName());

		// File Move To Folder
		File FolderFile = codeutil.createBaseFolder(service, folderName, qStart);

		String fileId = /* "1sTWaJ_j7PkjzaBWtNc3IzovK5hQf21FbOw9yLeeLPNQ"; */FileCop.getId();
		String folderId = /* "0BwwA4oUTeiV1TGRPeTVjaWRDY1E"; */FolderFile.getId();

		// Retrieve the existing parents to remove
		File file1 = service.files().get(fileId).setFields("parents").execute();
		StringBuilder previousParents = new StringBuilder();
		for (String parent : file1.getParents()) {
			previousParents.append(parent);
			previousParents.append(',');
		}
		// Move the file to the new folder
		service.files().update(fileId, null).setAddParents(folderId).setRemoveParents(previousParents.toString())
				.setFields("id, parents").execute();
	}
}
