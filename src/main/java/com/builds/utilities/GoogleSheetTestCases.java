/*
 * 
 * package com.builds.utilities;
 * 
 * import java.lang.reflect.Method; import java.util.ArrayList; import
 * java.util.Arrays; import java.util.HashMap; import java.util.List; import
 * java.util.Map; import java.util.Map.Entry; import java.util.Set; import
 * java.util.TreeMap;
 * 
 * import org.reflections.Reflections; import
 * org.reflections.scanners.MethodAnnotationsScanner; import
 * org.reflections.util.ClasspathHelper; import
 * org.reflections.util.ConfigurationBuilder; import
 * org.testng.annotations.Optional; import org.testng.annotations.Parameters;
 * import org.testng.annotations.Test;
 * 
 * import com.google.api.client.auth.oauth2.Credential; import
 * com.google.api.services.drive.Drive; import
 * com.google.api.services.sheets.v4.Sheets; import
 * com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetRequest; import
 * com.google.api.services.sheets.v4.model.CellData; import
 * com.google.api.services.sheets.v4.model.CellFormat; import
 * com.google.api.services.sheets.v4.model.DeleteSheetRequest; import
 * com.google.api.services.sheets.v4.model.ExtendedValue; import
 * com.google.api.services.sheets.v4.model.GridCoordinate; import
 * com.google.api.services.sheets.v4.model.Request; import
 * com.google.api.services.sheets.v4.model.RowData; import
 * com.google.api.services.sheets.v4.model.Sheet; import
 * com.google.api.services.sheets.v4.model.UpdateCellsRequest;
 * 
 * public class GoogleSheetTestCases {
 * 
 * @Parameters("moduleName")
 * 
 * @Test public void insertTestCasesGoogleSheet(@Optional String moduleName)
 * throws Exception{
 * 
 * GoogleDrive gd=new GoogleDrive(); List<String> sheetNameListUser=new
 * ArrayList<String>();
 * 
 * sheetNameListUser.add(moduleName);
 * 
 * String userSheetName=null; System.setProperty("https.protocols",
 * "TLSv1,TLSv1.1,TLSv1.2");
 * 
 * Drive service=gd.getDriveService(); Credential credential = gd.authorize();
 * Sheets sheetsService = gd.createSheetsService(credential);
 * 
 * try {
 * 
 * String spreadSheetId=gd.createSpreadSheet(service, credential,
 * sheetsService); List<Sheet> allSheets=gd.getAllSheets(spreadSheetId,
 * sheetsService); int sheetId=0;
 * 
 * for (int j = 0; j < sheetNameListUser.size(); j++) { // crm,sales,sales
 * boolean sheetExist = false;
 * 
 * if (allSheets.size()>0) {
 * 
 * for (int i = 0; i < allSheets.size(); i++) { // Sheet 1
 * 
 * String currentSheetName =allSheets.get(i).getProperties().getTitle(); //
 * Current Sheet Name Sheet 1
 * 
 * userSheetName=sheetNameListUser.get(j);
 * 
 * if(userSheetName.equalsIgnoreCase(currentSheetName)){ sheetExist = true;
 * sheetId=allSheets.get(i).getProperties().getSheetId();
 * 
 * 
 * } } if(sheetExist==true){
 * 
 * List<Request> requests = new ArrayList<>(); requests.add(new
 * Request().setDeleteSheet(new DeleteSheetRequest().setSheetId(sheetId)));
 * 
 * BatchUpdateSpreadsheetRequest body = new
 * BatchUpdateSpreadsheetRequest().setRequests(requests);
 * sheetsService.spreadsheets().batchUpdate(spreadSheetId, body).execute(); } }
 * 
 * 
 * userSheetName = userSheetName.substring(0, 1).toUpperCase() +
 * userSheetName.substring(1); gd.createNewSheetInsideSpreadSheet(spreadSheetId,
 * sheetsService, userSheetName); }
 * 
 * List<Sheet> googleSheets=gd.getAllSheets(spreadSheetId, sheetsService);
 * 
 * Reflections reflections = new Reflections(new ConfigurationBuilder()
 * .setUrls(ClasspathHelper.forPackage("com.builds.test")) .setScanners(new
 * MethodAnnotationsScanner()));
 * 
 * Set<Method> methods =
 * reflections.getMethodsAnnotatedWith(org.testng.annotations.Test.class);
 * 
 * System.out.println("Size of total method : "+methods.size());
 * 
 * String testCaseId=null; String testCaseDescription=null; String
 * createdOn=null; String updatedOn=null; String gr=null; String matchedModule =
 * "";
 * 
 * //HashMap<String,String> testCaseDuplicate = new HashMap<String,String>();
 * HashMap<String, MyClass> testCaseDuplicate = new HashMap<String, MyClass>();
 * 
 * 
 * int count = 0; for (Method meth : methods) {
 * if(meth.isAnnotationPresent(com.builds.utilities.TestCase.class)){ String[]
 * modulegroup = meth.getAnnotation(org.testng.annotations.Test.class).groups();
 * 
 * //System.out.println(" Group Test Case :"+modulegroup.length);
 * 
 * 
 * for (int i = 0; i < modulegroup.length; i++) { gr=modulegroup[i].trim();
 * if(gr.equalsIgnoreCase(moduleName)){ break; }
 * 
 * }
 * 
 * 
 * if (gr!=null && gr.length()>0 && gr.equalsIgnoreCase(moduleName)) {
 * 
 * 
 * matchedModule = gr; testCaseId =
 * meth.getAnnotation(com.builds.utilities.TestCase.class).testCaseId().trim();
 * testCaseDescription =
 * meth.getAnnotation(com.builds.utilities.TestCase.class).testCaseDescription()
 * .trim();
 * 
 * //updated by inzi
 * createdOn=meth.getAnnotation(com.builds.utilities.TestCase.class).createdOn()
 * .trim();
 * updatedOn=meth.getAnnotation(com.builds.utilities.TestCase.class).updatedOn()
 * .trim(); //testCaseDuplicate.put(testCaseId, testCaseDescription);
 * 
 * testCaseDuplicate.put(testCaseId, new MyClass(testCaseDescription, createdOn,
 * updatedOn)); } } }
 * 
 * //Map<String, String> treeMap = new TreeMap<String,
 * String>(testCaseDuplicate);
 * 
 * Map<String, MyClass> treeMap = new TreeMap<String,
 * MyClass>(testCaseDuplicate);
 * 
 * for(Entry<String, MyClass> entry : treeMap.entrySet()){
 * 
 * insertDataInSheet(googleSheets, matchedModule, spreadSheetId, entry.getKey(),
 * entry.getValue().getTestDescription(), entry.getValue().getCreatedOn() ,
 * entry.getValue().getUpdatedOn());
 * 
 * System.out.println(entry.getValue().getCreatedOn());
 * System.out.println(entry.getValue().getTestDescription());
 * System.out.println(entry.getValue().getUpdatedOn());
 * System.out.println(entry.getKey());
 * 
 * System.out.println("----------------Count : "+count); count++; } } catch
 * (Exception e) { System.out.println(e.toString()); } }
 * 
 * public void insertDataInSheet(List<Sheet> allSheets , String moduleGroupName
 * ,String spreadSheetId , String testCaseId ,String testCaseDescription, String
 * createdOn, String updatedOn) throws Exception{
 * 
 * GoogleDrive gd=new GoogleDrive(); Credential credential = gd.authorize();
 * Sheets sheetsService=gd.createSheetsService(credential);
 * 
 * List<Request> insertDataRequest = new ArrayList<Request>(); List<CellData>
 * values = new ArrayList<CellData>(); List<CellData> values1 = new
 * ArrayList<CellData>(); List<CellData> values2 = new ArrayList<CellData>();
 * List<CellData> values3 = new ArrayList<CellData>();
 * 
 * String currentSheetName=null; int sheetId=0;
 * 
 * boolean sheetFound = false; for (int i = 0; i < allSheets.size(); i++) {
 * 
 * currentSheetName =allSheets.get(i).getProperties().getTitle();
 * 
 * if (currentSheetName.equalsIgnoreCase(moduleGroupName)) { sheetFound = true;
 * sheetId=allSheets.get(i).getProperties().getSheetId();
 * 
 * int lastRow=gd.getRowNum(sheetsService, spreadSheetId, currentSheetName);
 * 
 * try {
 * 
 * values.add(new CellData().setUserEnteredValue(new
 * ExtendedValue().setStringValue(testCaseId)) .setUserEnteredFormat(new
 * CellFormat().setWrapStrategy("WRAP")));
 * 
 * insertDataRequest.add(new Request().setUpdateCells(new
 * UpdateCellsRequest().setStart(new GridCoordinate()
 * .setSheetId(sheetId).setRowIndex(lastRow).setColumnIndex(0)).setRows(Arrays.
 * asList(new RowData() .setValues(values))).setFields(
 * "userEnteredValue , userEnteredFormat.wrapStrategy")));
 * 
 * values1.add(new CellData() .setUserEnteredValue(new ExtendedValue()
 * .setStringValue(testCaseDescription)).setUserEnteredFormat(new
 * CellFormat().setWrapStrategy("WRAP")));
 * 
 * insertDataRequest.add(new Request().setUpdateCells(new
 * UpdateCellsRequest().setStart(new GridCoordinate()
 * .setSheetId(sheetId).setRowIndex(lastRow).setColumnIndex(1)).setRows(Arrays.
 * asList(new RowData(). setValues(values1))).setFields(
 * "userEnteredValue , userEnteredFormat.wrapStrategy")));
 * 
 * 
 * values2.add(new CellData() .setUserEnteredValue(new ExtendedValue()
 * .setStringValue(createdOn)).setUserEnteredFormat(new
 * CellFormat().setWrapStrategy("WRAP")));
 * 
 * insertDataRequest.add(new Request().setUpdateCells(new
 * UpdateCellsRequest().setStart(new GridCoordinate()
 * .setSheetId(sheetId).setRowIndex(lastRow).setColumnIndex(3)).setRows(Arrays.
 * asList(new RowData(). setValues(values2))).setFields(
 * "userEnteredValue , userEnteredFormat.wrapStrategy")));
 * 
 * 
 * values3.add(new CellData() .setUserEnteredValue(new ExtendedValue()
 * .setStringValue(updatedOn)).setUserEnteredFormat(new
 * CellFormat().setWrapStrategy("WRAP")));
 * 
 * insertDataRequest.add(new Request().setUpdateCells(new
 * UpdateCellsRequest().setStart(new GridCoordinate()
 * .setSheetId(sheetId).setRowIndex(lastRow).setColumnIndex(4)).setRows(Arrays.
 * asList(new RowData(). setValues(values3))).setFields(
 * "userEnteredValue , userEnteredFormat.wrapStrategy")));
 * 
 * } catch (Exception e) { System.out.println(e.toString()); }
 * 
 * if (insertDataRequest!=null && insertDataRequest.size()>0) {
 * 
 * BatchUpdateSpreadsheetRequest batchUpdateRequest = new
 * BatchUpdateSpreadsheetRequest() .setRequests(insertDataRequest);
 * 
 * sheetsService.spreadsheets().batchUpdate(spreadSheetId, batchUpdateRequest)
 * .execute(); } }
 * 
 * if(sheetFound==true){ i = allSheets.size(); } } } }
 * 
 */
