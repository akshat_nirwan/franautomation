package com.builds.utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.Sheet;
import com.google.api.services.sheets.v4.model.ValueRange;

public class GoogleSheetUpdateTestSteps {

	@Parameters("sheetNameToBeUpdated")
	@Test
	public void testSheetUpdate(@Optional String sheetNameToBeUpdated) throws Exception {

		AutomatedTestCaseGeneration testCaseG = new AutomatedTestCaseGeneration();

		GoogleDrive gd = new GoogleDrive();
		Drive service = gd.getDriveService();
		Credential credential = gd.authorize();
		Sheets sheetsService = gd.createSheetsService(credential);

		List<com.google.api.services.drive.model.File> result = new ArrayList<com.google.api.services.drive.model.File>();
		String spreadSheetName = null;
		boolean isSpreadSheetPresent = false;
		com.google.api.services.drive.model.File file = null;
		String spreadSheetId = null;
		int[] rowCulNumberArray = new int[2];
		int sheetId = 0;
		List<String> testCaseIdList = new ArrayList<String>();
		String testCaseId = null;

		// Get SpreadSheet Id If Present
		try {
			FileList files = service.files().list()
					.setQ(" 'root' in parents and mimeType = 'application/vnd.google-apps.spreadsheet'and trashed = false")
					.execute();
			result.addAll(files.getFiles());

			for (int i = 0; i < result.size(); i++) {

				spreadSheetName = result.get(i).get("name").toString();

				if (spreadSheetName != null && spreadSheetName.equalsIgnoreCase("Automation Test Cases")) {
					isSpreadSheetPresent = true;
					file = result.get(i);
					spreadSheetId = file.getId();
				}
			}

			// Get All Sheet If Present
			if (spreadSheetId != null && isSpreadSheetPresent == true) {

				List<Sheet> allGoogleSheet = gd.getAllSheets(spreadSheetId, sheetsService);

				if (allGoogleSheet.size() > 0) {

					for (int i = 0; i < allGoogleSheet.size(); i++) {

						allGoogleSheet.get(i).getProperties().getTitle();

						if (allGoogleSheet.get(i).getProperties().getTitle().equalsIgnoreCase(sheetNameToBeUpdated)) {
							sheetId = allGoogleSheet.get(i).getProperties().getSheetId();
						}
					}
				}

				System.out.println("googleSheetName :" + sheetNameToBeUpdated);

				// Get TestCase Id From Google Sheet
				testCaseIdList = readTestCaseIdFromGoogleSheet(sheetsService, spreadSheetId, sheetNameToBeUpdated);

				if (testCaseIdList.size() > 0) {

					for (int j = 0; j < testCaseIdList.size(); j++) {

						testCaseId = testCaseIdList.get(j);

						System.out.println("testCaseId :" + testCaseId);

						// Get Test Case Steps Corresponding to TestCase ID
						String testCaseSteps = testCaseG.readTestCaseStepsFromExcel(testCaseId);

						System.out.println("testCaseSteps :" + testCaseSteps);

						rowCulNumberArray = findTestCaseIdRowNumForUpdate(sheetNameToBeUpdated, sheetsService,
								spreadSheetId, testCaseId);

						// Update TestSteps At Google Sheet

						System.out.println("row num :" + rowCulNumberArray[0]);
						System.out.println("culumn num :" + rowCulNumberArray[1]);

						if (rowCulNumberArray[1] != 0) {
							gd.updateSheetWithTestSteps(spreadSheetId, sheetsService, sheetId, rowCulNumberArray[0],
									rowCulNumberArray[1], testCaseSteps);
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}

	// find test cases Id in google sheet

	public int[] findTestCaseIdRowNumForUpdate(String sheetName, Sheets sheetsService, String spreadSheetId,
			String testCaseId) throws IOException {

		String range = sheetName + "!A:A";
		ValueRange response = sheetsService.spreadsheets().values().get(spreadSheetId, range).execute();
		List<List<Object>> values = response.getValues();
		int rowNumber = 0;
		int culNumber = 0;

		int[] rowCulNumberArray = new int[2];

		if (values != null) {

			for (int i = 0; i < values.size(); i++) {

				// System.out.println(values);

				if (values.get(i).contains(testCaseId)) {

					rowNumber = i;
					culNumber = 2;

					rowCulNumberArray[0] = rowNumber;
					rowCulNumberArray[1] = culNumber;
				}
			}
		}

		return rowCulNumberArray;
	}

	public List<String> readTestCaseIdFromGoogleSheet(Sheets sheetsService, String spreadSheetId, String sheetName)
			throws IOException {
		List<String> testCaseList = new ArrayList<String>();
		String tempVal = null;
		String range = sheetName + "!A:A";
		ValueRange response = sheetsService.spreadsheets().values().get(spreadSheetId, range).execute();

		List<List<Object>> values = response.getValues();

		if (values != null) {

			for (int i = 1; i < values.size(); i++) {
				tempVal = values.get(i).toString().replace("[", "");
				tempVal = tempVal.replace("]", "");

				System.out.println(tempVal.trim());

				testCaseList.add(tempVal.trim());
			}
		}
		return testCaseList;
	}
}
