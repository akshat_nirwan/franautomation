package com.builds.utilities;

import java.net.URL;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

public class Grid {
	@Test
	public static void runOnNode(Map<String, String> config, String port, String url) throws Exception {

		/*
		 * Create a batch file java -jar selenium-server-standalone-2.46.0.jar
		 * -role node -port 4444 -hub http://192.168.8.67:4444/grid/register -
		 * browser browserName="internetexplorer"
		 * -Dwebdriver.ie.driver="c:\\IEDriverServer.exe" - browser
		 * browserName="chrome" -Dwebdriver.chrome.driver="c:\\chromedriver.exe
		 */
		String nodeip = config.get("systemIp");
		System.out.println(">>>>>>>>>>>>>>>>>>" + nodeip);
		String buildurl = config.get("buildUrl");
		System.out.println(">>>>>>>>>>>>>>>>>>" + buildurl);
		String browsername = config.get("browserName");
		browsername = "chrome";
		System.out.println(">>>>>>>>>>>>>>>>>>" + browsername);
		DesiredCapabilities capability = null;

		if (browsername != null && browsername.equalsIgnoreCase("firefox")) {
			capability = DesiredCapabilities.firefox();
		}
		if (browsername != null && browsername.equalsIgnoreCase("chrome")) {
			capability = DesiredCapabilities.chrome();
		}
		if (browsername != null && browsername.equalsIgnoreCase("internetExplorer")) {
			capability = DesiredCapabilities.internetExplorer();
		}

		WebDriver driver = new RemoteWebDriver(new URL("http://" + nodeip.concat(":").concat(port).concat("/wd/hub")),
				capability);
		for (int x = 1; x < 10; x++) {
			driver.get(config.get("buildUrl"));
			Thread.sleep(5000);
			System.out.println(config.get("portNumber"));
		}

	}
}
