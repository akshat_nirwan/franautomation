package com.builds.utilities;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class I18Comparison {
	/*
	 * This method is used to get the difference in the key - value pair
	 */
	@Test
	@Parameters("folderPath")
	public void readIntFileToKnowdiff(@Optional String folderPath) throws Exception {

		File folder = new File(folderPath);
		File[] files = folder.listFiles();

		for (File f : files) {
			try {
				// Reporter.log("Module = "+f.getAbsolutePath());
				System.out.println(f.getAbsolutePath());
				File file = new File(f.getAbsolutePath());
				FileInputStream fis = new FileInputStream(file);
				Workbook wb = WorkbookFactory.create(fis);
				Sheet sh = wb.getSheetAt(0);

				Row row = null;
				for (int x = 0; x < sh.getLastRowNum() + 1; x++) {
					row = sh.getRow(x);
					Cell cell0 = null;

					String key = null;
					String value = null;

					try {
						cell0 = row.getCell(0);
						key = cell0.getStringCellValue();
					} catch (Exception e) {

					}
					Cell cell1 = null;
					try {
						cell1 = row.getCell(1);
						value = cell1.getStringCellValue();
					} catch (Exception e) {

					}
					if (key != null && value != null) {
						if (!key.equals(value)) {
							System.out.println("key : " + key);
							// Reporter.log("key : "+key);
							System.out.println("val : " + value);
							// Reporter.log("val : "+value);
							System.out.println("");
							// Reporter.log("");
						}
					}
				}
				wb.close();
				Reporter.log("===================================================================================");
				Thread.sleep(15000);
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}
}
