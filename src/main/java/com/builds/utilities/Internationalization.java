package com.builds.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Internationalization {

	/*
	 * This method writes the key and value in excel sheet Login and go to admin
	 * > translation is pending
	 */
	@Test
	@Parameters({ "moduleName", "languageName", "type_of_key", "buildURL" })
	public void intValidation(@Optional String moduleName, @Optional String languageName, @Optional String type_of_key,
			@Optional String buildURL) throws Exception {

		String keyval = "";
		if (type_of_key.equalsIgnoreCase("Content Key(Labels and headers etc)")) {
			keyval = "labels_headers";
		} else if (type_of_key.equalsIgnoreCase("Message Key(Validation alerts).")) {
			keyval = "Messages";
		}

		String newfilepath = "C:\\int\\" + moduleName + "_" + keyval + ".xlsx";

		FileOutputStream foutnewwb = new FileOutputStream(newfilepath);
		File fileNew = new File(newfilepath);
		XSSFWorkbook workbook = new XSSFWorkbook();
		workbook.createSheet();
		workbook.write(foutnewwb);
		workbook.close();

		File file = new File(newfilepath);
		FileInputStream fis = new FileInputStream(file);
		Workbook wb = WorkbookFactory.create(fis);
		Sheet sheet = wb.getSheetAt(0);
		Row row = null;
		Cell cell0 = null;
		Cell cell1 = null;

		/*System.setProperty("webdriver.chrome.driver",
				FranconnectUtil.config.get("inputDirectory") + "\\exe\\chromedriver.exe");*/
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium_Test_Input\\exe\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(buildURL);
		driver.manage().window().maximize();
		driver.findElement(By.name("user_id")).sendKeys("adm");
		driver.findElement(By.name("password")).sendKeys("t0n1ght");
		driver.findElement(By.id("ulogin")).click();
		driver.findElement(By.id("dropdown")).click();
		driver.findElement(By.linkText("Admin")).click();

		Thread.sleep(180000);

		driver.findElement(By.xpath(".//*[@qat_adminlink='Configuration']")).click();
		try {
			Thread.sleep(5000);
			driver.findElement(By.xpath(".//*[@qat_adminlink='Configure Translations']")).click();

			Select sel = new Select(driver.findElement(By.id("modules")));
			sel.selectByVisibleText(moduleName);

			Select sel2 = new Select(driver.findElement(By.id("slanguage")));
			sel2.selectByVisibleText(languageName);

			if (type_of_key.equalsIgnoreCase("Content Key(Labels and headers etc)")) {
				driver.findElement(By.xpath(".//*[@name='sectionType' and @value='Yes']")).click();
			} else if (type_of_key.equalsIgnoreCase("Message Key(Validation alerts).")) {
				driver.findElement(By.xpath(".//*[@name='sectionType' and @value='No']")).click();
			}

			driver.findElement(By.xpath(".//*[@name='searchTypes' and @value='Yes']")).click();
			driver.findElement(By.xpath(".//input[@value='OK']")).click();

			try {
				driver.switchTo().alert().accept();
			} catch (Exception e) {

			}
		} catch (Exception e) {
			System.out.println(e);
		}

		Thread.sleep(10000);

		List<WebElement> list = new ArrayList<WebElement>();
		List<WebElement> list2 = new ArrayList<WebElement>();

		if (type_of_key.equalsIgnoreCase("Content Key(Labels and headers etc)")) {
			list = driver.findElements(By.xpath(
					".//*[contains(@id,'_Form')]/table/tbody/tr[3]/td/table/tbody/tr/td/table/tbody//td[@align='right'][1]"));
			list2 = driver.findElements(By.xpath(
					".//*[contains(@id,'_Form')]/table/tbody/tr[3]/td/table/tbody/tr/td/table/tbody//td[3]/input"));
		} else if (type_of_key.equalsIgnoreCase("Message Key(Validation alerts).")) {
			list = driver.findElements(By.xpath(
					".//*[contains(@id,'_Form')]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody//td[@align='right'][1]"));
			list2 = driver.findElements(By.xpath(
					".//*[contains(@id,'_Form')]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody//td[3]/input"));
		}

		List<String> list_1 = new ArrayList<String>();
		List<String> list2_2 = new ArrayList<String>();

		for (int x = 0; x < list.size(); x++) {
			list_1.add(list.get(x).getText());
			list2_2.add(list2.get(x).getAttribute("value"));
		}

		for (int x = 0; x < list_1.size(); x++) {
			try {
				row = sheet.createRow(sheet.getLastRowNum() + 1);
				cell0 = row.createCell(0);
				cell1 = row.createCell(1);

				String key = list_1.get(x);
				String value = list2_2.get(x);

				if (key != null && key.length() > 0) {
					cell0.setCellValue(key.trim());
				}
				if (value != null && value.length() > 0) {
					cell1.setCellValue(value.trim());
				}
			} catch (Exception e) {
				System.out.println(e);
			}
		}

		try {
			driver.quit();
		} catch (Exception e) {

		}

		FileOutputStream fout = new FileOutputStream(file);
		wb.write(fout);
		fout.close();
		fout.flush();
	}
}
