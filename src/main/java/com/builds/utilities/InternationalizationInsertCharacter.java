package com.builds.utilities;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class InternationalizationInsertCharacter {

	/*
	 * This method writes the key and value in excel sheet Login and go to admin
	 * > translation is pending
	 */
	@Test
	@Parameters({ "moduleName", "languageName", "type_of_key", "buildURL"/*, "buildName" */})
	public void intValidation(@Optional String moduleName, @Optional String languageName, @Optional String type_of_key,
			@Optional String buildURL/*, @Optional String buildName*/) throws Exception {

		/*System.setProperty("webdriver.chrome.driver",
				FranconnectUtil.config.get("inputDirectory") + "\\exe\\chromedriver.exe");*/
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium_Test_Input\\exe\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(buildURL);
		driver.manage().window().maximize();
		driver.findElement(By.name("user_id")).sendKeys("adm");
		driver.findElement(By.name("password")).sendKeys("t0n1ght");
		driver.findElement(By.id("ulogin")).click();
		driver.findElement(By.id("dropdown")).click();
		driver.findElement(By.linkText("Admin")).click();
		Thread.sleep(120000);
		try {
			/*if (buildName.equalsIgnoreCase("FC")) {

				driver.findElement(By.xpath(".//*[@qat_adminlink='Configuration']")).click();
				Thread.sleep(10000);
				driver.findElement(By.xpath(".//*[@qat_adminlink='Configure Translations']")).click();

			} else if (buildName.equalsIgnoreCase("ZC")) {

				driver.findElement(By.xpath(".//h3[contains(text () , 'Marketing')]")).click();
				driver.findElement(By.xpath(".//*[@qat_adminlink='Configuration']")).click();
				Thread.sleep(10000);
				driver.findElement(By.xpath(".//*[@qat_adminlink='Configure Translations']")).click();
			}*/

			driver.findElement(By.xpath(".//*[@qat_adminlink='Configuration']")).click();
			Thread.sleep(10000);
			driver.findElement(By.xpath(".//*[@qat_adminlink='Configure Translations']")).click();
			
			Select sel = new Select(driver.findElement(By.id("modules")));
			sel.selectByVisibleText(moduleName);

			Select sel2 = new Select(driver.findElement(By.id("slanguage")));
			sel2.selectByVisibleText(languageName);

			if (type_of_key.equalsIgnoreCase("Content Key(Labels and headers etc)")) {
				driver.findElement(By.xpath(".//*[@name='sectionType' and @value='Yes']")).click();
			} else if (type_of_key.equalsIgnoreCase("Message Key(Validation alerts).")) {
				driver.findElement(By.xpath(".//*[@name='sectionType' and @value='No']")).click();
			}

			driver.findElement(By.xpath(".//*[@name='searchTypes' and @value='Yes']")).click();
			driver.findElement(By.xpath(".//input[@value='OK']")).click();

			try {
				driver.switchTo().alert().accept();
			} catch (Exception e) {

			}
		} catch (Exception e) {
			System.out.println(e);
		}

		try {
			Thread.sleep(15000);

			List<WebElement> list2 = new ArrayList<WebElement>();

			if (type_of_key.equalsIgnoreCase("Content Key(Labels and headers etc)")) {
				list2 = driver.findElements(By.xpath(
						".//*[contains(@id,'_Form')]/table/tbody/tr[3]/td/table/tbody/tr/td/table/tbody//td[3]/input"));
			} else if (type_of_key.equalsIgnoreCase("Message Key(Validation alerts).")) {
				list2 = driver.findElements(By.xpath(
						".//*[contains(@id,'_Form')]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody//td[3]/input"));
			}

			for (WebElement temp : list2) {
				// temp.clear();
				// temp.sendKeys(readText());

				String text = temp.getAttribute("value");
				temp.clear();
				((JavascriptExecutor) driver).executeScript("arguments[0].value = arguments[1];", temp,
						text.concat(readText()));
			}
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
					driver.findElement(By.id("SaveButton1")));
			driver.findElement(By.id("SaveButton1")).click();
			Thread.sleep(5000);
		} catch (Exception e) {
			System.out.println("Error Msg : " + e.getMessage());
		}
		driver.quit();
	}

	public String readText() throws IOException {
		String fileName = "c://txt//specialcharacter.txt";
		String content = new String(Files.readAllBytes(Paths.get(fileName)));
		List<String> lines = Files.readAllLines(Paths.get(fileName));

		String specialchartext = "";
		for (String ab : lines) {
			specialchartext = ab;
		}
		return specialchartext;
	}

}
