package com.builds.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mysql.jdbc.Connection;

public class JiraWebServices {

	public static void main(String[] args) {
		String url = "";
		String login = "";
		String encoded = "";
		HttpResponse response1 = null;
		HttpGet request1 = null;
		BufferedReader rd = null;
		String line = "";
		StringBuffer result = null;
		String newFieldJson = "";
		JsonParser jsonParser = null;
		JsonElement element = null;
		JsonElement elementinner = null;
		JsonObject object = null;

		// 96 keys
		// String keys
		// ="FCSKY-8672,FCSKY-8671,FCSKY-8670,FCSKY-8665,FCSKY-8664,FCSKY-8662,FCSKY-8661,FCSKY-8660,FCSKY-8659,FCSKY-8658,FCSKY-8616,FCSKY-8609,FCSKY-8608,FCSKY-8607,FCSKY-8604,FCSKY-8602,FCSKY-8598,FCSKY-8597,FCSKY-8596,FCSKY-8512,FCSKY-8511,FCSKY-8510,FCSKY-8076,FCSKY-8051,FCSKY-8049,FCSKY-8047,FCSKY-8046,FCSKY-8045,FCSKY-8044,FCSKY-8025,FCSKY-8022,FCSKY-8019,FCSKY-8014,FCSKY-8010,FCSKY-8009,FCSKY-8005,FCSKY-8003,FCSKY-8001,FCSKY-7999,FCSKY-7981,FCSKY-7980,FCSKY-7973,FCSKY-7972,FCSKY-7971,FCSKY-7970,FCSKY-7969,FCSKY-7957,FCSKY-7954,FCSKY-7952,FCSKY-7943,FCSKY-7939,FCSKY-7938,FCSKY-7937,FCSKY-7936,FCSKY-7934,FCSKY-7933,FCSKY-7929,FCSKY-7919,FCSKY-7901,FCSKY-7619,FCSKY-7586,FCSKY-7577,FCSKY-7576,FCSKY-7575,FCSKY-7573,FCSKY-7570,FCSKY-7564,FCSKY-7563,FCSKY-7562,FCSKY-7552,FCSKY-7544,FCSKY-7542,FCSKY-7534,FCSKY-7494,FCSKY-7490,FCSKY-7487,FCSKY-7483,FCSKY-7476,FCSKY-7475,FCSKY-7474,FCSKY-7458,FCSKY-7446,FCSKY-7445,FCSKY-7444,FCSKY-7443,FCSKY-7442,FCSKY-7441,FCSKY-7440,FCSKY-7439,FCSKY-7438,FCSKY-7437,FCSKY-7436,FCSKY-7435,FCSKY-7419,FCSKY-7413,FCSKY-7402";
		// 127 Keys
		// String keys
		// ="FCSKY-8672,FCSKY-8671,FCSKY-8670,FCSKY-8665,FCSKY-8664,FCSKY-8662,FCSKY-8661,FCSKY-8660,FCSKY-8659,FCSKY-8658,FCSKY-8652,FCSKY-8648,FCSKY-8647,FCSKY-8616,FCSKY-8609,FCSKY-8608,FCSKY-8607,FCSKY-8604,FCSKY-8602,FCSKY-8598,FCSKY-8597,FCSKY-8596,FCSKY-8512,FCSKY-8511,FCSKY-8510,FCSKY-8076,FCSKY-8051,FCSKY-8049,FCSKY-8047,FCSKY-8046,FCSKY-8045,FCSKY-8044,FCSKY-8025,FCSKY-8022,FCSKY-8019,FCSKY-8014,FCSKY-8010,FCSKY-8009,FCSKY-8005,FCSKY-8003,FCSKY-8001,FCSKY-7999,FCSKY-7981,FCSKY-7980,FCSKY-7973,FCSKY-7972,FCSKY-7971,FCSKY-7970,FCSKY-7969,FCSKY-7957,FCSKY-7954,FCSKY-7952,FCSKY-7943,FCSKY-7939,FCSKY-7938,FCSKY-7937,FCSKY-7936,FCSKY-7934,FCSKY-7933,FCSKY-7929,FCSKY-7919,FCSKY-7901,FCSKY-7619,FCSKY-7586,FCSKY-7577,FCSKY-7576,FCSKY-7575,FCSKY-7573,FCSKY-7570,FCSKY-7564,FCSKY-7563,FCSKY-7562,FCSKY-7552,FCSKY-7544,FCSKY-7542,FCSKY-7534,FCSKY-7494,FCSKY-7490,FCSKY-7487,FCSKY-7483,FCSKY-7476,FCSKY-7475,FCSKY-7474,FCSKY-7458,FCSKY-7446,FCSKY-7445,FCSKY-7444,FCSKY-7443,FCSKY-7442,FCSKY-7441,FCSKY-7440,FCSKY-7439,FCSKY-7438,FCSKY-7437,FCSKY-7436,FCSKY-7435,FCSKY-7419,FCSKY-7413,FCSKY-7402,FCSKY-6638,FCSKY-6635,FCSKY-6616,FCSKY-6612,FCSKY-6578,FCSKY-6566,FCSKY-6560,FCSKY-6559,FCSKY-6557,FCSKY-6555,FCSKY-6554,FCSKY-6638,FCSKY-6635,FCSKY-6616,FCSKY-6612,FCSKY-6578,FCSKY-6566,FCSKY-6560,FCSKY-4702,FCSKY-4692,FCSKY-4690,FCSKY-4688,FCSKY-4686,FCSKY-4683,FCSKY-4681,FCSKY-4679,FCSKY-4677,FCSKY-4675";
		// 183 Keys
		String keys = "FCSKY-12901";

		String totalKeys[] = keys.split(",");
		HashMap<String, String> jiraIdMap = new HashMap<String, String>();
		HashMap<String, String> finalMap = new HashMap<String, String>();

		ArrayList<String> tcIdList = new ArrayList<String>();
		Map<String, String> jiraData = new HashMap<String, String>();
		Map<String, String> FinaljiraData = new HashMap<String, String>();
		for (int i = 0; i < totalKeys.length; i++) {

			url = "https://franconnect.atlassian.net/rest/api/2/issue/" + totalKeys[i];
			// "https://franconnect.atlassian.net/rest/api/2/search?jql=project%20%3D%20FCC%20AND%20resolution%20%3D%20Unresolved%20ORDER%20BY%20priority%20DESC%2C%20updated%20DESC"
			// ;
			// "https://franconnect.atlassian.net/rest/api/2/issue/FCSKY-8604";
			// "https://franconnect.atlassian.net/rest/api/2/user?username=rohit.kant";
			login = "ravi.pal:SINGtinytwenty!#%5556";
			final byte[] authBytes = login.getBytes(StandardCharsets.UTF_8);
			encoded = Base64.getEncoder().withoutPadding().encodeToString(authBytes);

			// System.out.println("Access Token URL: "+ url);
			result = null;
			try {
				// System.out.println("1");
				@SuppressWarnings({ "resource", "deprecation" })
				HttpClient client = new DefaultHttpClient();
				request1 = new HttpGet(url);

				request1.setHeader("Content-Type", "application/json");
				request1.setHeader("Authorization", "Basic " + encoded);

				// System.out.println("2");

				response1 = client.execute(request1);
				rd = new BufferedReader(new InputStreamReader(response1.getEntity().getContent()));
				// System.out.println("3");

				result = new StringBuffer();
				line = "";
				while ((line = rd.readLine()) != null) {
					// System.out.println("line " + line);
					result.append(line);
					// System.out.println("3");

				}
			} catch (UnsupportedOperationException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// System.out.println(result.toString());

			newFieldJson = "";
			jsonParser = new JsonParser();
			element = jsonParser.parse(result.toString());
			elementinner = null;
			object = element.getAsJsonObject();

			if (object != null) {
				for (Map.Entry<String, JsonElement> entry : object.entrySet()) {
					JsonElement array = entry.getValue();
					// System.out.println("First
					// key=="+entry.getKey()+"==Value==="+entry.getValue());
					if ("fields".equals(entry.getKey())) {
						elementinner = entry.getValue();
					}

				}
			}
			// String name = object.get("id").getAsString();

			if (elementinner != null) {
				object = elementinner.getAsJsonObject();

				for (Map.Entry<String, JsonElement> entry : object.entrySet()) {

					JsonElement array = entry.getValue();
					// System.out.println("Arrayy::::;"+array);
					// System.out.println("Second
					// key=="+entry.getKey()+"==Value==="+entry.getValue());
					if ("customfield_13700".equals(entry.getKey())) {
						// System.out.println(totalKeys[i]+"
						// ==="+entry.getValue());
						jiraData.put(totalKeys[i], (entry.getValue()).toString());

					}
				}

			}
			System.out.println("totalKeys>>>>>>::::" + i);

		}
		System.out.println("jiraData>>>" + jiraData);
		try {

			String DB_URL = "jdbc:mysql://192.168.8.199/FCAUTOMATION";
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = null;
			conn = (Connection) DriverManager.getConnection(DB_URL, "root", "root");
			PreparedStatement stmtp = conn.prepareStatement("INSERT INTO JIRA_DATA  (TC_ID,JIRA_ID) VALUE(?,?)");
			PreparedStatement ps = conn.prepareStatement("TRUNCATE JIRA_DATA");
			ps.executeUpdate();

			// Start iterating Code In Database
			String arr = null;
			Set<String> tcSet = jiraData.keySet();
			arr = jiraData.values().toString();
			System.out.println("ARRR::" + arr);

			Iterator itr = tcSet.iterator();
			while (itr.hasNext()) {
				String str = (String) itr.next();

				String val = jiraData.get(str);

				val = val.replaceAll("\\[", "");
				val = val.replaceAll("\\]", "");
				val = val.replaceAll("\"", "");
				String[] splitString = val.split(",");
				for (int i = 0; i < splitString.length; i++) {
					// finalMap.put(splitString[i],str);
					String tc_ID = splitString[i];
					String jira_id = str;
					stmtp.setString(1, tc_ID);
					stmtp.setString(2, jira_id);
					stmtp.executeUpdate();
				}
			}
			System.out.println("All records added");

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
