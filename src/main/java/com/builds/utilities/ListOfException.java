package com.builds.utilities;

import java.util.ArrayList;
import java.util.List;

public class ListOfException {

	public List<String> exceptionList() {

		String ElementNotSelectableException = "ElementNotSelectableException";
		String ElementNotVisibleException = "ElementNotVisibleException";
		String ErrorInResponseException = "ErrorInResponseException";
		String ImeActivationFailedException = "ImeActivationFailedException";
		String ImeNotAvailableException = "ImeNotAvailableException";
		String InvalidCookieDomainException = "InvalidCookieDomainException";
		String InvalidElementStateException = "InvalidElementStateException";
		String InvalidSelectorException = "InvalidSelectorException";
		String InvalidSwitchToTargetException = "InvalidSwitchToTargetException";
		String MoveTargetOutOfBoundsException = "MoveTargetOutOfBoundsException";
		String NoAlertPresentException = "NoAlertPresentException";
		String NoSuchAttributeException = "NoSuchAttributeException";
		String NoSuchElementException = "NoSuchElementException";
		String NoSuchFrameException = "NoSuchFrameException";
		String NoSuchWindowException = "NoSuchWindowException";
		String RemoteDriverServerException = "RemoteDriverServerException";
		String StaleElementReferenceException = "StaleElementReferenceException";
		String TimeoutException = "TimeoutException";
		String UnableToSetCookieException = "UnableToSetCookieException";
		String UnexpectedAlertPresentException = "UnexpectedAlertPresentException";
		String UnexpectedTagNameException = "UnexpectedTagNameException";
		String WebDriverException = "WebDriverException";
		String Exception = "Exception";
		String UnhandledAlertException = "UnhandledAlertException";

		List<String> listOfException = new ArrayList<String>();

		listOfException.add(ElementNotSelectableException);
		listOfException.add(ElementNotVisibleException);
		listOfException.add(ErrorInResponseException);
		listOfException.add(ImeActivationFailedException);
		listOfException.add(ImeNotAvailableException);
		listOfException.add(InvalidCookieDomainException);
		listOfException.add(InvalidElementStateException);
		listOfException.add(InvalidSelectorException);
		listOfException.add(InvalidSwitchToTargetException);
		listOfException.add(MoveTargetOutOfBoundsException);
		listOfException.add(NoAlertPresentException);
		listOfException.add(NoSuchAttributeException);
		listOfException.add(NoSuchElementException);
		listOfException.add(NoSuchFrameException);
		listOfException.add(NoSuchWindowException);
		listOfException.add(RemoteDriverServerException);
		listOfException.add(StaleElementReferenceException);
		listOfException.add(TimeoutException);
		listOfException.add(UnableToSetCookieException);
		listOfException.add(UnexpectedAlertPresentException);
		listOfException.add(UnexpectedTagNameException);
		listOfException.add(WebDriverException);
		listOfException.add(Exception);
		listOfException.add(UnhandledAlertException);

		List<String> listOfException2 = new ArrayList<String>();
		List<String> listOfException3 = new ArrayList<String>();

		for (String list : listOfException) {
			listOfException2.add(list.toLowerCase());
		}

		for (String list : listOfException) {
			listOfException3.add(list.toUpperCase());
		}

		listOfException.addAll(listOfException2);
		listOfException.addAll(listOfException3);

		return listOfException;
	}
}
