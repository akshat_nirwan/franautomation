package com.builds.utilities;

class ModuleReport {

	private String SMTP_HOST_NAME;
	private String SMTP_PORT;
	private String mailSubject;
	private String mailBody;
	private String mailTo;
	private String mailCC;
	private String mailBCC; 
	private String mailFrom;
	private String replyTo;
	private String attachment;
	private String buildUrl;
	private String os;
	private String browserName;
	private String module;
	private String BuildVersion;
	private String totalNumberOfTC;
	private String testCasesPassed;
	private String testCasesFailed;
	private String testCasesSkipped;
	private String failPercentage;
	private String totalTimeTaken;
	private String IPADDRESS;
	private String googleTestDire;
	private String requestIP;
	
	public String getBuildUrl() {
		return buildUrl;
	}
	public void setBuildUrl(String buildUrl) {
		this.buildUrl = buildUrl;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public String getBrowserName() {
		return browserName;
	}
	public void setBrowserName(String browserName) {
		this.browserName = browserName;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getBuildVersion() {
		return BuildVersion;
	}
	public void setBuildVersion(String buildVersion) {
		BuildVersion = buildVersion;
	}
	public String getTotalNumberOfTC() {
		return totalNumberOfTC;
	}
	public void setTotalNumberOfTC(String totalNumberOfTC) {
		this.totalNumberOfTC = totalNumberOfTC;
	}
	public String getTestCasesPassed() {
		return testCasesPassed;
	}
	public void setTestCasesPassed(String testCasesPassed) {
		this.testCasesPassed = testCasesPassed;
	}
	public String getTestCasesFailed() {
		return testCasesFailed;
	}
	public void setTestCasesFailed(String testCasesFailed) {
		this.testCasesFailed = testCasesFailed;
	}
	public String getTestCasesSkipped() {
		return testCasesSkipped;
	}
	public void setTestCasesSkipped(String testCasesSkipped) {
		this.testCasesSkipped = testCasesSkipped;
	}
	public String getFailPercentage() {
		return failPercentage;
	}
	public void setFailPercentage(String failPercentage) {
		this.failPercentage = failPercentage;
	}
	public String getTotalTimeTaken() {
		return totalTimeTaken;
	}
	public void setTotalTimeTaken(String totalTimeTaken) {
		this.totalTimeTaken = totalTimeTaken;
	}
	public String getIPADDRESS() {
		return IPADDRESS;
	}
	public void setIPADDRESS(String iPADDRESS) {
		IPADDRESS = iPADDRESS;
	}
	public String getGoogleTestDire() {
		return googleTestDire;
	}
	public void setGoogleTestDire(String googleTestDire) {
		this.googleTestDire = googleTestDire;
	}
	public String getRequestIP() {
		return requestIP;
	}
	public void setRequestIP(String requestIP) {
		this.requestIP = requestIP;
	}
	public String getSMTP_HOST_NAME() {
		return SMTP_HOST_NAME;
	}
	public void setSMTP_HOST_NAME(String sMTP_HOST_NAME) {
		SMTP_HOST_NAME = sMTP_HOST_NAME;
	}
	public String getSMTP_PORT() {
		return SMTP_PORT;
	}
	public void setSMTP_PORT(String sMTP_PORT) {
		SMTP_PORT = sMTP_PORT;
	}
	public String getMailSubject() {
		return mailSubject;
	}
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}
	public String getMailBody() {
		return mailBody;
	}
	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}
	public String getMailTo() {
		return mailTo;
	}
	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}
	public String getMailCC() {
		return mailCC;
	}
	public void setMailCC(String mailCC) {
		this.mailCC = mailCC;
	}
	public String getMailBCC() {
		return mailBCC;
	}
	public void setMailBCC(String mailBCC) {
		this.mailBCC = mailBCC;
	}
	public String getMailFrom() {
		return mailFrom;
	}
	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}
	public String getReplyTo() {
		return replyTo;
	}
	public void setReplyTo(String replyTo) {
		this.replyTo = replyTo;
	}
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	
	
}
