package com.builds.utilities;

public class MyClass {

	private String testCaseDescription;
	private String createdOn;
	private String updatedOn;

	public MyClass(String tcD, String co, String uo) {
		this.testCaseDescription = tcD;
		this.createdOn = co;
		this.updatedOn = uo;
	}

	public String getTestDescription() {
		return testCaseDescription;
	}

	public void setTestCaseDes(String item) {
		this.testCaseDescription = item;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String type) {
		this.createdOn = type;
	}

	public String getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(String price) {
		this.updatedOn = price;
	}
}
