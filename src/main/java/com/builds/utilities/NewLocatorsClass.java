package com.builds.utilities;

public class NewLocatorsClass {
	public static void newLoc(String testScript) {

		String keyval1 = null;
		String keyval2 = null;
		String newString = null;
		String extraText = null;
		int secondpos;
		// System.out.println(testScript);
		if (testScript.contains(";")) {
			String[] args = testScript.split("\n");
			// System.out.println("Lines : "+args.length);
			for (int x = 0; x < args.length; x++) {
				if (args[x].contains("pobj") && args[x].contains("sendKeys") && !args[x].contains("Element")) {
					secondpos = args[x].lastIndexOf(".send");
					keyval1 = args[x].substring(0, secondpos);
					extraText = keyval1.replace(keyval1.trim(), "");
					keyval2 = args[x].substring(args[x].indexOf("(") + 1, args[x].indexOf(")"));
					newString = "ElementUtil.sendKeys(" + keyval1.trim() + "," + keyval2.trim() + ");";
					args[x] = newString;
					System.out.println(extraText + args[x]);

					keyval1 = null;
					keyval2 = null;
					extraText = null;

				} else if (args[x].contains("pobj") && args[x].contains("click") && !args[x].contains("Element")) {
					secondpos = args[x].lastIndexOf(".click");
					// pobj.submitBtn.click();
					// ElementUtil.clickElement(pobj.submitBtn);
					keyval1 = args[x].substring(0, secondpos);
					extraText = keyval1.replace(keyval1.trim(), "");
					newString = "ElementUtil.clickElement(" + keyval1.trim() + ");";
					args[x] = newString;
					System.out.println(extraText + args[x]);
					keyval1 = null;
					keyval2 = null;
					extraText = null;
				} else if (args[x].contains("Thread") && args[x].contains("sleep")) {
					extraText = NewLocatorsClass.abc(args[x]);
					newString = extraText + "ElementUtil.sleep();";
					args[x] = newString;
					System.out.println(args[x]);
				}

				else if (args[x].contains("Select") && args[x].contains("new") && args[x + 1].contains("selectBy")
						&& !args[x].contains("Element")) {
					// Select category = new Select(pobj.category);
					// category.selectByVisibleText("Domestic");

					keyval1 = args[x].substring(args[x].indexOf("(") + 1, args[x].indexOf(")"));
					// System.out.println(keyval1);
					keyval2 = args[x + 1].substring(args[x + 1].indexOf("(") + 1, args[x + 1].indexOf(")"));
					// System.out.println(keyval2);
					extraText = NewLocatorsClass.abc(args[x]);
					newString = extraText + "ElementUtil.selectDropDown(" + keyval1 + ", " + keyval2 + ");";
					args[x] = newString;
					args[x + 1] = "//";
					System.out.println(args[x]);
				} else {
					System.out.println(args[x]);
				}
				// System.out.println(args[x]);
			}
		}
	}

	public static String abc(String arg) {

		int a = arg.length();
		int b = arg.trim().length();
		int space = a - b - 1;
		String extraSpace = "";
		if (space > 0) {
			for (int x = 0; x < space; x++) {
				extraSpace = extraSpace + "\t";
			}
		}
		return extraSpace;
	}
}
