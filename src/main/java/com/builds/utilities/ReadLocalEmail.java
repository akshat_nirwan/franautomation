package com.builds.utilities;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;

import org.testng.annotations.Test;

public class ReadLocalEmail {

	@Test
	public Map<String, String> emailReceivedValidator(String host, String mailStoreType, String user, String password,
			String fromEmail, String returnPath, String[] emailRecepient, String subject, String mailBody,
			String[] attachmentName) throws Exception {

		Map<String, String> mailData = null;
		Store store = createStoreLocalEmails(host, mailStoreType, user, password);
		Folder emailFolder = readFolder(store);
		Message[] messages = retrieveMessagesFromInbox(store, emailFolder, host, mailStoreType, user, password);

		if (messages.length > 0) {
			mailData = checkMail(messages, fromEmail, returnPath, emailRecepient, subject, mailBody, attachmentName);
		} else {
			// send test email

		}

		emailFolder.close(false);
		store.close();

		return mailData;
	}

	@Test
	public void emailReceivedValidator() throws Exception {
		String host = "192.168.9.2";// change accordingly
		String mailStoreType = "pop3";
		String user = "testautomation@franqa.net";// change accordingly
		String password = "sdg@1a@Hfs";// change accordingly
		String text = null;
		String fromEmail = "automation@franconnect.net";
		String returnPath = "rohit.kant@franconnect.net";
		String[] emailRecepient = { "rohit.kant@franconnect.net", "inzamam.haq@franconnect.net" };
		String subject = "Your Account has been created";
		String mailBody = "Login ID : test5555";
		String[] attachmentName = { "attachment1.txt" };

		Store store = createStoreLocalEmails(host, mailStoreType, user, password);
		Folder emailFolder = readFolder(store);
		Message[] messages = retrieveMessagesFromInbox(store, emailFolder, host, mailStoreType, user, password);

		if (messages.length > 0) {
			checkMail(messages, fromEmail, returnPath, emailRecepient, subject, mailBody, attachmentName);
		} else {
			// send test email

		}

		emailFolder.close(false);
		store.close();
	}

	// emailReceivedValidator
	private Store createStoreLocalEmails(String host, String mailStoreType, String user, String password)
			throws Exception {
		Properties properties = new Properties();

		properties.put("mail.pop3.host", host);
		properties.put("mail.pop3.port", "110");
		properties.put("mail.pop3.starttls.enable", "true");

		// Session emailSession = Session.getDefaultInstance(properties);

		Session emailSession = Session.getInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, password);
			}
		});

		// create the POP3 store object and connect with the pop server
		Store store = emailSession.getStore(mailStoreType);

		store.connect(host, user, password);

		return store;
	}

	// emailReceivedValidator
	private Folder readFolder(Store store) throws Exception {
		Folder emailFolder = store.getFolder("INBOX");
		emailFolder.open(Folder.READ_ONLY);
		return emailFolder;
	}

	// emailReceivedValidator
	private Message[] retrieveMessagesFromInbox(Store store, Folder emailFolder, String host, String mailStoreType,
			String user, String password) throws Exception {
		Message[] messages = emailFolder.getMessages();
		return messages;
	}

	// emailReceivedValidator
	private Map<String, String> checkMail(Message[] messages, String fromEmail, String returnPath,
			String[] emailRecepient, String subject, String mailBody, String[] attachmentName) {
		Map<String, String> mailData = null;
		mailData = new HashMap<>();
		try {
			int totalNoOfEmails = 0;
			totalNoOfEmails = messages.length;

			// boolean isMailFound = false;
			System.out.println("Total mails in Inbox : " + totalNoOfEmails);
			int n = messages.length;
			Message message = null;

			String receivedEmailMessageBody = null;

			for (int i = 0; i < n; i++) { // looping in the total email found!
				message = messages[i];

				Object receivedMail = message.getContent();
				String receivedMailContent = receivedMail.toString();

				if (receivedMail instanceof String) {
					System.out.println(message.getContent().toString());
					String receivedMailBody = (String) receivedMailContent;

				} else if (receivedMail instanceof Multipart) {
					Multipart mp = (Multipart) message.getContent();
					BodyPart bp = mp.getBodyPart(0);
					// System.out.println("\n\nCONTENT : " +
					// bp.getContent()+"\n\n");
					receivedEmailMessageBody = bp.getContent().toString();
					boolean mailHasAttachment = mailHasAttachments(message);
					// System.out.println("Does Email has attachment ? "
					// +mailHasAttachment);
				}

				String receivedMailSubject = message.getSubject();
				String receivedMailContentType = message.getContentType();
				String receivedMailReceipients = InternetAddress.toString(message.getAllRecipients());
				Date receivedMailSentDate = message.getSentDate();
				String receivedMailReplyTo = InternetAddress.toString(message.getReplyTo());
				String receivedMailGetFrom = InternetAddress.toString(message.getFrom());

				if (subject.equalsIgnoreCase(receivedMailSubject) && receivedEmailMessageBody.contains(mailBody)) {
					mailData.put("EmailBody", receivedEmailMessageBody);
					mailData.put("EmailSubject", receivedMailSubject);
					mailData.put("EmailContentType", receivedMailContentType);
					mailData.put("EmailReceipients", receivedMailReceipients);
					mailData.put("receivedMailSentDate", receivedMailSentDate.toString());
					mailData.put("EmailReplyTo", receivedMailReplyTo);
					mailData.put("EmailGetFrom", receivedMailGetFrom);

				}
			}
			// close the store and folder objects

		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mailData;
	}

	// emailReceivedValidator
	private boolean mailHasAttachments(Message message) throws Exception {
		int count = 0;
		if (message.isMimeType("multipart/mixed")) {
			Multipart mp = (Multipart) message.getContent();
			if (mp.getCount() > 1) {
				count = 1;
			}
		}
		if (count == 1) {
			return true;
		} else {
			return false;
		}
	}
}