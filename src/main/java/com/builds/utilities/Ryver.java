package com.builds.utilities;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.apache.http.client.ClientProtocolException;
import org.testng.annotations.Test;

public class Ryver {
	
	@Test
	public void PostOnRyverGroup(String message,String GroupName) throws MalformedURLException, IOException
	{
		
		System.out.println("--------------------->>>>>>>>>>>>>>>>>>>>> " +message);
		
			//String login = "rohit_kant:Rohitk@123";
			String login = "inzamam_haq:0nl1n3Ryv3r";
			final byte[] authBytes = login.getBytes(StandardCharsets.UTF_8);
			String encoded = Base64.getEncoder().withoutPadding().encodeToString(authBytes);

			/*
			 * HttpURLConnection httpcon = (HttpURLConnection) ((new URL(
			 * "https://franconnect.ryver.com/api/1/odata.svc/forums(id=1143611)/Chat.PostMessage()")
			 * .openConnection()));
			 */

			HttpURLConnection httpcon = (HttpURLConnection) ((new URL(
					"https://franconnect.ryver.com/api/1/odata.svc/workrooms(id="+GroupName+")/Chat.PostMessage()").openConnection()));
			httpcon.setDoOutput(true);
			httpcon.setRequestProperty("Content-Type", "application/json");
			httpcon.setRequestProperty("Accept-Charset", "UTF-8");
			httpcon.setRequestProperty("Accept", "application/json");
			httpcon.setRequestProperty("Authorization", "Basic " + encoded);
			httpcon.setRequestMethod("POST");
			httpcon.connect();

			// For next line \\n

			String payload = "{\"body\":\"" + message + "\"}";
			byte[] outputBytes = payload.getBytes("UTF-8");
			OutputStream os = httpcon.getOutputStream();
			os.write(outputBytes);

			os.close();
			System.out.println("httpcon===" + httpcon.getResponseCode());
	}
	
	
	public void PostOnRyverForum(String message, String forumsId) throws ClientProtocolException, IOException {
		String login = "automation@franconnect.com:fran@123";
		final byte[] authBytes = login.getBytes(StandardCharsets.UTF_8);
		String encoded = Base64.getEncoder().withoutPadding().encodeToString(authBytes);

		/*
		 * HttpURLConnection httpcon = (HttpURLConnection) ((new URL(
		 * "https://franconnect.ryver.com/api/1/odata.svc/forums(id=1143611)/Chat.PostMessage()")
		 * .openConnection()));
		 */

		HttpURLConnection httpcon = (HttpURLConnection) ((new URL(
				"https://franconnect.ryver.com/api/1/odata.svc/forums(id=" + forumsId + ")/Chat.PostMessage()")
						.openConnection()));
		httpcon.setDoOutput(true);
		httpcon.setRequestProperty("Content-Type", "application/json");
		httpcon.setRequestProperty("Accept-Charset", "UTF-8");
		httpcon.setRequestProperty("Accept", "application/json");
		httpcon.setRequestProperty("Authorization", "Basic " + encoded);
		httpcon.setRequestMethod("POST");
		httpcon.connect();

		// For next line \\n

		String payload = "{\"body\":\"" + message + "\"}";
		byte[] outputBytes = payload.getBytes("UTF-8");
		OutputStream os = httpcon.getOutputStream();
		os.write(outputBytes);

		os.close();
		System.out.println("httpcon===" + httpcon.getResponseCode());
	}
}
