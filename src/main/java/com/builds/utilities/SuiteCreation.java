package com.builds.utilities;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.testng.annotations.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class SuiteCreation {

	@Test
	public void createTestSuiteFile() {
		String folderName = "d://abcd";
		File folder = new File(folderName);

		if (folder.exists() == false) {
			folder.mkdirs();
		}

		String fileName = folder.getAbsolutePath() + File.separator + "custom.xml";

		File file = new File(fileName);

		createXMLFiles(file, "testsuite", "testName", "10");

	}

	public String createXMLFiles(File file, String suiteName, String testName, String threadCount) {

		String buildName = "--";
		String buildURL = "--";

		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// suite elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("suite");
			doc.appendChild(rootElement);

			Attr attr = doc.createAttribute("name");
			attr.setValue(suiteName);
			rootElement.setAttributeNode(attr);

			attr = doc.createAttribute("verbose");
			attr.setValue("7");
			rootElement.setAttributeNode(attr);

			attr = doc.createAttribute("parallel");
			attr.setValue("methods");
			rootElement.setAttributeNode(attr);

			attr = doc.createAttribute("thread-count");
			attr.setValue(threadCount);
			rootElement.setAttributeNode(attr);
			int counter = 1;

			// staff elements
			Element test = doc.createElement("test");
			rootElement.appendChild(test);

			// set attribute to staff element
			Attr attr2 = doc.createAttribute("name");
			attr2.setValue(testName);
			test.setAttributeNode(attr2);
			counter++;

			// shorten way
			// staff.setAttribute("id", "1");

			// firstname elements
			Element parameter = doc.createElement("parameter");
			test.appendChild(parameter);

			attr = doc.createAttribute("name");
			attr.setValue("path");
			parameter.setAttributeNode(attr);

			attr = doc.createAttribute("value");
			attr.setValue("path");
			parameter.setAttributeNode(attr);

			Element classes = doc.createElement("classes");
			test.appendChild(classes);

			Element class_child = doc.createElement("class");
			classes.appendChild(class_child);

			attr = doc.createAttribute("name");
			attr.setValue("SaasBuildsLoginValidation.BuildLoginThroughFSS");
			class_child.setAttributeNode(attr);

			Element methods = doc.createElement("methods");
			class_child.appendChild(methods);

			Element exclude = doc.createElement("include");
			methods.appendChild(exclude);

			attr = doc.createAttribute("name");
			attr.setValue("openBuild");
			exclude.setAttributeNode(attr);

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(file.getAbsolutePath());

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);

			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			// transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,
			// "ROOT-VALUE");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "http://testng.org/testng-1.0.dtd");

			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

			transformer.transform(source, result);

			System.out.println("File saved!");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}

		return file.getAbsolutePath();
	}

}
