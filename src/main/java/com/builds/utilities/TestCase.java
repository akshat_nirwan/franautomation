package com.builds.utilities;

import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Rohit
 *
 */

@Target({ METHOD, TYPE, CONSTRUCTOR })
@Retention(RetentionPolicy.RUNTIME)
public @interface TestCase {
	public String testCaseId();

	public String testCaseDescription();

	public String createdOn();

	public String updatedOn();

	public String[] reference() default "";
}
