package com.builds.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.Test;

import com.mysql.jdbc.Connection;

public class TestCaseDataBase {

	public static Map<String, Map<String, String>> supporttableData;

	public static String path = FranconnectUtil.config.get("inputDirectory") + "\\testData\\fsmodule.xls";
	String summary = "";

	BaseUtil b = new BaseUtil();

	public void insertTestData() throws Exception {
		String DB_URL = "jdbc:mysql://192.168.8.199/FCAUTOMATION";
		java.sql.Connection conn = null;

		Statement stmt = null;
		String sql;
		Class.forName("com.mysql.jdbc.Driver");
		conn = DriverManager.getConnection(DB_URL, "root", "root");
		// stmt = conn.createStatement();
		PreparedStatement stmtp = conn.prepareStatement("INSERT INTO SALES (TC_ID,TCKEY,TCVAL) VALUES(?,?,?)");

		FileInputStream fis = new FileInputStream(path);
		Workbook wb = new HSSFWorkbook(fis);
		Sheet sh = wb.getSheetAt(0);
		String TC_ID = "";
		Row row = null;

		Map<String, String> testData;

		for (int x = 0; x < sh.getLastRowNum(); x++) {
			row = sh.getRow(x);
			Cell cell = null;
			if (row != null) {
				cell = row.getCell(0);
				if (cell != null) {
					cell.setCellType(CellType.STRING);
					TC_ID = cell.getStringCellValue();
				}

				if (cell != null && (TC_ID.startsWith("TC_") || TC_ID.startsWith("FC_"))) {
					System.out.println(cell.getStringCellValue());
					if (TC_ID != null && TC_ID.length() > 0) {
						testData = readTestData(TC_ID);
						for (Entry<String, String> entry : testData.entrySet()) { // print
																					// keys
																					// and
																					// values
							String TCKEY = entry.getKey();
							String TCVAL = entry.getValue();
							System.out.println(TC_ID + " : " + TCKEY + " : " + TCVAL);

							/*
							 * sql =
							 * "INSERT INTO CRM (TC_ID,TCKEY,TCVAL) VALUES ('"
							 * +TC_ID+"','"+TCKEY+"','"+TCVAL+"')";
							 */

							stmtp.setString(1, TC_ID);
							stmtp.setString(2, TCKEY);
							stmtp.setString(3, TCVAL);

							stmtp.executeUpdate();

						}
					}
				}
			}
		}

		wb.close();
		fis.close();

		stmtp.close();
		conn.close();
	}

	@Test
	public void insertTranslationData() throws Exception {
		String DB_URL = "jdbc:mysql://192.168.8.199/FCAUTOMATION";
		java.sql.Connection conn = null;
		String Intpath = "C:\\int\\Training_Messages.xlsx";
		Class.forName("com.mysql.jdbc.Driver");
		conn = DriverManager.getConnection(DB_URL, "root", "root");
		PreparedStatement stmtp = conn
				.prepareStatement("INSERT INTO TRANS_TRAINING_MESSAGES (KEY_NAME,VALUE_NAME) VALUES(?,?)");

		FileInputStream fis = new FileInputStream(Intpath);
		OPCPackage pkg = OPCPackage.open(fis);
		XSSFWorkbook wb = new XSSFWorkbook(pkg);
		Sheet sh = wb.getSheetAt(0);
		String keyName = "";
		String valueName = "";
		Row row = null;

		for (int x = 0; x <= sh.getLastRowNum(); x++) {
			Cell cell1 = null;
			Cell cell2 = null;
			row = sh.getRow(x);

			try {
				cell1 = row.getCell(0);
				cell2 = row.getCell(1);
			} catch (Exception e) {
				System.out.println("EXCEPTION " + e + " at row no.::::" + x);
				continue;
			}
			if (row != null && (cell1 != null || cell2 != null)) {
				cell1.setCellType(CellType.STRING);
				keyName = cell1.getStringCellValue();
				cell2.setCellType(CellType.STRING);
				valueName = cell2.getStringCellValue();
				System.out.println(x + "  ::KEYNAME::::" + keyName + "    valuename::::" + valueName);
				if (keyName != null && !keyName.isEmpty()) {
					if (valueName != null && !valueName.isEmpty()) {
						stmtp.setString(1, keyName);
						stmtp.setString(2, valueName);
						stmtp.executeUpdate();
					}
				}
			}

		}

		wb.close();
		fis.close();

		stmtp.close();
		conn.close();
	}

	public Map<String, String> readTestData(String testCaseId) {
		Map<String, String> testData = new HashMap<String, String>();
		CodeUtility cu = new CodeUtility();
		boolean isTestCaseFound = false;
		try {
			FileInputStream fis = null;
			int tcRowId = 0;

			// iterating through each sheet

			fis = new FileInputStream(path);
			HSSFWorkbook wb = new HSSFWorkbook(fis);
			try {
				wb.setMissingCellPolicy(MissingCellPolicy.CREATE_NULL_AS_BLANK);
			} catch (Exception e) {
				Reporter.log(e.getMessage());
			}

			Sheet sh = wb.getSheetAt(0);
			int rowCount = sh.getLastRowNum();

			if (rowCount > 10000) {
				rowCount = sh.getPhysicalNumberOfRows();
			}
			// System.out.println("ROW COUNT>>>>>>>>>"+rowCount);

			if (rowCount > 0 && isTestCaseFound == false) {
				for (int rcnt = 0; rcnt < rowCount + 1; rcnt++) {

					if (isTestCaseFound == false) {
						Row row = null;
						try {
							row = sh.getRow(rcnt);
						} catch (Exception e) {
							Reporter.log(e.getMessage());
						}
						// System.out.println(rcnt);
						if (row != null) {
							boolean isRowEmpty = cu.isRowEmpty(row);
							if (isRowEmpty == false) {
								Cell cell = null;

								try {
									cell = row.getCell(0);
									cell.setCellType(CellType.STRING);
								} catch (Exception e) {
									Reporter.log(e.getMessage());
								}

								if (cell != null) {
									String cellVal = cell.getStringCellValue();
									if (cellVal != null && cellVal.trim().equalsIgnoreCase(testCaseId.trim())) {
										tcRowId = rcnt;
										testData = cu.getTestDataFromRow(wb, sh, tcRowId);
										isTestCaseFound = true;
									}
								}
							}
						}
					}
					// System.out.println("TEST CASE ID>>>>>"+testCaseId);
				}
			}
			wb.close();
			fis.close();

		} catch (Exception e) {
			Reporter.log("Exception in reading excel : " + e);
		}

		// System.out.println("Reached................");
		return testData;
	}

	public void getTableData(String tableName) throws Exception {
		Map<String, Map<String, String>> testData = new HashMap<String, Map<String, String>>();

		tableName = tableName.trim().toUpperCase();

		try {

			String DB_URL = "jdbc:mysql://192.168.8.199/FCAUTOMATION";
			java.sql.Connection conn = null;
			Statement stmt = null;
			String sql;
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL, "root", "root");
			stmt = conn.createStatement();

			String TC_ID = "";
			String TCKEY = "";
			String TCVAL = "";

			sql = "SELECT * FROM " + tableName;
			ResultSet rs = stmt.executeQuery(sql);
			int size = rs.getFetchSize();
			// ResultSet rsTemp = stmt.executeQuery(sql);
			// rsTemp.next();
			int cnt = 0;
			while (rs.next()) {
				Map<String, String> keyval = new HashMap<String, String>();
				TC_ID = rs.getString("TC_ID");
				TCKEY = rs.getString("TCKEY");
				TCVAL = rs.getString("TCVAL");
				keyval.put(TCKEY, TCVAL);
				// System.out.println(TC_ID);
				// rs.next();
				if (rs.isLast() == false) {
					while (rs.next()) {
						String nextTestCaseId = rs.getString("TC_ID");
						if (nextTestCaseId.equalsIgnoreCase(TC_ID)) {
							// TC_ID = rs.getString("TC_ID");
							// System.out.println("Compare : "+tempTestCaseId);
							TCKEY = rs.getString("TCKEY");
							TCVAL = rs.getString("TCVAL");
							keyval.put(TCKEY, TCVAL);

							testData.put(TC_ID, keyval);
						}

						else {
							rs.previous();
							break;
						}
					}
				} else {
					testData.put(TC_ID, keyval);
				}
			}
			supporttableData = testData;
			stmt.close();
			conn.close();
		} catch (Exception e) {
			throw new SkipException(e.getMessage());
		}
	}

	/*
	 * @Test public void testing1() throws Exception { Map<String,String>
	 * testData = getTestData("TC_01_Add_New_Category");
	 * printMapString(testData); }
	 */

	public Map<String, String> getTestData(String moduleName, String testCaseId) throws Exception {

		String tableName = "";

		if (moduleName.toLowerCase().contains("support")) {
			tableName = "SUPPORT";
		}

		getTableData(tableName);

		Map<String, String> testData = new HashMap<String, String>();

		for (Entry<String, Map<String, String>> entry : supporttableData.entrySet()) {
			if (entry.getKey().equalsIgnoreCase(testCaseId)) {
				return entry.getValue();
			}
		}
		return testData;
	}

	/*
	 * public void printMap() { for(Entry<String, Map<String, String>> entry :
	 * supporttableData.entrySet())//print keys and values { String key =
	 * entry.getKey();
	 * 
	 * Map<String,String> keyvaldisplay = entry.getValue();
	 * 
	 * for(Entry<String, String> entrydisplay : keyvaldisplay.entrySet()){
	 * //print keys and values String keydisplay = entrydisplay.getKey(); String
	 * valuedisplay = entrydisplay.getValue(); System.out.println(key +" -> "
	 * +keydisplay+" -> "+valuedisplay); }
	 * System.out.println("---------------------------------------------------")
	 * ; } }
	 */

	/*
	 * public void printMapString(Map<String,String> testData) {
	 * for(Entry<String, String> entry : testData.entrySet()) {
	 * System.out.println(entry.getKey()+" - > "+entry.getValue()); } }
	 */

	public static String translateDB(String searchKey) throws IOException {
		String value = null;

		try {
			String DB_URL = "jdbc:mysql://192.168.8.199/FCAUTOMATION";
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = null;
			conn = (Connection) DriverManager.getConnection(DB_URL, "root", "root");
			PreparedStatement stmtp = conn.prepareStatement("select VALUE_NAME from TRANSLATION where KEY_NAME=(?)");

			stmtp.setString(1, searchKey);
			ResultSet rs = stmtp.executeQuery();

			while (rs.next()) {
				value = rs.getString("VALUE_NAME");
			}
			stmtp.close();
			conn.close();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return value;

	}

}
