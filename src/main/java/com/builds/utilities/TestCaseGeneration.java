/*
 * package com.builds.utilities;
 * 
 * import java.io.File; import java.io.FileInputStream; import
 * java.io.FileOutputStream; import java.lang.reflect.Method; import
 * java.util.ArrayList; import java.util.List; import java.util.Set;
 * 
 * import org.apache.poi.ss.usermodel.Cell; import
 * org.apache.poi.ss.usermodel.CellStyle; import
 * org.apache.poi.ss.usermodel.FillPatternType; import
 * org.apache.poi.ss.usermodel.Font; import
 * org.apache.poi.ss.usermodel.IndexedColors; import
 * org.apache.poi.ss.usermodel.Row; import org.apache.poi.ss.usermodel.Sheet;
 * import org.apache.poi.ss.usermodel.Workbook; import
 * org.apache.poi.ss.usermodel.WorkbookFactory; import
 * org.apache.poi.xssf.usermodel.XSSFWorkbook; import
 * org.reflections.Reflections; import
 * org.reflections.scanners.MethodAnnotationsScanner; import
 * org.reflections.util.ClasspathHelper; import
 * org.reflections.util.ConfigurationBuilder; import
 * org.testng.annotations.Test;
 * 
 * public class TestCaseGeneration {
 * 
 * private void listTests(String fileName,List<String> listOfModule) throws
 * Exception{
 * 
 * 
 * 
 * 
 * Reflections reflections = new Reflections(new ConfigurationBuilder()
 * .setUrls(ClasspathHelper.forPackage("com.builds.test")) .setScanners(new
 * MethodAnnotationsScanner())); Set<Method> methods =
 * reflections.getMethodsAnnotatedWith(org.testng.annotations.Test.class);
 * 
 * System.out.println("Size of total method : "+methods.size());
 * 
 * for (Method meth : methods) {
 * if(meth.isAnnotationPresent(com.builds.utilities.TestCase.class)){ String
 * testCaseId =
 * meth.getAnnotation(com.builds.utilities.TestCase.class).testCaseId(); String
 * testCaseDescription =
 * meth.getAnnotation(com.builds.utilities.TestCase.class).testCaseDescription()
 * ; String[] reference =
 * meth.getAnnotation(com.builds.utilities.TestCase.class).reference(); String[]
 * modulegroup = meth.getAnnotation(org.testng.annotations.Test.class).groups();
 * 
 * System.out.println("\n\n");
 * 
 * System.out.println("TC ID "+testCaseId); System.out.println("TC Desc "+
 * testCaseDescription);
 * 
 * System.out.println("Ref"); for(String ref:reference){ System.out.print(ref+
 * " "); }
 * 
 * System.out.println("group"); for(String gr:modulegroup){ System.out.print(gr+
 * " "); }
 * 
 * printTestInExcel_Annotation(fileName,listOfModule,testCaseId,
 * testCaseDescription,reference,modulegroup); } } }
 * 
 * private void printTestInExcel_Annotation(String fileName,List<String>
 * listOfModule,String testCaseId,String testCaseDescription,String[]
 * reference,String[] modulegroup) throws Exception{ File file = new
 * File(fileName);
 * 
 * if(file.exists()==false){
 * 
 * FileOutputStream fileOut = new FileOutputStream(file.getAbsolutePath());
 * Workbook workbook = new XSSFWorkbook(); CellStyle style =
 * workbook.createCellStyle();
 * style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
 * style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
 * 
 * Font font = workbook.createFont();
 * 
 * font.setBold(true); font.setFontName("Courier New"); style.setFont(font);
 * 
 * Sheet worksheet = null; for(int x=0;x<listOfModule.size();x++){ worksheet =
 * workbook.createSheet(listOfModule.get(x)); Row rHeader =
 * worksheet.createRow(0); Cell cHeader = rHeader.createCell(0);
 * cHeader.setCellValue("Test_WorkFlow_Id"); cHeader.setCellStyle(style);
 * 
 * cHeader = rHeader.createCell(1);
 * cHeader.setCellValue("Test_Case_Description"); cHeader.setCellStyle(style);
 * 
 * cHeader = rHeader.createCell(2); cHeader.setCellValue("References (If Any)");
 * cHeader.setCellStyle(style);
 * 
 * worksheet.createFreezePane(0, 1);
 * 
 * Row rHeader_space = worksheet.createRow(1); }
 * 
 * workbook.write(fileOut); fileOut.flush(); fileOut.close(); workbook.close();
 * }else{ FileInputStream fis = new FileInputStream(file); Workbook wb =
 * WorkbookFactory.create(fis);
 * 
 * CellStyle style = wb.createCellStyle(); Font font = wb.createFont();
 * font.setFontName("Courier New"); style.setFont(font);
 * 
 * 
 * for(int x=0;x<wb.getNumberOfSheets();x++){ Sheet sh = wb.getSheetAt(x);
 * 
 * String sheetName = sh.getSheetName();
 * 
 * boolean isSheetPresent = false; for(int y=0;y<modulegroup.length;y++){
 * 
 * if(modulegroup[y].equalsIgnoreCase(sheetName)){ isSheetPresent = true;
 * sheetName= sh.getSheetName(); break; }
 * 
 * }
 * 
 * if(isSheetPresent==true){ Row row = sh.createRow(sh.getLastRowNum()+1);
 * 
 * Cell celltestCaseId = row.createCell(0);
 * 
 * if(testCaseId!=null && testCaseId.length()>0){
 * celltestCaseId.setCellValue(testCaseId); }else{ celltestCaseId.setCellValue(
 * "Test Case Id Missing!"); } celltestCaseId.setCellStyle(style);
 * 
 * 
 * Cell celltestCaseDescription = row.createCell(1);
 * celltestCaseDescription.setCellValue(testCaseDescription);
 * celltestCaseDescription.setCellStyle(style);
 * 
 * Cell cellreference = row.createCell(2); String ref=""; for(String refer :
 * reference){ ref = ref.concat(refer); } cellreference.setCellValue(ref);
 * cellreference.setCellStyle(style); sh.autoSizeColumn(0);
 * sh.autoSizeColumn(1); sh.autoSizeColumn(2); } }
 * 
 * 
 * 
 * 
 * 
 * FileOutputStream fout = new FileOutputStream(file); wb.write(fout);
 * fout.flush(); fout.close(); wb.close();
 * 
 * 
 * } } }
 * 
 */
