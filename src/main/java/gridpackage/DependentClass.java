package gridpackage;

import org.testng.annotations.Test;

public class DependentClass {

	@Test(groups = "RunFirst", priority = 1, dependsOnMethods = { "maintest" })
	public void testing1() {
		System.out.println("testing1");
	}

	@Test(priority = 2)
	public void maintest() {
		System.out.println("maintest");
	}

	@Test(dependsOnGroups = "RunFirst")
	public void testing2() {
		System.out.println("testing2");
	}

}
