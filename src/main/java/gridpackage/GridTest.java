package gridpackage;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

public class GridTest {

	@Test
	public void openBrowserOnNode() throws MalformedURLException {
		String url = "http://cluster.franconnect.net/fcautomation";
		String nodeUrl = "http://192.168.8.199:1990/wd/hub";
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setBrowserName("chrome");
		cap.setPlatform(Platform.WIN10);
		cap.setPlatform(Platform.WIN8);
		cap.setPlatform(Platform.WIN8_1);
		// System.setProperty("webdriver.chrome.driver",config.get("inputDirectory")+"\\exe\\chromedriver.exe");
		WebDriver driver = new RemoteWebDriver(new URL(nodeUrl), cap);
		driver.get(url);
		System.out.println(driver.getCurrentUrl());
		System.out.println(driver.getTitle());

		System.out.println("--------------------------------------------------------------------------------");

		System.out.println(((RemoteWebDriver) driver).getSessionId());

		System.out.println("--------------------------------------------------------------------------------");

		driver.quit();
	}
}
