package listeners;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import org.testng.Reporter;

public class Retry implements IRetryAnalyzer {
	private int count = 0;
	private int maxCount = 2;

	// Below method returns 'true' if the test method has to be retried else
	// 'false'
	// and it takes the 'Result' as parameter of the test method that just ran
	@Override
	public boolean retry(ITestResult result) {
		if (!result.isSuccess()) {
			if (count < maxCount) {
				count++;
				result.setStatus(ITestResult.SUCCESS);
				String message = Thread.currentThread().getName() + ": Error in " + result.getName() + " Retrying "
						+ (maxCount + 1 - count) + " more times";
				System.out.println(message);
				Reporter.log(message);
				return true;
			} else {
				result.setStatus(ITestResult.FAILURE);
			}
		}
		return false;
		/*
		 * if (retryCount < maxRetryCount) { System.out.println("Retrying test "
		 * + result.getName() + " with status " +
		 * getResultStatusName(result.getStatus()) + " for the " +
		 * (retryCount+1) + " time(s)."); retryCount++; return true; } return
		 * false;
		 */
	}

	public String getResultStatusName(int status) {
		String resultName = null;
		if (status == 1) {
			resultName = "SUCCESS";
			System.out.println("resultName = SUCCESS");
		}
		if (status == 2) {
			resultName = "FAILURE";
			System.out.println("resultName = FAILURE");
		}
		if (status == 3) {
			resultName = "SKIP";
			System.out.println("resultName = SKIP");
		}
		return resultName;
	}
}
