package listeners;

import java.util.Set;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener {

	@Override
	public void onFinish(ITestContext context) {
		Set<ITestResult> failedTests = context.getFailedTests().getAllResults();
		System.out.println("Count of Failed Tests : " + failedTests.size());

		Set<ITestResult> skippedTests = context.getSkippedTests().getAllResults();
		System.out.println("Count of Skipped Tests : " + skippedTests.size());

		Set<ITestResult> passedTests = context.getPassedTests().getAllResults();
		System.out.println("Count of Passed Tests : " + passedTests.size());

		for (ITestResult tempskip : skippedTests) {
			// System.out.println("tempskip : " + tempskip.getMethod());
			for (ITestResult tempfail : failedTests) {
				// System.out.println("tempfail : "+tempfail.getMethod());
				if (tempskip.getMethod().equals(tempfail.getMethod())) {
					// System.out.println("Match Found!");
					skippedTests.remove(tempskip);
				}
			}
		}

		for (ITestResult tempskip : skippedTests) {
			// System.out.println("tempskip : " + tempskip.getMethod());
			for (ITestResult temppass : passedTests) {
				// System.out.println("tempfail : "+temppass.getMethod());
				if (tempskip.getMethod().equals(temppass.getMethod())) {
					// System.out.println("Match Found!");
					skippedTests.remove(tempskip);
				}
			}
		}
	}

	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub

	}
}
