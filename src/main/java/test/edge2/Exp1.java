package test.edge2;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.builds.utilities.BaseUtil;

public class Exp1 {

	BaseUtil utobj=new BaseUtil();
	
	/*
	 * @Test public void tc() { WebDriver driver = null;
	 * TimeZone.setDefault(TimeZone.getTimeZone("GMT-5:00"));
	 * System.out.println(new SimpleDateFormat("MM/dd/yyyy hh:mm aa").format(new
	 * Date()));
	 * 
	 * String hrefLink =
	 * driver.findElement(By.xpath(".//link[@rel='alternate']")).getAttribute(
	 * "href"); if (!hrefLink.isEmpty()) { hrefLink = hrefLink.trim(); }
	 * 
	 * if (hrefLink.equalsIgnoreCase("testData")) {// <link rel="alternate" //
	 * href="android-app://com.railyatri.in.mobile/http/m.rytr.in/PNR">
	 * System.out.println("Pass"); } else { System.out.println("failed"); } }
	 */

	/*
	 * @Test private File getLatestFilefromDir(String dirPath) { File dir = new
	 * File(dirPath); File[] files = dir.listFiles(); if (files == null ||
	 * files.length == 0) { return null; }
	 * 
	 * File lastModifiedFile = files[0]; for (int i = 1; i < files.length; i++)
	 * { if (lastModifiedFile.lastModified() < files[i].lastModified()) {
	 * lastModifiedFile = files[i]; } } return lastModifiedFile; }
	 */

	/*@Test
	public void name() {
		WebDriver driver = new ChromeDriver();
		WebElement act = driver.findElement(By.xpath(".//input[@type='button' and @value='Actions']"));
		Actions builder = new Actions(driver);
		builder.moveToElement(act).build().perform();
		driver.findElement(By.xpath(".//div[@id='actionListButtons']//tr[2]//img")).click();
	}

	 Get the latest file from a specific directory 
	File getLatestFilefromDir( String dirPath ) {
		
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			return null;
		}
		
		File lastModifiedFile = files[0];
		for (int i = 1; i < files.length; i++) {
			if (lastModifiedFile.lastModified() < files[i].lastModified()) {
				lastModifiedFile = files[i];
			}
		}
		return lastModifiedFile;
	}*/
	
	
	/*@Test
	public boolean isFileFound(File downloadPath, String fileName) throws Exception {
		
		File downloadPath=new File("D:\\Selenium_Test_Output\\2018_06_26\\192.168.8.51\\15_17_08_687099\\thehubdev1\\logs");
		String fileName="taskFile.pdf";
		
		boolean isFileFound = false;
		for (int i = 0; i < 60; i++) {
			File file=getLatestFilefromDir(downloadPath.getAbsolutePath());
			if (file==null) {
				isFileFound=false;
			}else if(file.getName().equalsIgnoreCase(fileName)){
				isFileFound=true;
				break;
			}
			
			if (!isFileFound) {
				Thread.sleep(3000);
				file = getLatestFilefromDir(downloadPath.getAbsolutePath());
			}
		}
		return isFileFound;
	}*/
	
	/*@Test
	public void sub(){
		String text="javascript:ShowHide('div1509739527','ta1509739527')";
		text=text.substring(text.indexOf("'")+1, text.indexOf(",")-1);
		System.out.println(text);
	}*/
	
	@Test
	public void readMailBox(/*String expectedSubject, String expectedMessageBody, String userName,
			String password*/) throws Exception {
		
		String expectedSubject="TestSubjecty24165054";
		String expectedMessageBody="TestMessagesy24165040";
		String userName="hubautomation@staffex.com";
		String password="sdg@1a@Hfs";
		
		Map<String, String> mailInfo = new HashMap<String, String>();

		for (int it = 0; it < 20; it++) {
			try {
				String hostName = "mail2.staffex.com";
				String provider = "pop3";
				Properties props = new Properties();

				// Create Session
				Session session = Session.getDefaultInstance(props, null);
				Store store = session.getStore(provider);
				store.connect(hostName, userName, password);
				// Open Folder

				Folder inbox = store.getFolder("INBOX");
				inbox.open(Folder.READ_ONLY);

				Message[] messages = inbox.getMessages();

				List<Message> m = new ArrayList<Message>();
				for (int x = 0; x < messages.length; x++) {
					m.add(messages[x]);
				}
				Collections.reverse(m);

				for (int x = 0; /*x < 1000 && */x < m.size(); x++) {
					
					System.out.println("Value of x : "+x +" "+getSubject(m.get(x)));
					
					if (getSubject(m.get(x)).equalsIgnoreCase(expectedSubject)) {
						System.out.println("Value of x : "+x);
						break;
					}
					
					
					
					/*if (m != null && !m.isEmpty()) {
						String from = getFrom(m.get(x));
						String to = getTo(m.get(x));
						String subject = getSubject(m.get(x));
						String receivedDate = getReceivedDate(m.get(x));
						String mailBody = null;
						if (expectedSubject != null && subject != null
								&& expectedSubject.trim().equalsIgnoreCase(subject.trim())) {

							mailBody = getTextFromMessage(m.get(x));
							if (mailBody.contains(expectedMessageBody)) {
								mailInfo.put("from", from);
								mailInfo.put("to", to);
								mailInfo.put("subject", subject);
								mailInfo.put("mailBody", mailBody);
								mailInfo.put("receivedDate", receivedDate);
							}
						}
					}*/
				}
				inbox.close(false);
				store.close();
			} catch (Exception e) {
				System.out.println(e.getMessage().toString());
			}

		}
	}
	
	private String getTextFromMessage(Message message) throws Exception {
		String result = "";
		if (message.isMimeType("text/plain")) {
			result = message.getContent().toString();
		} else if (message.isMimeType("multipart/*")) {
			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		}
		return result;
	}

	private String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws Exception {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}

	private static String getFrom(Message javaMailMessage) throws MessagingException {
		String from = "";
		Address a[] = javaMailMessage.getFrom();
		if (a == null)
			return null;
		for (int i = 0; i < a.length; i++) {
			Address address = a[i];
			from = from + address.toString();
		}
		return from;
	}

	private static String getTo(Message javaMailMessage) throws MessagingException {
		String to = "";
		Address a[] = javaMailMessage.getAllRecipients();
		if (a == null)
			return null;
		for (int i = 0; i < a.length; i++) {
			Address address = a[i];
			to = to + address.toString();
		}
		return to;
	}

	private static String getSubject(Message javaMailMessage) throws MessagingException {
		String subject = "";

		try {
			subject = javaMailMessage.getSubject();
		} catch (Exception e) {
			// Reporter.log(e.getMessage());
		}

		return subject;
	}

	private String getReceivedDate(Message javaMailMessage) throws MessagingException {
		Date date = null;
		String dateStr = null;

		try {
			date = javaMailMessage.getReceivedDate();
			dateStr = date.toString();
		} catch (Exception e) {
			// Reporter.log(e.getMessage());
		}

		return dateStr;
	}
}
