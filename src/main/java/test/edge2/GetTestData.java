package test.edge2;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.Reporter;

public class GetTestData {

	public ArrayList<String> getTestDataFromRow(String sectionName) throws IOException {

		// String sectionName="Physical Environment";
		String locationReportPath = "D:/Visit/CenterVisitForm.xls";

		File reportFile = new File(locationReportPath);
		FileInputStream f = new FileInputStream(reportFile.getAbsolutePath());
		Workbook wb = new HSSFWorkbook(f);
		Sheet sh = wb.getSheetAt(0);
		int lastRowNum = sh.getLastRowNum();

		ArrayList<String> lstForQuestion = new ArrayList<String>();

		try {

			if (sectionName.equalsIgnoreCase("Physical Environment")) {

				for (int j = 0; j <= lastRowNum; j++) {
					Row row = null;
					Cell cell = null;

					row = sh.getRow(0);
					cell = row.getCell(0);
					String text = cell.getStringCellValue();

					if (text.equalsIgnoreCase(sectionName)) {
						if (j == 0) {
							j++;
						}
						row = sh.getRow(j);
						cell = row.getCell(0);
						if (cell != null) {
							String question = cell.getStringCellValue();
							lstForQuestion.add(question.trim());
						}

					}
				}
			} else if (sectionName.equalsIgnoreCase("Curriculum")) {

				for (int j = 0; j <= lastRowNum; j++) {

					Row row = null;
					Cell cell = null;

					row = sh.getRow(0);
					cell = row.getCell(1);
					String text = cell.getStringCellValue();

					if (text.equalsIgnoreCase(sectionName)) {
						if (j == 0) {
							j++;
						}
						row = sh.getRow(j);
						cell = row.getCell(1);
						if (cell != null) {
							String question = cell.getStringCellValue();
							lstForQuestion.add(question.trim());
						}
					}
				}

			} else if (sectionName.equalsIgnoreCase("Health & Safety")) {

				for (int j = 0; j <= lastRowNum; j++) {

					Row row = null;
					Cell cell = null;

					row = sh.getRow(0);
					cell = row.getCell(2);
					String text = cell.getStringCellValue();

					if (text.equalsIgnoreCase(sectionName)) {
						if (j == 0) {
							j++;
						}
						row = sh.getRow(j);
						cell = row.getCell(2);
						if (cell != null) {
							String question = cell.getStringCellValue();
							lstForQuestion.add(question.trim());
						}
					}
				}

			} else if (sectionName.equalsIgnoreCase("Interactions")) {

				for (int j = 0; j <= lastRowNum; j++) {

					Row row = null;
					Cell cell = null;

					row = sh.getRow(0);
					cell = row.getCell(3);
					String text = cell.getStringCellValue();

					if (text.equalsIgnoreCase(sectionName)) {
						if (j == 0) {
							j++;
						}
						row = sh.getRow(j);
						cell = row.getCell(3);

						if (cell != null) {
							String question = cell.getStringCellValue();
							lstForQuestion.add(question.trim());
						}
					}
				}
			}
		} catch (Exception e) {
			Reporter.log(e.getMessage());
		}

		/*
		 * for (int i = 0; i <lstForQuestion.size(); i++) {
		 * 
		 * System.out.println("lstForQuestion : "+lstForQuestion.get(i)); }
		 */
		wb.close();
		return lstForQuestion;
	}
}
