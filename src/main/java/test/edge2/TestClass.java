package test.edge2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestClass extends Thread {

	
	
	public void testThread() {
		Callable<Void> callable1 = new Callable<Void>() {

			@Override
			public Void call() throws Exception {
				t1();
				return null;
			}
		};

		Callable<Void> callable2 = new Callable<Void>() {

			@Override
			public Void call() throws Exception {
				t2();
				return null;
			}
		};

		Callable<Void> callable3 = new Callable<Void>() {

			@Override
			public Void call() throws Exception {
				t3();
				return null;
			}
		};

		Callable<Void> callable4 = new Callable<Void>() {

			@Override
			public Void call() throws Exception {
				t4();
				return null;
			}
		};

		Callable<Void> callable5 = new Callable<Void>() {

			@Override
			public Void call() throws Exception {
				t4();
				return null;
			}
		};

		List<Callable<Void>> taskList = new ArrayList<Callable<Void>>();
		taskList.add(callable1);
		taskList.add(callable2);
		taskList.add(callable3);
		taskList.add(callable4);
		taskList.add(callable5);

		// create a pool executor with 3 threads
		ExecutorService executor = Executors.newFixedThreadPool(5);

		try {
			// start the threads and wait for them to finish
			executor.invokeAll(taskList);
			executor.shutdown();
		} catch (InterruptedException ie) {
			// do something if you care about interruption;
		}
	}

	public void t1() throws Exception {
		opendriver().get("https://www.google.com");
	}

	public void t2() throws Exception {
		opendriver().get("https://www.facebook.com");
	}

	public void t3() throws Exception {
		opendriver().get("https://www.yahoo.com");
	}

	public void t4() throws Exception {
		opendriver().get("https://www.wynk.com");
	}

	public void t5() throws Exception{
		opendriver().get("https://www.msn.com");
	}
	
	WebDriver opendriver(){
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium_Test_Input\\exe\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		return driver;
	}
	
	public static void main(String args[]){
		TestClass tc=new TestClass();
		tc.testThread();
	}

}
