package test.edge2;

import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.common.LoginPage;
import com.builds.utilities.FranconnectUtil;

public class TestClassWait {

	FranconnectUtil fc = new FranconnectUtil();

	@Test
	public void tc() throws Exception {

		try {
			System.setProperty("webdriver.chrome.driver", "C:\\Selenium_Test_Input\\exe\\chromedriver.exe");

			URL url = new URL("http://" + "192.168.8.199" + ":" + "4444" + "/wd/hub");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			RemoteWebDriver rmDriver = new RemoteWebDriver(url, capabilities);
			rmDriver.setFileDetector(new LocalFileDetector());
			WebDriver driver = rmDriver;

			/* WebDriver driver = new ChromeDriver(); */
			LoginPage pobj = new LoginPage(driver);

			driver.get("https://qaautomation.franconnectqa.net/fc/");
			fc.utobj().sendKeys(driver, pobj.userid, "adm");
			fc.utobj().sendKeys(driver, pobj.password, "t0n1ght");
			fc.utobj().clickElement(driver, pobj.login);
			fc.adminpage().adminPage(driver);

			driver.findElement(By.linkText("Configure Custom SSO")).click();

			System.out.println("SuccessFully Click");

			driver.quit();

		} catch (Exception e) {
			fc.utobj().throwsException("Error Page is coming");
		}
	}

}
