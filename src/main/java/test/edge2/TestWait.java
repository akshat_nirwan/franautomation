package test.edge2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class TestWait {

	public WebDriver waitTest(WebDriver driver) throws InterruptedException {
		/*
		 * WebDriverWait wait = new WebDriverWait(driver, 60); driver =
		 * wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(
		 * "cboxIframe"));
		 */
		Thread.sleep(5000);
		driver = driver.switchTo().frame("cboxIframe");

		return driver;

	}

	public WebElement waitTest22(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.id("subject")));
		return element;

	}

	public void getCurrent() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date)); // 2016/11/16 12:08:43
	}

	@Test
	public void login() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium_Test_Input\\exe\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://qaautomation.franconnectqa.net/fc/");
		getCurrent();
		WebDriverWait wait = new WebDriverWait(driver, 60);
		WebElement element1 = wait.until(ExpectedConditions.elementToBeClickable(By.id("user_id1")));
		element1.sendKeys("adm");

		driver.findElement(By.id("password")).sendKeys("t0n1ght");
		driver.findElement(By.id("ulogin")).click();

		driver.findElement(By.xpath(".//a[@qat_submodule='Lead Management']")).click();
		driver.findElement(By.xpath(".//*[@id='containsImage']/tbody/tr/td/span/a")).click();
		driver.findElement(By.partialLinkText("Log a Task")).click();

		System.out.println("Driver : Before Switch " + driver);
		driver = waitTest(driver);
		System.out.println("Driver : After Switch " + driver);

		waitTest22(driver).sendKeys("ABC");
		getCurrent();
		driver.quit();
	}
}
