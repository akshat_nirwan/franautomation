package testpackageforwait;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;

public class TestClass {
FranconnectUtil fc = new FranconnectUtil();
	
Wait<WebDriver> wait;
private void setWaitTime(WebDriver driver)
{
	wait = new FluentWait<WebDriver>(driver)
			.withTimeout(60,TimeUnit.SECONDS)
			.pollingEvery(1,TimeUnit.SECONDS)
			.ignoring(Exception.class);
}

	@Test
	public void openBrowser() throws InterruptedException
	{
		
		WebDriver driver = null;
		try
		{
			System.setProperty("webdriver.chrome.driver", "C:\\Selenium_Test_Input\\exe\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.get("http://www.gmail.com");
			System.out.println(fc.codeUtil().getCurrentDateTimeStamp());
			getElement(driver, "name", "identifier1");
			System.out.println(fc.codeUtil().getCurrentDateTimeStamp());
			getElement(driver, driver.findElement(By.name("identifier")));
			System.out.println(fc.codeUtil().getCurrentDateTimeStamp());
		}finally{
			System.out.println(fc.codeUtil().getCurrentDateTimeStamp());
			if(driver!=null)
			{
				Thread.sleep(10000);
				driver.quit();
			}
		}
	}
	
	
	public void getElement(WebDriver driver,String locator,String value)
	{
		setWaitTime(driver);
		if(locator.equalsIgnoreCase("name"))
		{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(value))).sendKeys("adm");
		}
	}
	public void getElement(WebDriver driver,WebElement element)
	{
		TCP t = new TCP(driver);
		System.out.println(t.name.isDisplayed());
	}
	
}
