package testpackageforwait;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import com.builds.utilities.WriteToFileExample1;

public class UpdateJavaFiles {

	String text="";
	
	WriteToFileExample1 w = new WriteToFileExample1();
	
	@Test
	public void readProgramLines()
			throws FileNotFoundException, EncryptedDocumentException, InvalidFormatException, InterruptedException {

		File file = new File(".");

		String folderPath = file.getAbsolutePath();
		if (folderPath.endsWith(".")) {
			folderPath = folderPath.substring(0, folderPath.length() - 1);
		}

		File tempFile = new File(folderPath);

		File[] fileList = tempFile.listFiles();

		for (int x = 0; x < fileList.length; x++) {
			displayAll(fileList[x]);
		}
	}

	public void displayAll(File path) {
		if (path.isFile() && path.getAbsolutePath().endsWith(".java") && path.getAbsolutePath().contains("test") && path.getAbsolutePath().contains("testpackageforwait"))
		 {
			readLine(path);
		} else if (path.isDirectory()) {
			File files[] = path.listFiles();
			for (File dirOrFile : files) {
				displayAll(dirOrFile);
			}
		}
	}

	public void readLine(File file) {
		try {
			FileReader fr = new FileReader(file);
			LineNumberReader lnr = new LineNumberReader(fr);

			try {
				for (String line; (line = lnr.readLine()) != null;) {
					if (line.toLowerCase().contains(".isselected()") && !line.toLowerCase().contains("utobj()"))
					{
						System.out.println(file.getAbsolutePath() + " > " + line);
						text = text + file.getAbsolutePath() + " \n " + line+"\n";
					}
				}
				fr.close();
				lnr.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	@AfterTest
	public void write()
	{
		//w.writeInFile(text);
	}
}
