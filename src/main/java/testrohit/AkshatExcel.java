package testrohit;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class AkshatExcel {

	public static void main(String[] arg) {

		// Blank Workbook
		XSSFWorkbook workbook = new XSSFWorkbook();

		// Create a blank sheet
		XSSFSheet sheet = workbook.createSheet("myTestSheet");

		// Create new Row in sheet
		Row row ;
		Cell cell ;
		
		for (int i = 0; i < 10; i++) {
			
			row = sheet.createRow(i);
			
			for (int j = 0; j < 5; j++) {
				
				cell = row.createCell(j);
				cell.setCellValue("abc" + j);
			}	
		}
		try {

			// Write to workbook
			FileOutputStream out = new FileOutputStream(new File("D:\\myTestFile.xlsx"));
			workbook.write(out);
			
			System.out.println("File written successfully");
		
		for (int i = 0; i < sheet.getLastRowNum(); i++) 
		{
			row = sheet.getRow(i);
			
			for (int j = 0; j < row.getLastCellNum(); j++) {
				System.out.println(row.getCell(j));
			}
			
			out.close();
		
		}

		}
		catch (Exception e) {
			e.printStackTrace();

		}
	}
}

