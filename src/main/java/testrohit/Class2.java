package testrohit;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.builds.utilities.BaseUtil;

final class Class2 {

	public static void main(String[] args) throws Exception {


		WebDriver driver = null;
		BaseUtil bs = new BaseUtil();
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium_Test_Input\\exe\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		
		options.addArguments("--start-maximized");
		driver=new ChromeDriver(options);
		driver.manage().timeouts().setScriptTimeout(45, TimeUnit.SECONDS);
		
		
		driver.get("https://qaautomation.franconnectqa.net/fc/");
		driver.findElement(By.id("user_id")).sendKeys("adm");
		driver.findElement(By.id("password")).sendKeys("t0n1ght");
		driver.findElement(By.id("ulogin")).click();
		
		WebDriverWait wait = new WebDriverWait(driver, 30, 1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("searchString"))).sendKeys("Abcd Dcbe");
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("searchString")));
		
		bs.clickEnterOnElement(driver, element);
		
		wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Abcd Dcbe"))).click();
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("leadLogCallSummary")));
		
		WebElement remarks = wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Remarks")));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", remarks);
		remarks.click();
		
		driver.switchTo().defaultContent();
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("cboxIframe"));
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@type='button' and @value='Close']"))).click();

	}

}
