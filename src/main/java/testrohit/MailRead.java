package testrohit;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;

import org.testng.annotations.Test;

public class MailRead {

	@Test
	public void readMail() {

		Map<String,String> mailInfo = new HashMap<String,String>();
		
		String expectedSubject = "New Contact Added";
		String expectedMessageBody = "cuh30115235 cur30115206";
		
		try {
			String hostName = "mail.franiq.com";
			String provider = "pop3";

			Properties props = new Properties();

			// Create Session
			Session session = Session.getDefaultInstance(props, null);
			Store store = session.getStore(provider);
			store.connect(hostName, "crmautomation@staffex.com", "sdg@1a@Hfs");

			// Open Folder
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);

			Message[] messages = inbox.getMessages();
			
			
			System.out.println(messages.length);

			List<Message> m = new ArrayList<Message>();
			
			
			for (int x = 0; x < messages.length; x++) {
				m.add(messages[x]);
			}

			System.out.println(m.size());
			// Collections.reverse(m);

			for (int x = 0; x < m.size(); x++) {
				if (m != null && !m.isEmpty()) {
					String from = getFrom(m.get(x));
					String to = getTo(m.get(x));
					String subject = getSubject(m.get(x));
					String receivedDate = getReceivedDate(m.get(x));
					
					//System.out.println(m.get(x).getSentDate());
										
					String mailBody = null;
					if (expectedSubject != null && subject != null
							&& expectedSubject.trim().equalsIgnoreCase(subject.trim())) {

						mailBody = getTextFromMessage(m.get(x));
						if (mailBody.contains(expectedMessageBody)) {
							mailInfo.put("from", from);
							mailInfo.put("to", to);
							mailInfo.put("subject", subject);
							mailInfo.put("mailBody", mailBody);
							mailInfo.put("receivedDate", receivedDate);
							System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>Mail Found");
						}
					}
				}
			}
			inbox.close(false);
			store.close();
		} catch (Exception e) {

		}
	}

	private static String getFrom(Message javaMailMessage) throws MessagingException {
		String from = "";
		Address a[] = javaMailMessage.getFrom();
		if (a == null)
			return null;
		for (int i = 0; i < a.length; i++) {
			Address address = a[i];
			from = from + address.toString();
		}
		return from;
	}

	private static String getTo(Message javaMailMessage) throws MessagingException {
		String to = "";
		Address a[] = javaMailMessage.getAllRecipients();
		if (a == null)
			return null;
		for (int i = 0; i < a.length; i++) {
			Address address = a[i];
			to = to + address.toString();
		}
		return to;
	}

	private static String getSubject(Message javaMailMessage) throws MessagingException {
		String subject = "";

		try {
			subject = javaMailMessage.getSubject();
		} catch (Exception e) {
			// Reporter.log(e.getMessage());
		}

		return subject;
	}

	private String getReceivedDate(Message javaMailMessage) throws MessagingException {
		Date date = null;
		String dateStr = null;

		try {
			date = javaMailMessage.getReceivedDate();
			dateStr = date.toString();
		} catch (Exception e) {
			// Reporter.log(e.getMessage());
		}

		return dateStr;
	}
	private String getTextFromMessage(Message message) throws Exception {
		String result = "";
		if (message.isMimeType("text/plain")) {
			result = message.getContent().toString();
		} else if (message.isMimeType("multipart/*")) {
			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		}
		return result;
	}
	
	private String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws Exception {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}
}
