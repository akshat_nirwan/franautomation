package testrohit;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;

import com.google.common.base.Function;

public class WaitForFrame {

	public void cboxIframe(WebDriver driver,String frameId) {

		FluentWait<WebDriver> fwait = new FluentWait<WebDriver>(driver).pollingEvery(Duration.ofSeconds(1))
				.withTimeout(Duration.ofMinutes(1)).ignoring(Exception.class);

		WebElement foo = fwait.until(new Function<WebDriver, WebElement>() {
			
			@Override
			public WebElement apply(WebDriver driver) {
				WebElement frame = driver.findElement(By.id(frameId));
				System.out.println("------------------"+frame.getAttribute("id"));
				driver.switchTo().frame(frame);
				System.out.println("Switched Frame ----------------------------- ");
				
				WebElement frame2 = driver.findElement(By.id(frameId));
				System.out.println("jkkkkkkkkkkkkkkkk"+frame2.getAttribute("id"));
				
				return frame;
			}
		});
	}
}
