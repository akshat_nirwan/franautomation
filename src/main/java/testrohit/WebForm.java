package testrohit;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class WebForm {
	WebDriver driver;
	
	@Test
	public void webform1() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver","C:\\Selenium_Test_Input\\exe\\chromedriver.exe");
		driver  = new ChromeDriver();
		
		driver.get("https://qaautomation.franconnectqa.net/fc/");
		driver.findElement(By.id("user_id")).sendKeys("adm");
		driver.findElement(By.id("password")).sendKeys("t0n1ght");
		driver.findElement(By.id("ulogin")).click();
		
		driver.navigate().to("https://qaautomation.franconnectqa.net/fc/control/createExternalForm?formId=391275239&stepCount=2&formXMLName=testframe_1300220218&emailKeyword=&eventType=modify");
		//Thread.sleep(70000);
		
		driver.findElement(By.xpath(".//li[@id='fsLeadDetails_fld']")).click();
		
		List<WebElement> listOfFields = driver.findElements(By.xpath(".//*[@id='fsLeadDetails_fld']//li[@data-fieldname]//a"));
		
		for(int x=0;x<listOfFields.size();x++)
		{
			try
			{
				System.out.println(listOfFields.get(x).getText());
				moveFieldsToForm(listOfFields.get(x).getText());
				listOfFields = driver.findElements(By.xpath(".//*[@id='fsLeadDetails_fld']//li[@data-fieldname]//a"));
				Thread.sleep(2000);
				listOfFields.remove(listOfFields.get(x));
			}catch(Exception e)
			{
				System.out.println(e.getMessage());
			}
			
		}
		
		Thread.sleep(5000);
	}
	
	public void moveFieldsToForm(String keywrd)
	{
		System.out.println(keywrd);
		driver.findElement(By.xpath(".//input[@data-searchid='fsLeadDetails']")).clear();
		driver.findElement(By.xpath(".//input[@data-searchid='fsLeadDetails']")).sendKeys(keywrd);
		
		List<WebElement> listOfElement = driver.findElements(By.xpath(".//li[@id='fsLeadDetails_fld']//a"));
		
		for(int x=0;x<listOfElement.size();x++)
		{
			if(listOfElement.get(x).getText().equalsIgnoreCase(keywrd)){
				WebElement dest = driver.findElement(By.xpath(".//ul[contains(@id,'_defaultSection') and contains(@id,'Flds')]"));
				Actions act = new Actions(driver);
				act.dragAndDrop(listOfElement.get(x), dest).build().perform();
				//driver.findElement(By.xpath(".//input[@data-searchid='fsLeadDetails']")).clear();
				WebElement toClear = driver.findElement(By.xpath(".//input[@data-searchid='fsLeadDetails']"));
				toClear.sendKeys(Keys.CONTROL + "a");
				toClear.sendKeys(Keys.DELETE);
				
				
				break;
			}
		}
	}
	
	
	@AfterTest
	public void quit()
	{
		driver.quit();
	}
}
