package testrohit;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.utilities.BaseUtil;
import com.builds.utilities.FranconnectUtil;

public class WebServices {
	
	@Test
	public static void main() throws Exception
	{
		
		for(int x=1;x<=1000;x++)
		{
		
			System.out.println("Record No. "+x);
			Reporter.log("Record No. "+x);
			
			
			BaseUtil b = new BaseUtil();
			
			String started = b.getCurrentDateAndTime();
			long s = System.currentTimeMillis();
						
			WebDriver driver = null;
			AdminUsersManageCorporateUsersAddCorporateUserPageTest cu = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			FranconnectUtil fc = new FranconnectUtil();
			FranconnectUtil.config.put("buildUrl","http://192.168.9.170:8080/fcsky/");
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail("test@fran.com");	
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			cu.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			System.out.println(corpUser.getFirstName());
			System.out.println(corpUser.getLastName());
			Reporter.log(corpUser.getFirstName());
			Reporter.log(corpUser.getLastName());
			
			long e = System.currentTimeMillis();
			
			long d = e-s;
			
			double duration = d/1000;
			
			System.out.println("Started : "+started + "Ended : "+b.getCurrentDateAndTime() +"Total Duration : "+ duration);
			Reporter.log("Started : "+started + "Ended : "+b.getCurrentDateAndTime() +"Total Duration : "+ duration);
		}
	}
}
