<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta content = "text/html; charset = ISO-8859-1" http-equiv = "content-type">
		
      <script type = "application/javascript">
         function loadJSON(){
             var msg_value = document.getElementById('msg_body').value;
             var groupID = document.getElementById('group_id').value;
             if (msg_value == "") {
                 alert("Message body cannot be empty!");
                 return false;
             }
             if (groupID == "") {
                 var conf = confirm("Group ID is empty, message will be posted to All Company");
                 if (conf == false) {
                     return false;
                 }
             }
             yam.platform.request({
                 url: "https://api.yammer.com/api/v1/messages.json",
                 method: "POST",
                 data: {
                     "body": msg_value,
                     //"title" : msg_title,
                     
                 },
                 success: function(msg) {
                     alert("Post was Successful!");
                 },
                 error: function(msg) {
                     alert("Post was Unsuccessful");
                 }
             })
         }
		
      </script>
	
      <title>tutorialspoint.com JSON</title>
   </head>
	
   <body>
     <h1>Hello</h1>
		
   </body>
		
</html>