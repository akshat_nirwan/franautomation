<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="CssFile.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<script type="text/javascript">
    function ShowHideIpField() {
        var ipaddress = document.getElementById("ipaddress");
        ipdiv.style.display = ipaddress.value == "No" ? "block" : "none";
    }
    
    function ShowHideUserList() {
        var usertype = document.getElementById("category");
        
        if(usertype.value=="1"){
        	usertypelist.style.display = usertype.value == "2" ? "block" : "none";
        	usertypelistlabel.style.display = usertype.value == "2" ? "block" : "none";
        	functionality.style.display = usertype.value == "3" ? "block" : "none";
        	functionalitylabel.style.display = usertype.value == "3" ? "block" : "none";
        }
        else if(usertype.value=="2"){
        	usertypelist.style.display = usertype.value == "2" ? "block" : "none";
        	usertypelistlabel.style.display = usertype.value == "2" ? "block" : "none";
        	functionality.style.display = usertype.value == "3" ? "block" : "none";
        	functionalitylabel.style.display = usertype.value == "3" ? "block" : "none";
        }
        else if(usertype.value=="3"){
        	functionality.style.display = usertype.value == "3" ? "block" : "none";
        	functionalitylabel.style.display = usertype.value == "3" ? "block" : "none";
        	usertypelist.style.display = usertype.value == "2" ? "block" : "none";
        	usertypelistlabel.style.display = usertype.value == "2" ? "block" : "none";
        }
        else if(usertype.value=="4"){
        	functionality.style.display = usertype.value == "3" ? "block" : "none";
        	functionalitylabel.style.display = usertype.value == "3" ? "block" : "none";
        	usertypelist.style.display = usertype.value == "2" ? "block" : "none";
        	usertypelistlabel.style.display = usertype.value == "2" ? "block" : "none";
        }
    }
    
    function ShowHideCustomReportField() {
        var customField = document.getElementById("functions");
        t1.style.display= customField.value =="4" ? "block" : "none";
        t2.style.display= customField.value =="4" ? "block" : "none";
    }
    
    
</script>
	<form name="ScriptExecuter" action="ProductAutomation" method="post">
		<table align="center" border="1" >
			<tr>
				<td><label>*Build Url </label></td>
				<td><input type="text" name="buildUrl"></input></td>
				<td><label>*User Name </label></td>
				<td><input type="text" name="userName"></input></td>
				<td><label>*Password </label></td>
				<td><input type="text" name="password"></input></td>
			</tr>
			<tr>
				<td><label>*Browser </label></td>
				<td colspan="5">
					<select id="browserName" name="browserName">
  						<option value="chrome">Chrome</option>
  						<option value="firefox">Firefox</option>
  						<option value="internetExplorer">Internet Explorer</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<label>*Run on Server ? </label>
				</td>
				<td>
					<select id = "ipaddress" name="ipaddress">
        				<option value="Yes">Yes</option>
        				<option value="No">No</option>            
    				</select>
				</td>
				<td colspan="4" id="ipdiv" style="display: none">
    					<label>IP Address:</label>
    					<input type="text" id="ipaddresstxt" name="ipaddresstxt" />
				</td>
			</tr>
			<tr>
				<td>
					<label>*Email </label>
				</td>
				<td colspan="5">
					<input type="text" name="emailId"></input>
				</td>
			</tr>
			
			<tr>
				<td>
					<label>*Module Selection </label>
				</td>
				<td>
					<select id="category" name="category" onchange = "ShowHideUserList()" >
  						<option value="1">All</option>
  						<option value="2">User Creation</option>
  						<option value="3">Saas Upgrade</option>
  						<option value="4">Product Upgrade</option>
					</select>
				</td>
				<td id="usertypelistlabel" style="display: none"><label>User Type: </label> </td>
				<td id="usertypelist" style="display: none">
    					<select id="usertype" name="usertype">
  						<option value="1">All</option>
  						<option value="2">Corporate User</option>
  						<option value="3">Regional User</option>
  						<option value="4">Franchise User</option>
					</select>
				</td>
				<td id="functionalitylabel" style="display: none"><label>Methods: </label> </td>
				<td id="functionality" style="display: none">
    					<select id="functions" onchange = "ShowHideCustomReportField()">
  						<option value="1">All</option>
  						<option value="2">Admin</option>
  						<option value="3">User Creation</option>
  						<option value="4">Custom Report</option>
					</select>
				</td>
			</tr>
	
			<tr id="t1" style="display: none">
				<td><label>*Build Url 1 </label></td>
				<td><input type="text" name="buildUrl1"></input></td>
				<td><label>*User Name </label></td>
				<td><input type="text" name="userName1"></input></td>
				<td><label>*Password</label></td>
				<td><input type="text" name="password1"></input></td>
			</tr>
			
			<tr id="t2" style="display: none">
				<td><label>*Build Url 2 </label></td>
				<td><input type="text" name="buildUrl2"></input></td>
				<td><label>*User Name </label></td>
				<td><input type="text" name="userName2"></input></td>
				<td><label>*Password</label></td>
				<td><input type="text" name="password2"></input></td>
			</tr>
	
			<tr>
				<td>
					<button type="submit">Submit</button>		
				</td>
			</tr>
		
		</table>
	</form>
</body>

</html>