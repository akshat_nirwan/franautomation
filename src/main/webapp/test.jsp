<!DOCTYPE html>

	<%@page import="java.net.InetAddress"%>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link REL = StyleSheet HREF = "CssFile.css" TYPE="text/css" MEDIA=screen>
	<link REL = StyleSheet HREF = "bootstrap.min.css" TYPE="text/css" MEDIA=screen>
	
	<%
		//InetAddress inetAddress = InetAddress.getLocalHost();
   		//String ipAddress = inetAddress.getHostAddress();

   		String sereverIpAdd = request.getLocalAddr();
   		
   		out.print(sereverIpAdd+"<br>");
   		String ipAddress = null;
   		
   		if (request.getHeader("X-Forwarded-For") != null) {
   		    out.print("Proxy");
   		 	ipAddress = request.getHeader("X-FORWARDED-FOR");
   		 	
   		}else{
   			ipAddress = request.getRemoteAddr();
   		}
   		
	%>
	
	<script type="text/javascript">
		function hidebuildrow(){
			 var subCategoryVar = document.getElementById("subcategory");
			 if(subCategoryVar.value=="Custom Report"){
				document.getElementById('build2row1').style.display='block';
				document.getElementById('build2row2').style.display='block';
				document.getElementById('buildUrl2').value="";
				document.getElementById('userName2').value="";
				document.getElementById('password2').value="";
				
			 }else{
				 document.getElementById('build2row1').style.display='none';
				 document.getElementById('build2row2').style.display='none';
				 document.getElementById('buildUrl2').value="na";
				 document.getElementById('userName2').value="na";
				 document.getElementById('password2').value="na";
			 }
			 
			 
		}
	
	
		function dropdownlist(listindex){			
			document.formname.subcategory.options.length = 0;
			switch (listindex){
			
				case "Category" :
				document.formname.subcategory.options[0]=new Option("Select Sub-Category","");
				 
				break;
				
				case "Product" :
				document.formname.subcategory.options[0]=new Option("Select Sub-Category","");
				document.formname.subcategory.options[1]=new Option("Admin","Admin");
				document.formname.subcategory.options[2]=new Option("Info Mgr","Info Mgr");
				document.formname.subcategory.options[3]=new Option("Sales Module","Sales Module");
				document.formname.subcategory.options[4]=new Option("User Creation","User Creation");
				document.formname.subcategory.options[4]=new Option("Login","Login");
				 
				break;
				
				case "Saas" :
				document.formname.subcategory.options[0]=new Option("Select Sub-Category","");
				document.formname.subcategory.options[1]=new Option("Custom Report","Custom Report");
				document.formname.subcategory.options[2]=new Option("User Creation","User Creation");
				 
				break;
			}
			return true;
		}
		
		
		function getServerIP(tokenCheckBox){
			
			<%
			
	   		String ipAddress2 = null;
	   		if (request.getHeader("X-Forwarded-For") != null) {
	   		    out.print("Proxy");
	   		 	ipAddress2 = request.getHeader("X-FORWARDED-FOR");
	   		 	
	   		}else{
	   			ipAddress2 = request.getRemoteAddr();
	   		}
	   		
			%>
			
			
		 	if (tokenCheckBox.checked == true) {
		 		document.getElementById('ipaddresscol').style.display='none';
		    }else {
		    	document.getElementById('ipaddresscol').style.display='block';
		    }		 	
		}
		
	</script>
	
	<title>FranConnect Sky</title>
</head>



<body>
  <div class='container'>
    <div class='panel panel-primary dialog-panel'>
      <div class='panel-heading'>
        <h5>FranConnect Automation</h5>
      </div>
      <div class='panel-body'>
        <form class='form-horizontal' role='form' id="formname" name="formname" action="ExecuteTestngXml" method="post">
			<div class='form-group'>
				<label class='control-label col-md-2 col-md-offset-2' for='id_title'>Category</label>
            <div class='col-md-8'>
				<div class='col-md-2'>
					<div class='form-group internal'>
	                   	<select class='form-control' name="category" id="category" onchange="javascript: dropdownlist(this.options[this.selectedIndex].value); " required>
							<option value="Category">Category</option>
							<option value="Product">Product</option>
							<option value="Saas">Saas</option>
						</select>
                	</div>
				</div>
              <label class='control-label col-md-2 col-md-offset-2' for='id_title'>Sub-Category</label>
             <div class='col-md-2'>
				<div class='form-group internal'>
					<select class='form-control' name="subcategory" id="subcategory" required onchange="hidebuildrow()">
						<option value="">SubCategory</option>
					</select>
				</div>
			</div>
            </div>
		</div> 
		
         <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_email'>Build Url</label>
            <div class='col-md-8'>
              <div class='form-group'>
                <div class='col-md-11'>
                  <input class='form-control' id='buildUrl1' name='buildUrl1' placeholder='URL Ex. http://192.168.8.3/franconnect' type='text' required>
                </div>
              </div>
            </div>
		</div>
       <div class='form-group'>
			<label class='control-label col-md-2 col-md-offset-2' for='id_title'>Login</label>
            <div class='col-md-8'>
              <div class='col-md-2 indent-small'>
                <div class='form-group internal'>
                  <input class='form-control' id='userName1' name='userName1' placeholder='User Name' type='text' required>
                </div>
              </div>
 				<label class='control-label col-md-2 col-md-offset-2' for='id_title'>Pwd</label>
              <div class='col-md-2 indent-small'>
                <div class='form-group internal'>
                  <input class='form-control' id='password1' name='password1' placeholder='Password' type='text' required>
                </div>
              </div>
            </div>
		</div>
		<div class='form-group' id="build2row1" style='display:none'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_email'>Build Url2</label>
            <div class='col-md-6'>
              <div class='form-group'>
                <div class='col-md-11'>
                  <input class='form-control' id='buildUrl2' name='buildUrl2' placeholder='URL Ex. http://192.168.8.3/franconnect92' type='text' required>
                </div>
              </div>
            </div>
		</div>
        <div class='form-group' id="build2row2" style='display:none'>
			<label class='control-label col-md-2 col-md-offset-2' for='id_title'>Login2</label>
            <div class='col-md-8'>
              <div class='col-md-2 indent-small'>
                <div class='form-group internal'>
                  <input class='form-control' id='userName2' name='userName2' placeholder='User Name' type='text' required>
                </div>
              </div>
 				<label class='control-label col-md-2 col-md-offset-2' for='id_title' >Pwd2</label>
              <div class='col-md-2 indent-small'>
                <div class='form-group internal'>
                  <input class='form-control' id='password2' name='password2' placeholder='Password' type='text' required>
                </div>
              </div>
            </div>
		</div>
        <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='emailId'>Email</label>
            <div class='col-md-6'>
              <div class='form-group'>
                <div class='col-md-11'>
                  <input class='form-control' id='emailId' name='emailId' placeholder='email id' type='text' required>
                </div>
              </div>
            </div>
		</div>
        <div class='form-group'>
           <label class='control-label col-md-2 col-md-offset-2' for='id_equipment'>Browser</label>
			<div class='col-md-8'>
				<div class='col-md-3'>
					<div class='form-group internal'>
						<select class='form-control' id='browserName' name='browserName' required>
		                   <option value="chrome">Chrome</option>
		                   <option value="firefox">Firefox</option>
		                   <option value="internetExplorer">Internet Explorer</option>
                 		</select>
              		</div>
             	</div>
			</div>
		</div>
		
        <div class='form-group'>
          <label class='control-label col-md-2 col-md-offset-2' for='id_pets'>Run On Server ?</label>
          <div class='col-md-8'>
          <div class='col-md-2'>
            <div class='make-switch' data-off-label='NO' data-on-label='YES' id='id_pets_switch'>
              <input id='ipaddress' name='ipaddress' type='checkbox' value='checked' onclick='getServerIP(this);'>
            </div>
          </div>
          <div class='col-md-3 indent-small' id="ipaddresscol">
             <div class='form-group internal'>
               <input class='form-control' id='ipAddressTxt' name='ipAddressTxt' type='text' value="<%=ipAddress%>" readonly>
             </div>
           </div>
          </div>
        </div>
        
          <!-- <div class='form-group'>
            <label class='control-label col-md-2 col-md-offset-2' for='id_comments'>Comments</label>
            <div class='col-md-6'>
              <textarea class='form-control' id='id_comments' placeholder='Additional comments' rows='3'></textarea>
            </div>
          </div>
          -->


		<input id="serverIPAdd" name="serverIPAdd" value="<%=sereverIpAdd%>" type="hidden">

		<div class='form-group'>
			<div class='col-md-offset-4 col-md-3'>
				<button class='btn-lg btn-primary' type='submit'>Run Test</button>
			</div>
				<div class='col-md-3'>
				<button class='btn-lg btn-danger' style='float:right' type='reset'>Reset</button>
			</div>
		</div>
	</form>
	</div>
	</div>
	</div>
</body>